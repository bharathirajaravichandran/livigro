<?php

use Illuminate\Routing\Router;

Admin::registerAuthRoutes();
 error_log(print_r($_SERVER,true),3,'/var/www/html/error.log');
Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {

    $router->get('/', 'HomeController@index');
	$router->resources([
		'doctors'                   => DoctorsController::class,
		'patients'  			    => PatientsController::Class,
		'labs'					    => LabsController::class,
		'hospitals'				    => HospitalController::class,
		'labtest'			 	    => LabsTestController::class,
		'usertype'				    => UserTypeController::class,
		'webservice'				=> WebserviceController::class,
		'raisetest'					=> RaiseTestController::class,
		'blood'						=> BloodController::class,
		'speciality'				=> SpecialityController::class,
		'education'					=> EducationController::class,
		'degree'					=> DegreeController::class,
		'college'					=> CollegeController::class,
		'labtestgroup'				=> LabTestGroupController::class,
		'samplespecimens'			=> SampleSpecimensController::class,
		'mytest'					=> MyLabController::class,
		'mypackage'					=> MyPackageController::class,
		'units'						=> UnitsController::class,
		'contacts'					=> ContactsController::class,
		'faq'						=> FAQController::class,
		'sitesetting'				=> SiteSettingController::class,
		'country'					=> ContriesController::class,
		'addcountry'				=> AddContriesController::class,
		'addstate'					=> AddStatesController::class,
		'addcity'					=> AddCitiesController::class,
		'package'					=> PackageController::class,
		'plans'						=> PlansController::class,
		'subscription'				=> SubscriptionController::class,
		]);
	$router->get('doctors/{id}/show','DoctorsController@showNew');
	$router->get('patient/{id}/show','PatientsController@showNew');
	$router->get('labs/{id}/show','LabsController@showNew');
	$router->get('mypatient','LabsController@mypatient');
	$router->get('mydoctor','LabsController@mydoctor');

	// Expired Notification
	$router->get('expiredNotification','SubscriptionController@expiredNotification');

	// Elastic Search
	$router->get('search','SearchController@index');
	$router->post('searchresult','SearchController@search');

	$router->get('hospital/{id}/show','HospitalController@showNew');
	$router->get('raisetest/{id}/show','LabsController@testShow');
	$router->post('export','LabsController@export')->name('export');
	$router->post('enable_log','LabsController@enablelog')->name('enable_log');
	$router->post('sendreportmail','LabsController@sendreportmail');
	$router->get('download','LabsController@download')->name('download');
	$router->get('filedelete','LabsController@filedelete');
	$router->get('raisetest/{id}/sample_collection','LabsController@sample_collection');
	$router->post('raisetest/sample/save','LabsController@samplesave')->name('samplesave');
	$router->post('raisetest/report/save','LabsController@reportsave')->name('reportsave');
	$router->post('raisetest/receipt/save','LabsController@receiptsave')->name('receiptsave');
	$router->post('raisetest/receiptgenerate/save','LabsController@reportgenerate')->name('reportgenerate');
	$router->post('reportdownload','LabsController@reportdownload')->name('reportdownload');
	$router->get('raisetest/{id}/report_generation','LabsController@report_generation');
	$router->get('raisetest/{id}/receipt_collection','LabsController@receipt_collection');
	$router->get('raisetest/{id}/cancel','LabsController@cancel');
	$router->post('raisetest/raise/cancel','LabsController@cancelsave')->name('cancelsave');

	$router->post('updatereceipt','LabsController@updatereceipt');
	$router->resource('coupans','CoupansController');

	$router->get('patient_report','ReportController@patient_report');
	$router->get('doctor_report','ReportController@doctor_report');
	$router->get('lab_report','ReportController@lab_report');

	// Neelamegam_02/08/2018
	// For Mobile Number Unique Valiadtion
	
	$router->post('doctors/phonevalidation','DoctorsController@getphone');
	$router->post('patients/phonevalidation','PatientsController@getphone');
	$router->post('labs/phonevalidation','LabsController@getphone');
	$router->post('raisetest/phonevalidation','LabsController@getphone');
	
	// Edit
	
	$router->post('doctors/{id}/phonevalidation','DoctorsController@getphone');
	$router->post('patients/{id}/phonevalidation','PatientsController@getphone');
	$router->post('labs/{id}/phonevalidation','LabsController@getphone');
	$router->post('doctors/{id}/blood','DoctorsController@getblood');
	$router->post('doctors/{id}/city','DoctorsController@getcity');
	$router->post('labs/{id}/city','LabsController@getcity');
	// Neelamegam_vendor.encore.laravel-admin.src.controller.Usercontroller_20/08/2018
	// For Mobile Number Unique Valiadtion
	$router->post('auth/users/phonevalidation','PatientsController@getphones');

	// Delete
	$router->post('CommonDelete/{id}','CommonDeleteController@CommonDelete');
	$router->post('auth/CommonDelete/{id}','CommonDeleteController@CommonDelete');

	// Neelamegam_CountiesController_05/09/2018
	// For patine with mobile Number
    $router->post('raisetest/getvalue', 'RaiseTestController@getvalue');
	// Neelamegam_CountiesController_05/09/2018
	// For Country_State_City
	$router->post('country/states','ContriesController@getState');
	$router->post('country/cities','ContriesController@getCity');
	$router->post('doctors/cities','ContriesController@getCity');
	$router->post('doctors/{id}/cities','ContriesController@getCity');
	$router->post('labs/cities','LabsController@getCity');
});
