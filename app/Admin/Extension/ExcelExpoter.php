<?php

namespace App\Admin\Extension;
use Excel;
use Illuminate\Contracts\View\View;
use App\Models\RaiseTest;
use Maatwebsite\Excel\Concerns\FromView;
class ExcelExpoter implements FromView
{
    private $id;
	public function __construct($id){
		$this->id = $id;
		//$this->type = $type;
	}

    public function view(): View
    {
        libxml_use_internal_errors(true);
        $labtest = RaiseTest::find($this->id);
        /*if($this->type == 'report')
        	return view('labtest.test_report',compact('labtest'));
    	else if($this->type == 'receipt')*/
    		return view('labtest.test_receipt',compact('labtest'));
    }
}