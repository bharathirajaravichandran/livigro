<?php

namespace App\Admin\Controllers;

use App\Models\Webservice;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class WebserviceController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Webservice::class, function (Grid $grid) {
            $grid->model()->orderBy('id', 'desc');
             $grid->actions(function ($actions) {
                  $actions->disableDelete();
                   $key = $actions->getKey();
                   $WebserviceStatus = Webservice::find($key);
                  $value = $WebserviceStatus->status;
                  if($value !=2){
                          $actions->append("<a href=''><i class='fa fa-trash delete' id='$key'></i></a>");
                           }
                      });
         
                   
             $grid->tools(function ($tools) {
                 $tools->batch(function ($batch) {
                   $batch->disableDelete();
                });
               });
             $grid->disableRowSelector();
             
            $grid->id('ID')->sortable();
            $grid->name('Name');
            $grid->column('status','Status')->display(function ($title) {
                return $this->status == '1' ? '<span class="btn btn-success btn-xs">Active</span>' : ($this->status == '0' ? '<span class="btn btn-primary btn-xs">InActive</span>': '<span class="btn btn-danger btn-xs">Deleted</span>');
            });
            // Neelamegam_29/08/2018_CommonDelete
            Admin::script('
                $( document ).ready(function(){
                    $(".delete").click(function(){
                        var id = $(this).prop("id");
                        var url = "CommonDelete/" + id;
                        var fullurl = window.location.pathname.split("/"); 
                        var uri_segment = fullurl[2];

                        swal({
                          title: "DeleteConfirm",
                          type: "warning",
                          showCancelButton: true,
                          confirmButtonColor: "#DD6B55",
                          confirmButtonText: "Confirm",
                          closeOnConfirm: false,
                          cancelButtonText: "Cancel"
                        },
                        function(){
                            $.ajax({

                                method: "post",
                                url: url,
                                data: {
                                    id:id,
                                    _token:LA.token,
                                    uri_segment:uri_segment,
                                },
                                success: function (data) {
                                    $.pjax.reload("#pjax-container");

                                    if (typeof data === "object") {
                                        if (data.status) {
                                            swal(data.message, "", "success");
                                        } else {
                                            swal(data.message, "", "error");
                                        }
                                    }
                                }
                            });
                        });            
                    });
                });
            ');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Webservice::class, function (Form $form) {

            $form->display('id', 'ID');
            $form->text('name','Name')->rules('required');
            $form->textarea('description','Description')->rules('required');
            $form->textarea('parameter','Parameter')->rules('required');
            $form->textarea('success_result')->rules('required');
            $form->textarea('failure_result')->rules('required');
            $states = [
                        'off'  => ['value' => 0, 'text' => 'Inactive', 'color' => 'danger'],
                        'on' => ['value' => 1, 'text' => 'Active', 'color' => 'success'],
                        ];
            $form->switch('status','Status')->states($states)->default('1');
            // $form->display('created_at', 'Created At');
            // $form->display('updated_at', 'Updated At');
        });
    }
}
