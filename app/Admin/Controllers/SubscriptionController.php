<?php

namespace App\Admin\Controllers;

use App\Models\Subscription;
use App\Models\Labs;
use App\Models\Doctors;
use App\Models\Notification;
use Carbon\Carbon;
use Mail;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Illuminate\Support\Facades\DB;

class SubscriptionController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form());
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function expiredNotification()
    {
      // dd("Hi");
      $subscription = Subscription::where('plan_end_date', '=', Carbon::now()->addDays(4)->format('d F Y'))->get();
      if(count($subscription)!=0){
        foreach ($subscription as $key => $value) {
            if($value->user_type ==1){
                $labs = Labs::find($value->user_id);
                // $expired_date = $value->plan_end_date->format('d/m/Y');
                if($labs){
                 $title = "Livigro - Expired Subscription";
                 $msg = "Hi, Your Subscription has been Expired on {$value->plan_end_date}";
                 $labPhone = $labs->mobile;
                 if(substr($labPhone,0,1) == '+'){
                      send_sms(substr($labPhone,3), $msg);
                    }else{
                      send_sms($labPhone, $msg);
                    }
                  $labs_email = $labs->email;
                  $data['to'] = $labs_email; 
                  $data['subject'] = 'Subscription Expired';    
                  $data['expired_date'] = $value->plan_end_date;       
                   // dd(substr($raise_test->getLab->mobile,3));
                   //Mail::to($patient_email)->send(new ReportToPatient($data));     
                  if($labs_email !=''){
                    Mail::send('email.subscription',$data, function($message) use ($data)
                        {
                            $message->to($data['to']); 
                            $message->subject($data['subject']);
                        });

                  }
              }
            }else  if($value->user_type ==2){
                $doctors = Doctors::find($value->user_id);
                if($doctors){
                 $title = "Livigro - Expired Subscription";
                 $msg = "Hi, Your Subscription has been Expired on {$value->plan_end_date}";
                 $labPhone = $doctors->mobile;
                 if(substr($labPhone,0,1) == '+'){
                      send_sms(substr($labPhone,3), $msg);
                    }else{
                      send_sms($labPhone, $msg);
                    }
                  $labs_email = $doctors->email;
                  $data['to'] = $labs_email; 
                  $data['subject'] = 'Subscription Expired';        
                  $data['expired_date'] = $value->plan_end_date;        
                   // dd(substr($raise_test->getLab->mobile,3));
                   //Mail::to($patient_email)->send(new ReportToPatient($data));     
                  if($labs_email !=''){
                    Mail::send('email.subscription',$data, function($message) use ($data)
                        {
                            $message->to($data['to']); 
                            $message->subject($data['subject']);
                        });

                  }
                }
            }
        }
      }
      return back();
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Subscription::class, function (Grid $grid) {
            $grid->disableCreateButton();
             $grid->model()->orderBy('id', 'desc');
              $grid->actions(function ($actions) {
                 $actions->disableDelete();
              });
               $grid->tools(function ($tools) {
                 $tools->batch(function ($batch) {
                   $batch->disableDelete();
                });
            });

             $grid->disableRowSelector();
            $grid->id('ID')->sortable();
            $grid->column('User Name')->display(function(){
                if($this->user_type ==2){
                    $docname=DB::table('doctors_details')->where('id',$this->user_id)->pluck('name')->first();
                }else{
                    $docname=DB::table('labs_table')->where('id',$this->user_id)->pluck('name')->first();
                }
                if($docname!="" || $docname !=null){
                     return $docname;
                }
               
                
            });
            $grid->column('plan_start_date','Plan Start Date');
            $grid->column('plan_end_date','Plan End Date');
            $grid->column('Plan Name')->display(function(){
                 $plans=DB::table('plan_details')->where('id',$this->plan_id)->pluck('plan_name')->first();
                 return $plans;
            });
            // $grid->created_at();
            // $grid->updated_at();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Subscription::class, function (Form $form) {

            $form->display('id', 'ID');
            $form->date('plan_start_date', 'Start');
            $form->date('plan_end_date', 'End');

            // $form->display('created_at', 'Created At');
            // $form->display('updated_at', 'Updated At');
        });
    }
}
