<?php

namespace App\Admin\Controllers;

use App\Models\Education;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use App\Models\Degree;
use App\Models\College;
use App\Models\Doctors;
class EducationController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Education::class, function (Grid $grid) {

            $grid->actions(function ($actions) {
                  $actions->disableDelete();
                   $key = $actions->getKey();
                   $Education = Education::find($key);
                  $value = $Education->status;
                  if($value !=2){
                          $actions->append("<a href=''><i class='fa fa-trash delete' id='$key'></i></a>");
                           }
                      });
         
                   
             $grid->tools(function ($tools) {
                 $tools->batch(function ($batch) {
                   $batch->disableDelete();
                });
               });

             $grid->disableRowSelector();
                 
            $grid->doctor_id('Doctor')->sortable();
            $grid->column('getDoctor.name','Doctor Name');
            $grid->degree('Degree')->sortable();
            $grid->college('College')->sortable();
            $grid->year('Year')->sortable();
             $grid->column('status','Status')->display(function ($title) {
                    return $this->status == '1' ? '<span class="btn btn-success btn-xs">Active</span>' : ($this->status == '0' ? '<span class="btn btn-primary btn-xs">InActive</span>': '<span class="btn btn-danger btn-xs">Deleted</span>');
                });
              // Neelamegam_29/08/2018_CommonDelete
            Admin::script('
                $( document ).ready(function(){
                    $(".delete").click(function(){
                        var id = $(this).prop("id");
                        var url = "CommonDelete/" + id;
                        var fullurl = window.location.pathname.split("/"); 
                        var uri_segment = fullurl[2];

                        swal({
                          title: "DeleteConfirm",
                          type: "warning",
                          showCancelButton: true,
                          confirmButtonColor: "#DD6B55",
                          confirmButtonText: "Confirm",
                          closeOnConfirm: false,
                          cancelButtonText: "Cancel"
                        },
                        function(){
                            $.ajax({

                                method: "post",
                                url: url,
                                data: {
                                    id:id,
                                    _token:LA.token,
                                    uri_segment:uri_segment,
                                },
                                success: function (data) {
                                    $.pjax.reload("#pjax-container");

                                    if (typeof data === "object") {
                                        if (data.status) {
                                            swal(data.message, "", "success");
                                        } else {
                                            swal(data.message, "", "error");
                                        }
                                    }
                                }
                            });
                        });            
                    });
                });
            ');


           
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Education::class, function (Form $form) {
           $form->select('doctor_id','Doctor')->options(Doctors::all()->pluck('name','id'))->rules('required');
           $form->select('degree','Degree')->options(Degree::all()->pluck('name','id'))->rules('required');
           $form->select('college','College/Certificate')->options(College::all()->pluck('name','id'))->rules('required');
           $form->date('year','Year')->format('YYYY')->rules('required');
           $form->multipleImage('degree_certificate','Degree Certificate');
           $states = [
                        'off'  => ['value' => 0, 'text' => 'Inactive', 'color' => 'danger'],
                        'on' => ['value' => 1, 'text' => 'Active', 'color' => 'success'],
                        ];
            $form->switch('status','Status')->states($states)->default('1');
        });
    }
}
