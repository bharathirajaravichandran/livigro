<?php

namespace App\Admin\Controllers;


use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Form\Tab;
use Encore\Admin\Widgets\Table;
// use Illuminate\Support\Facades\Request;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use App\User;
use App\Models\Doctors;
use App\Models\Hospital;
use App\Models\Labs;
use App\Models\Patients;
use App\Models\LabTestGroup;
use App\Models\LabsCategory;
use App\Models\Coupans;
use App\Models\Contacts;
use App\Models\LabsTest;
use App\Models\Webservice;
use App\Models\City;
use App\Models\Blood;
use App\Models\Degree;
use App\Models\Education;
use App\Models\College;
use App\Models\Speciality;
use App\Models\Units;
use App\Models\FAQ;
use App\Models\RaiseTest;


class CommonDeleteController extends Controller
{
    use ModelForm;

     // Neelamegam_29/08/2018
    // To Commondelete function
    // For Delete Status Update
    public function CommonDelete(Request $request) {

      if($request->uri_segment == 'users'){
          $user_detail = User::where('id',$request->id)->first();
          
           $users = User::where('id',$request->id)->first();
           $users->status='2';
            $users->save();
            if($user_detail->user_type_id ==1){
               $labs = Labs::where('id',$user_detail->linked_id)->first();
               if($labs){
               $labs->status='2';
                $labs->save();
              }
            }else if($user_detail->user_type_id == 2){
               $doctor = Doctors::where('id',$user_detail->linked_id)->first();
               if($doctor){
               $doctor->status='2';
               $doctor->save();
              }
            }else if($user_detail->user_type_id ==3){
              $patients = Patients::where('id',$user_detail->linked_id)->first();
              if($patients){
                $patients->status='2';
              $patients->save();
              }
              
            }
            $responses = array(
          'status' => 'status',
          'message' =>'Success',                
          );
      return response()->json($responses);
        }
        // dd($request->uri_segment);
        if($request->uri_segment == 'doctors'){
            $doctor = Doctors::where('id',$request->id)->first();
             // dd($doctor);
                 $doctor->status='2';
                  $doctor->save();
             $user = User::where('linked_id',$request->id)->first();
             $user->status ='2';
             $user->save();
                  $responses = array(
                'status' => 'status',
                'message' =>'Success',                
                );
            return response()->json($responses);

        }
        if($request->uri_segment == 'labs'){
            $labs = Labs::where('id',$request->id)->first();
                 $labs->status='2';
                  $labs->save();
             $user = User::where('linked_id',$request->id)->first();
             $user->status ='2';
             $user->save();
            // dd($user);
                  $responses = array(
                'status' => 'status',
                'message' =>'Success',                
                );
            return response()->json($responses);
        }
        if($request->uri_segment == 'patients'){
            $patients = Patients::where('id',$request->id)->first();
            $patients->status='2';
            $patients->save();
             $user = User::where('linked_id',$request->id)->first();
             $user->status ='2';
             $user->save();
             $responses = array(
            'status' => 'status',
            'message' =>'Success',                
            );
            return response()->json($responses);
        }
        if($request->uri_segment == 'hospitals'){
            $hospitals = Hospital::where('id',$request->id)->first();
                 $hospitals->status='2';
                  $hospitals->save();
                  $responses = array(
                'status' => 'status',
                'message' =>'Success',                
                );
            return response()->json($responses);
        }
        if($request->uri_segment == 'labtestgroup'){
            $labtestgroup = LabTestGroup::where('id',$request->id)->first();
                 $labtestgroup->status='2';
                  $labtestgroup->save();
                  $responses = array(
                'status' => 'status',
                'message' =>'Success',                
                );
            return response()->json($responses);
        }
        if($request->uri_segment == 'samplespecimens'){
            $samplespecimens = LabsCategory::where('id',$request->id)->first();
                 $samplespecimens->status='2';
                  $samplespecimens->save();
                  $responses = array(
                'status' => 'status',
                'message' =>'Success',                
                );
            return response()->json($responses);
        } 
        if($request->uri_segment == 'coupans'){
            $coupans = Coupans::where('id',$request->id)->first();
                 $coupans->status='2';
                  $coupans->save();
                  $responses = array(
                'status' => 'status',
                'message' =>'Success',                
                );
            return response()->json($responses);
        } 
        if($request->uri_segment == 'contacts'){
            $contacts = Contacts::where('id',$request->id)->first();
                 $contacts->status='2';
                  $contacts->save();
                  $responses = array(
                'status' => 'status',
                'message' =>'Success',                
                );
            return response()->json($responses);
        } 
        if($request->uri_segment == 'labtest'){
            $labtest = LabsTest::where('id',$request->id)->first();
                 $labtest->status='2';
                  $labtest->save();
                  $responses = array(
                'status' => 'status',
                'message' =>'Success',                
                );
            return response()->json($responses);
        } 
        if($request->uri_segment == 'webservice'){
            $webservice = Webservice::where('id',$request->id)->first();
                 $webservice->status='2';
                  $webservice->save();
                  $responses = array(
                'status' => 'status',
                'message' =>'Success',                
                );
            return response()->json($responses);
        } 
        if($request->uri_segment == 'blood'){
            $blood = Blood::where('id',$request->id)->first();
                 $blood->status='2';
                  $blood->save();
                  $responses = array(
                'status' => 'status',
                'message' =>'Success',                
                );
            return response()->json($responses);
        } 
        if($request->uri_segment == 'degree'){
            $degree = Degree::where('id',$request->id)->first();
                 $degree->status='2';
                  $degree->save();
                  $responses = array(
                'status' => 'status',
                'message' =>'Success',                
                );
            return response()->json($responses);
        } 
        if($request->uri_segment == 'education'){
            $education = Education::where('id',$request->id)->first();
                 $education->status='2';
                  $education->save();
                  $responses = array(
                'status' => 'status',
                'message' =>'Success',                
                );
            return response()->json($responses);
        } 
        if($request->uri_segment == 'college'){
            $college = College::where('id',$request->id)->first();
                 $college->status='2';
                  $college->save();
                  $responses = array(
                'status' => 'status',
                'message' =>'Success',                
                );
            return response()->json($responses);
        } 
        if($request->uri_segment == 'speciality'){
            $speciality = Speciality::where('id',$request->id)->first();
                 $speciality->status='2';
                  $speciality->save();
                  $responses = array(
                'status' => 'status',
                'message' =>'Success',                
                );
            return response()->json($responses);
        }
        if($request->uri_segment == 'units'){
            $units = Units::where('id',$request->id)->first();
                 $units->status='2';
                  $units->save();
                  $responses = array(
                'status' => 'status',
                'message' =>'Success',                
                );
            return response()->json($responses);
        }
        if($request->uri_segment == 'faq'){
            $faq = FAQ::where('id',$request->id)->first();
                 $faq->status='2';
                  $faq->save();
                  $responses = array(
                'status' => 'status',
                'message' =>'Success',                
                );
            return response()->json($responses);
        }
        if($request->uri_segment == 'raisetest'){
            $raisetest = RaiseTest::where('id',$request->id)->first();
                 $raisetest->status='2';
                  $raisetest->save();
                  $responses = array(
                'status' => 'status',
                'message' =>'Success',                
                );
            return response()->json($responses);
        }

    }

}
