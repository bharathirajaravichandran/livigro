<?php

namespace App\Admin\Controllers;

use App\Models\SiteSetting;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class SiteSettingController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(SiteSetting::class, function (Grid $grid) {

            $grid->model()->orderBy('id', 'desc');
            $grid->id('ID')->sortable();
             $grid->column('name','Site Name');
             $grid->column('logo','Site Logo')->image();
             $grid->column('mobile','Site Mobile');
             $grid->column('phone','Site Phone');
             $grid->column('email','Site Email');
             $grid->column('address','Site Address');
             $grid->column('address','Site Address');
              $grid->column('footer_logo','Site Footer Logo')->image();
             $grid->column('footer','Site Footer Content');
            $grid->created_at();
            $grid->updated_at();
            $grid->actions(function ($actions) {
                  $actions->disableDelete();         
             });
             $grid->tools(function ($tools) {
                 $tools->batch(function ($batch) {
                   $batch->disableDelete();
                });
               });
            $grid->disableRowSelector();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(SiteSetting::class, function (Form $form) {

            $form->display('id', 'ID');
             $form->text('name','Site Name')->rules('required');
             $form->image('logo','Site Logo')->rules('required |mimes:jpeg,png,bmp,tiff |max:4096');
             $form->text('mobile','Site Mobile')->rules('required');
             $form->text('phone','Site Phone')->rules('required');
             $form->email('email','Site Email')->rules('required');
             $form->textarea('address','Site Address')->rules('required');
             $form->image('footer_logo','Site Footer Logo')->rules('required |mimes:jpeg,png,bmp,tiff |max:4096');
             $form->text('footer','Site Footer Content')->rules('required');
            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');
        });
    }
}
