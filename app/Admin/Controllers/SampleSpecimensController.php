<?php

namespace App\Admin\Controllers;

use App\Models\LabTestGroup;
use App\Models\LabsTest;
use App\Models\LabsCategory;
use Auth;
use App\User;
use Encore\Admin\Form\Tab;
use Encore\Admin\Widgets\Table;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Illuminate\Support\Facades\DB;

class SampleSpecimensController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {
           /*if(Auth::guard('admin')->user()->isRole('administrator')){
                    $content->header('Sample Specimens');
            }*/
            $content->header('Sample Specimens');
            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Sample Specimens');
            // $content->description('description');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Sample Specimens');
            // $content->description('description');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(LabsCategory::class, function (Grid $grid) {

        	// $grid->model()->check();
            //$grid->disableActions();
            // $grid->disableFilter();
             if(Admin::user()->status !=1){
              $grid->disableActions();
              $grid->disableCreateButton();
             
            }
              $grid->model()->orderBy('id', 'desc');
            $grid->actions(function ($actions) {
            $actions->disableDelete();
             $key = $actions->getKey();
             $LabsCategoryStatus = LabsCategory::find($key);
            $value = $LabsCategoryStatus->status;
            if($value !=2){
                    $actions->append("<a href=''><i class='fa fa-trash delete' id='$key'></i></a>");
                     }
                });
   
                   
             $grid->tools(function ($tools) {
                 $tools->batch(function ($batch) {
                   $batch->disableDelete();
                });
             });
               $grid->disableRowSelector();
             if(!Auth::guard('admin')->user()->isRole('administrator')){
                $grid->disableCreateButton();
                $grid->disablePagination();
                $grid->disableFilter();
                $grid->disableExport();
                $grid->disableRowSelector();
               // $grid->disableActions();
             }

            $grid->id('ID')->sortable();
            $grid->column('name','Name');
             $grid->column('Test Name')->display(function() {
                $testname = DB::table('lab_tests')->where('id', $this->lab_test_id)->pluck('name')->first();
             	return  ($testname != null || $testname !='')?$testname:'';
            });
              $grid->column('status','Status')->display(function ($title) {
                    return $this->status == '1' ? '<span class="btn btn-success btn-xs">Active</span>' : ($this->status == '0' ? '<span class="btn btn-primary btn-xs">InActive</span>': '<span class="btn btn-danger btn-xs">Deleted</span>');
                });

            $grid->filter(function($filter){
            // Remove the default id filter
            $filter->disableIdFilter();        
            $filter->ilike('getTest.name', 'Test Name');
            });

            // Neelamegam_29/08/2018_CommonDelete
            Admin::script('
                $( document ).ready(function(){
                    $(".delete").click(function(){
                        var id = $(this).prop("id");
                        var url = "CommonDelete/" + id;
                        var fullurl = window.location.pathname.split("/"); 
                        var uri_segment = fullurl[2];

                        swal({
                          title: "DeleteConfirm",
                          type: "warning",
                          showCancelButton: true,
                          confirmButtonColor: "#DD6B55",
                          confirmButtonText: "Confirm",
                          closeOnConfirm: false,
                          cancelButtonText: "Cancel"
                        },
                        function(){
                            $.ajax({

                                method: "post",
                                url: url,
                                data: {
                                    id:id,
                                    _token:LA.token,
                                    uri_segment:uri_segment,
                                },
                                success: function (data) {
                                    $.pjax.reload("#pjax-container");

                                    if (typeof data === "object") {
                                        if (data.status) {
                                            swal(data.message, "", "success");
                                        } else {
                                            swal(data.message, "", "error");
                                        }
                                    }
                                }
                            });
                        });            
                    });
                });
            ');

            
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(LabsCategory::class, function (Form $form) {

            $form->display('id', 'ID');
            $form->text('name', 'Name')->rules('required');
            /* $form->select('lab_test_id','Test Name')->options(LabsTest::all()->pluck('name','id'))->rules('required');*/
             $states = [
                        'off'  => ['value' => 0, 'text' => 'Inactive', 'color' => 'danger'],
                        'on' => ['value' => 1, 'text' => 'Active', 'color' => 'success'],
                        ];
            $form->switch('status','Status')->states($states)->default('1');
             Admin::script('
                $( document ).ready(function(){
               
                     $("#name").keypress(function(event){
                             var regex = new RegExp("^[a-zA-Z0-9\b? ,_-]+$");
                            var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
                            if (!regex.test(key)) {
                               event.preventDefault();
                            }
                        });
                });
            ');
            // $form->display('created_at', 'Created At');
            // $form->display('updated_at', 'Updated At');
        });
    }
}
