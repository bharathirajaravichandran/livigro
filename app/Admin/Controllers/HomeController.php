<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\Dashboard;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Column;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;
use Encore\Admin\Widgets\InfoBox;
use Illuminate\Support\Facades\DB;
use Auth;
use App\User;
use App\Models\Doctors;
use App\Models\Patients;
use App\Models\Hospital;
use App\Models\Labs;
use App\Models\RaiseTest;


class HomeController extends Controller
{
	public function index()
	{
		// dd(Auth::guard('admin')->user()->isRole('lab'));

		if(Auth::guard('admin')->user()->isRole('administrator')){
			// dd('sdasdsad');
			$data = $this->getAdminDetails();
			// dd($data);
			return Admin::content(function (Content $content) use ($data) {

					$content->header('Dashboard');
					$content->body(view('dashborad',compact('data'))); // need to compact the data variable
					//$content->body('<h1>Need To Implement Dashboard</h1>'); // need to compact the data variable


					});
		} else if(Auth::guard('admin')->user()->isRole('doctor')){
			// dd('doctor');
			$data = $this->getDoctorDetail();
			return Admin::content(function (Content $content) use ($data) {

					$content->header('Dashboard');
					$content->body(view('dashboard_doctor',compact('data'))); // need to compact the data variable


					});
		}else{
			// dd(Auth::guard('admin')->user()->isRole('lab'));
			$data = $this->getLabsDetails();
			return Admin::content(function (Content $content) use ($data) {

					$content->header('Dashboard');
					$content->body(view('dashboard_lab',compact('data'))); // need to compact the data variable


					});
		}
		// else{
		// 	dd('patient');
		// 	$data = $this->getPatientDetails();
		// 	return Admin::content(function (Content $content) use ($data) {

		// 			$content->header('Dashboard');
		// 			$content->body(view('dashborad')); // need to compact the data variable


		// 			});
		// }

		return Admin::content(function (Content $content) {

				$content->header('Dashboard');
				$content->body(view('dashborad'));


				});
	}


	public function getAdminDetails() {		
		//1.need to get the total doctors
		$data['doctor_count'] = Doctors::where('status',1)->count();
		//2.to get the hospital count
		$data['hopital_count'] = Hospital::where('status',1)->count();
		//3.to get the patient count
		$data['patient_count'] = Patients::where('status',1)->count();
		//4.to get the lab count
		$data['labs_count'] = Labs::where('status',1)->count();

		//5. which doctor earned more, in the last 7 days


		//6. which lab earned more, in the last 7 days
		//7. which doctor have the more patients and lab 5 records
		$data['doc_lab'] = DB::table('doctor_lab')
			->join('doctors_details', 'doctors_details.id', '=', 'doctor_lab.doctor_id')
			->join('labs_table', 'labs_table.id', '=', 'doctor_lab.lab_id')
			->select(DB::raw('count(*) as countno,doctor_lab.doctor_id,
						array_agg(doctor_lab.lab_id) as lab_id,
						doctors_details.salutation as salutation,
						doctors_details.name as name,
						array_agg(labs_table.name) as lab_name'))
			->groupBy('doctor_lab.doctor_id','doctors_details.salutation','doctors_details.name')
			->orderBy('countno', 'desc')
			->limit(5)
			->get();

		$data['doc_patient'] = DB::table('doctor_patient')
			->join('doctors_details', 'doctors_details.id', '=', 'doctor_patient.doctor_id')
			->join('patients_details', 'patients_details.id', '=', 'doctor_patient.patient_id')
			->select(DB::raw('count(*) as countno,doctor_patient.doctor_id as doc_id,
						doctors_details.salutation as salutation,
						doctors_details.name as name,
						array_agg(patients_details.name) as patient_name'))
			->groupBy('doctor_patient.doctor_id','doctors_details.name','doctors_details.salutation')
			->orderBy('countno', 'desc')
			->limit(5)
			->get();	
		 $data['lab_taken_test']  =   $data['most_taken_test']  = $data['doctor_raised_test'] = [];

		//8. which lab take more number of tests 5 records
/*		$data['lab_taken_test'] = DB::table('raise_test')
			->join('labs_table', 'labs_table.id', '=', 'raise_test.lab_id')
			->join('lab_tests','lab_tests.id','=','raise_test.lab_tests_id')
			->select(DB::raw('count(*) as countno,raise_test.lab_id,array_agg(raise_test.id) as test_id,
						array_agg(labs_table.name) as lab_name,
						array_agg(lab_tests.name) as test_name
						'))
			->groupBy('raise_test.lab_id')
//			->orderBy('countno','desc')
			->limit(5)
			->get();
		//9. which test taken mostly 5 records
		$data['most_taken_test'] = DB::table('raise_test')
			->join('lab_tests','lab_tests.id','=','raise_test.lab_tests_id')
			->select(DB::raw('count(*) as countno, raise_test.lab_tests_id,
						count(*) as countno,
						array_agg(lab_tests.name) as test_name
						'))
			->groupBy('raise_test.lab_tests_id')
//			->orderBy('countno','desc')
			->limit(5)
			->get();
		$data['doctor_raised_test'] = [];
		//10. which doctor raised more tests 5 records
		$data['doctor_raised_test'] = DB::table('raise_test')
			->join('lab_tests', 'lab_tests.id', '=', 'raise_test.lab_tests_id')
			->join('doctors_details', 'raise_test.doctor_id', '=', 'doctors_details.id')
			->select(DB::raw('count(*) as countno,raise_test.doctor_id,
						array_agg(lab_tests.name) as test_name,
						array_agg(doctors_details.salutation) as salutation,
						array_agg(doctors_details.name) as name
						'))
			->groupBy('raise_test.doctor_id')
//			->orderBy('countno','desc')
			->limit(5)
			->get();*/
		$data['most_taken_test'] = DB::table('receipt_table')
			->join('lab_tests', 'lab_tests.id', '=', 'receipt_table.tests_id')
			->select(DB::raw('count(*) as countno,receipt_table.tests_id as lab_id,
						lab_tests.name as test_name'))
			->groupBy('receipt_table.tests_id','lab_tests.name')
			->orderBy('countno', 'desc')
			->limit(5)
			->get();
		$data['lab_taken_test'] = DB::table('raise_test')
			->join('labs_table', 'labs_table.id', '=', 'raise_test.lab_id')
			->select(DB::raw('count(*) as countno,raise_test.lab_id as lab_id,
						labs_table.name as lab_name'))
			->groupBy('raise_test.lab_id','labs_table.name')
			->where('raise_test.lab_id','!=',null )
			->where('labs_table.name','!=',null )
			->orderBy('countno', 'desc')
			->limit(5)
			->get();	
		$data['doctor_raised_test'] = DB::table('raise_test')
			->join('doctors_details', 'doctors_details.id', '=', 'raise_test.doctor_id')
			->select(DB::raw('count(*) as countno,raise_test.doctor_id as doctor_id,
						doctors_details.salutation as salutation,
						doctors_details.name as name'))
			->groupBy('raise_test.doctor_id','doctors_details.name','doctors_details.salutation')
			->orderBy('countno', 'desc')
			->limit(5)
			->get();	
			//dd($data);
		$user = Auth::guard('admin')->user();
		if(isset($user->id) && $user->id == 1){	
		//get latest lab patient doctor
		$latest_lab = Labs::where('created_at','>=',$user->logout_date)->get()->toArray();
		$latest_doctor = Doctors::where('created_at','>=',$user->logout_date)->get()->toArray();
		$latest_patient = Patients::where('created_at','>=',$user->logout_date)->get()->toArray();			
		$data['latest_data'] = array_merge($latest_lab,$latest_doctor,$latest_patient);		
	    //get latest lab patient doctor
	        }
		return $data;


	}

	public function getDoctorDetail() {
		$user = Auth::guard('admin')->user();		
		$doctor = Doctors::find($user->linked_id);
		
		$patient_counts=0;
		if($doctor->patients())
		{
			$patient_counts = $doctor->patients()->count();
		}
		$data['patient_count'] = $patient_counts;
		//2.to get the lab count
		$data['labs_count'] = $doctor->labs()->count();
		//3. no of hospital
		$data['hospital_count'] = $doctor->hospitals()->count();

		//4. no of raised test
		$data['raised_test'] = $doctor->raised_test()->count();
		//5. no of completed test
		$count_completed = $count_not_completed = 0;
		$completed_raised_test = $pending_raised_test = [];
		$raised_test = $doctor->raised_test()->get();
		foreach ($raised_test as $key => $raise_test) {
			if($raise_test->getReport()->count() > 0 ){
				$completed_raised_test[] = $raise_test;
				$count_completed++;
			}
			else{
				$count_not_completed++;
				$pending_raised_test[] = $raise_test;
			}
		}

		$data['completed_test'] = $count_completed;
		$data['completed_raised_test'] = $completed_raised_test;
		$data['pending_raised_test'] = $pending_raised_test;
		//6. no of pending test
		$data['count_not_completed'] = $count_not_completed;
		//7. Total payment on this month
		//8. total new patient on this week
		$data['new_Week_ patient'] = $doctor->thisWeekPatient()->get();
		//9. new labs on this week
		$data['new_Week_lab'] = $doctor->thisWeekLab()->get();

		
		return $data;
		//10.which lab he prescribe mostly
		//

	}

	public function getLabsDetails() {	

		$user = Auth::guard('admin')->user();
		$lab = Labs::find($user->linked_id);
		//1.no of raised test by doctor
		$data['raised_test_by_doctor'] = $lab->test_assigned();
		//2.no of raised test by itself
		$data['raised_test_by_self'] = $lab->total_raised_test()->get();
		// dd($lab->total_raised_test()->get());
		//3.completed test
		$count_completed = $count_not_completed = 0;
		$completed_raised_test = $pending_raised_test = [];
		//dd($lab->total_raised_test()->get());
		foreach($lab->total_raised_test()->get() as $raise_test) {
			if($raise_test->getReport()->count() > 0 ){
				$completed_raised_test[] = $raise_test;
				$count_completed++;
			}
			else{
				$count_not_completed++;
				$pending_raised_test[] = $raise_test;
			}
		}
		$data['lab_completed_test'] = $completed_raised_test;
		$data['completed_test_count'] = $count_completed;
		//4. pending test
		$data['lab_pending_test'] = $pending_raised_test;
		$data['pending_test_count'] = $count_not_completed;
		//5. which test taken mostly
		//6. which doctor perscibe mostly
		//7. monthly earning
		//8. number of doctor
		$data['doctors_list'] = $lab->doctors()->get();
		$doctorcount = $doctorsList = DB::table('raise_test')
		      ->join('doctors_details', 'doctors_details.id', '=', 'raise_test.doctor_referer_id')
		      ->select(DB::raw('count(*) as countno'))
		      ->where('raise_test.lab_id', '=', $user->linked_id)
		      ->where('raise_test.raised_by','1')
		       ->groupBy('raise_test.doctor_id','raise_test.doctor_referer_id','doctors_details.id')
		      ->get();  

		$data['doctors_count'] = count($doctorcount);
		$patientCount = DB::table('doctor_patient')
		      ->join('patients_details', 'patients_details.id', '=', 'doctor_patient.patient_id')
		      ->select(DB::raw('count(*) as countno'))
		      ->where('doctor_patient.lab_id', '=', $user->linked_id)
		      ->groupBy('doctor_patient.patient_id','patients_details.name','patients_details.id')
		      ->get(); 

		$data['patient_count'] = count($patientCount);		
		$data['labs_count'] = $lab->labs()->count();
		//$data['raised_test_count'] = $lab->total_raised_test()->count();
		$raise_test = RaiseTest::where('lab_id',$user->linked_id)
									 ->where('raised_by',1)->get();	
		$data['raised_test_count'] = count($raise_test);
		// dd($lab->raised_test()->get()->id);
		$data['raised_doc_test_count'] = $lab->test_assigned()->count();
		// dd($user->linked_id);
		//$amounts = RaiseTest::select(DB::raw('sum(cast(received_amount as double precision))'))
		$amounts = RaiseTest::select(DB::raw('sum(cast(received_amount as double precision))'))
									 ->where('lab_id',$user->linked_id)
									 ->where('paid_status',1)
									  ->where('raised_by',1)
									 ->get();	
		// dd($amounts);
		$revenue_amount = (isset($amounts[0]->sum)) ? $amounts[0]->sum : 0;
		$data['revenue'] = $revenue_amount;
		//dd($data);
		//9.
		
		return $data;


	}

	public function getPatientDetails() {
		$user_id = Auth::guard('admin')->id();

		//1.Patient Report
		//2. Doctors list
		//3. lab list
		//4. how much he spend
		//5. 
	}
}
