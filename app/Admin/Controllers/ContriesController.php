<?php

namespace App\Admin\Controllers;

use App\Models\Countries;
use App\Models\States;
use App\Models\Cities;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class ContriesController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form());
        });
    } 



    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Countries::class, function (Grid $grid) {

            $grid->id('ID')->sortable();

            $grid->created_at();
            $grid->updated_at();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Countries::class, function (Form $form) {
            $form->setId('country');
            $form->display('id', 'ID');
            $form->select('country')->options(Countries::all()->pluck('name','id'));
             $form->select('state');
             $form->select('city');

            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');
            Admin::script('
                $( document ).ready(function(){
                     $("#country").change(function(){
                        var country = $(".country").val();
                        var token = $("input[name=_token]").val();
                        var url = "states";
                        if(country){
                        $.ajax({
                            type:"post",
                            dataType: "json",
                            url: url,                       
                            data: {
                                _token: token,
                                country: country,
                            },                        
                            success: function( data ){
                                console.log(data);
                                if(data)
                                 $(".state").html(data);
                                 else
                                 $(".state").empty();
                            },

                        });
                        }else{
                            $(".state").empty();
                        }
                         $(".state").change(function(){
                        var state = $(".state").val();
                        $("select[name=state] option[value =6]").attr("selected","selected");
                        $(".state option[value=state]").attr("selected","selected");
                        var token = $("input[name=_token]").val();
                        var url = "cities";
                        if(state){
                        $.ajax({
                            type:"post",
                            dataType: "json",
                            url: url,                       
                            data: {
                                _token: token,
                                state: state,
                            },                        
                            success: function( datas ){
                                
                                if(datas)
                                 $(".city").html(datas);
                                 else
                                 $(".city").empty();
                            },
                        });
                        }else{
                            $(".city").empty();
                        }
                    });
                    });
                });
            ');
        });
    }
     public function getState(Request $request)
    {
        $states = States::where('country_id',$request->country)->get();

        $states_count=count($states);
        if($states_count!=0){
            $stateValue = "";
            foreach ($states as $value) 
            {
                $stateValue .= "<option value='".$value->id."'>".$value->name."</option>";
            }
        }

      return response()->json($stateValue); 
    }
     public function getCity(Request $request)
    {
        $cities = Cities::where('state_id',$request->state)->get();

        $cities_count=count($cities);
        if($cities_count!=0){
            $cityValue = "";
            foreach ($cities as $value) 
            {
                $cityValue .= "<option value='".$value->id."'>".$value->name."</option>";
            }
        }

      return response()->json($cityValue); 
    }
}
