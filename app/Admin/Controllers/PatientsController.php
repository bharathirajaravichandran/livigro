<?php

namespace App\Admin\Controllers;

use App\Models\Patients;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Illuminate\Support\Facades\Route;
use Auth;
use App\Models\City;
use App\Models\Blood;

class PatientsController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Patients');
            // $content->description('description');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Patients');
            $patient = Patients::find($id);
            //$content->description();
            $content->body('<p>Last update at '.date('d-m-Y h:iA',strtotime($patient->updated_at)).'</p>');
            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Patients');
            // $content->description('description');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Patients::class, function (Grid $grid) {
            $grid->model()->check();
             $grid->model()->orderBy('id', 'desc');
              if(Admin::user()->status !=1){
              $grid->disableActions();
              $grid->disableCreateButton();
             
            }
              $grid->actions(function ($actions) {
                    $actions->disableDelete();
                     $key = $actions->getKey();
                    $actions->append("<a href=''><i class='fa fa-trash delete' id='$key'></i></a>");
                });
             $grid->tools(function ($tools) {
                 $tools->batch(function ($batch) {
                   $batch->disableDelete();
                });
            });
              $grid->disableRowSelector();
            // if(!Auth::guard('admin')->user()->isRole('administrator')){
            //     $grid->disableCreateButton();
            //     $grid->disablePagination();
            //     $grid->disableFilter();
            //     $grid->disableExport();
            //     $grid->disableRowSelector();
            //     // $grid->disableActions();
            //  }
            //$grid->id('ID')->sortable();
            // $grid->name('Name');
             // Neelamegam_30/07/2018
           $grid->column('name','Name')->display(function () {
                return "<a href='patient/$this->id/show'>$this->name</a>";
            });
            // $grid->email('Email');
            $grid->gender('Gender')->display(function($gender) {
                if($gender==0)  $gender='Female';
                else if ($gender==1) $gender='Male';
                else if ($gender==2) $gender='Non-Binary';
                else $gender='Null';
                return $gender;
            });
            // $grid->blood_group('Blood Group')->display(function() {
            // return  DB::table('blood_group')->where('id', $this->blood_group)
            //         ->pluck('blood_group')->first();
            // });
             $grid->column('blood_group','Blood Group');
             $grid->column('phone','Mobile Number');
             $grid->column('city','City');
             $grid->column('state','State');
              $grid->column('status','Status')->display(function ($title) {
                    return $this->status == '1' ? '<span class="btn btn-success btn-xs">Active</span>' : ($this->status == '0' ? '<span class="btn btn-primary btn-xs">InActive</span>' : '<span class="btn btn-danger btn-xs">Deleted</span>');
                });
            $grid->filter(function($filter){

            // Remove the default id filter
            $filter->disableIdFilter();
            $filter->ilike('name', 'Patient Name');
            $filter->ilike('getCityName.city_name', 'City');
            $filter->ilike('phone', 'Mobile Number');
            $filter->in('gender')->checkbox([
                    '1'    => 'Male',
                    '0'    => 'Female',
                    '2'    => 'Non-Binary',
                ]);

            });
             $grid->disableRowSelector(); 
            // Neelamegam_29/08/2018_CommonDelete
            Admin::script('
                $( document ).ready(function(){
                    $(".delete").click(function(){
                        var id = $(this).prop("id");
                        var url = "CommonDelete/" + id;
                        var fullurl = window.location.pathname.split("/"); 
                        var uri_segment = fullurl[2];

                        swal({
                          title: "DeleteConfirm",
                          type: "warning",
                          showCancelButton: true,
                          confirmButtonColor: "#DD6B55",
                          confirmButtonText: "Confirm",
                          closeOnConfirm: false,
                          cancelButtonText: "Cancel"
                        },
                        function(){
                            $.ajax({

                                method: "post",
                                url: url,
                                data: {
                                    id:id,
                                    _token:LA.token,
                                    uri_segment:uri_segment,
                                },
                                success: function (data) {
                                    $.pjax.reload("#pjax-container");

                                    if (typeof data === "object") {
                                        if (data.status) {
                                            swal(data.message, "", "success");
                                        } else {
                                            swal(data.message, "", "error");
                                        }
                                    }
                                }
                            });
                        });            
                    });
                });
            ');


            // $grid->created_at();
            // $grid->updated_at();
        });
    }


    public function showNew($id){
        $patient = Patients::find($id);
        if($patient){            
             return Admin::content(function (Content $content) use($patient){
                    $doctors = @$patient->doctors()->get();
                    $city = City::where('city_name', @$patient->city)->pluck('city_name')->first();
                    $bg = Blood::where('blood_group', @$patient->blood_group)->pluck('blood_group')->first();
                    $age = $patient->age();                    
                   // dd($age);
            $content->header('Patient Details');            
            $content->body(view('patient.patient_detail',compact('patient','doctors','city','bg','age')));
        });
        }
    }
    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Patients::class, function (Form $form) {
            $form->setId('patient_form');
            // $form->display('id', 'ID');
            $form->tab('Basic',function($form) {
                $form->text('name','Name');
                $form->text('email','Email');
                $form->text('phone','Mobile')->rules('required');
                $form->mobile('mobile','Phone');
                
                //$form->mobile('phone','Phone')->rules('required');
                $form->date('dob','Date Of Birth');
                $form->hidden('created_by')->value(Auth::guard('admin')->user()->id);
                $form->hidden('updated_by')->value(Auth::guard('admin')->user()->id);
                $blood[0]="Please Select Blood Group";
               
                $bloods = Blood::orderBy('id','asc')->get()->pluck('blood_group','id');
                foreach ($bloods as $key => $value) {
                        $blood[$key] = $value;
                }
                $form->select('blood_group','Blood Group')->options($blood);
                
                $form->text('height','Height');
                $form->text('weight','Weight');
                $form->select('gender','Gender')->options([0=>'Female',1=>'Male',2=>'Non Binary']);
                $states = [
                        'off'  => ['value' => 0, 'text' => 'Inactive', 'color' => 'danger'],
                        'on' => ['value' => 1, 'text' => 'Active', 'color' => 'success'],
                        ];
            $form->switch('status','Status')->states($states)->default('1');

            })->tab('Address',function($form){
                // $form->html( ' <div id="locationField">
                //                <input id="autocomplete" class="form-control" placeholder="Enter your address"
                //               onkeyup="geolocate()" type="text"></input>
                //             </div>'
                //             );
                $form->textarea('address','Address')->attribute(['id' => 'route']);
                $form->select('city','City')->options(City::all()->pluck('city_name','city_id'));

              //  $form->text('city','City')->attribute(['id'=>'locality']);
                 $form->text('state','Area')->attribute(['id'=>'administrative_area_level_1']);
                 $form->hidden('lat')->attribute(['id'=>'lat']);
                 $form->hidden('lan')->attribute(['id'=>'lng']);
                // $form->text('lat');
                // $form->text('lon');
               // $form->map('lat', 'lon', 'Mapa')->useGoogleMap();
            })->tab('Password', function ($form) {

                $form->password('password');
                $form->password('password_confirmation');

            })->tab('Documents',function ($form) {
                $form->image('profile_image','Profile Image')->move(function($form) {
                    return 'Patient/'.encrypt($form->mobile);
                });
            });
            $form->disableSubmit();
            $form->disableReset();
            $form->ignore(['password_confirmation']);
                 // $form->saving(function(Form $form) {
                 //                $city = getCityName($form->city); // From Helper file

                 //                        $address = $form->address .' ' .$city. ' ' .$form->state;
                 //                        $prepAddr = str_replace(' ','+',$address);
                 //                        $geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&key='.env('GOOGLE_API_KEY'));
                 //                        $output= json_decode($geocode);
                                       
                 //                        $form->lat = $output->results[0]->geometry->location->lat;
                 //                        $form->lan = $output->results[0]->geometry->location->lng;
                 //                        });
          $form->saving(function (Form $form) {

            if(Route::getCurrentRoute()->getName() == "patients.update"){

                 $patients = Patients::find($form->model()->id);
                  $patientPhone = $patients->phone;

                 $password = $patients->password;
                 // dd($patients);
                 if($form->password != $password){
                     // dd($patientPhone);
                     $title = "Livigro - Password Change";
                     $msg = "Hi, Your Password is Changed.";
                     $patientPhone = $patients->phone;
                     if(substr($patientPhone,0,1) == '+'){
                          send_sms(substr($patientPhone,3), $msg);
                        }else{
                          send_sms($patientPhone, $msg);
                        }
                 }
                   // dd($password);
              } 

          });
          $form->saved(function (Form $form) {


  			  $patients = Patients::find($form->model()->id);
              $patients->phone = "+91".$form->phone;
			  $patients->reference_doctor_id = 0;
              if(Route::getCurrentRoute()->getName() == "patients.update"){
                 if($form->blood_group ==0){
                $patients->blood_group = null;
              }
              }else{
                 if($patients->blood_group ==0 || $patients->blood_group ==null){
                $patients->blood_group = null;
              }
              }
             
			  $patients->save();


             
            
              // $user_ = User::where('linked_id',$form->model()->id)
              //             ->where('user_type_id',3)
              //             ->first();
             //  if($user_)
             //      $user = User::find($user_->id);
             //  else
             //      $user = new User;
             //  $user->password = bcrypt($form->password);
             //  $user->name = $form->name;
             //  $user->username = "+91".$form->mobile;
             //  $user->avatar = $form->model()->profile_image;
             //  $user->user_type_id = 3; // For patient
             //  $user->linked_id = $form->model()->id;
             //  $user->status = $form->model()->status;
             //  $user->save();
             //  //For Roles
             // if(!$user_)
             //      DB::table('admin_role_users')->insert(['role_id'=>4,'user_id'=>$user->id]); // For Patient Roles
                });
            // $form->display('created_at', 'Created At');
            // $form->display('updated_at', 'Updated At');

            // Neelamegam_02/08/2018
            // To getphone function
            // For Mobile Number Unique Valiadtion

                Admin::script('
                $( document ).ready(function(){

                     var fullurl = window.location.pathname.split("/"); 
                        var uri_segment = fullurl[4];
                        var phone_no = $(".phone").val();
                        if(uri_segment == "edit"){
                           var str_phone = phone_no.substring(3);
                           // alert(str_phone);
                           $(".phone").val(str_phone);
                        }

                   var mobile_icon = $("#phone").closest(".form-group").children(".col-sm-8").children(".input-group").find(".input-group-addon");
                    var mobile_icons = $("#phone").closest(".form-group").children(".col-sm-8").children(".input-group").children(".input-group-addon").find("i");
                   // alert(mobile_icons);
                   var new_icon = "<span><i>+91</i></span>";
                    mobile_icons.removeClass("fa fa-pencil");
                   mobile_icons.append(new_icon);
                    $("li.next a").click(function(){
                        var phone_number = $("#phone").val();
                        if(phone_number==""){
                         var error="<small id=small class=help-block>The Mobile Number is Required</small>";
                          $("#small").remove();                          
                          $("#phone").closest(".form-group").addClass("has-error");
                        $("#phone").closest(".col-sm-8").append(error);
                        }

                     });

                    $("#phone").blur(function(){
                     $("#small").remove();    
                    $("#phone").closest(".form-group").addClass("has-error");
                    var phone_number = $("#phone").val();
                    if(phone_number==""){
                     var error="<small id=small class=help-block>The Mobile Number is Required</small>";
                      $("#small").remove();                     
                      $("#phone").closest(".form-group").addClass("has-error");
                    $("#phone").closest(".col-sm-8").append(error);
                    }else{
                        var url = "phonevalidation";
                        var token = $("input[name=_token]").val();
                        var fullurl = window.location.pathname.split("/"); 
                        var uri_segment = fullurl[3];                          
                            $.ajax({
                                type:"post",
                                dataType: "json",
                                url: url,                       
                                data: {
                                    _token: token,
                                    phone_number: phone_number,
                                    uri_segment : uri_segment,
                                },                        
                                success: function( responses ){
                                    
                                    if(responses.phone_id == 0){                                          
                                        var error="<small id=small class=help-block>  The Mobile Number is Already Exists</small>";
                                        $("#small").remove();                                       
                                        $("#phone").closest(".form-group").addClass("has-error");
                                        $("#phone").closest(".col-sm-8").append(error);
                                    }
                                },
                                complete: function(data) {
                                   
                                   if(data.responseJSON.message == "Mobile Number Already Exist")
                                   {
                                     $("#phone").closest(".form-group").addClass("has-error");
                                   }
                                   else
                                   {
                                    $("#phone").closest(".form-group").removeClass("has-error").addClass("has-success");
                                   }
                                    
                                 },
                            });
                       
                        }
                   
                    });
                    $("#email").blur(function(){
                        email = $("#email").val();
                        if(email != ""){
                              var emailRegex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                              if(!(emailRegex.test(email))){
                                var error="<small id=small class=help-block>Please enter valid Email address</small>";
                                $("#small").remove();                          
                                $("#email").closest(".form-group").addClass("has-error");
                                $("#email").closest(".col-sm-8").append(error);
                                $("#email").closest(".nav-tabs-custom").children("ul.nav-tabs").children("li.active").addClass("bv-tab-error").removeClass("bv-tab-sucess");
                              }
                              else{
                               $("#small").remove();         
                                $("#email").closest(".form-group").removeClass("has-error").addClass("has-success");
                                 $("#email").closest(".nav-tabs-custom").children("ul.nav-tabs").children("li.active").removeClass("bv-tab-error").addClass("bv-tab-sucess");
                        }
                        }
                      });
                    $("#phone").keypress(function(){
                        var firstVal = $("#phone").val().charAt(0);
                        var numbericCheck = $("#phone").val();
                        if(!$.isNumeric(numbericCheck)){
                                var error="<small id=small class=help-block>Mobile number must contain only numeric</small>";
                                $("#small").remove();                          
                                $("#phone").closest(".form-group").addClass("has-error");
                                $("#phone").closest(".col-sm-8").append(error);
                                $("#phone").closest(".nav-tabs-custom").children("ul.nav-tabs").children("li.active").addClass("bv-tab-error").removeClass("bv-tab-sucess");
                            }else{
                                 $("#small").remove();         
                                $("#phone").closest(".form-group").removeClass("has-error").addClass("has-success");
                                 $("#phone").closest(".nav-tabs-custom").children("ul.nav-tabs").children("li.active").removeClass("bv-tab-error").addClass("bv-tab-sucess");
                            }
                        });
                });
            ');
        });
    }
    // Neelamegam_02/08/2018
    // To getphone function
    // For Mobile Number Unique Valiadtion
     public function getphone(Request $request)
    {     
         if($request->uri_segment == 'create'){
        $mobile=DB::table('admin_users')->where('username',$request->phone_number)->get();  
      }else{        
        $mobile=DB::table('admin_users')
                  ->where('username',$request->phone_number)
                  ->where('linked_id','!=' ,$request->uri_segment)                  
                  ->get();  
      }

        $mobile_count=count($mobile);        
        if($mobile_count!=0){
             $responses = array(
                'phone_id' => '0',
                'message' =>'Mobile Number Already Exist',                
                );
            return response()->json($responses);
        }
        else{
             $responses = array(
                'phone_id' => '1',
                'message' =>'New User',                
                );
            return response()->json($responses);
        }

    }

    // Neelamegam_vendor.encore.laravel-admin.src.controller.Usercontroller_20/08/2018


    public function getphones(Request $request)
    {     
        // dd();  
        $mobile ="";  
        $mobiles = "+91".$request->phone_number;
        // dd($mobiles);
        if($request->uri_segment == 'create'){
        if($request->roles ==3){
            $mobile=DB::table('labs_table')->where('mobile',$mobiles)->get(); 
        }
        if($request->roles ==2){
            $mobile=DB::table('doctors_details')->where('mobile',$mobiles)->get();  
        }
      
      }else{     
       if($request->roles ==2){   
        $mobile=DB::table('doctors_details')
                  ->where('mobile',$mobiles)
                  ->where('id','!=' ,$request->uri_segment)                  
                  ->get();  
        }
         if($request->roles ==3){
            $mobile=DB::table('labs_table')
                  ->where('mobile',$mobiles)
                  ->where('id','!=' ,$request->uri_segment)                  
                  ->get();  
         }
      }
       if($mobile){
        $mobile_count=count($mobile);        
        if($mobile_count!=0){
             $responses = array(
                'phone_id' => '0',
                'message' =>'Mobile Number Already Exist',                
                );
            return response()->json($responses);
        }
        else{
             $responses = array(
                'phone_id' => '1',
                'message' =>'New User',                
                );
            return response()->json($responses);
        }
       }
        

    }
}
