<?php

namespace App\Admin\Controllers;

use App\Models\Coupans;
use App\Models\City;
use App\Models\Labs;
use App\Models\Doctors;
use App\Models\Patients;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Encore\Admin\Controllers\ModelForm;

class CoupansController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Coupans');
            //$content->description('description');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Coupans');
           // $content->description('description');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Coupans');
            //$content->description('description');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Coupans::class, function (Grid $grid) {

            $grid->model()->orderBy('id', 'desc');
             $grid->actions(function ($actions) {
                    $actions->disableDelete();
                     $key = $actions->getKey();
                     $CoupansStatus = Coupans::find($key);
                    $value = $CoupansStatus->status;
                    if($value !=2){
                            $actions->append("<a href=''><i class='fa fa-trash delete' id='$key'></i></a>");
                             }
                        });
           
                   
             $grid->tools(function ($tools) {
                 $tools->batch(function ($batch) {
                   $batch->disableDelete();
                });
             });
              $grid->disableRowSelector();
            $grid->id('ID')->sortable();
            $grid->name('Name');
           // $grid->area('area');
            $grid->area('City')->display(function() {
            return  DB::table('statelist')->where('city_id', $this->area)    
                    ->pluck('city_name')->first();
            });
            //$grid->labs('labs');
             $grid->labs('Labs')->display(function($labs) {
				$users_array = explode(",",$labs);
				$user_names_array = array();
				foreach($users_array as $users_array_item){
					$user =DB::table('labs_table')->where(['id' => $users_array_item])->first();
					if ($user) { 
						$user_names_array[] = $user->name;
					}
				}
				return   implode(",",$user_names_array);
             });
                    
            //$grid->doctors('doctors');
             $grid->doctors('Doctors')->display(function($doctor) {
		     $users_array = explode(",",$doctor);
		     $user_names_array = array();
             foreach($users_array as $users_array_item)
             {
             $user =DB::table('doctors_details')->where(['id' => $users_array_item])->first();
				if ($user) { 
					$user_names_array[] = $user->name;
				}
             }
             return   implode(",",$user_names_array);
             });
            
            //$grid->patients('patients');
             $grid->patients('Patients')->display(function($patient) {
		     $users_array = explode(",",$patient);
		     $user_names_array = array();
             foreach($users_array as $users_array_item)
             {
             $user =DB::table('patients_details')->where(['id' => $users_array_item])
             ->first();
             if ($user) { 
					$user_names_array[] = $user->name;
				}
             }
             return   implode(",",$user_names_array);
             });
            
            $grid->amount('Amount');
            $grid->column('status','Status')->display(function ($title) {
                    return $this->status == '1' ? '<span class="btn btn-success btn-xs">Active</span>' : ($this->status == '0' ? '<span class="btn btn-primary btn-xs">InActive</span>': '<span class="btn btn-danger btn-xs">Deleted</span>');
                });
            //$grid->expirydate('expiry_date');
            $grid->filter(function($filter){

            // Remove the default id filter
            $filter->disableIdFilter();
            $filter->ilike('name', 'Coupan Name');
           
            });
                 // Neelamegam_29/08/2018_CommonDelete
            Admin::script('
                $( document ).ready(function(){
                    $(".delete").click(function(){
                        var id = $(this).prop("id");
                        var url = "CommonDelete/" + id;
                        var fullurl = window.location.pathname.split("/"); 
                        var uri_segment = fullurl[2];

                        swal({
                          title: "DeleteConfirm",
                          type: "warning",
                          showCancelButton: true,
                          confirmButtonColor: "#DD6B55",
                          confirmButtonText: "Confirm",
                          closeOnConfirm: false,
                          cancelButtonText: "Cancel"
                        },
                        function(){
                            $.ajax({

                                method: "post",
                                url: url,
                                data: {
                                    id:id,
                                    _token:LA.token,
                                    uri_segment:uri_segment,
                                },
                                success: function (data) {
                                    $.pjax.reload("#pjax-container");

                                    if (typeof data === "object") {
                                        if (data.status) {
                                            swal(data.message, "", "success");
                                        } else {
                                            swal(data.message, "", "error");
                                        }
                                    }
                                }
                            });
                        });            
                    });
                });
            ');


            $grid->created_at();
            $grid->updated_at();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Coupans::class, function (Form $form) {

            $form->display('id', 'ID');
             //~ $form->text('name')->value('');
            $form->text('name','Name')->rules('required');
            $form->select('area','City')->options(City::all()->pluck('city_name','city_id'));
            $lab=Labs::all()->pluck('name', 'id');
            $form->multipleSelect('labs')->options($lab)->rules('required');
            $doctor=Doctors::all()->pluck('name', 'id');
            $form->multipleSelect('doctors')->options($doctor)->rules('required');
            $patient=Patients::all()->pluck('name', 'id');
            $form->multipleSelect('patients')->options($patient)->rules('required');
            $form->text('amount','Amount');
            $form->date('expiry_date','Expiry Date');
           $states = [
                        'off'  => ['value' => 0, 'text' => 'Inactive', 'color' => 'danger'],
                        'on' => ['value' => 1, 'text' => 'Active', 'color' => 'success'],
                        ];
            $form->switch('status','Status')->states($states)->default('1');
            
           

            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');
        });
    }
}
