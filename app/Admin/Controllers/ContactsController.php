<?php

namespace App\Admin\Controllers;

use App\Models\Contacts;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class ContactsController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Contacts');
            //$content->description('description');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Contacts::class, function (Grid $grid) {
            $grid->disableCreateButton();
            $grid->model()->orderBy('id', 'desc');
            $grid->actions(function ($actions) {
                    $actions->disableDelete();
                     $key = $actions->getKey();
                     $ContactsStatus = Contacts::find($key);
                    $value = $ContactsStatus->status;
                    if($value !=2){
                            $actions->append("<a href=''><i class='fa fa-trash delete' id='$key'></i></a>");
                             }
                        });
           
                   
             $grid->tools(function ($tools) {
                 $tools->batch(function ($batch) {
                   $batch->disableDelete();
                });
               });
             
             $grid->disableRowSelector();
            $grid->id('ID')->sortable();
            //$grid->lab_id('Lab')->display(function () {
              //  return "<a href='raisetest/$this->lab_id/show'>$this->lab_id</a>";
            //});
            $grid->column('getLab.name','Lab');
            $grid->column('message','Message');
            $grid->column('photo','Photo')->image();
             $grid->column('status','Status')->display(function ($title) {
                    return $this->status == '1' ? '<span class="btn btn-success btn-xs">Active</span>' : ($this->status == '0' ? '<span class="btn btn-primary btn-xs">InActive</span>': '<span class="btn btn-danger btn-xs">Deleted</span>');
                });
             $grid->disableRowSelector(); 
             // Neelamegam_29/08/2018_CommonDelete
            Admin::script('
                $( document ).ready(function(){
                    $(".delete").click(function(){
                        var id = $(this).prop("id");
                        var url = "CommonDelete/" + id;
                        var fullurl = window.location.pathname.split("/"); 
                        var uri_segment = fullurl[2];

                        swal({
                          title: "DeleteConfirm",
                          type: "warning",
                          showCancelButton: true,
                          confirmButtonColor: "#DD6B55",
                          confirmButtonText: "Confirm",
                          closeOnConfirm: false,
                          cancelButtonText: "Cancel"
                        },
                        function(){
                            $.ajax({

                                method: "post",
                                url: url,
                                data: {
                                    id:id,
                                    _token:LA.token,
                                    uri_segment:uri_segment,
                                },
                                success: function (data) {
                                    $.pjax.reload("#pjax-container");

                                    if (typeof data === "object") {
                                        if (data.status) {
                                            swal(data.message, "", "success");
                                        } else {
                                            swal(data.message, "", "error");
                                        }
                                    }
                                }
                            });
                        });            
                    });
                });
            ');

            // $grid->created_at();
            // $grid->updated_at();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Contacts::class, function (Form $form) {

            $form->display('id', 'ID');
            $form->text('lab_id','Lab');
             
            $form->text('doctor_id','Doctor');
            $form->text('message','Message');
            $form->image('photo', 'Photo');
            $states = [
                        'off'  => ['value' => 0, 'text' => 'Inactive', 'color' => 'danger'],
                        'on' => ['value' => 1, 'text' => 'Active', 'color' => 'success'],
                        ];
            $form->switch('status','Status')->states($states)->default('1');
            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');
        });
    }
}
