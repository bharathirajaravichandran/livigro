<?php

namespace App\Admin\Controllers;
use App\Services\DoctorServices as DoctorService;
use App\Services\LoginServices as LoginService;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use DB;
use App\Models\Education;
use App\Models\Doctors;
use App\Models\Labs;
use App\Models\Patients;
use App\Models\LabsTest;
use App\Models\LabsAvaTest;
use App\Models\RaiseTest;
use App\Models\WorkingTime;
use App\Models\Report;
use App\Models\Notification;
use App\Models\Receipt;
use App\Mail\SupportMail;
use App\Models\DocPatient;
use App\Models\College;
use App\Models\Blood;
use App\Models\Degree;
use Carbon\Carbon;
use JWTAuth;

class DocApiController extends Controller
{
	private $request;
	private $loginservice;
	private $doctorservice;
	public function __construct(Request $request)
	{
		$this->request  = $request;
		$this->loginservice = new LoginService();
		$this->doctorservice = new DoctorService();
	}	
	

     /**
	 * Get doctor Education  details
	 * @param  [type] $user [description]
	 * @return [type]       [description]
	 */
	public function getEducationDetails() {
			return $this->doctorservice->getEducationDetails($this->request);
		}
	/**
	 * Get doctor Education  details
	 * @param  [type] $user [description] 
	 * @return [type]       [description]
	 */
	public function checkDoctorExist() {
			return $this->loginservice->checkDoctorExist($this->request);
		}

	/**
	 * [doctorLab description]
	 * @param   [description]
	 * @return [type]                 [description]
	 */
	public function doctorLab(){
		return $this->doctorservice->doctorLab($this->request);
	}

    /**
	 * add doctor Education 
	 * @param  [type]      [description]
	 * @return [type]       [description]
	 */
	public function addEducation() {
			return $this->doctorservice->addEducation($this->request);
		}
     /**
	 * edit doctor Education 
	 * @param  [type]  [description]
	 * @return [type]       [description]
	 */
	public function editEducation() {
			return $this->doctorservice->editEducation($this->request);
		}
     /**
	 * get doc raised test details
	 * @param  [type]  [description]
	 * @return [type]       [description]
	 */
	public function Docraisedtest() {
			return $this->doctorservice->Docraisedtest($this->request);
		}
     /**
	 * get lab raised test details
	 * @param  [type]  [description]
	 * @return [type]       [description]
	 */
	public function Labraisedtest() {
			return $this->doctorservice->Labraisedtest($this->request);
		}
     /**
	 * get generate order details
	 * @param  [type]  [description]
	 * @return [type]       [description]
	 */
	public function getgeneratedorder() {
			return $this->doctorservice->getgeneratedorder($this->request);
		}
     /**
	 * send mail 
	 * @param  [type]  [description]
	 * @return [type]       [description]
	 */
	public function sendSupportMail() {
			return $this->doctorservice->sendSupportMail($this->request);
		}
	/**
	 * doctor dashboard details
	 * @param  [type]  [description]
	 * @return [type]       [description]
	 */
	public function getDoctorDashboard() {
			return $this->doctorservice->getDoctorDashboard($this->request);
		}

	/**
 	 * doctor all details
	 * @param  [type]  [description]
	 * @return [type]       [description]
	 */
	public function getdoctorDetails() {
			return $this->doctorservice->getdoctorDetails($this->request);
		}
	/**
 	 * doctor notification
	 * @param  [type]  [description]
	 * @return [type]       [description]
	 */

	public function getDoctorNotification() {
			return $this->doctorservice->getDoctorNotification($this->request);
		}

     /**
 	 * doctor register
	 * @param  [type]  [description]
	 * @return [type]       [description]
	 */
	public function getDoctorRegistration() {
			return $this->doctorservice->getDoctorRegistration($this->request);
		}

     /* doctor forgot password 
	 * @param  [type]  [description]
	 * @return [type]       [description]
	 */
	public function DoctorForgotPassword() {
			return $this->doctorservice->DoctorForgotPassword($this->request);
		}

    /**
	 * Login function
	 *
	 * @param Request $request
	 * @return void
	 */
	public function Doctorlogin(){
		return $this->loginservice->Doctorlogin($this->request);
	}

	/**
 	 * doctor notification save 
	 * @param  [type]  [description]
	 * @return [type]       [description]
	 */
	public function DoctorsaveNotification() {
			return $this->doctorservice->DoctorsaveNotification($this->request);
		}

	/**
 	 * doctor cart add to test in cart table  
	 * @param  [type]  [description]
	 * @return [type]       [description]
	 */
	public function DoctorinsertTestCart() {
			return $this->doctorservice->DoctorinsertTestCart($this->request);
		}

		/**
 	 * doctor order individual showing 
	 * @param  [type]  [description]
	 * @return [type]       [description]
	 */
	public function getDoctorOrderdetail() {
			return $this->doctorservice->getDoctorOrderdetail($this->request);
		}

		/**
 	 * doctor order list showing 
	 * @param  [type]  [description]
	 * @return [type]       [description]
	 */
	public function getDoctorOrderList() {
			return $this->doctorservice->getDoctorOrderList($this->request);
		}

		/**
 	 * doctor get raise all test  
	 * @param  [type]  [description]
	 * @return [type]       [description]
	 */
	public function getDoctorRaisetest() {
			return $this->doctorservice->getDoctorRaisetest($this->request);
		}

	/**
 	 * get doctor test without added test list  
	 * @param  [type]  [description]
	 * @return [type]       [description]
	 */
	public function FilterDoctorlab() {
			return $this->doctorservice->FilterDoctorlab($this->request);
		}
  
}
