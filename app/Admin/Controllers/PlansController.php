<?php

namespace App\Admin\Controllers;

use App\Models\Plans;
use App\User;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\DB;
class PlansController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Plans');
            // $content->description('description');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Plans');
            // $content->description('description');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Plans');
            // $content->description('description');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Plans::class, function (Grid $grid) {

            $grid->id('ID')->sortable();
            $grid->plan_name('Plan Name');
            $grid->plan_period('Plan Month');
            $grid->plan_amount('Plan Amount');
            // $grid->plan_start_date('Plan Start Date');
            // $grid->plan_end_date('Plan End Date');
            $grid->model()->orderBy('id', 'desc');
            $grid->actions(function ($actions) {
                    $actions->disableDelete();
                   
                        });
           
                   
             $grid->tools(function ($tools) {
                 $tools->batch(function ($batch) {
                   $batch->disableDelete();
                });
             });
              $grid->disableRowSelector();
            // $grid->created_at(); 
            // $grid->updated_at();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Plans::class, function (Form $form) {

            // $form->display('id', 'ID');

           
                $form->text('plan_name','Plan Name')->rules('required');
                $form->text('plan_period','Expire Month')->placeholder("Plan Period in Months");
                $form->currency('plan_amount','Amount');
                // $form->date('plan_start_date','Start Date');
                // $form->date('plan_end_date','End Date');
               Admin::script('
                    $( document ).ready(function(){  
                       var mobile_icon = $("#plan_period").closest(".form-group").children(".col-sm-8").find(".input-group");
                        var mobile_icons = $("#plan_period").closest(".form-group").children(".col-sm-8").children(".input-group").children(".input-group-addon").find("i");
                       var new_icon = "<span style=width:25% class=input-group-addon><i style=font-weight:bold>Months</i></span>";
                        mobile_icon.removeClass("fa fa-pencil");
                       mobile_icon.append(new_icon);  
                   });

               ');
        });
    }
}
