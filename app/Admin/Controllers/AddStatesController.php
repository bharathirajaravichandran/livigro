<?php

namespace App\Admin\Controllers;

use App\Models\Countries;
use App\Models\States;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class AddStatesController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(States::class, function (Grid $grid) {

            $grid->model()->orderBy('id', 'desc');
            $grid->id('ID')->sortable();
            $grid->column('name','State Name');
            $grid->column('country_id','Country Name')->display(function ($title) {
                return  Countries::where('id', $this->country_id)
                    ->pluck('name')->first();
                });
             // $grid->model()->orderBy('id', 'asec');
                $grid->actions(function ($actions) {
                         $actions->disableDelete();
                  });
                $grid->tools(function ($tools) {
                     $tools->batch(function ($batch) {
                       $batch->disableDelete();
                    });
                });
                 $grid->disableRowSelector();
            // $grid->created_at();
            // $grid->updated_at();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(States::class, function (Form $form) {

            $form->select('country_id','country')->options(Countries::all()->pluck('name','id'))->rules('required');
            $form->text('name','State Name')->rules('required');

        });
    }
}
