<?php

namespace App\Admin\Controllers;

use App\Models\Blood;
use App\User;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Doctors;
use App\Models\RaiseTest;
use App\Models\LabsTest;
use App\Models\Labs;
use App\Models\DocPatient;
use App\Models\Patients;
class ReportController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Blood Group');
            // $content->description('description');

            $content->body($this->grid());
        });
    }


    public function doctor_report() {
           return Admin::content(function (Content $content) {

            $content->header('Doctor Report');
            //1.need to show all the doctors including the area
                $doctors_data = Doctors::with('patients','hospitals','labs')->get();
            //2.which doctor raised more test and which test and which lab
            $doctor_raised_test = DB::table('raise_test')
                                 ->join('doctors_details', 'raise_test.doctor_id', '=', 'doctors_details.id')
                               
                                 ->select(DB::raw('count(*) as test_count, doctor_id,doctors_details.name as doctor_name,
                                   doctors_details.city as city'))
                                 ->where('raised_by', 0)
                                 ->groupBy('doctor_id','doctors_details.city','doctors_details.name')
                                 ->orderBy('test_count','desc')
                                 ->get();
            //3.doctors lab list
            $doctor_lab_list = DB::table('doctor_lab')
                                ->join('doctors_details', 'doctor_lab.doctor_id', '=', 'doctors_details.id')
                                ->select(DB::raw('count(*) as lab_count, doctor_id,doctors_details.name as doctor_name,
                                    doctors_details.city as city'))
                                ->groupBy('doctor_id','doctors_details.city','doctors_details.name')
                                 ->orderBy('lab_count','desc')
                                 ->get();
            //4.doctors patient list
             $doctor_patient_list = DB::table('doctor_patient')
                                ->join('doctors_details', 'doctor_patient.doctor_id', '=', 'doctors_details.id')
                                ->select(DB::raw('count(*) as patient_count, doctor_id,doctors_details.name as doctor_name,
                                    doctors_details.city as city'))
                                ->groupBy('doctor_id','doctors_details.name','doctors_details.city')
                                 ->orderBy('patient_count','asc')
                                 ->get();
            //6. which test is mostly prescribed by which doctor
            // $doctor_test_details = [];
            //     foreach($doctor_raised_test as $raise_data) {
            //         $doctor_id = $raise_data->doctor_id;
            //         $doctor = Doctors::find($doctor_id);
            //         $doc_raise_data = RaiseTest::where('doctor_id',$doctor_id)->where('raised_by',0)->get();
            //         $tests = [];
            //         foreach($doc_raise_data as $dat) {
            //             foreach($dat->lab_tests_id as $val)
            //             array_push($tests,$val);
            //         }
            //           $orderd_array = array_count_values($tests);
            //           $key = array_keys($orderd_array);
            //           $lab_name = [];
            //           foreach($key as $key_value) {
            //             $lab_name[] = LabsTest::find($key_value)->name;
            //           }
            //           $c = array_combine($lab_name,$orderd_array);
            //            $doctor_test_details[$doctor->name] = $c;
            //     }
            $content->body(view('report.doctor',compact('doctors_data','doctor_raised_test','doctor_lab_list','doctor_patient_list','doctor_test_details')));
        });
    }

    public function patient_report() {
           return Admin::content(function (Content $content) {
            $content->header('Patient Report');
            //1.Patient list view
            $patient = Patients::all();
            //2.which patient take more order 
            $patient_raised_test = DB::table('raise_test')
                                 ->join('patients_details', 'raise_test.patients_id', '=', 'patients_details.id')   
                                 ->select(DB::raw('count(*) as test_count, patients_id,patients_details.name as patient_name,
                                   patients_details.city as city'))
                                 ->where('raise_test.raised_by',1)                                 
                                 ->groupBy('patients_id','patients_details.city','patients_details.name')
                                 ->orderBy('test_count','desc')
                                 ->get();
            //dd($patient_raised_test);
            //3.which test mostly taken by patient

            //4.patient consult which doctor mostly
            $patient_consultant_mostly_test = DB::table('raise_test')
           ->join('patients_details', 'raise_test.patients_id', '=', 'patients_details.id')   
           ->select(DB::raw('count(*) as test_count, patients_id,raise_test.doctor_referer_name as doctor_name ,patients_details.name as patient_name,
             patients_details.city as city'))
           ->where('raise_test.raised_by',1)                                 
           ->where('raise_test.doctor_referer_name','!=',null)                                  
           ->where('raise_test.doctor_referer_name','!=',0)                                 
           ->groupBy('patients_id','raise_test.doctor_referer_name','patients_details.city','patients_details.name')
           ->orderBy('test_count','desc')
           ->get();
           //dd($patient_consultant_mostly_test);
            //5.patient consult which lab mostly
            $patient_consultant_mostly_test_lab = DB::table('raise_test')
           ->join('labs_table', 'labs_table.id', '=', 'raise_test.lab_id') 
           ->select(DB::raw('count(*) as test_count, lab_id as lab , labs_table.name as lab_name,
             labs_table.city as city'))
           ->where('labs_table.name','!=',null) 
           ->where('raise_test.raised_by',1)                                        
           ->groupBy('lab','labs_table.city','labs_table.name')
           ->orderBy('test_count','desc')           
           ->get();
           //dd($patient_consultant_mostly_test_lab);
            $content->body(view('report.patient',compact('patient','patient_raised_test','patient_consultant_mostly_test_lab','patient_consultant_mostly_test')));
        });
    }


    public function lab_report() {

           return Admin::content(function (Content $content) {

            $content->header('Lab Report');
            //1.Labs List view including the city
            $labs = Labs::all();

            //2.labs patient count
           $lab_patient = DB::table('doctor_patient')
                                ->join('labs_table','doctor_patient.lab_id','=','labs_table.id')
                                ->select(DB::raw('count(*) as patient_count, 
                                    lab_id,labs_table.name as lab_name,
                                  	labs_table.city as city'))
                                ->where('lab_id','<>',NULL)
                                //->distinct('patient_id','labs_table.city','labs_table.name')
                                ->groupBy('patient_id','labs_table.city','labs_table.name')
                                ->groupBy('lab_id')
                                ->get();

            //3.labs order count
           $labs_order_data = DB::table('raise_test')
                             ->join('labs_table', 'raise_test.lab_id', '=', 'labs_table.id')
                             ->select(DB::raw('count(*) as test_count, lab_id,labs_table.name as lab_name,
                                labs_table.city as city'))
                             ->where('raised_by', 1)
                             ->groupBy('lab_id','labs_table.name','labs_table.city')
                             ->orderBy('test_count','desc')
                             ->get();

            //5.canceled Order
            //6.labs tie up with the doctor
             $lab_doctor_list = DB::table('doctor_lab')
                                ->join('labs_table', 'doctor_lab.lab_id', '=', 'labs_table.id')
                                ->select(DB::raw('count(*) as doctor_count, lab_id,labs_table.name as lab_name,
                                    labs_table.city  as city'))
                                ->groupBy('lab_id','labs_table.city','labs_table.name')
                                 ->orderBy('doctor_count','desc')
                                 ->get();
                                 //dd($lab_doctor_list);
            //7.completed order count
            //8.pending order count
            //9.which test taken mostly, which lab and city
              // $lab_test_details = [];
              //   foreach($labs_order_data as $raise_data) {

              //       $lab_id = $raise_data->lab_id;
              //       $lab = Labs::find($lab_id);
              //       if($lab!=null){
              //           $doc_raise_data = RaiseTest::where('lab_id',$lab_id)->where('raised_by',1)->get();
              //           $tests = [];
              //           foreach($doc_raise_data as $dat) {
              //               foreach($dat->lab_tests_id as $val)
              //               array_push($tests,$val);
              //           }
              //           $orderd_array = array_count_values($tests);
              //           $key = array_keys($orderd_array);
              //           $lab_name = [];
              //           foreach($key as $key_value) {

              //               $lt=LabsTest::find($key_value);
              //               if($lt!=null)
              //                   $lab_name[] = $lt->name;
              //           }
              //             $c = array_combine($lab_name,$orderd_array);
              //             dd($c);
              //           $lab_test_details[$lab->name] = $c;
              //       }
              //   }
           // print_r($labs);exit;
            $content->body(view('report.lab',compact('labs','lab_patient','labs_order_data','lab_doctor_list')));
        });
    }


}
