<?php

namespace App\Admin\Controllers;

use App\Models\LabsTest;
use App\Models\Units;
use App\Models\LabsCategory;
use App\User;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\DB;
use App\Models\LabTestGroup;
class LabsTestController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('LabsTest');
            // $content->description('description');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('LabsTest');
            // $content->description('description');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('LabsTest');
            // $content->description('description');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(LabsTest::class, function (Grid $grid) {
          // dd(Admin::user());
           if(Admin::user()->status !=1 ){
              $grid->disableActions();
              $grid->disableCreateButton();
             
            }
          $grid->model()->orderBy('name', 'asc');
          $grid->actions(function ($actions) {
            $actions->disableDelete();
            $key = $actions->getKey();
            $LabsTestStatus = LabsTest::find($key);
            $value = $LabsTestStatus->status;
            if($value !=2){
               $actions->append("<a href=''><i class='fa fa-trash delete' id='$key'></i></a>");
            }
          });
          $grid->tools(function ($tools) {
            $tools->batch(function ($batch) {
              $batch->disableDelete();
            });
          });
            $grid->disableRowSelector();
            $grid->name('Name');
            $grid->price('price');
            $grid->column('price_edit_status','Price Status')->display(function ($title) {
                    return $this->price_edit_status == '0' ? '<span class="btn btn-success btn-xs">Editable</span>' : '<span class="btn btn-danger btn-xs">Non-Editable</span>';
            });
            $grid->measurement('Units')->display(function ($title) {
                    if($this->measurement){
                      if (is_numeric($this->measurement)) {
                        $units = Units::find($this->measurement);
                        if($units)
                          $units_name = $units->name;
                        else
                          $units_name = "";
                      }else{
                         $units_name = $this->measurement;
                      }
                    }else{
                      $units_name =" ";
                    }

                    return $units_name;
                     });
             $grid->column('status','Status')->display(function ($title) {
                    return $this->status == '1' ? '<span class="btn btn-success btn-xs">Active</span>' : ($this->status == '0' ? '<span class="btn btn-primary btn-xs">InActive</span>': '<span class="btn btn-danger btn-xs">Deleted</span>');
                });
            $grid->filter(function($filter){
            // Remove the default id filter
              $filter->disableIdFilter();
              $filter->ilike('name', 'Test Name');
            });
            // Neelamegam_29/08/2018_CommonDelete
            Admin::script('
                $( document ).ready(function(){
                    $(".delete").click(function(){
                        var id = $(this).prop("id");
                        var url = "CommonDelete/" + id;
                        var fullurl = window.location.pathname.split("/"); 
                        var uri_segment = fullurl[2];

                        swal({
                          title: "DeleteConfirm",
                          type: "warning",
                          showCancelButton: true,
                          confirmButtonColor: "#DD6B55",
                          confirmButtonText: "Confirm",
                          closeOnConfirm: false,
                          cancelButtonText: "Cancel"
                        },
                        function(){
                            $.ajax({

                                method: "post",
                                url: url,
                                data: {
                                    id:id,
                                    _token:LA.token,
                                    uri_segment:uri_segment,
                                },
                                success: function (data) {
                                    $.pjax.reload("#pjax-container");

                                    if (typeof data === "object") {
                                        if (data.status) {
                                            swal(data.message, "", "success");
                                        } else {
                                            swal(data.message, "", "error");
                                        }
                                    }
                                }
                            });
                        });            
                    });
                });
            ');
            // $grid->created_at();
            // $grid->updated_at();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(LabsTest::class, function (Form $form) {

            // $form->display('id', 'ID');
           $form->setId('labs_test_form');

                $form->setId('labs_test_form');
                $form->text('name','Test Name')->rules('required');
                $form->textarea('description','Description');
                $form->text('price','Amount')->rules('required');
                // ->rules('required|regex:/^\d+$/',[
                //               'regex' => 'code must be numbers',                              
                //           ]);
                /*$form->currency('estimated_price','Estimated Price');*/
                $form->text('estimated_price','Estimated Price');
                // ->rules('required|regex:/^\d+$/',[
                //               'regex' => 'code must be numbers',                              
                //           ]);
                /*$form->text('measurement','Units');*/
                 $states1 = [
                        'off'  => ['value' => 1, 'text' => 'Non-Edit', 'color' => 'danger'],
                        'on' => ['value' => 0, 'text' => 'Editable', 'color' => 'success'],
                        ];
                $form->switch('price_edit_status','Edit Price')->states($states1)->default('0');
                $form->select('measurement','Units')->options(Units::where('status',1)->get()->pluck('name','id'));
                $form->textarea('reference_range','Reference Start Range');
                $form->textarea('reference_endrange','Reference End Range');
                $form->text('equipment1','Consumables1');
                $form->text('equipment2','Consumables2');
                $form->text('equipment3','Consumables3');
                $form->text('equipment4','Consumables4');
                $form->text('equipment5','Consumables5');
                $form->text('equipment6','Consumables6');
                $form->text('equipment7','Consumables7');
                $form->text('equipment8','Consumables8');
                $form->text('equipment9','Consumables9');
                $form->text('equipment10','Consumables10');
                $form->textarea('default_value1','DISEASE ASSOCIATED');
                $form->tags('search_tags','Search Tags');
               /* $form->text('speciman','Speciman');*/
               $form->select('speciman','Speciman')->options(LabsCategory::where('status',1)->pluck('name','id'))->rules('required');
                $form->multipleSelect('group_id','Profile')->options(LabTestGroup::all()->pluck('name','id'));
                $form->text('category', 'Category');
                $form->text('code', 'Code');
               $states = [
                        'off'  => ['value' => 0, 'text' => 'Inactive', 'color' => 'danger'],
                        'on' => ['value' => 1, 'text' => 'Active', 'color' => 'success'],
                        ];
            $form->switch('status','Status')->states($states)->default('1');
                $form->saved(function(Form $form) {
                  // dd($form->speciman);
                    if($form->measurement == '' || $form->measurement == null){
                        LabsTest::where('id',Request::segment(3))->update(['measurement' =>'-nill-']);}

                    foreach($form->group_id as $val) {
                        if($val != '') {
                            $lab_group = LabTestGroup::find($val);
                            $test_id = $lab_group->tests_id;
                            if(!in_array( $form->model()->id,$test_id )){
                                array_push($test_id,$form->model()->id);
                            }
                            $lab_group->tests_id = $test_id;
                            $lab_group->save();
                        }
                    }
                    if($form->speciman){
                    $speciman =LabsCategory::find($form->speciman);
                    $speciman->lab_test_id = $form->model()->id;                   
                    $speciman->save();
                    }
                });

                
             
        });
    }
}
