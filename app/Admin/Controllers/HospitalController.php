<?php

namespace App\Admin\Controllers;

use App\Models\Hospital;
use App\User;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\DB;
class HospitalController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Hospital');
            // $content->description('description');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Hospital');
            // $content->description('description');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Hospital');
            // $content->description('description');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Hospital::class, function (Grid $grid) {
            // Neelamegam_30/07/2018
            // $grid->id('ID')->sortable();
          //  $grid->name('Name');
              $grid->model()->orderBy('id', 'desc');
             $grid->actions(function ($actions) {
                    $actions->disableDelete();
                     $key = $actions->getKey();
                    $HospitalStatus = Hospital::find($key);
                    $value = $HospitalStatus->status;
                    if($value !=2){
                            $actions->append("<a href=''><i class='fa fa-trash delete' id='$key'></i></a>");
                             }
                });
             $grid->tools(function ($tools) {
                 $tools->batch(function ($batch) {
                   //$batch->disableDelete();
                });
            });
             $grid->disableRowSelector();
             
             $grid->column('name','Name')->display(function () {
                return "<a href='hospital/$this->id/show'>$this->name</a>";
            });
            $grid->city('City');
            $grid->state('Area');
             $grid->column('status','Status')->display(function ($title) {
                    return $this->status == '1' ? '<span class="btn btn-success btn-xs">Active</span>' : ($this->status == '0' ? '<span class="btn btn-primary btn-xs">InActive</span>': '<span class="btn btn-danger btn-xs">Deleted</span>');
                });
            $grid->filter(function($filter){

            // Remove the default id filter
            $filter->disableIdFilter();
            $filter->ilike('name', 'Name');
            $filter->ilike('city', 'City');


            });

            $grid->disableRowSelector(); 
            // Neelamegam_29/08/2018_CommonDelete
             Admin::script('
                $( document ).ready(function(){
                    $(".delete").click(function(){
                        var id = $(this).prop("id");
                        var url = "CommonDelete/" + id;
                        var fullurl = window.location.pathname.split("/"); 
                        var uri_segment = fullurl[2];

                        swal({
                          title: "DeleteConfirm",
                          type: "warning",
                          showCancelButton: true,
                          confirmButtonColor: "#DD6B55",
                          confirmButtonText: "Confirm",
                          closeOnConfirm: false,
                          cancelButtonText: "Cancel"
                        },
                        function(){
                            $.ajax({

                                method: "post",
                                url: url,
                                data: {
                                    id:id,
                                    _token:LA.token,
                                    uri_segment:uri_segment,
                                },
                                success: function (data) {
                                    $.pjax.reload("#pjax-container");

                                    if (typeof data === "object") {
                                        if (data.status) {
                                            swal(data.message, "", "success");
                                        } else {
                                            swal(data.message, "", "error");
                                        }
                                    }
                                }
                            });
                        });            
                    });
                });
            ');

            // $grid->created_at();
            // $grid->updated_at();
        });
    }


    
/*
 To show the detail view of Hospital
 */
public function showNew($id){
    return Admin::content(function (Content $content) use ($id){
            $content->header('Hospital Details');
            $hospital = Hospital::find($id);
            $doctors = $hospital->doctors()->get();
            $content->body(view('hospital.hospital_detail',compact('hospital','doctors')));
        });
}
    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Hospital::class, function (Form $form) {

            // $form->display('id', 'ID');
                $form->setId('hospital_form');
           
                $form->text('name','Name')->rules('required');
                $form->textarea('address','Address')->attribute(['id' => 'route'])->rules('required');
                $form->text('city','City')->attribute(['id'=>'locality']);
                 $form->text('state','Area')->attribute(['id'=>'administrative_area_level_1']);
                 $form->hidden('lat')->attribute(['id'=>'lat']);
                 $form->hidden('lan')->attribute(['id'=>'lng']);
               $form->text('clinic_reg_no','Reg No');
                $form->text('patient_beds','Patient Beds');
               $form->select('24_7_emerg_ward','24/7 Emergency Services')->options(['0'=>'No','1'=>'Yes']);
               $form->select('wheel_charir','Wheel Chair Accessibility')->options(['0'=>'No','1'=>'Yes']);
               $form->select('pharmacy','Pharmacy')->options(['0'=>'No','1'=>'Yes']);
               $form->select('in_house_lab','Laboratory Services')->options(['0'=>'No','1'=>'Yes']);
                $form->select('car_parking','Car Parking Space')->options(['0'=>'No','1'=>'Yes']);
                $form->select('ambulance_service','Ambulance Services')->options(['0'=>'No','1'=>'Yes']);
                $states = [
                    'on'  => ['value' => 1, 'text' => 'Active', 'color' => 'success'],
                        'off' => ['value' => 0, 'text' => 'Inactive', 'color' => 'danger'],
                        ];
                
                $form->switch('status','Status')->states($states);
                 $form->disableReset();

              
        });
    }
}
