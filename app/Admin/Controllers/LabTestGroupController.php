<?php

namespace App\Admin\Controllers;

use App\Models\LabTestGroup;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use App\Models\LabsTest;

class LabTestGroupController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Test Profile');
           // $content->description('description');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Test Profile');
         //   $content->description('description');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Test Profile');
          //  $content->description('description');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(LabTestGroup::class, function (Grid $grid) {
            $grid->model()->orderBy('id', 'desc');
            $grid->actions(function ($actions) {
                    $actions->disableDelete();
                     $key = $actions->getKey();
                     $LabTestGroupStatus = LabTestGroup::find($key);
                    $value = $LabTestGroupStatus->status;
                    if($value !=2){
                            $actions->append("<a href=''><i class='fa fa-trash delete' id='$key'></i></a>");
                             }
                        });
           
                   
             $grid->tools(function ($tools) {
                 $tools->batch(function ($batch) {
                   $batch->disableDelete();
                });
             });
             $grid->disableRowSelector();
          $grid->name('Profile Name');
          $grid->column('Test Name')->display(function () {
            // dd($this->tests_id);
            $name = '';
            foreach($this->tests_id as $id) {
              // dd($id);
                $late=LabsTest::find($id);
                if($late!=null)
                $name .= LabsTest::find($id)->name .',';

            }
            $name = rtrim($name,',');
            return str_replace(',', ', ', $name);
             $test_name = $this->tests()->pluck('name')->toArray();
             return implode(', ',$test_name);
          });


          $grid->column('Test count')->display(function () {       
            return count($this->tests_id);            
          });


          $grid->column('status','Status')->display(function ($title) {
                    return $this->status == '1' ? '<span class="btn btn-success btn-xs">Active</span>' : ($this->status == '0' ? '<span class="btn btn-primary btn-xs">InActive</span>': '<span class="btn btn-danger btn-xs">Deleted</span>');
                });
            $grid->filter(function($filter){
            $filter->disableIdFilter();           
            $filter->ilike('getTest.name', 'Test Name');
            });
             // Neelamegam_29/08/2018_CommonDelete
            Admin::script('
                $( document ).ready(function(){
                    $(".delete").click(function(){
                        var id = $(this).prop("id");
                        var url = "CommonDelete/" + id;
                        var fullurl = window.location.pathname.split("/"); 
                        var uri_segment = fullurl[2];

                        swal({
                          title: "DeleteConfirm",
                          type: "warning",
                          showCancelButton: true,
                          confirmButtonColor: "#DD6B55",
                          confirmButtonText: "Confirm",
                          closeOnConfirm: false,
                          cancelButtonText: "Cancel"
                        },
                        function(){
                            $.ajax({

                                method: "post",
                                url: url,
                                data: {
                                    id:id,
                                    _token:LA.token,
                                    uri_segment:uri_segment,
                                },
                                success: function (data) {
                                    $.pjax.reload("#pjax-container");

                                    if (typeof data === "object") {
                                        if (data.status) {
                                            swal(data.message, "", "success");
                                        } else {
                                            swal(data.message, "", "error");
                                        }
                                    }
                                }
                            });
                        });            
                    });
                });
            ');

            $grid->created_at();
            $grid->updated_at();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(LabTestGroup::class, function (Form $form) {

            // $form->display('id', 'ID');
            $form->text('name','Profile Name')->rules('required');
             $states1 = [
                        'off'  => ['value' => 0, 'text' => 'No', 'color' => 'danger'],
                        'on' => ['value' => 1, 'text' => 'Yes', 'color' => 'success'],
                        ];
            $form->switch('show_profile_name','Show Profile Name')->states($states1)->default('1'); 
            $form->multipleSelect('tests_id','Tests')->options(LabsTest::where('status',1)->orderBy('name','asc')->pluck('name', 'id'))->rules('required');
             $form->text('category', 'Category');
             $form->text('code', 'Code');
            $states = [
                        'off'  => ['value' => 0, 'text' => 'Inactive', 'color' => 'danger'],
                        'on' => ['value' => 1, 'text' => 'Active', 'color' => 'success'],
                        ];
            $form->switch('status','Status')->states($states)->default('1');
            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');

            $form->saving(function(Form $form) {
                foreach($form->tests_id as $val) {
                     if($val != '' && $form->model()->id) {
                        $lab_test = LabsTest::find($val);
                        $new_array = [];
                        foreach($lab_test->group_id as $val) {
                            if($val == $form->model()->id)
                                continue;
                            $new_array[] = $val;
                        }
                        $lab_test->group_id = json_encode($new_array);
                        $lab_test->save();
                    }
                }
            });

            $form->saved(function (Form $form) {
                //error_reporting(E_ALL);ini_set('display_errors','On');
                $id = $form->model()->id;
                foreach($form->tests_id as $val) {
                    if($val != '') {
                        $lab_test = LabsTest::find($val);
                        $group_id = $lab_test->group_id;
                        if($group_id == '')
                            $group_id = [];
                        array_push($group_id,$id);
                        $lab_test->group_id = json_encode($group_id);
                        $lab_test->save();
                    }
                }

            }); 
        });
    }
}
