<?php

namespace App\Admin\Controllers;
use App\Services\LoginServices as LoginService;
use App\Services\LabServices as LabService;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use DB;
class ApiController extends Controller
{
	private $request;
	private $labservice;
	private $loginservice;
	public function __construct(Request $request)
	{
		$this->request  = $request;
		$this->labservice = new LabService();
		$this->loginservice = new LoginService();
		
	}	
	/**
	 * Get ALl Lab Test function
	 *
	 * @return void
	 * We have send all http status 200 in all return call
	 * livigro status 200 = success , 400 = error
	 */
	public function alltest() {
		return $this->labservice->alltest();
	}

	/**
	 * Get Lab All Test function
	 *
	 * @param Request $request
	 * @return void
	 */
	public function lab_alltest() {
	  	return $this->labservice->lab_alltest($this->request);
	} 
	/**
	 * [getImagePath description]
	 * @return [type] [description]
	 */
	public function getImagePath() {
		$response['path']   = env('AWS_IMAGE_URL');
		$response['status'] = config('resource.status_200');
		return response()->json($response,config('resource.status_200'));
	}
	/**
	 * [lab_alltest_update description]
	 * @param   [description]
	 * @return [type]                 [description]
	 */
	public function lab_alltest_update() {
		 return $this->labservice->lab_alltest_update($this->request);
	}
	/**
	 * [lab_test_selectlist description]
	 * @param   [description]
	 * @return [type]                 [description]
	 */
	public function lab_test_selectlist() {
		return $this->labservice->lab_test_selectlist($this->request);
	} 

	/**
	 * [lab_test_categories description]
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function lab_test_categories() {     
		return $this->labservice->lab_test_categories($this->request);
	} 	

	/**
	 * [checkUserExist description]
	 * @param   [description]
	 * @return [type]                     [description]
	 */
	public function checkUserExist(){		
		return $this->loginservice->checkUserExist($this->request);
	}

	
	/**
	 * Login function
	 *
	 * @param Request $request
	 * @return void
	 */
	public function login(){
		return $this->loginservice->login($this->request);
	}

	/**
	 * Signup function
	 * @param Request $request
	 * @return void
	 */
	public function signUp() {
		return $this->loginservice->signUp($this->request);
	}

	/**
	 * ProfileUpdate function
	 *
	 * @param Request $request
	 * @return void
	 */
	public function profileUpdate() {
		return $this->labservice->profileUpdate($this->request);
	}

	/**
	 * Profile Completion function
	 *
	 * @param Request $request
	 * @return void
	 */
	public function getProfileCompletion(){
		return $this->labservice->getProfileCompletion($this->request);
	}
	/**
	 * Get Patient List function
	 *
	 * @param Request $request
	 * @return void
	 */
	public function getPatientList() {
		return $this->labservice->getPatientList($this->request);
	}



	/**
	 * Undocumented function
	 *
	 * @param Request $request
	 * @return void
	 */
	public function getPatientProfile() {
		return $this->labservice->getPatientProfile($this->request);
	}
	/**
	 *Update Patient Info Function
	 * @param Request $request
	 * @return void
	 */
	public function updatePatientInfo() {
		return $this->labservice->updatePatientInfo($this->request);
	}
//5. Need to show All labs available test
/**
 * Show All Lab Test function
 *
 * @param Request $request
 * @return void
 */
public function showAllLabTest(){
	return $this->labservice->showAllLabTest($this->request);
}

	/**
	 * Show Test Based on the Category function
	 *
	 * @param Request $request
	 * @return void
	 */
	public function showTestBaseOnCategory(){
		return $this->labservice->showTestBaseOnCategory($this->request);
	} 
	//7. raise a test 
	/**
	 * Raise Test function
	 *
	 * @param Request $request
	 * @return void
	 */
	public function RaiseTest(){		
		return $this->labservice->RaiseTest($this->request);
	}

	//lab can update their individual available tests price
	/**
	 * Lab Can Update their test Data function
	 *
	 * @param Request $request
	 * @return void
	 */
	public function labupdatetheirtestdata(){
		return $this->labservice->labupdatetheirtestdata($this->request);
	}
	/**
	 * Update Specimen Collected function
	 *
	 * @param Request $request
	 * @return void
	 */
	public function specimencollected() {
		return $this->labservice->specimencollected($this->request);
	}

	//Lab can cancel the raised test
	/**
	 * Cancel Test function
	 *
	 * @param Request $request
	 * @return void
	 */
	public function Canceltest(){
		return $this->labservice->Canceltest($this->request);
	}

	//TODO need to show the labs indidual avaiable test
	/**
	 * Get Labs Individual Test function
	 *
	 * @param Request $request
	 * @return void
	 */
	public function indidualtest(){
		return $this->labservice->indidualtest($this->request);
	}

	//1.Need to show the booked test 
	//1.1 show both doctor raised test and walk in test in lab module
	//1.2 need to show the status, 1.specimen collected 2. amount due 3. test completed
	/**
	 * Doctor Raised Test to Lab function
	 *
	 * @param Request $request
	 * @return void
	 */
	public function raisedtesttolab(){
		return $this->labservice->raisedtesttolab($this->request);
	}
	/**
	 * Recipe Update function
	 *
	 * @param Request $request
	 * @return void
	 */
	public function updateReceipt(){
		return $this->labservice->updateReceipt($this->request);
	}
	/**
	 * Get WORKING TIME function
	 *
	 * @param Request $request
	 * @return void
	 */
	public function getWorkingTime(){
		return $this->labservice->getWorkingTime($this->request);
	}
	/**
	 * Update Working Time function
	 *
	 * @param Request $request
	 * @return void
	 */
	public function UpdateWorkingTime() {
	 	return $this->labservice->UpdateWorkingTime($this->request);  	
	}
	/**
	 * Generate Report function
	 *
	 * @param Request $request
	 * @return void
	 */
	public function generateReport() {
		return $this->labservice->generateReport($this->request);
	}


	/**
	 * Undocumented function
	 *
	 * @param Request $request
	 * @return void
	 */
	public function getCountry() {
		return $this->labservice->getCountry($this->request);
	}
	/**
	 * [getLabNotification description]
	 * @param   [description]
	 * @return [type]                 [description]
	 */
	public function getLabNotification(){
		return $this->labservice->getLabNotification($this->request);
	}
	/**
	 * [raise_test_all description]
	 * @param   [description]
	 * @return [type]                 [description]
	 */
	public function raise_test_all(){
		return $this->labservice->raise_test_all($this->request);
	}
	/**
	 * [lab_receipt_list description]
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function lab_receipt_list(){
	return 	$this->labservice->lab_receipt_list($this->request);
	}
	/**
	 * [lab_test_subcategories description]
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function lab_test_subcategories(){
		return	$this->labservice->lab_test_subcategories($this->request);
	}
	/**
	 * [add_new_rise_test description]
	 * @param  [description]
	 */
	public function add_new_rise_test(){
		return $this->labservice->add_new_rise_test($this->request);  	
	}
	/**
	 * [change_collected_confirm_test description]
	 * @param   [description]
	 * @return [type]                 [description]
	 */
	public function change_collected_confirm_test(){
		return $this->labservice->change_collected_confirm_test($this->request);
	}
	/**
	 * [updateReceiptamount description]
	 * @param   [description]
	 * @return [type]                 [description]
	 */
	public function updateReceiptamount(){
		return $this->labservice->updateReceiptamount($this->request);
	}
	/**
	 * [add_new_test_lab description]
	 * @param  [description]
	 */
	public function add_new_test_lab(){
		return  $this->labservice->add_new_test_lab($this->request);
	}
	/**
	 * [lab_test_id_subcategories description]
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function lab_test_id_subcategories(){
		return $this->labservice->lab_test_id_subcategories($this->request);
	}

	/**
	 * [lab_add_update_collectiondata description]
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function lab_add_update_collectiondata(){
		return $this->labservice->lab_add_update_collectiondata($this->request);
	}

	/**
	 * [getReceipt description]
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function getReceipt(){
		return $this->labservice->getReceipt($this->request);
	}
	/**
	 * [getlabgenerateReport description]
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function getlabgenerateReport(){	
		return $this->labservice->getlabgenerateReport($this->request);
	}
	/**
	 * [editGentrateReport description]
	 * @param   [description]
	 * @return [type]                 [description]
	 */
	public function editGentrateReport(){
		return $this->labservice->editGentrateReport($this->request);
	}

	/**
	 * [clinicProfile description]
	 * @param   [description]
	 * @return [type]                 [description]
	 */
	public function clinicProfile(){ 
		return $this->labservice->clinicProfile($this->request);
	}
	/**
	 * [raise_user_list description]
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function raise_user_list(){
		return $this->labservice->raise_user_list($this->request);
	}
	
	/**
	 * [saveNotification description]
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function saveNotification(Request $request){
		return $this->labservice->saveNotification($this->request);
	}

	/**
	 * [getReport description]
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function getReport(){
		return $this->labservice->getReport($this->request);
	}
	/**
	 * [getRaisedTestDetail description]
	 * @param   [description]
	 * @return [type]                 [description]
	 */
	public function getRaisedTestDetail(){
		return $this->labservice->getRaisedTestDetail($this->request);
	}


	/**
	 * [getCommonData description]
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function getCommonData(){
		return $this->labservice->getCommonData($this->request);
	}

	/**
	 * [updateRangeOwnTest description]
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function updateRangeOwnTest() {
		return $this->labservice->updateRangeOwnTest($this->request);
	}


	/**
	 * [insertTestCart or Delete description]
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function insertTestCart() {
		return $this->labservice->insertTestCart($this->request);
	}
	/**
	 * for doctor module need to send the lab_id
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function getCartTestDetail(){
		return $this->labservice->getCartTestDetail($this->request);
	}
	/**
	 * [onclickCart description]
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function onclickCart() {
		return $this->labservice->onclickCart($this->request);
	}
	
	public function onclickCart1() {
		return $this->labservice->onclickCart1($this->request);
	}


    /**
	 * [newDoctoradd description]
	 * @param  Request $request [description]
	 * @return [type]           [description] 
	 */
	public function newDoctoradd() {
		return $this->labservice->newDoctoradd($this->request);
	}

	 /**
	 * [labForgotPassword description]
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function labForgotPassword() {
		return $this->labservice->labForgotPassword($this->request);
	}


    /**
	 * [labForgotPassword description]
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function contacts() {
		return $this->labservice->contacts($this->request);
	}

    /**
	 * [labPatientsorder description]
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function labPatientsorder() {
		return $this->labservice->labPatientsorder($this->request);
	}
	
	/**
	 * [getgrouppackage description]
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function getgrouppackage() {
		return $this->labservice->getgrouppackage($this->request);
	}
/**
	 * [getLabgrouppackage description]
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function getLabgrouppackage() {
		return $this->labservice->getLabgrouppackage($this->request);
	}

	public function testingsms(LabService $labservice) {		
		/*send_push_notification('9842363703','test message,test message,test message,test messagetest messagetest messagetest messagetest messagetest messagetest message','New test raised');*/
		$msg = "New test raised for livigro, Raise test id -000 ";
		$mobile = '+919123526263';
        send_sms(substr($mobile,3),$msg);
		dd('sucess'); 
		return $labservice->testingsms($this->request);
	}

	/**
	 * Get  All faq data
	 *
	 * @param Request $request
	 * @return void
	 */
	public function getFaq() { 
	  	return $this->labservice->getFaq($this->request);
	} 

    /**
	 * doctor dashboard all details
	 * @param  [type]  [description]
	 * @return [type]       [description]
	 */
	public function AddNewPackageSamplepage() {		
			return $this->labservice->AddNewPackageSamplepage($this->request);
		}

	/**
	 * New package test add from sample collection page
	 * @param  [type]  [description]
	 * @return [type]       [description]
	 */
	public function get_all_Doctor_details_Dashboard() {		
			return $this->labservice->get_all_Doctor_details_Dashboard($this->request);
		}

       /**
	 * sample excel download
	 * @param  [type]  [description]
	 * @return [type]       [description]
	 */
	public function exportexcel() {		
			return $this->labservice->exportexcel($this->request);
		}
		
	 /**
	 * sample excel download
	 * @param  [type]  [description]
	 * @return [type]       [description]
	 */
	public function getSubscriptionExpiredData() {		
			return $this->labservice->getSubscriptionExpiredData($this->request);
		}


}
