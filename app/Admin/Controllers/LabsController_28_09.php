<?php

namespace App\Admin\Controllers;
use App\Models\Labs;
use App\Models\Doctors;
use App\Models\LabsAvaTest;
use App\Models\LabsTest;
use App\Models\RaiseTest;
use App\Models\Patients;
use App\Models\Units;
use App\Models\Package;
use App\Models\LabTestGroup;
use App\Models\Notification;
use App\Models\WorkingTime;
use App\User;
use Excel;
use PDF;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Illuminate\Support\Facades\Route;
// use Illuminate\Support\Facades\Request;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use App\Admin\Extension\ExcelExpoter;
use Illuminate\Support\Facades\Storage;
use Auth;
use Mail;
use App\Mail\ReportToPatient;
use App\Models\City;
use App\Models\Receipt;
use App\Models\LabsCategory;
use App\Models\Report;
use App\Models\Plans;
use App\Models\Subscription;
use Illuminate\Support\MessageBag;
use Carbon\Carbon;
class LabsController extends Controller 
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Labs');
            // $content->description('description');

            $content->body($this->grid());
        });
    }
    /**
     * [enablelog description]
     * @param  Request $request [description]
     * @return String          [description]
     */
    public function enablelog(Request $request) {
          $value = DB::table('fc_WSAuth')->first()->enable_log;
          if(!$value)   {
              DB::table('fc_WSAuth')->update(['enable_log'=>1]);
              echo "Disable Log";die;
            }
          else {
               DB::table('fc_WSAuth')->update(['enable_log'=>0]);
              echo "Enable Log";die;
            }

    }
    public function mypatient(){
      /*$patientsList = DB::table('doctor_patient')
      ->leftjoin('patients_details', 'patients_details.id', '=', 'doctor_patient.patient_id')
      ->leftjoin('raise_test', 'patients_details.id', '=', 'raise_test.patients_id')
      ->select(DB::raw('doctor_patient.patient_id,
            patients_details.name as name, patients_details.*, COALESCE(ARRAY_AGG(raise_test.id), ARRAY[]::INT[]) AS test_name'))
      ->where('doctor_patient.lab_id', '=', Admin::user()->linked_id)
      ->groupBy('doctor_patient.patient_id','patients_details.name','patients_details.id')
      ->orderBy('patients_details.id')
      ->get(); */
      $patientsList = DB::table('doctor_patient')
      ->leftjoin('patients_details', 'patients_details.id', '=', 'doctor_patient.patient_id')
      ->select(DB::raw('patients_details.name as name, patients_details.*'))
      ->where('doctor_patient.lab_id', '=', Admin::user()->linked_id)
      ->groupBy('doctor_patient.patient_id','patients_details.name','patients_details.id')
      ->orderBy('patients_details.id')
      ->where('patients_details.status',1)
      ->get(); 
      foreach ($patientsList as $key => $val){
        $getTest = DB::table('raise_test')
                  ->select(DB::raw('COALESCE(ARRAY_AGG(raise_test.id), ARRAY[]::INT[]) AS test_name'))
                  ->where('lab_id', Admin::user()->linked_id)
                  ->where('patients_id',$val->id)
                  ->first();
        $val->test_name = $getTest->test_name;
         // print_r($getTest);die;
      }
      //echo "<pre>";print_r($patientsList);die;
        return Admin::content(function (Content $content) use($patientsList) {
            // dd(Auth::guard('admin')->user()->inRoles('labs'));
             if(Auth::guard('admin')->user()->user_type_id ==1){
                 // $ids = Auth::guard('admin')->user()->linked_id;
                $content->header('My Patients');
                $content->description('Patients Details');
                $content->body(view('mypatients',compact('patientsList')));
             }
             
        });
    }

    public function mydoctor(){
      /*$doctorsList = DB::table('raise_test')
      ->join('doctors_details', 'doctors_details.id', '=', 'raise_test.doctor_id')
      ->join('patients_details', 'patients_details.id', '=', 'raise_test.patients_id')
      ->select(DB::raw(' patients_details.*, doctors_details.*, array_agg(raise_test.id) as test_name, array_agg(raise_test.patients_id) as patient_list'))
      ->where('raise_test.lab_id', '=', Admin::user()->linked_id)
      ->groupBy('raise_test.doctor_id','patients_details.name','patients_details.id','doctors_details.id','raise_test.patients_id')
      ->get();  */
       $doctorsList = DB::table('raise_test')
      ->join('doctors_details', 'doctors_details.id', '=', 'raise_test.doctor_id')
      ->select(DB::raw('doctors_details.*, array_agg(raise_test.id) as test_name'))
      //->select(DB::raw('doctors_details.*'))
      ->where('raise_test.lab_id', '=', Admin::user()->linked_id)
      ->groupBy('raise_test.doctor_id','doctors_details.id')
      ->get();  
        return Admin::content(function (Content $content) use($doctorsList) {
            // dd(Auth::guard('admin')->user()->inRoles('labs'));
             if(Auth::guard('admin')->user()->user_type_id ==1){
                 // $ids = Auth::guard('admin')->user()->linked_id;
                $content->header('My Doctors');
                $content->description('Doctor Details');
                $content->body(view('mydoctor',compact('doctorsList')));
             }
             
        });
    }

    /**
     * [sample_collection description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
      public function sample_collection(Request $request,$id) {
        $raise_test = RaiseTest::find($id);
        $lab_tests_id = $raise_test->lab_tests_id;
        $newPackages ="";
        // dd($lab_tests_id);
        foreach ($lab_tests_id as $value) {
          $values = array();
          if($value!=''){
           $oldPackages = Receipt::where([['raise_test_id',$id],
                    ['tests_id',$value]
                    ])->get();
           foreach ($oldPackages as $oldPackage) {
            if($oldPackage->test_type != "single")
              $values[] = $oldPackage->test_type;
           }
         }
          // dd();
         $newPackages = Package::whereNotIn('id',array_filter($values))->get();
        }
       $lab_tests = LabsTest::whereIn('id',$lab_tests_id)->get();
       foreach($lab_tests as $lab_test) {
        // $oldPackage = Receipt::where([['raise_test_id',$id],
        //             ['tests_id',$lab_test->id]
        //             ])->whereNotIn('test_type','single')->get();
        //   dd($oldPackage);
         $position = Receipt::where([['raise_test_id',$id],
                    ['tests_id',$lab_test->id]
                    ])->first();
  if($position)
           $lab_test->position = $position->labtest_sub_name;
  else
     $lab_test->position = 0;
       }
       $labsAvTest = LabsAvaTest::where('lab_id',$raise_test->lab_id)->pluck('lab_tests_id');
       $lab_test_all = LabsTest::whereIn('id',$labsAvTest)->get();
       $lab_sub = LabsCategory::all();
       if($lab_tests){
                 return Admin::content(function (Content $content) use($lab_tests,$id,$lab_sub,$lab_test_all,$newPackages){
                    
                    $content->header('Sample Collection');
                    ///$content->description('description');

                    $content->body(view('sample',compact('lab_tests','id','lab_sub','lab_test_all','newPackages')));
                });
        }else{
            return Admin::content(function (Content $content) {

            $content->header('No Tests is Assigned for this Raise Test');
            ///$content->description('description');

            $content->body("Nothing Found");
        });

        }
    }


    public function receipt_collection(Request $request,$id) {
        $raise_test = RaiseTest::find($id);
        $patient = $raise_test->getPatient;
        $lab_tests_id = $raise_test->lab_tests_id;
        $lab_tests = array();
        $raise_test_id = $id;
       //$lab_tests = LabsTest::whereIn('id',$lab_tests_id)->get();

       /*$lab_tests = DB::table('receipt_table')
                  ->join('lab_tests', 'lab_tests.id', '=', 'receipt_table.tests_id')
                  ->select(DB::raw('receipt_table.tests_id as id,
                        lab_tests.name as name, receipt_table.amount as price'))
                  ->where('receipt_table.raise_test_id',$id)
                  ->get();*/
        /*$receipt_data = Receipt::where('raise_test_id',$raise_test_id)->where('test_type','single')->get();    
        $receipt_list = array();          
        foreach ($receipt_data as $key => $value) 
        {            
          $value->test_name  = getLabtestdetails($value->tests_id,'name');                     
          $receipt_list[] = $value;

        }
        $unique_data = array_unique($receipt_list);       
        // now use foreach loop on unique data
        $receipt_lists = array();
        foreach($unique_data as $val) {
          $receipt_lists[] = $val; 
        }
*/
        $receipt_data_package = Receipt::where('raise_test_id',$raise_test_id)
                             ->with('testData')
                             ->get()->toArray();               
        foreach ($receipt_data_package as $key => $value){
          if($value['test_type']!='' && $value['test_type'] != NULL){
            if($value['test_type'] == 'single'){
              $lab_tests['single'.$value['tests_id']] = $value;
              $lab_tests['single'.$value['tests_id']]['name'] = $value['test_data']['name'];
            }else{
              if(isset($value['test_data']) && $value['test_data'] != ''){
                $packageDetails = Package::where('id',$value['test_type'])->first();
                $packageAmount = LabsAvaTest::where('lab_tests_id',$value['test_type'])->where('lab_id',$raise_test->lab_id)->where('test_type','group')->first();
                $lab_tests[$value['test_type']]['id'] = $value['test_type'];
                $lab_tests[$value['test_type']]['test_type'] = $value['test_type'];
                $lab_tests[$value['test_type']]['packageName'] = $packageDetails->package_name;
                $lab_tests[$value['test_type']]['name'] = $packageDetails->package_name;
                $lab_tests[$value['test_type']]['amount'] = (isset($packageAmount) && $packageAmount->amount != null && $packageAmount->amount!='')?$packageAmount->amount:$packageDetails->amount;
                foreach ($packageDetails->profile_id as $key => $val) {
                    $packageTestDetails = LabTestGroup::where('id',$val)->first()->toArray();
                    $lab_tests[$value['test_type']]['package'][$packageTestDetails['id']]['id'] = $packageTestDetails['id'];
                    $lab_tests[$value['test_type']]['package'][$packageTestDetails['id']]['groupName'] = $packageTestDetails['name'];
                     $lab_tests[$value['test_type']]['package'][$packageTestDetails['id']]['showName'] = $packageTestDetails['show_profile_name'];
                    foreach($packageTestDetails['tests_id'] as $k => $v){
                      $testDetails = Receipt::where('raise_test_id',$id)->where('tests_id',$v)->with('testData')->first();
                      $lab_tests[$value['test_type']]['package'][$packageTestDetails['id']]['testList'][$v] =  $testDetails;
                    }
                }
              }
            }
          }
        }        
       // echo "<pre>"; print_r($lab_tests); die;
       if($raise_test){
                 return Admin::content(function (Content $content) use($lab_tests,$id,$patient,$raise_test){
                    
                    $content->header('Receipt Collection');
                    ///$content->description('description');

                    $content->body(view('receipt',compact('lab_tests','id','patient','raise_test')));
                });
        }else{
            return Admin::content(function (Content $content) {

            $content->header('No Tests is Assigned for this Raise Test');
            ///$content->description('description');

            $content->body("Nothing Found");
        });

        }
    }

    public function cancel(Request $request,$id) {
      // dd('Hi');
           $raise_test = RaiseTest::find($id);
           $status = $raise_test->paid_status;
        return Admin::content(function (Content $content) use($id,$raise_test,$status){
                    
                    $content->header('Cancel Form');
                    ///$content->description('description');

                    $content->body(view('cancel',compact('id','raise_test','status')));
                });
        }
    

    public function cancelsave(Request $request) {
        $raise_test_id = $_POST['raise_test_id'];
        $reason = $_POST['check'];
        $raise = RaiseTest::find($raise_test_id);
        $raise->paid_status = $reason;
        $raise->save();
        $success = new MessageBag([
            'title'   => 'Cancel Test',
            'message' => 'Saved Successfully',
        ]);
        return back()->with(compact('success'));
    }

    public function report_generation(Request $request,$id){

        $raise_test = RaiseTest::find($id);
        // dd($raise_test);
        $reports = Report::where('raise_test_id',$id)->get();
        $report = count($reports);
         $patient = $raise_test->getPatient;
         $doctor = $raise_test->getDoctor;
         $lab_tests_id = $raise_test->lab_tests_id;
		     $units = Units::all();
          $signature="";
         if($raise_test->raised_by ==1){
          $lab = Labs::where('id',$raise_test->lab_id)->first();
           $signature = $lab->signature;
         }
         if($raise_test->raised_by ==2){
           $doctor = Doctors::where('id',$raise_test->doctor_id)->first();
            $signature = $doctor->signature;
         }
		    // dd($signature);
        $lab_tests = LabsTest::whereIn('id',$lab_tests_id)->get();
        // dd($doctor);
       if($lab_tests){
		             //dd($lab_tests);
                 return Admin::content(function (Content $content) use($lab_tests,$id,$patient,$raise_test,$doctor,$units, $report,$signature){
                    
                    $content->header('Report Collection');
                    ///$content->description('description');

                    $content->body(view('report',compact('lab_tests','id','patient','raise_test','doctor','units','report','signature')));
                });
        }else{
            return Admin::content(function (Content $content) {

            $content->header('No Tests is Assigned for this Raise Test');
            ///$content->description('description');

            $content->body("Nothing Found");
        });

        }
    }

    /**
     *  save the data in receipt table regarding the raise test id
     * @param  Request $request [description]
     * @return [type]           [description]
     */
   public function samplesave(Request $request) {
      // dd($_POST);
        $raise_test_id = $_POST['raise_test_id'];
        $raise = RaiseTest::find($raise_test_id);
        $old_test_id = $raise->lab_tests_id;
        $old_amount = $raise->original_amount;
        // dd($old_amount);
        $raise->update([
            'specimen_collected' => '1'
        ]);

        foreach($_POST as $key=>$value) {         
            //single update price test amount
            if($key != 'raise_test_id' && $key !='_token' && $key !='package') {
                $test_id = $key;
                $receipt = Receipt::where([['raise_test_id',$raise_test_id],['tests_id',$test_id]]);
                if($receipt->count() > 0) {
                    //Update the receipt table
                    $receipt->update([
                        'labtest_sub_name' => $value
                    ]);
                    
                    $success = new MessageBag([
                        'title'   => 'Sample Collection',
                        'message' => 'Successfully Updated Sample Collection',
                    ]);
                }else{
                    //insert new record in the receipt table
                //get amount start
                $amount_count = LabsAvaTest::where('lab_id',$raise->lab_id)->where('lab_tests_id',$test_id)->get();
                  if($amount_count->count() ==0)
                      {
                  $labtest_data = LabsTest::find($test_id);
                  $amount    = $labtest_data->price;
                  $range     = $labtest_data->reference_range;
                  $range_end = $labtest_data->reference_endrange;
                      }else{
                  $amount = $amount_count->first()->amount;
                  $range     = $amount_count->first()->range;
                  $range_end = $amount_count->first()->range_end;
                      }
                //get amount end
                //units start
                 $lab_count =LabsTest::where('id',$test_id)->get();       
                 if($lab_count->count() !=0){        
                    $units_count = Units::where('id',$lab_count->first()->measurement)->get();
                    if($units_count->count() !=0){
                     $units = $units_count->first()->name;
                    }else{
                     $units = '-nill-' ;
                    }}else{
                     $units = '-nill-' ;
                    }
                 //units end
                  Receipt::create([
                        'raise_test_id' => $raise_test_id,
                        'tests_id' => $test_id,
                        'amount' => $amount,
                        'labtest_sub_name' => $value,
                        'test_type' =>'single',
                        'units' => $units,
                        'reference_range' => $range,
                        'reference_endrange' =>$range_end,
                    ]);
                    $temp=count($old_test_id);
                    $old_test_id[$temp]     ="$test_id";
                    $raise->lab_tests_id    = $old_test_id;
                    $raise->original_amount = $old_amount + $amount;
                    $raise->save();
                     $success = new MessageBag([
                        'title'   => 'Sample Collection',
                        'message' => 'Successfully Added New Sample Collection',
                    ]);
                }
            }
            //single end update test and amount

            //group package amount update
             if($key == 'package'){           
            foreach ($_POST['package'] as $key => $value) {              
              $package ="";
              if($value !="" || $value !=null){
                $package_id = $value;
              if($package_id!=null || $package_id !=""){
                $package = Package::find($package_id);
              }
              if($package){
                if($package->tests_id!=""){
                  foreach ($package->tests_id as $key => $values) {  
                  $labtest = LabsCategory::where('lab_test_id',$values)->first();
                  // dd($package_id);  
                  $receipt = Receipt::where('raise_test_id',$raise_test_id)->where('tests_id',$values)->where('test_type','!=','single')->get();
                    if($receipt->count() ==0){
                      $newreceipt = new Receipt;
                      $newreceipt->raise_test_id = $raise_test_id;
                      $newreceipt->tests_id = $values;
                      if($key == 0){
                         $newreceipt->amount =$package->amount;
                      }else{
                         $newreceipt->amount =0;
                      }                     
                      $newreceipt->test_type =$package_id;
                      $newreceipt->units = '-nill-';
                      $newreceipt->reference_range    = 0;
                      $newreceipt->reference_endrange = 0;
                      if($labtest)
                         $newreceipt->labtest_sub_name =$labtest->id;
                      $newreceipt->save();
                    }
                 }
                }
              }

              $raisePackage = RaiseTest::find($raise_test_id);
              $packagetestid = $raisePackage->lab_tests_id;
              $packageamount = $raisePackage->original_amount;
              $raisePackage->original_amount = $package->amount+$packageamount;
               if($package->tests_id!="")
                   $raisePackage->lab_tests_id =array_merge($packagetestid,$package->tests_id);
              $raisePackage->save();
              }             
            }               
          }
          //group package amount update
            
        }
         
        return back()->with(compact('success'));
        // return back();
    }

    public function reportsave() {
        $raise_test_id = $_POST['raise_test_id'];
        $raise = RaiseTest::find($raise_test_id);
        $old_test_id = $raise->lab_tests_id;
        // dd($_POST);
        foreach($_POST as $key=>$value) {
            if($key != 'raise_test_id' && $key !='_token') {

                if (preg_match('/start/',$key)) {
                    //Start range update in receipt table
                    $test_id = explode('start_',$key)[1];
                    Receipt::where([['raise_test_id',$raise_test_id],
                        ['tests_id',$test_id]
                    ])->update(['reference_range'=>$value]);
                }else if (preg_match('/end/',$key)) {
                    //Start range update in receipt table
                    $test_id = explode('end_',$key)[1];
                    Receipt::where([['raise_test_id',$raise_test_id],
                        ['tests_id',$test_id]
                    ])->update(['reference_endrange'=>$value]);
                }else if (preg_match('/result/',$key)) {
                    //Start range update in receipt table
                    $test_id = explode('result_',$key)[1];
                    Receipt::where([['raise_test_id',$raise_test_id],
                        ['tests_id',$test_id]
                    ])->update(['result'=>$value]);
                }else if (preg_match('/unit/',$key)) {
                    //Start range update in receipt table
                    $test_id = explode('unit_',$key)[1];
                    Receipt::where([['raise_test_id',$raise_test_id],
                        ['tests_id',$test_id]
                    ])->update(['units'=>$value]);
                }
            }
        }
        //TODO need to create the record in report table
            Report::create([
                'raise_test_id' => $raise_test_id
            ]);

         $success = new MessageBag([
                        'title'   => 'Report Collection',
                        'message' => 'Successfully Updated Report Collection',
                    ]);
        //Need send the Notification to Lab,Patient and Doctor TODO
      // $file_name = 'Report_'.$raise_test_id.'.pdf';
      // $file_path = storage_path().'/app/public/export/';
      // libxml_use_internal_errors(true);
     
      //save notification start
      $report =  new Notification();
      $report->type          = 'report';
      $report->lab_id        = $raise->lab_id ;
      $report->doctor_id     = $raise->doctor_id ;
      $report->patient_id    = $raise->patients_id ;
      $report->raise_test_id = $raise->id; 
      $report->read_status      =  0;
      $report->save(); 
      //save notification end

      // $mpdf = new \Mpdf\Mpdf();
      // $mpdf->WriteHTML($this->generatePdf($raise_test_id,'report'));
      // $mpdf->Output($file_path.$file_name,\Mpdf\Output\Destination::FILE);
      // $mpdf->Output($file_name,'D');

      return back()->with(compact('success'));
    }

    public function reportgenerate(Request $request){  
      // dd("Hi");
       $raise_test_id = $_POST['raise_test_id'];
       //Need send the Notification to Lab,Patient and Doctor TODO
      $file_name = 'Report_'.$raise_test_id.'.pdf';
      $file_path = storage_path().'/app/public/export/';
      libxml_use_internal_errors(true);

      $mpdf = new \Mpdf\Mpdf();
      $mpdf->WriteHTML($this->generatePdf($raise_test_id,'report'));
      $mpdf->Output($file_path.$file_name,\Mpdf\Output\Destination::FILE);
      $mpdf->Output($file_name,'D');
        return back();

    }  
    public function updatereceipt(Request $request){     
      $raise_test = RaiseTest::find($_POST['raise_test_id']);       
      //print_r($_POST);die;
      $amount_received = ($_POST['amount_received'] != '')?$_POST['amount_received']:'0' ;
      $discount_price  = ($_POST['discount_price'] != '')?$_POST['discount_price']: '0' ;
      
        $originalAmount = ($raise_test->received_amount == null || $raise_test->received_amount == 0)?$raise_test->original_amount:$raise_test->remaining_amount;
        //$originalAmount = $raise_test->original_amount;
        $input_amount = $amount_received + $discount_price ; 
         if($originalAmount < $input_amount){
              $received_amount_wrong = 'wrong';
             echo $received_amount_wrong;die;
         }

      if($_POST['remaining_amount'] == 0){      
             
        $remaining_amount = $originalAmount - $input_amount;
      }else{ 
        $input_amount = $amount_received + $discount_price ;       
        $remaining_amount = $originalAmount - $input_amount;
      }
      echo $remaining_amount;die;

    	/*if($_POST['value']==null)
    	$_POST['value']=0;
         $raise_test = RaiseTest::find($_POST['raise_test_id']);
          $remaining_amount = '';
         $original_amount = $raise_test->original_amount;
        if($raise_test->remaining_amount >= $_POST['value']){
	        if($_POST['id'] != 'amount_received') {                       
	            $receipt = Receipt::where([['raise_test_id',$_POST['raise_test_id']],
	                        ['tests_id',$_POST['id']],
	                        ])->update(['amount' => $_POST['value']]);
	            $remaining_amount = $raise_test->remaining_amount;
	        }else
          { 
	          if($raise_test->remaining_amount==null)
            {
              $discount_price = $_POST['discount_price'];
	            $subtraction_amount = $raise_test->original_amount - $_POST['value'] ;
	            $received_amounts = $_POST['value'] ;
            }else{              
	              $subtraction_amount = $raise_test->remaining_amount - $_POST['value'] ;
	              $received_amounts = $raise_test->received_amount + $_POST['value'] ;
	          }
               
	         $remaining_amount = $original_amount - $_POST['value'];
	         $raise_test->update(['received_amount' => $received_amounts,'remaining_amount' => $subtraction_amount]);
	        } 
        }    
      echo $remaining_amount;die;*/
    }

    public function receiptsave() {            
        $raise_test_id = $_POST['raise_test_id'];
        $raise = RaiseTest::find($raise_test_id);
        $amount_received = ($_POST['amount_received'] != '')?$_POST['amount_received']:'0' ;
        $discount_price  = ($_POST['discount_price'] != '')?$_POST['discount_price']: '0' ;
       
        $input_amount = $amount_received ; 
        $remaining_amount = $raise->original_amount - $input_amount ; 
         if($raise->original_amount < $input_amount){
              $warning = new MessageBag([
                        'title'   => 'Warning',
                        'message' => 'Please check your input amount',
                    ]);
              return back()->with(compact('warning'));
         }
         $set_discount_price = $discount_price;
         $set_remaining_amount = $remaining_amount - $discount_price;
         $set_amount_received = $input_amount;
          if($raise->received_amount != null || $raise->received_amount != 0 )
              $set_amount_received = $raise->received_amount+ $input_amount;
          if($raise->discount_price != null || $raise->discount_price != 0 )
              $set_discount_price = $raise->discount_price + $discount_price;
          if($raise->remaining_amount != null || $raise->remaining_amount != 0 )
             $set_remaining_amount = $raise->original_amount - $set_amount_received - $set_discount_price;

        $mode = $_POST['payment_mode'];        
        if($set_remaining_amount >= 0 ){
          $raise->discount_price = $set_discount_price;
          $raise->remaining_amount = $set_remaining_amount;
          $raise->received_amount = $set_amount_received;
          $raise->mode_of_payment = $mode;
          if(($raise->received_amount+$raise->discount_price) == $raise->original_amount)
            $raise->paid_status = '1';

          $raise->save();
          if($raise->remaining_amount == 0 && ($raise->received_amount+$raise->discount_price) == $raise->original_amount){
            Receipt::where('raise_test_id',$raise_test_id)->update(['specimen_collected'=>1]);
          }

         $success = new MessageBag([
            'title'   => 'Receipt',
            'message' => 'Saved Successfully',
        ]);
        return back()->with(compact('success'));
      }else{
        $success = new MessageBag([
                        'title'   => 'Warning',
                        'message' => 'Please check your input amount',
                    ]);
              return back()->with(compact('success'));
      }

    }

    public function showAllPatientTestDetails() {
      //here need to show in table view
      //need to get the patient id
      
    }
    /**
     * [export description]
     * @return [type] [description]
     */
    public function export()
    {
    //error_reporting(E_ALL);ini_set('display_errors','On');
      $id = $_REQUEST['id'];
      $file_name = 'Receipt_'.$id.'.pdf';
      $file_path = storage_path().'/app/public/export/';
      libxml_use_internal_errors(true);    

      $mpdf = new \Mpdf\Mpdf();
      $mpdf->WriteHTML($this->generatePdf($id,'receipt'));
      $mpdf->Output($file_path.$file_name,\Mpdf\Output\Destination::FILE);
      $mpdf->Output($file_name,'D');

    }
    public function generatePdf($id, $type){
      $raise_test = RaiseTest::find($id);
      if($type == 'receipt'){
        if($raise_test->receipt_no == '' || $raise_test->receipt_no == 0){
            $raise_test->receipt_no = rand ( 10000 , 99999 );
            $raise_test->save();
        }
      }
      $raise_test_time = new Carbon($raise_test->updated_at);  
      //collection date
      $raise_test->collection_date = $raise_test_time->format('d F Y h:m A');
      $raise_test->receipt_date = $raise_test_time->format('d F Y');
      //collection date
      $raise_test_id =  $id;
      $Receipt = Receipt::where('raise_test_id',$id)->first();
      $Report = Report::where('raise_test_id',$id)->first();
      // dd($Receipt);
      //report date
      $receipt_time = new Carbon($Receipt->updated_at);
      $raise_test->report_date = $receipt_time->format('d F Y h:m');

      $raise_test->report_id = $Report->id;
      //report date
   
       //test data
       // $receipt_data = Receipt::where('raise_test_id',$id)->get();
       //testing date
      $receipt_data_package = Receipt::where('raise_test_id',$id)
                             ->with('testData')
                             ->get()->toArray();
//echo "<pre>";print_r($receipt_data_package);
        foreach ($receipt_data_package as $key => $value){
          if($value['test_type']!='' && $value['test_type'] != NULL){
            if($value['test_type'] == 'single'){
              $receipt_data['single'.$value['tests_id']] = $value;
              $receipt_data['single'.$value['tests_id']]['name'] = $value['test_data']['name'];
              $receipt_data['single'.$value['tests_id']]['test_name'] = $value['test_data']['name'];
            }else{
              if(isset($value['test_data']) && $value['test_data'] != ''){
                $packageDetails = Package::where('id',$value['test_type'])->first();
                $packageAmount = LabsAvaTest::where('lab_tests_id',$value['test_type'])->where('lab_id',$raise_test->lab_id)->where('test_type','group')->first();
                $receipt_data[$value['test_type']]['id'] = $value['test_type'];
                $receipt_data[$value['test_type']]['test_type'] = $value['test_type'];
                $receipt_data[$value['test_type']]['packageName'] = $packageDetails->package_name;
                $receipt_data[$value['test_type']]['amount'] = (isset($packageAmount) && $packageAmount->amount != null && $packageAmount->amount!='')?$packageAmount->amount:$packageDetails->amount;
                foreach ($packageDetails->profile_id as $key => $val) {
                    $packageTestDetails = LabTestGroup::where('id',$val)->first()->toArray();
                    $receipt_data[$value['test_type']]['package'][$packageTestDetails['id']]['id'] = $packageTestDetails['id'];
                    $receipt_data[$value['test_type']]['package'][$packageTestDetails['id']]['groupName'] = $packageTestDetails['name'];
                     $receipt_data[$value['test_type']]['package'][$packageTestDetails['id']]['showName'] = $packageTestDetails['show_profile_name'];
                    foreach($packageTestDetails['tests_id'] as $k => $v){
                      $testDetails = Receipt::where('raise_test_id',$id)->where('tests_id',$v)->with('testData')->first();
                      $receipt_data[$value['test_type']]['package'][$packageTestDetails['id']]['testList'][$v] =  $testDetails;
                    }
                }
              }
           }
          }
        }   
//echo "<pre>";print_r($receipt_data);die;
        $patient = $raise_test->getPatient;
        $doctor = $raise_test->getDoctor;
        /*foreach ($receipt_data as $key => $value) {
              $value->test_name = getLabtestdetails($value->tests_id,'name');              
              $value->test_collection_name = LabsCategorydata($value->labtest_sub_name,'name');              
        }*/
        $lab_data = Labs::where('id',$raise_test->lab_id)->first();
        $doctor_data = Doctors::where('id',$raise_test->doctor_id)->first();
        $data['raise_test']= $raise_test;
        $data['receipt_data']= $receipt_data;
        $data['patient']= $patient;
        $data['doctor']= $doctor;
        $data['lab_data'] = $lab_data;
        $data['doctor_data'] = $doctor_data;
/*echo "<pre>";
        print_r($data);die;*/
        if($type =='report'){
            return view('labtest.test_report',compact('data'));
        }else if($type == 'receipt'){
          return view('labtest.test_receipt',compact('data'));
        }

    }

    public function filedelete() {
        $file_name = 'Report_'.$_REQUEST['id'].'.pdf';
        Storage::disk('export')->delete($file_name);
        echo true;
    }
    /**
     * Send the Mail to Patient Email
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function sendreportmail(Request $request) { 
         
      $raise_test_id = $_POST['raise_test_id'];
      $raise_test = RaiseTest::find($raise_test_id);

      
      /*$raise_test_time = new Carbon($raise_test->updated_at);  
      //collection date
      $raise_test->collection_date = $raise_test_time->format('d F Y');
      //collection date

      $Receipt = Receipt::where('raise_test_id',$raise_test_id)->first();
      //report date
      $receipt_time = new Carbon($Receipt->updated_at);
      $raise_test->report_date = $receipt_time->format('d F Y');
      //report date
   
       //test data
       /* $receipt_data = Receipt::where('raise_test_id',$raise_test_id)->get();
       //testing date
        foreach ($receipt_data as $key => $value) {
              $value->test_name = getLabtestdetails($value->tests_id,'name');              
              $value->test_collection_name = LabsCategorydata($value->labtest_sub_name,'name');              
        }*/

      $patient_email = $raise_test->getPatient->email;
      $lab_email = $raise_test->getLab->email;
      $lab_name = $raise_test->getLab->name;
      $code = $patient_email .'_'.$raise_test_id;
      //Link Generation
      //need to encode the email and phone nuumber
      $encode = base64_encode($code);      
      $data['receipt_link'] = url('/').'/receipt/'.$encode;  
      $data['report_link'] = url('/').'/report/'.$encode;  

      $filepath = storage_path().'/app/public/export/';
      $file_name = 'Report_'.$raise_test_id.'.pdf';
      $file_name1 = 'Receipt_'.$raise_test_id.'.pdf';
      $data['testId'] =$raise_test_id;
      $mpdf = new \Mpdf\Mpdf();

     // if(!file_exists($filepath.$file_name)){
        //echo "no report";die;
         $mpdf->WriteHTML($this->generatePdf($raise_test_id, 'report'));
         $mpdf->Output($filepath.$file_name,\Mpdf\Output\Destination::FILE); 
     // }
     // if(!file_exists($filepath.$file_name1)){
        //echo "no receipt";die;
         $mpdf1 = new \Mpdf\Mpdf();
         $mpdf1->WriteHTML($this->generatePdf($raise_test_id, 'receipt'));
         $mpdf1->Output($filepath.$file_name1,\Mpdf\Output\Destination::FILE); 
     // }
      $data['receipt_link'] = url('/').'/receipt/'.$encode;   
      $data['report_link'] = url('/').'/report/'.$encode;  
      $data['pdf'] = $filepath.$file_name;
      $data['receipt'] = $filepath.$file_name1;
      $data['to'] = $patient_email; 
       $data['lab_to'] = $lab_email; 
      $data['subject'] = 'Report & Receipt To Patient - Order Id #'.$raise_test_id;        
      $data['lab_subject'] = $lab_name.' - Report & Receipt To Patient - Order Id #'.$raise_test_id; 
       // dd(substr($raise_test->getLab->mobile,3));
       //Mail::to($patient_email)->send(new ReportToPatient($data));     
      if($patient_email !=''){
        Mail::send('email.report_email',$data, function($message) use ($data)
            {
                $message->to($data['to']); 
                $message->attach($data['pdf']);
                $message->attach($data['receipt']);
                $message->subject($data['subject']);
            });

      }
      if($lab_email !=''){
        //Mail::to($patient_email)->send(new ReportToPatient($data));
        Mail::send('email.report',$data, function($message) use ($data)
            {
                $message->to($data['lab_to']); 
                $message->attach($data['pdf']);
                $message->attach($data['receipt']);
                $message->subject($data['lab_subject']);
            });

      }
     
     // Patient Notification
      if($raise_test->getPatient->phone !=''){
        $msg = 'Dowload your Receipt & Report for Order id #'.$raise_test_id.'using the below link %0a Receipt : '.$data['receipt_link'].'%0a %0a Report : '.$data['report_link'].'%0a %0a Thanks %0a Livigro';
         if(substr($raise_test->getPatient->phone,0,1) == '+'){
            send_sms(substr($raise_test->getPatient->phone,3), $msg);
          }else{
               send_sms($raise_test->getPatient->phone, $msg);
          }
      }

      // Lab Notification
      if($raise_test->getLab->mobile !=''){
        $msg = $lab_name.' - Dowload your Receipt & Report for Order id #'.$raise_test_id.'using the below link %0a Receipt : '.$data['receipt_link'].'%0a %0a Report : '.$data['report_link'].'%0a %0a Thanks %0a Livigro';
          if(substr($raise_test->getLab->mobile,0,1) == '+'){
            send_sms(substr($raise_test->getLab->mobile,3), $msg);
          }else{
               send_sms($raise_test->getLab->mobile, $msg);
          }
      }


      return back();
    }

    public function generatePatientView(Request $request,$id){     
      $data = explode('_',base64_decode($id));
      $email = $data[0];
      $raise_test_id = $data[1];
      $labtest = RaiseTest::find($raise_test_id);
      $file_name = 'Report_'.$raise_test_id.'.pdf';
      $mpdf = new \Mpdf\Mpdf();
      $mpdf->WriteHTML($this->generatePdf($raise_test_id, 'report'));
      //$mpdf->Output($file_path.$file_name,\Mpdf\Output\Destination::FILE);
      $mpdf->Output($file_name,'D');

      //return view('labtest.labtest_detail_new',compact('labtest'));
    }


    public function generateReceiptView(Request $request,$id){     
      $data = explode('_',base64_decode($id));
      $email = $data[0];
      $raise_test_id = $data[1];
      $labtest = RaiseTest::find($raise_test_id);
      $file_name = 'Receipt_'.$raise_test_id.'.pdf';
      $mpdf = new \Mpdf\Mpdf();
      $mpdf->WriteHTML($this->generatePdf($raise_test_id, 'receipt'));
      //$mpdf->Output($file_path.$file_name,\Mpdf\Output\Destination::FILE);
      $mpdf->Output($file_name,'D');

      //return view('labtest.labtest_detail_new',compact('labtest'));
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Labs');
            $labs = Labs::find($id);
            //$content->description();
            $content->body('<p>Last update at '.date('d-m-Y h:iA',strtotime($labs->updated_at)).'</p>');
            // $content->description('description');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Labs');
            // $content->description('description');

            $content->body($this->form());
        });
    }

    public function testShow($id){

        $labtest = RaiseTest::find($id);
        $labtests = RaiseTest::where('id',$id)->get();
        $raise_test_id = $id;
        if($labtests->count() ==0)
        {
          $success = new MessageBag([
                                'title'   => 'Raise Test',
                                'message' => 'Saved Successfully',
                            ]);
           return back()->with(compact('success'));
        }

         $patient = $labtest->getPatient;
         $receipt_data = array();
         $receipt_data_package = Receipt::where('raise_test_id',$raise_test_id)
                             ->with('testData')
                             ->get()->toArray();
        // echo "<pre>";print_r($receipt_data_package);die;             
        foreach ($receipt_data_package as $key => $value){
          if($value['test_type']!='' && $value['test_type'] != NULL){
            if($value['test_type'] == 'single'){
              $receipt_data['single'.$value['tests_id']] = $value;
              $receipt_data['single'.$value['tests_id']]['name'] = $value['test_data']['name'];
              $receipt_data['single'.$value['tests_id']]['test_name'] = $value['test_data']['name'];
            }else{
              if(isset($value['test_data']) && $value['test_data'] != ''){
                $packageDetails = Package::where('id',$value['test_type'])->first();
                $packageAmount = LabsAvaTest::where('lab_tests_id',$value['test_type'])->where('lab_id',$labtest->lab_id)->where('test_type','group')->first();
                $receipt_data[$value['test_type']]['id'] = $value['test_type'];
                $receipt_data[$value['test_type']]['test_type'] = $value['test_type'];
                $receipt_data[$value['test_type']]['packageName'] = $packageDetails->package_name;
                $receipt_data[$value['test_type']]['amount'] = (isset($packageAmount) && $packageAmount->amount != null && $packageAmount->amount!='')?$packageAmount->amount:$packageDetails->amount;
                foreach ($packageDetails->profile_id as $key => $val) {
                    $packageTestDetails = LabTestGroup::where('id',$val)->first()->toArray();
                    $receipt_data[$value['test_type']]['package'][$packageTestDetails['id']]['id'] = $packageTestDetails['id'];
                    $receipt_data[$value['test_type']]['package'][$packageTestDetails['id']]['groupName'] = $packageTestDetails['name'];
                     $receipt_data[$value['test_type']]['package'][$packageTestDetails['id']]['showName'] = $packageTestDetails['show_profile_name'];
                    foreach($packageTestDetails['tests_id'] as $k => $v){
                      $testDetails = Receipt::where('raise_test_id',$id)->where('tests_id',$v)->with('testData')->first();
                      $receipt_data[$value['test_type']]['package'][$packageTestDetails['id']]['testList'][$v] =  $testDetails;
                    }
                }
              }
            }
          }
        }   
        //$receipt_data['lab_id']=$labtest->lab_id;
       //echo "<pre>";print_r($receipt_data);die;
      //  dd($patient);
        if($labtests){
                 return Admin::content(function (Content $content) use($labtest,$patient,$receipt_data){


                    
                    $content->header('Lab Test Report Details');
                    ///$content->description('description');

                    $content->body(view('labtest.labtest_detail',compact('labtest','patient','receipt_data')));
                });
        }else{
            return Admin::content(function (Content $content) {

            $content->header('Lab Test Report');
            ///$content->description('description');

            $content->body("Nothing Found");
        });

        }
    }

/*
  
 */
    public function showNew($id, Request $request) {
        $lab = Labs::find($id);
        if($lab) {
             return Admin::content(function (Content $content) use($lab){
				//dd($lab);
                    $doctors = $lab->doctors()->get();
                    $raised_test =RaiseTest::where('lab_id',$lab->id)->get();
                    $completed_test = $lab->getTestStatus();
                    $working_time = WorkingTime::where('lab_id',$lab->id)->get();
                    // dd(); 
                    $paid_status = $lab->paid_status()->count(); 
                    $paid_amount = $lab->paid_status();
                    // dd($paid_amount);
                    $amount=array();
                    $amounts=0;
                    foreach ($paid_amount as $value) {
                      $amount[] = $value->original_amount;
                    }
                    foreach ($amount as $value) {
                      // dd($value);
                      $amounts += $value;
                    }
                    // dd($amounts);
                    $pending_test = $lab->pending_test()->count();
                    $cancel_test = $lab->cancel_test()->count();
                    // $completed_test = $raised_test->paid_status;
                    $raised_test_count = count(RaiseTest::where('lab_id',$lab->id)->get());
                    // $labs=$lab->test_assigneds();
                    // dd($labs);
                    //dd( $raised_test);
                    $city = City::where('city_name', $lab->city)->pluck('city_name')->first();
            $content->header('Labs Details');
            ///$content->description('description');
//dd($bg);
            $content->body(view('labs.labs_detail',compact('lab','doctors','raised_test','city','raised_test_count','paid_status','pending_test','cancel_test','amounts','completed_test','working_time')));
        });
        } else {
             return Admin::content(function (Content $content) {

            $content->header('Labs');
            ///$content->description('description');

            $content->body("Nothing Found");
        });
        }

    }
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Labs::class, function (Grid $grid) {
            $grid->model()->orderBy('id', 'desc');
           if(Admin::user()->status !=1){
              $grid->disableActions();
              $grid->disableCreateButton();
             
            }
            $grid->model()->check();
             $grid->actions(function ($actions) {
                  $actions->disableDelete();
                   $key = $actions->getKey();
                   $LabStatus = Labs::find($key);
                  $value = $LabStatus->status;
                  if($value !=2){
                          $actions->append("<a href=''><i class='fa fa-trash delete' id='$key'></i></a>");
                           }
                      });
         
                   
             $grid->tools(function ($tools) {
                 $tools->batch(function ($batch) {
                   $batch->disableDelete();
                });
               });
               if(!Auth::guard('admin')->user()->isRole('administrator')){
                $grid->disableCreateButton();
                $grid->disablePagination();
                $grid->disableFilter();
                $grid->disableExport();
                $grid->disableRowSelector();
                // $grid->disableActions();
             }
             // Neelamegam_30/07/2018
           // $grid->id('ID')->sortable();
            $grid->column('name','Name');
            $grid->mobile('Mobile')->display(function () {
                return "<a href='labs/$this->id/show'>$this->mobile</a>";
                });
            
            // $grid->city('City')->display(function() {
            // return  DB::table('statelist')->where('city_id', $this->city)
            //         ->pluck('city_name')->first();
            // });

             $grid->column('city','City');

             // Neelamegam_grid 27/07/2018

            $grid->column('promoters_name','Promoters name');
            $grid->column('description','Test type');
            $grid->address('Address');
            $grid->column('status','Status')->display(function ($title) {
                    return $this->status == '1' ? '<span class="btn btn-success btn-xs">Active</span>' : ($this->status == '0' ? '<span class="btn btn-primary btn-xs">InActive</span>': '<span class="btn btn-danger btn-xs">Deleted</span>');
                });
            $grid->filter(function($filter){

            // Remove the default id filter
            $filter->disableIdFilter();
            $filter->ilike('name', 'Name');
            $filter->ilike('promoters_name', 'Promoters name');
            $filter->ilike('description', 'Test type');
            $filter->ilike('getCityName.city_name', 'City');
            $filter->ilike('mobile', 'Mobile Number');

            });
             $grid->disableRowSelector(); 
            // Neelamegam_29/08/2018_CommonDelete
            Admin::script('
                $( document ).ready(function(){
                    $(".delete").click(function(){
                        var id = $(this).prop("id");
                        var url = "CommonDelete/" + id;
                        var fullurl = window.location.pathname.split("/"); 
                        var uri_segment = fullurl[2];

                        swal({
                          title: "DeleteConfirm",
                          type: "warning",
                          showCancelButton: true,
                          confirmButtonColor: "#DD6B55",
                          confirmButtonText: "Confirm",
                          closeOnConfirm: false,
                          cancelButtonText: "Cancel"
                        },
                        function(){
                            $.ajax({

                                method: "post",
                                url: url,
                                data: {
                                    id:id,
                                    _token:LA.token,
                                    uri_segment:uri_segment,
                                },
                                success: function (data) {
                                    $.pjax.reload("#pjax-container");

                                    if (typeof data === "object") {
                                        if (data.status) {
                                            swal(data.message, "", "success");
                                        } else {
                                            swal(data.message, "", "error");
                                        }
                                    }
                                }
                            });
                        });            
                    });
                });
            ');


            // $grid->created_at();
            // $grid->updated_at();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Labs::class, function (Form $form) {

            // $form->display('id', 'ID');
                $form->setId('labs_form');
            // $form->display('created_at', 'Created At');
            // $form->display('updated_at', 'Updated At');
            $form->model()->makeVisible('password');
            $form->tab('Basic', function ($form) {
                $form->text('name','Lab Name');
                $form->textarea('description','Description');
                 $form->text('promoters_name','Promoters Name');
                 $states2 = [
                        'off'  => ['value' => 0, 'text' => 'No', 'color' => 'danger'],
                        'on' => ['value' => 1, 'text' => 'Yes', 'color' => 'primary'],
                        ];
                $form->switch('home_collection','Home Collection')->states($states2);
                $form->text('mobile','Mobile')->rules('required');
                $form->mobile('phone','Work Phone');
                $form->email('email','Email');
                $form->text('livigro_name','Livigro Name');
               $states = [
                        'off'  => ['value' => 0, 'text' => 'Inactive', 'color' => 'danger'],
                        'on' => ['value' => 1, 'text' => 'Active', 'color' => 'success'],
                        ];
            $form->switch('status','Status')->states($states)->default('1');
            })->tab('GST Registered Details',function($form){
              $states1 = [
                        'off'  => ['value' => 0, 'text' => 'No', 'color' => 'danger'],
                        'on' => ['value' => 1, 'text' => 'Yes', 'color' => 'primary'],
                        ];
                $form->switch('gst_registered','GST Registered')->states($states1);

                $form->text('gst_number','GST Number');
                $form->multipleImage('gst_certificate_image','GST Certificate')->move(function($form) {
                    return 'Labs/'.$form->mobile;
                });
                $form->text('aadhar_id','Aadhaar No');
                $form->multipleImage('aadhar_image','Aadhaar Upload')->move(function($form) {
                    return 'Labs/'.$form->mobile;
                });
            })->tab('NABL Details',function($form) {
                $form->select('nabl_certified','NABL Accredited')->options(['test'=>'Test','test1'=>'Test1']);
                $form->text('nabl_id','NABL Certificate No');
                $form->multipleImage('nabl_certificate','NABL Certificate')->move(function($form) {
                    return 'Labs/'.$form->mobile;
                });;
            })->tab('Address',function($form){
                // $form->html( ' <div id="locationField">
                //                <input id="autocomplete" class="form-control" placeholder="Enter your address"
                //               onkeyup="geolocate()" type="text"></input>
                //             </div>'
                //             );
                $form->textarea('address','Address')->attribute(['id' => 'route']);
                $form->select('city','City')->options(City::all()->pluck('city_name','city_id'));

               // $form->text('city','City')->attribute(['id'=>'locality']);
                 $form->text('state','Area')->attribute(['id'=>'administrative_area_level_1']);
                 $form->hidden('lat')->attribute(['id'=>'lat']);
                 $form->hidden('lan')->attribute(['id'=>'lng']);
                // $form->text('lat');
                // $form->text('lon');
               // $form->map('lat', 'lon', 'Mapa')->useGoogleMap();
            })->tab('Password', function ($form) {

                $form->password('password');
                $form->password('password_confirmation');
                // ->rules('required')
                // ->default(function ($form) {
                //     return $form->model()->password;
                // });

            })->tab('Documents',function ($form) {
                $form->image('profile_image','Profile Image')->move(function($form) {
                    return 'Labs/'.encrypt($form->mobile);
                });
                $form->multipleImage('signature','Signature')->move(function($form) {
                    return 'Labs/'.$form->mobile;
                });
            })->tab('Labs Test',function($form) {
              $form->multipleSelect('tests')->options(LabsTest::where('status',1)->orderBy('name','asc')->pluck('name', 'id'));
              $form->multipleSelect('package')->options(Package::where('status',1)->orderBy('package_name','asc')->pluck('package_name', 'id'));
              $form->select('plans')->options(Plans::all()->pluck('plan_name', 'id'))->default(1);
            });
           $form->disableSubmit();
           $form->disableReset();
           
             $form->ignore(['password_confirmation']);

             $form->saving(function (Form $form) {

            if(Route::getCurrentRoute()->getName() == "labs.update"){

                 $labs = Labs::find($form->model()->id);
                  $labPhone = $labs->mobile;

                 $password = $labs->password;
                 // dd($patients);
                 if($form->password != $password){
                     // dd($patientPhone);
                     $title = "Livigro - Password Change";
                     $msg = "Hi, Your Password is Changed.Username :  {$labPhone} Password: {$form->password}";
                     $labPhone = $labs->mobile;
                     if(substr($labPhone,0,1) == '+'){
                          send_sms(substr($labPhone,3), $msg);
                        }else{
                          send_sms($labPhone, $msg);
                        }
                 }
                   // dd($password);
              } 

             });
                 // $form->saving(function(Form $form) {
                 //                $city = getCityName($form->city); // From Helper file

                 //                        $address = $form->address .' ' .$city. ' ' .$form->state;
                 //                        $prepAddr = str_replace(' ','+',$address);
                 //                        $geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&key='.env('GOOGLE_API_KEY'));
                 //                        $output= json_decode($geocode);
                                       
                 //                        $form->lat = $output->results[0]->geometry->location->lat;
                 //                        $form->lan = $output->results[0]->geometry->location->lng;
                 //                        });
             // $form->saving(function (Form $form) {
               
                  // $package[] = $form->package;   
                  // $form->ignore(['package']);             
              // });
               
              $form->saved(function (Form $form) {

                        $labs = Labs::find($form->model()->id);
                        $labs->mobile = "+91".$form->mobile;
                        $labs->save();

              $labAvl =LabsAvaTest::where('lab_id',$form->model()->id)->get(); 
                    if($labAvl){                     
                          foreach ($labAvl as $value){ 
                          if($value->lab_package_id == null)  {                        
                              $labTestsValue = LabsTest::find($value->lab_tests_id);
                              if($labTestsValue){                           
                                  $value->lab_id = $form->model()->id;
                                  $value->lab_tests_id = $labTestsValue->id;
                                  $value->amount = $labTestsValue->price;
                                  $value->range = $labTestsValue->reference_range;
                                  $value->range_end = $labTestsValue->reference_endrange;
                                  $value->status = $labTestsValue->status;
                                  $value->test_type ="single";
                                  $value->save();                           
                              }
                            }else{
                              $labPackageValue = Package::find($value->lab_package_id);
                              if($labPackageValue){                            
                                  $value->lab_id = $form->model()->id;                              
                                  $value->lab_tests_id = $value->lab_package_id;                            
                                  $value->amount = $labPackageValue->amount;                              
                                  $value->status = $labPackageValue->status;
                                  $value->test_type ="group";
                                  $value->save();
                              }
                            }
                      }                         
                  }
                      
                    $subscriptions = Subscription::where('user_id',$form->model()->id)
                                    ->where('user_type',1)
                                    ->first();
                    $plans = Plans::find($form->plans);
                    
                    if($plans){
                     $labs = Labs::find($form->model()->id);
                     // dd($plans->plan_period);
                     $created = $labs->created_at;
                     $plan_start = $labs->created_at;
                     $plan_end =  $created->addDays($plans->plan_period);
                     // dd($plan_end);
                    }
                    if(!$subscriptions){
                      $subscription = new Subscription;
                      $subscription->user_type = 1;
                      $subscription->user_id = $form->model()->id;
                      $subscription->plan_id = $form->plans;
                      $subscription->plan_start_date = $plan_start->format('d F Y');
                      $subscription->plan_end_date = $plan_end->format('d F Y');
                      $subscription->save();
                    }
                    $user_ = User::where('linked_id',$form->model()->id)
                                ->where('user_type_id',1)
                                ->first();
                    if($form->status == 'on'){
                      $status = 1;
                    }else{
                      $status = 0;
                    }
                    if($user_)
                        $user = User::find($user_->id);
                    else
                    $user = new User;
                    $user->password = bcrypt($form->password);
                    $user->name = $form->name;
                    $user->username = "+91".$form->mobile;
                    $user->avatar = $form->model()->profile_image;
                    $user->user_type_id = 1; // For Lab
                    $user->linked_id = $form->model()->id;
                    $user->status = $status;
                    $user->save();
                    //For Roles
                   if(!$user_)
                        DB::table('admin_role_users')->insert(['role_id'=>3,'user_id'=>$user->id]); // For Lab Roles

                  
                });
               $form->disableReset();
               // Neelamegam_02/08/2018
            // To getphone function
            // For Mobile Number Unique Valiadtion

	                Admin::script('
	                 $( document ).ready(function(){  
                   var fullurl = window.location.pathname.split("/"); 
                        var uri_segment = fullurl[4];
                        var phone_no = $(".mobile").val();
                        if(uri_segment == "edit"){
                           var str_phone = phone_no.substring(3);
                           // alert(str_phone);
                           $(".mobile").val(str_phone);
                        }

                   var mobile_icon = $("#mobile").closest(".form-group").children(".col-sm-8").children(".input-group").find(".input-group-addon");
                    var mobile_icons = $("#mobile").closest(".form-group").children(".col-sm-8").children(".input-group").children(".input-group-addon").find("i");
                   // alert(mobile_icons);
                   var new_icon = "<span><i>+91</i></span>";
                    mobile_icons.removeClass("fa fa-pencil");
                   mobile_icons.append(new_icon);                
	                   
	                     
	                     $("#mobile").keypress(function(){
	                		var firstVal = $("#mobile").val().charAt(0);
	                		var numbericCheck = $("#mobile").val();

	                		if(!$.isNumeric(numbericCheck)){
	                			var error="<small id=small class=help-block>Mobile number must contain only numeric</small>";
		                        $("#small").remove();                          
		                        $("#mobile").closest(".form-group").addClass("has-error");
		                        $("#mobile").closest(".col-sm-8").append(error);
		                        $("#mobile").closest(".nav-tabs-custom").children("ul.nav-tabs").children("li.active").addClass("bv-tab-error").removeClass("bv-tab-sucess");
	                		}else{
	                			 $("#small").remove();         
	                        	$("#mobile").closest(".form-group").removeClass("has-error").addClass("has-success");
	                             $("#mobile").closest(".nav-tabs-custom").children("ul.nav-tabs").children("li.active").removeClass("bv-tab-error").addClass("bv-tab-sucess");
	                        }

	                		});

	                    $("#mobile").blur(function(){
	                     $("#small").remove();    
	                    $("#mobile").closest(".form-group").addClass("has-error");
	                    var phone_number = $("#mobile").val();
	                    var firstVal = $("#mobile").val().charAt(0);
	                    var numbericCheck = $("#mobile").val();	         
	                    if(phone_number==""){
	                     var error="<small id=small class=help-block>The Mobile Number is Required</small>";
	                      $("#small").remove();
	                       $("#mobile").closest(".nav-tabs-custom").children("ul.nav-tabs").children("li.active").addClass("bv-tab-error").removeClass("bv-tab-sucess");            
	                      $("#mobile").closest(".form-group").addClass("has-error");
	                    $("#mobile").closest(".col-sm-8").append(error);
	                    }else if((phone_number!="")&&(!$.isNumeric(numbericCheck))){
	                    	var error="<small id=small class=help-block>Mobile number must contain only numeric</small>";
	                      $("#small").remove();
	                       $("#mobile").closest(".nav-tabs-custom").children("ul.nav-tabs").children("li.active").addClass("bv-tab-error").removeClass("bv-tab-sucess");            
	                      $("#mobile").closest(".form-group").addClass("has-error");
	                    $("#mobile").closest(".col-sm-8").append(error);
	                    }else{

	                        var url = "phonevalidation";                 
	                        var token = $("input[name=_token]").val();
	                        var fullurl = window.location.pathname.split("/"); 
	                        var uri_segment = fullurl[3];                       
	                       
	                            $.ajax({
	                                type:"post",
                                   async: true,
	                                dataType: "json",
	                                url: url,                       
	                                data: {
	                                    _token: token,
	                                    phone_number: phone_number,
	                                    uri_segment : uri_segment,                                   
	                                },                        
	                                success: function( responses ){
	                                 
	                                    if(responses.phone_id == 0){                                          
	                                        var error="<small id=small class=help-block>  The Mobile Number is Already Exists</small>";
	                                        $("#small").remove();                                       
	                                        $("#mobile").closest(".form-group").addClass("has-error");
	                                        $("#mobile").closest(".col-sm-8").append(error);
                                          $("#mobile").focus();
                                        /* $("li.next a").click(function(e){
                                          //e.preventDefault();
                                           //return false;
                                           });*/
	                                    } 
                                      
                                      //if(responses.phone_id ==1){
                                     /* else{  
                                         $("li.next a").click(function(){    
                                          alert("Hi"); 
                                           return false;
                                           });
                                      }*/
                                       // $("li.next a").click(function(){ 
                                       //     return true;
                                       //     });
                                       // return true;
                                       
	                                },
	                                complete: function(data) {
	                                  
	                                   if(data.responseJSON.message == "Mobile Number Already Exist")
	                                   {
	                                     $("#mobile").closest(".form-group").addClass("has-error");
	                                     $("#mobile").closest(".nav-tabs-custom").children("ul.nav-tabs").children("li.active").addClass("bv-tab-error").removeClass("bv-tab-sucess");
	                                   }
	                                   else
	                                   {
	                                    $("#mobile").closest(".form-group").removeClass("has-error").addClass("has-success");
	                                    $("#mobile").closest(".nav-tabs-custom").children("ul.nav-tabs").children("li.active").removeClass("bv-tab-error").addClass("bv-tab-sucess");
                                       
	                                   }
	                                    
	                                 },
	                            });
	                       
	                        }
	                   
	                    });
	                });
	            ');

        });
    }
     // Neelamegam_02/08/2018
    // To getphone function
    // For Mobile Number Unique Valiadtion
     public function getphone(Request $request)
    {   
      $mobiles = "+91".$request->phone_number;
        if($request->uri_segment == 'create'){
        $mobile=DB::table('labs_table')->where('mobile',$mobiles)->get();  
      }else{        
        $mobile=DB::table('labs_table')
                  ->where('mobile',$mobiles)
                  ->where('id','!=' ,$request->uri_segment)                  
                  ->get();  
      }

        $mobile_count=count($mobile);   
         // dd($mobile_count);     
        if($mobile_count!=0){
             $responses = array(
                'phone_id' => '0',
                'message' =>'Mobile Number Already Exist',                
                );
            
        } 
        if($mobile_count ==0){
             $responses = array(
                'phone_id' => '1',
                'message' =>'New User',                
                );
            
        }
      return response()->json($responses);
    }
}
