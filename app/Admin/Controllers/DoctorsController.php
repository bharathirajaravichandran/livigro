<?php

namespace App\Admin\Controllers;

use App\Models\Doctors;
use App\Models\Hospital;
use App\Models\Labs;
use App\Models\Patients;
use App\User;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Form\Tab;
use Encore\Admin\Widgets\Table;
use Illuminate\Support\Facades\Route;
// use Illuminate\Support\Facades\Request;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use App\Models\City;
use App\Models\States;
use App\Models\Blood;
use App\Models\Speciality;
use App\Models\Plans;
use App\Models\Subscription;

class DoctorsController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
 
    
    public function alldoctors(){
        $doctors = Doctors::all();
        return response()->json(['data'=> $doctors],200);
    }

    public function index()
    {
        return Admin::content(function (Content $content) {
            if(Auth::guard('admin')->user()->isRole('administrator')){
                    $content->header('Doctors');
                }
            //$content->description('description');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Doctors');
            $doctor = Doctors::find($id);
            $content->body('<p>Last update at '.date('d-m-Y h:iA',strtotime($doctor->updated_at)).'</p>');
            // $content->description('de');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Doctors');
            ///$content->description('description');

            $content->body($this->form());
        });
    }

    public function showNew($id, Request $request) {
        $doctor = Doctors::find($id);
        if($doctor) {
             return Admin::content(function (Content $content) use($doctor){
				
				//~ $dd=Doctors::pluck('blood_group');
                $patients = $doctor->patients()->get();
            
                $hospitals = $doctor->hospitals()->get();
                $labs = $doctor->labs()->get();
                $age = $doctor->age();                
                $raised_test = $doctor->raised_test()->get();
                $bg = Blood::where('blood_group', $doctor->blood_group)->pluck('blood_group')->first();
                $sp=Speciality::where('id', $doctor->specialization)->pluck('name')->first();
                //~ $bg = DB::table('doctors_details')->get();
                //~ $bg = DB::table('blood_group')   
				//~ ->join('doctors_details','doctors_details.blood_group','=','blood_group.id') 
				//~ ->where('doctor_details.id',$doctor)
				//~ ->get();
				//~ dd($labs);
				
            $content->header('Doctor Details');
            ///$content->description('description');
            

            $content->body(view('doctor.doctors_detail',compact('doctor','patients','hospitals','labs','raised_test','bg','sp','age')));
        });
        }
        else{
             return Admin::content(function (Content $content) {

            $content->header('Doctors');
            ///$content->description('description');

            $content->body("Nothing Found");
        });
        }

    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Doctors::class, function (Grid $grid) {
        
            $grid->model()->check();
            //$grid->disableActions();
            // $grid->disableFilter();
           $grid->model()->orderBy('id', 'desc');
            if(Admin::user()->status !=1){
              $grid->disableActions();
              $grid->disableCreateButton();
             
            }

             $grid->actions(function ($actions) {
                    $actions->disableDelete();
                    $key = $actions->getKey();
                    $DoctorStatus = Doctors::find($key);
                    $value = $DoctorStatus->status;
                    if($value !=2){
                            $actions->append("<a href=''><i class='fa fa-trash delete' id='$key'></i></a>");
                             }
                        });
           
             $grid->tools(function ($tools) {
                 $tools->batch(function ($batch) {
                   $batch->disableDelete();
                });
            });
             $grid->disableRowSelector();
             if(!Auth::guard('admin')->user()->isRole('administrator')){
                $grid->disableCreateButton();
                $grid->disablePagination();
                $grid->disableFilter();
                $grid->disableExport();
                $grid->disableRowSelector();
               // $grid->disableActions();
             }

           // $grid->id('ID')->sortable();
            $grid->column('name','Name')->display(function () {
                return "<a href='doctors/$this->id/show'>$this->salutation $this->name</a>";
            });
            $grid->profile_image('Profile Image')->image();
            $grid->specialization('Specialist')->display(function() {
            return  DB::table('spceiality')->where('id', $this->specialization)
                    ->pluck('name')->first();
            });
           
            // Neelamegam_grid 30/07/2018
            $grid->column('Hospital')->display(function() {
                // dd( $hospitals_name);
            $hospitals_name= DB::table('hospitals')
                    ->join('hospital_doctor','hospital_doctor.hospital_id','=','hospitals.id')
                    ->where('hospital_doctor.doctor_id',$this->id)
                    ->pluck('hospitals.name');
                    // dd( $hospitals_name);
                    $datas=array();
                    foreach ($hospitals_name as $key => $value) {
                        $datas[] = $value;
                    }
                    $hospitals = implode(',', $datas);                  
           return $hospitals;
            });
            $grid->blood_group('Blood Group');
            $grid->column('mobile','Mobile');
            $grid->column('address','Address');
            $grid->column('city','City');
            $grid->column('status','Status')->display(function ($title) {
                    return $this->status == '1' ? '<span class="btn btn-success btn-xs">Active</span>' : ($this->status == '0' ? '<span class="btn btn-primary btn-xs">InActive</span>': '<span class="btn btn-danger btn-xs">Deleted</span>');
                });

            $grid->filter(function($filter){

            // Remove the default id filter
            $filter->disableIdFilter();
            $filter->ilike('name', 'Doctor Name');
            $filter->ilike('getHospitals.name', 'Hospital Name');
            $filter->ilike('getCityName.city_name', 'City');
            $filter->ilike('mobile', 'Mobile Number');

            });
             $grid->disableRowSelector(); 
            // Neelamegam_29/08/2018_CommonDelete
             Admin::script('
                $( document ).ready(function(){
                    $(".delete").click(function(){
                        var id = $(this).prop("id");
                        var url = "CommonDelete/" + id;
                        var fullurl = window.location.pathname.split("/"); 
                        var uri_segment = fullurl[2];

                        swal({
                          title: "DeleteConfirm",
                          type: "warning",
                          showCancelButton: true,
                          confirmButtonColor: "#DD6B55",
                          confirmButtonText: "Confirm",
                          closeOnConfirm: false,
                          cancelButtonText: "Cancel"
                        },
                        function(){
                            $.ajax({

                                method: "post",
                                url: url,
                                data: {
                                    id:id,
                                    _token:LA.token,
                                    uri_segment:uri_segment,
                                },
                                success: function (data) {
                                    $.pjax.reload("#pjax-container");

                                    if (typeof data === "object") {
                                        if (data.status) {
                                            swal(data.message, "", "success");
                                        } else {
                                            swal(data.message, "", "error");
                                        }
                                    }
                                }
                            });
                        });            
                    });
                });
            ');

            // $grid->created_at();
            // $grid->updated_at();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Doctors::class, function (Form $form) {

         $form->model()->makeVisible('password');
         $form->setId('doctors_form');
        // $form->setAttribute('id','doctors_form');
          $form->tab('Basic', function ($form) {

              
                $form->select('salutation', 'Salutation')->options(['Mr'=>'Mr','Mrs'=>'Mrs','Miss'=>'Miss']);
                $form->text('name','Name');
                $form->email('email','Email');
                $form->text('mobile','Mobile')->rules('required');
                $form->date('dob','Date Of Birth');
                $form->select('gender', 'Gender')->options([0=>'Female',1=>'Male',2=>'Non Binary']);
                // $form->select('blood_group','Blood Group')->options(['A1+ve'=>'A1+ve','A+ve'=>'A+ve','A1-ve'=>'A1-ve','B+ve'=>'B+ve','B-ve'=>"B-ve",'B1+ve'=>'B1+ve','B1-ve'=>'B1-ve','AB-ve'=>'AB-ve','AB+ve'=>'AB-ve','O+ve'=>'O+ve','O-ve'=>'O-ve']);
                // $form->select('blood_group','Blood Group')->options(Blood::all()->pluck('blood_group','id'));
                $blood[0]="Please Select Blood Group";
               
                $bloods = Blood::orderBy('id','asc')->get()->pluck('blood_group','id');
                foreach ($bloods as $key => $value) {
                        $blood[$key] = $value;
                }
                $form->select('blood_group','Blood Group')->options($blood);
                $states = [
                        'off'  => ['value' => 0, 'text' => 'Inactive', 'color' => 'danger'],
                        'on' => ['value' => 1, 'text' => 'Active', 'color' => 'success'],
                        ];
            $form->switch('status','Status')->states($states)->default('1');
                // $form->select('status', 'Status')->options(['0'=>'InActive','1'=>"Active"]);
                
            })->tab('Other Details', function ($form) {
                $form->textarea('mcid','MCID');
                $form->select('register_council','Registration Details')->options(['state'=>'state','center'=>'center','tamilnadu'=>'tamilnadu']);
                // $form->select('specialization','Specialization')->options(['Heart'=>'Heart','eye'=>'Eye','Ear'=>'Ear']);
                $form->select('specialization','Speciality')->options(Speciality::all()->pluck('name','id'));
               // $form->select('educational_qualification','Educational Qualification')->options(['MBBS'=>'MBBS','MD'=>'MD','MS'=>'MS']);
                // $form->select('certificate_course','Certificate Course')
                //             ->options(['Master Class Diabetics'=>'Master Class Diabetics',
                //                         'Master Class Heart' => 'Master Class Heart',
                //                     ]);
               // $form->multipleImage('educational_certificate','Educational Certificate');
                $form->text('yearofexperience','Year Of Experience');
                $form->text('aadhar_no','Aadhaar No');
                // $form->text('availability_day','Availablity Day');
                // $form->text('availability_timing','Availablity Timing');

            })->tab('Address',function($form){
                // $form->html( ' <div id="locationField">
                //                <input id="autocomplete" class="form-control" placeholder="Enter your address"
                //               onkeyup="geolocate4()" type="text"></input>
                //             </div>'
                //             );
                $form->textarea('address','Address')->attribute(['id' => 'route']);
                // $form->select('state')->options(States::all()->pluck('name','id'));
                // $form->select('city');
                $form->select('city','City')->options(City::all()->pluck('city_name','city_id'));
                 $form->text('state','Area')->attribute(['id'=>'administrative_area_level_1']);
                 $form->hidden('lat')->attribute(['id'=>'lat']);
                 $form->hidden('lng')->attribute(['id'=>'lng']);
                // $form->text('lat');
                // $form->text('lon');
            })->tab('Password', function ($form) {

                $form->password('password');
                $form->password('password_confirmation');
// ->rules('mimes:jpeg,png,bmp,tiff |max:4096')
            })->tab('Documents',function ($form) {
                $form->image('profile_image','Profile Image')->uniqueName()->move(function($form) {
                    return 'Doctors/'.$form->mobile;
                });
                $form->image('Signature','Signature')->uniqueName()->move(function($form) {
                    return 'Doctors/'.$form->mobile;
                });
                $form->multipleImage('aadhar_image','Aadhaar Upload')->rules('mimes:jpeg,png,bmp,tiff |max:4096')->uniqueName()->move(function($form) {
                    return 'Doctors/'.$form->mobile;
                });
            })->tab('RelatedList',function($form) {
             $form->multipleSelect('hospitals')->options(Hospital::all()->pluck('name', 'id'));
            $form->multipleSelect('labs')->options(Labs::all()->where('status','1')->pluck('name', 'id'));
            $form->multipleSelect('patients')->options(Patients::all()->where('status','1')->pluck('name', 'id'));
             $form->select('plans')->options(Plans::all()->pluck('plan_name', 'id'))->default(1);
          


            });
            $form->disableSubmit();
            $form->disableReset();
            // $tab = $form->getTab();
            // $tabs = $tab->getTabs();
            // if($tabs[4]['active'])
               // dd($tabs);die('bala');
             $form->ignore(['password_confirmation']);
               // $form->saving(function(Form $form) {
               //                  $city = getCityName($form->city); // From Helper file
               //                          $address = $form->address .' ' .$city. ' ' .$form->state;
               //                          $prepAddr = str_replace(' ','+',$address);
               //                          $geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&key='.env('GOOGLE_API_KEY'));
               //                          $output= json_decode($geocode);

               //                          $form->lat = $output->results[0]->geometry->location->lat;
               //                          $form->lng = $output->results[0]->geometry->location->lng;
               //                          });

          $form->saving(function (Form $form) {
           
                // if(Route::getCurrentRoute()->getName() == "doctors.update"){
                //     // dd($form->blood_group);
                //     $doctor = Doctors::find($form->model()->id);
                //     if($form->blood_group ==0){
                //     $doctor->blood_group = null;
                //     }
                //     $doctor->save();
                // }else{
                // if($form->blood_group ==0){
                //     $newdoctor = new Doctors;
                //     $newdoctor->blood_group=null;
                //     $newdoctor->save();
                //   }    
                // }
                
            if(Route::getCurrentRoute()->getName() == "doctors.update"){

                 $labs = Doctors::find($form->model()->id);
                  $labPhone = $labs->mobile;

                 $password = $labs->password;
                 // dd($patients);
                 if($form->password != $password){
                     // dd($patientPhone);
                     $title = "Livigro - Password Change";
                     $msg = "Hi, Your Password is Changed.User Name: {$labs->mobile} Password: {$form->password}.";
                     $labPhone = $labs->mobile;
                     if(substr($labPhone,0,1) == '+'){
                          send_sms(substr($labPhone,3), $msg);
                        }else{
                          send_sms($labPhone, $msg);
                        }
                 }
                   // dd($password);
              } 

             });
             $form->saved(function (Form $form) {
                // dd($form);
                 $doctor = Doctors::find($form->model()->id);
                  $doctor->mobile = "+91".$form->mobile;
                 if($doctor){
                 if(Route::getCurrentRoute()->getName() == "doctors.update"){
                    if($form->blood_group ==0){
                      $doctor->blood_group = null;
                     
                    }
                 }else{
                     if($doctor->blood_group ==0 || $doctor->blood_group ==null){
                         $doctor->blood_group = null;
                     }
                 }
                   $doctor->save();
                 }
              
                 $subscriptions = Subscription::where('user_id',$form->model()->id)
                                    ->where('user_type',2)
                                    ->first();
                    $plans = Plans::find($form->plans);
                    
                    if($plans){
                     $labs = Doctors::find($form->model()->id);
                     // dd($plans->plan_period);
                     $created = $labs->created_at;
                     $plan_start = $labs->created_at;
                     $plan_end =  $created->addDays($plans->plan_period);
                     // dd($plan_end);
                    }
                    if(!$subscriptions){
                      $subscription = new Subscription;
                      $subscription->user_type = 2;
                      $subscription->user_id = $form->model()->id;
                      $subscription->plan_id = $form->plans;
                      $subscription->plan_start_date = $plan_start->format('d F Y');
                      $subscription->plan_end_date = $plan_end->format('d F Y');
                      $subscription->save();
                    }
                    $user_ = User::where('linked_id',$form->model()->id)
                                ->where('user_type_id',2)
                                ->first();
                    if($form->status == 'on'){
                    $status = '1';
                    }else{
                     $status = '0';
                   }
                    if($user_)
                        $user = User::find($user_->id);
                    else
                    $user = new User;
                    $user->password = bcrypt($form->password);
                    $user->name = $form->name;
                    $user->username = "+91".$form->mobile;
                    $user->avatar = $form->model()->profile_image;
                    $user->user_type_id = 2; // For Doctor
                    $user->linked_id = $form->model()->id;
                    $user->status = $status;
                   $user->save();
                    //For Roles
                   if(!$user_)
                        DB::table('admin_role_users')->insert(['role_id'=>2,'user_id'=>$user->id]);
                         // For Doctor Roles

                });

             // Neelamegam_02/08/2018
             // To getphone function
             // For Mobile Number Unique Valiadtion 
           
            // If it's the last tab then hide the last button and show the finish instead
            //   var $rootwizard= $(".box-body");
            // $next = $rootwizard.find(".pager .next").addClass("disabled");
          // foreach ($variable as $key => $value) {
          //   # code...
          // }

             Admin::script('
                $( document ).ready(function(){
                    var fullurl = window.location.pathname.split("/"); 
                        var uri_segment = fullurl[4];
                       
                        var phone_no = $(".mobile").val();
                        if(uri_segment == "edit"){
                           var str_phone = phone_no.substring(3);
                           // alert(str_phone);
                           $(".mobile").val(str_phone);
                          
                            var doctor_id = fullurl[3];
                            // alert(doctor_id);
                           var url = "blood";  
                           var urls = "city";          
                           var token = $("input[name=_token]").val();                          
                           $.ajax({
                            type:"post",
                            dataType: "json",
                            url: url,                       
                              data: {
                                  _token: token,
                                  doctor_id: doctor_id,
                              },                        
                              success: function( datas ){
                                console.log(datas);
                                  if(datas)
                                   // $(".blood_group option[value=datas]").attr("selected",true);
                                  $(".blood_group").val(datas).select2({"allowClear":true,"placeholder":""});

                              },
                             });
                                        
                           $.ajax({
                            type:"post",
                            dataType: "json",
                            url: urls,                       
                              data: {
                                  _token: token,
                                  doctor_id: doctor_id,
                              },                        
                              success: function( datas ){
                                console.log(datas);
                                  if(datas)
                                   // $(".blood_group option[value=datas]").attr("selected",true);
                                  $(".city").val(datas).select2({"allowClear":true,"placeholder":""});

                              },
                             });
                     
                        }

                   var mobile_icon = $("#mobile").closest(".form-group").children(".col-sm-8").children(".input-group").find(".input-group-addon");
                    var mobile_icons = $("#mobile").closest(".form-group").children(".col-sm-8").children(".input-group").children(".input-group-addon").find("i");
                   // alert(mobile_icons);
                   var new_icon = "<span><i>+91</i></span>";
                    mobile_icons.removeClass("fa fa-pencil");
                   mobile_icons.append(new_icon);                
                     $(".state").change(function(){
                        var state = $(".state").val();
                        $("select[name=state] option[value =6]").attr("selected","selected");
                        $(".state option[value=state]").attr("selected","selected");
                        var token = $("input[name=_token]").val();
                        var url = "cities";
                        if(state){
                        $.ajax({
                            type:"post",
                            dataType: "json",
                            url: url,                       
                            data: {
                                _token: token,
                                state: state,
                            },                        
                            success: function( datas ){
                               
                                if(datas)
                                 $(".city").html(datas);
                                 else
                                 $(".city").empty();
                            },
                        });
                        }else{
                            $(".city").empty();
                        }
                    });

                	$("#mobile").keypress(function(){
                		var firstVal = $("#mobile").val().charAt(0);
	                	var numbericCheck = $("#mobile").val();
                		if(!$.isNumeric(numbericCheck)){
	                			var error="<small id=small class=help-block>Mobile number must contain only numeric</small>";
		                        $("#small").remove();                          
		                        $("#mobile").closest(".form-group").addClass("has-error");
		                        $("#mobile").closest(".col-sm-8").append(error);
		                        $("#mobile").closest(".nav-tabs-custom").children("ul.nav-tabs").children("li.active").addClass("bv-tab-error").removeClass("bv-tab-sucess");
	                		}else{
	                			 $("#small").remove();         
	                        	$("#mobile").closest(".form-group").removeClass("has-error").addClass("has-success");
	                             $("#mobile").closest(".nav-tabs-custom").children("ul.nav-tabs").children("li.active").removeClass("bv-tab-error").addClass("bv-tab-sucess");
	                        }
                		});
                    $("#mobile").blur(function(){
                    var firstVal = $("#mobile").val().charAt(0);
	                var numbericCheck = $("#mobile").val();	
                     $("#small").remove();    
                    $("#mobile").closest(".form-group").addClass("has-error");
                    var phone_number = $("#mobile").val();
                    if(phone_number==""){
                      $("#small").remove();
                       $("#mobile").closest(".nav-tabs-custom").children("ul.nav-tabs").children("li.active").addClass("bv-tab-error").removeClass("bv-tab-sucess");            
                      $("#mobile").closest(".form-group").addClass("has-error");
                    $("#mobile").closest(".col-sm-8").append(error);
                    }else if((phone_number!="")&&(!$.isNumeric(numbericCheck))){
	                    	var error="<small id=small class=help-block>Mobile number must contain only numeric</small>";
	                      $("#small").remove();
	                       $("#mobile").closest(".nav-tabs-custom").children("ul.nav-tabs").children("li.active").addClass("bv-tab-error").removeClass("bv-tab-sucess");            
	                      $("#mobile").closest(".form-group").addClass("has-error");
	                    $("#mobile").closest(".col-sm-8").append(error);
	                 }else{
                        var url = "phonevalidation";
                        var token = $("input[name=_token]").val();
                        var fullurl = window.location.pathname.split("/"); 
                        var uri_segment = fullurl[3];                       
                       
                            $.ajax({
                                type:"post",
                                dataType: "json",
                                url: url,                       
                                data: {
                                    _token: token,
                                    phone_number: phone_number,
                                     uri_segment : uri_segment,
                                },                        
                                success: function( responses ){
                                    
                                    if(responses.phone_id == 0){                                          
                                        var error="<small id=small class=help-block>  The Mobile Number is Already Exists</small>";
                                        $("#small").remove();                                       
                                        $("#mobile").closest(".form-group").addClass("has-error");
                                        $("#mobile").closest(".col-sm-8").append(error);

                                    }
                                },
                                complete: function(data) {
                                   
                                   if(data.responseJSON.message == "Mobile Number Already Exist")
                                   {
                                     $("#mobile").closest(".form-group").addClass("has-error");
                                     $("#mobile").closest(".nav-tabs-custom").children("ul.nav-tabs").children("li.active").addClass("bv-tab-error").removeClass("bv-tab-sucess");

                                   }
                                   else
                                   {
                                    $("#mobile").closest(".form-group").removeClass("has-error").addClass("has-success");
                                    $("#mobile").closest(".nav-tabs-custom").children("ul.nav-tabs").children("li.active").removeClass("bv-tab-error").addClass("bv-tab-sucess");
                                   }
                                    
                                 },
                            });
                       
                        }
                   
                    });
                });
            ');

        });
    }

    // Neelamegam_02/08/2018
    // To getphone function
    // For Mobile Number Unique Valiadtion
     public function getblood(Request $request)
    {  

      $blood=DB::table('doctors_details')
                  ->where('id',$request->doctor_id)                  
                  ->pluck('blood_group')->first();
     
    return $blood;
                  // dd($blood->blood_group);
    }
    public function getcity(Request $request)
    {  

      $city=DB::table('doctors_details')
                  ->where('id',$request->doctor_id)                  
                  ->pluck('city')->first();
     
    return $city;
                  // dd($blood->blood_group);
    }
    public function getphone(Request $request)
    {     
        // dd($request->uri_segment);
       $mobiles = "+91".$request->phone_number;
     if($request->uri_segment == 'create'){
        $mobile=DB::table('doctors_details')->where('mobile',$mobiles)->get();  
      }else{        
        $mobile=DB::table('doctors_details')
                  ->where('mobile',$mobiles)
                  ->where('id','!=' ,$request->uri_segment)                  
                  ->get();  
      }

        $mobile_count=count($mobile);        
       
        if($mobile_count!=0){
             $responses = array(
                'phone_id' => '0',
                'message' =>'Mobile Number Already Exist',                
                );
            return response()->json($responses);
        }
        else{
             $responses = array(
                'phone_id' => '1',
                'message' =>'New User',                
                );
            return response()->json($responses);
        }

    }
}
