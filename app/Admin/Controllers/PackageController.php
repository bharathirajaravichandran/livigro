<?php

namespace App\Admin\Controllers;

use App\Models\Package;
use App\Models\LabTestGroup;
use App\Models\LabsTest;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class PackageController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Package');
            // $content->description('description');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Package');
            // $content->description('description');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Package');
            // $content->description('description');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Package::class, function (Grid $grid) {

            $grid->model()->orderBy('id', 'desc');
            $grid->actions(function ($actions) {
                    $actions->disableDelete();
                   
                        });
           
                   
             $grid->tools(function ($tools) {
                 $tools->batch(function ($batch) {
                   $batch->disableDelete();
                });
             });
              $grid->disableRowSelector();
            $grid->id('ID')->sortable();
            $grid->package_name('Package Name');
            $grid->description('Description')->limit(20);

             $grid->column('Test Name')->display(function () {
                $name = '';
                $testname = $this->tests_id;
                if( $testname){
                foreach($testname as $id) {
                    $late=LabsTest::find($id);
                    if($late!=null)
                    $name .= LabsTest::find($id)->name .',';
                }
                return str_replace(',', ', ', $name);
                 $test_name = $this->tests()->pluck('name')->toArray();
                 return implode(',',$test_name);
                }else{
                    return $name="";
                }
              });

            $grid->column('Profile Name')->display(function () {
                $profileName ='';
                if($this->profile_id){
                foreach($this->profile_id as $id) {
               $profileName .= LabTestGroup::find($id)->name.',';
                     }
                
                if($profileName)
                    return str_replace(',', ', ', $profileName);
                }
            });
            $grid->amount('Amount Name');
            $grid->column('price_edit_status','Price Status')->display(function ($title) {
                    return $this->price_edit_status == '0' ? '<span class="btn btn-success btn-xs">Editable</span>' : '<span class="btn btn-danger btn-xs">Non-Editable</span>';
            });
          $grid->column('Test count')->display(function () { 
           $testname = $this->tests_id; 
           if($testname != null){
            return count($testname);
                }else{
                    return $testname="";
                }
           // $testount = '';
          
                        
          });
            $grid->column('status','Status')->display(function ($title) {
                    return $this->status == '0' ? '<span class="btn btn-primary btn-xs">Inactive</span>' : '<span class="btn btn-success btn-xs">Acive</span>';
            });

            // $grid->created_at();
            // $grid->updated_at();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Package::class, function (Form $form) {

            $form->display('id', 'ID');
             $form->text('package_name', 'Package Name ')->rules('required');
             $form->textarea('description', 'Package Description ');
            // $form->select('test_id','Blood Group')->options(LabTestGroup::all()->pluck('blood_group','id'));
            $form->multipleSelect('profile_id','Profile Group')->options(LabTestGroup::all()->where('status',1)->pluck('name','id'))->rules('required');
             $states1 = [
                        'off'  => ['value' => 1, 'text' => 'Non-Edit', 'color' => 'danger'],
                        'on' => ['value' => 0, 'text' => 'Editable', 'color' => 'success'],
                        ];
                $form->switch('price_edit_status','Edit Price')->states($states1)->default('0');
            // $form->select('profile_id','Profile Group')->options(LabTestGroup::all()->pluck('name','id'));
             $form->text('amount', 'Price')->rules('required');
             $form->text('category', 'Category');
             $form->text('code', 'Code');

            

              $states = [
                        'off'  => ['value' => 0, 'text' => 'Inactive', 'color' => 'danger'],
                        'on' => ['value' => 1, 'text' => 'Active', 'color' => 'success'],
                        ];
              $form->switch('status','Status')->states($states)->default('1');

            $form->saved(function(Form $form){
                $testId = array();

                foreach ($form->profile_id as $value) {
                     $test = LabTestGroup::find($value);
                     if($test)
                        $testId[]=$test->tests_id;
                }
                
                $result = array(); 
                  foreach ($testId as $key => $value) { 
                   
                    if (is_array($value)) { 
                      $result = array_merge($result, array_flatten($value)); 

                    } else { 
                      $result[$key] = $value; 
                    } 
                  } 
               
                $package = Package::find($form->model()->id);           
                $package->tests_id =$result;
                $package->save();
                
            });
        });
    }
}
