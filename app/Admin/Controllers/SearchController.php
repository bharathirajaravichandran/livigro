<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Labs;
use App\Models\Doctors;
use App\Models\Patients;

class SearchController extends Controller
{
    use ModelForm;
     /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Search');
            // $content->description('description');

            $content->body(view('search'));
        });
    }
    public function search(Request $request)
    {
    	$result = "";
    	// dd($request->search);
    	$lab = DB::table('labs_table')->where('name','ILIKE','%'.$request->search.'%')
    						          ->orWhere('email','ILIKE','%'.$request->search.'%')
    						          ->orWhere('mobile','ILIKE','%'.$request->search.'%')->get();
    	// dd($lab->id);
    	if($lab){
    		foreach ($lab as $key => $value) {
    			$result .= "<tr>".
    						"<td>".$value->id."</td>".
    						"<td>"."<a href='labs/{$value->id}/show' style='color:#72afd2;'>".$value->name."</a>"."</td>".
    						"<td>".$value->mobile."</td>".
    						"<td>".$value->email."</td>".
    						"<td>Lab</td>".
    						"<td>".$value->status."</td>".
    					  "</tr>";
    		}
    	}
    	$doctors = DB::table('doctors_details')->where('name','ILIKE','%'.$request->search.'%')
    						          ->orWhere('email','ILIKE','%'.$request->search.'%')
    						          ->orWhere('mobile','ILIKE','%'.$request->search.'%')->get();
    	// dd($lab->id);doctorsdoctors patients_details
    	if($doctors){
    		foreach ($doctors as $key => $value) {
    			$result .= "<tr>".
    						"<td>".$value->id."</td>".
    						"<td>"."<a href='doctors/{$value->id}/show' style='color:#72afd2;'>".$value->name."</a>".
    						"<td>".$value->mobile."</td>".
    						"<td>".$value->email."</td>".
    						"<td>Doctor</td>".
    						"<td>".$value->status."</td>".
    					  "</tr>";
    		}
    	}
    	$patients = DB::table('patients_details')->where('name','ILIKE','%'.$request->search.'%')
    						          ->orWhere('email','ILIKE','%'.$request->search.'%')
    						          ->orWhere('phone','ILIKE','%'.$request->search.'%')->get();
    	// dd($lab->id);doctorsdoctors patients_details
    	if($patients){
    		foreach ($patients as $key => $value) {
    			$result .= "<tr>".
    						"<td>".$value->id."</td>".
    						"<td>"."<a href='patient/{$value->id}/show' style='color:#72afd2;'>".$value->name."</a>".
    						"<td>".$value->phone."</td>".
    						"<td>".$value->email."</td>".
    						"<td>Patient</td>".
    						"<td>".$value->status."</td>".
    					  "</tr>";
    		}
    	}



    	 return response()->json($result);
    	
    }
}
