<?php

namespace App\Admin\Controllers;

use App\Models\Package;
use App\Models\LabsAvaTest;
use App\Models\LabsTest;
use Auth;
use App\User;
use DB;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\Route;

class MyPackageController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {
            // dd(Auth::guard('admin')->user()->inRoles('labs'));
             if(Auth::guard('admin')->user()->user_type_id ==1){
                 // $ids = Auth::guard('admin')->user()->linked_id;
                $content->header('My Test');
                $content->description('description');
                $content->body($this->grid());
             }
             
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
      
        // $lab_value = DB::table('labs_available_test')->where('lab_id', $id)->first();
        // dd( $lab_value);
        // if($lab_value)
        // {        
        return Admin::grid(LabsAvaTest::class, function (Grid $grid) {
             // $ids = Auth::guard('admin')->user()->linked_id;
             // dd(Admin::user());
            if(Admin::user()->status !=1){
              $grid->disableActions();
              $grid->disableCreateButton();
             
            }
            // $grid->disableActions();
              // $grid->disableCreateButton();
           $grid->model()->orderBy('id', 'desc');
             $grid->actions(function ($actions) {
                  $actions->disableDelete();
                   $key = $actions->getKey();
                  // dd($key);
                   $labAvl = LabsAvaTest::find($key);
                    if($labAvl){
                        $labtest = Package::where('id',$labAvl->lab_package_id)->first();
                        if($labtest){
                             if($labtest->price_edit_status != 0){
                            $actions->disableEdit();
                            }
                        }
                       
                    }
                  //  $LabsTestStatus = LabsTest::find($key);
                  // $value = $LabsTestStatus->status;
                  // if($value !=2){
                  //         $actions->append("<a href=''><i class='fa fa-trash delete' id='$key'></i></a>");
                  //          }
                      });
             $grid->tools(function ($tools) {
                 $tools->batch(function ($batch) {
                   $batch->disableDelete();
                });
               });
             $grid->disableRowSelector();
             $grid->id('ID')->sortable();
            $grid->column('Test Name')->display(function(){
                 // dd($this->lab_tests_id);
                    $testnames = DB::table('package')->where('id',$this->lab_tests_id)->pluck('package_name');
                    // dd($testnames);
                    $testname='';
                    foreach ($testnames as $key => $value) {
                        $testname=$value;
                    }
                    return $testname;
            });
            $grid->column('amount','Price');
            // $grid->column('range','Reference Start Range');
            // $grid->column('range_end','Reference End Range');
            $grid->model()->where('lab_id', '=', Admin::user()->linked_id)->where('test_type', '=', 'group');
           
            // $grid->created_at();
            // $grid->updated_at();
        });
        // }
    }

     public function getTest($id){
        // dd($id);
        $labAvl = LabsAvaTest::where('lab_id',$id)->where('test_type','group')->pluck('lab_package_id');
        if($labAvl){
            // dd($labAvl);
            $data = "";
            $values="";
         
              $values = Package::whereNotIn('id',$labAvl)->where('status',1)->get();
              
            if($values){
                  // dd($values);
                foreach ($values as $val) {
                $data .= "<option value=".$val->id.">".$val->package_name."</option>";
                } 
                 return $data;
            } else {
                // dd("else");
                $values = Package::where('status',1)->get();
                foreach ($values as $val) {
                $data .= "<option value=".$val->id.">".$val->package_name."</option>";
                }
                
             
             return $data;
         }
       
             
        }
     

    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(LabsAvaTest::class, function (Form $form) {
            if(Route::getCurrentRoute()->getName() == "mypackage.create"){

                $form->display('id', 'ID');
                $form->multipleSelect('lab_package_id','Package')->options();
                $form->hidden('userid')->value(Auth::guard('admin')->user()->linked_id);
                $form->display('created_at', 'Created At');
                $form->display('updated_at', 'Updated At');
                // $form->hidden('userid')->value(Auth::guard('admin')->user()->linked_id);
            }else{
               $form->display('id', 'ID');
                $form->display('getTestName','Test');
                $form->text('amount','Price');
                // $form->text('range','Reference Start Range');
                // $form->text('range_end','Reference End Range');
            }
              $form->saving(function (Form $form) {
                // dd($form->lab_tests_id);
                 // dd($form->lab_package_id);
                if(Route::getCurrentRoute()->getName() == "mypackage.update"){
                    $raise_test = LabsAvaTest::find($form->model()->id);
                        $raise_test->lab_id =$form->model()->lab_id;
                        $raise_test->lab_package_id = $form->model()->lab_package_id;
                        $raise_test->amount = $form->amount;
                        // $raise_test->range = $form->range;
                        // $raise_test->range_end = $form->range_end;
                        $raise_test->save();
                } else {
                     // dd($form->lab_package_id);
                $ids = Auth::guard('admin')->user()->linked_id;
                foreach($form->lab_package_id as $packageId) {
                    // $testIds = Package::where('id',$packageId)->get();
                    $testIds = Package::find($packageId);
                    // dd($testIds);
                    // if(count($testIds)!=0){    
                    //     dd($testIds);
                        if($testIds){
                        // dd($testIds->amount);
                        $raise_test =  new LabsAvaTest;
                        $raise_test->lab_id = $ids;
                        $raise_test->lab_package_id = $testIds->id;
                        $raise_test->amount = $testIds->amount;
                        $raise_test->test_type = "group";
                        $raise_test->status = $form->status;
                        $raise_test->save();
                        }                  
                        
                    // }

                }
            }

              $success = new MessageBag([
                    'title'   => 'My Lab Test',
                    'message' => 'Saved Successfully',
                ]);
                return back()->with(compact('success'));
              });
                Admin::script('
                    $( document ).ready(function(){
                        var lab_id = $(".userid").val();
                        var lab_package_id=$(".lab_package_id");
                        lab_package_id.empty();
                            $.get("/admin/mypackage/getTest/" + lab_id, function(data) {
                             lab_package_id.append(data);
                            });
                    });
                '); 
        });
    }
}
