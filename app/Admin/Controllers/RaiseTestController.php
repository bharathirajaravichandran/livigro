<?php

namespace App\Admin\Controllers;

use App\Models\RaiseTest;
use App\Models\Receipt;
use App\User;
use Auth;
use App\Models\Doctors;
use App\Models\Labs;
use App\Models\LabsTest;
use App\Models\LabsAvaTest;
use App\Models\LabsCategory;
use App\Models\DocPatient;
use App\Models\Patients;
use App\Models\Notification;
use App\Models\Package;
use App\Models\Units;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use App\Models\Blood;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\Route;
use Carbon\Carbon;


class RaiseTestController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Raise Test');
            // $content->description('description');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            // dd($this->form()->edit($id));

            $content->header('Raise Test');
            // $content->description('description');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Raise Test');
            // $content->description('description');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(RaiseTest::class, function (Grid $grid) {
            $grid->column('id','Order ID')->display(function () {
              if($this->getTestStatus() == 'Completed')
                return "<a href='raisetest/$this->id/show'>$this->id</a>";
              else
                return $this->id;
            });
          //  $grid->id('Order ID');
           // $grid->patients_id('Patient ID');
            $grid->column('getLab.name','Lab')->limit(10);
            //$grid->column('getDoctor.name','Doctor');
             if(Admin::user()->user_type_id != '1'){
            $grid->column('Doctor')->display(function ($title) {
              $docname=DB::table('doctors_details')->where('id',$this->doctor_id)->pluck('name')->first();
                    return ($docname != '' || $docname != null) ? $docname : '-';
                });
            }
            if(Admin::user()->user_type_id == '1'){
            $grid->column('doctor_referer_name','Reference Doctor');
            }

            $grid->column('getPatient.name','Patient')->limit(10);
            // $grid->column('getLabTest.name','Test Name');
            //  $grid->column('Test Name')->display(function() {
            //     dd( $hospitals_name);
            //      $TestName = array();
            //      $datas=array();
            //    $test_ids = $this->lab_tests_id;
            //     foreach($test_ids as $testid) {
            //     $TestName[]= DB::table('lab_tests')
            //         ->where('id',$testid)
            //         ->pluck('name');
            //         }
            //         $count =count($TestName);
            //         for($i=0;$i<=$count;$i++)
            //         {
            //             $datas[]=$TestName[$i][$i];
            //         }
            //         dd($datas);
            //         foreach ($TestName as $value) {
            //             $datas[]= $value;
            //         }
            //            dd($datas);
            //            $data = explode(',',$datas);
            //          $hospitals=implode(',', $data); 
            //                    dd($hospitals);           
                    
            //           return $hospitals;
            // });
            $grid->column('raised_by','Raised_by')->display(function ($title) {
                    return $this->raised_by == '1' ? 'Lab' : ($this->raised_by == '2' ? 'Doctor': 'Admin');
                });

           
               //$grid->column('lab_referer_name','Reference Lab')->limit(10);
          
               // $grid->column('doctor_referer_name','Reference Doctor')->limit(10);
     
            
           
            $grid->column('paid_status','Paid Status')->display(function () {
              return $this->getStatus();
            })->limit(10);
            $grid->column('original_amount','Amount');

            // $grid->created_at();
            // $grid->updated_at();
            
            $grid->filter(function($filter){

                // Remove the default id filter
                $filter->disableIdFilter();

                // Add a column filter
                $filter->equal('id', 'Order ID');
                $filter->equal('patients_id','Patient ID');
                $filter->between('created_at', 'Created Date')->datetime();

            });
            // dd(Admin::user()->status);

            if(Admin::user()->user_type_id == '1')
              $grid->model()->where('lab_id', '=', Admin::user()->linked_id);
            else if(Admin::user()->user_type_id == '2')
              $grid->model()->where('doctor_id', '=', Admin::user()->linked_id);
             $grid->column('status','Status')->display(function ($title) {
                    return $this->status == '1' ? '<span class="btn btn-success btn-xs">Active</span>' : ($this->raised_by == '0' ? '<span class="btn btn-primary btn-xs">InActive</span>': '<span class="btn btn-danger btn-xs">Deleted</span>');
                }); 
             $grid->column('raised_for','Raised For')->display(function ($title) {
                    return $this->raised_for == '1' ? '<span class="btn btn-primary btn-xs">Lab</span>' : ($this->raised_for == '0' ? '<span class="btn btn-primary btn-xs">Doctor</span>': '<span class="btn btn-primary btn-xs">Self</span>');
                });
              if(Admin::user()->status !=1){
              $grid->disableActions();
              $grid->disableCreateButton();
             
            }
            $grid->model()->orderBy('id', 'desc');
            $grid->actions(function ($actions) {
                  // prepend an action.
$actions->disableEdit();
                  $key = $actions->getKey();               
                   $getRaisetest = RaiseTest::find($key);
                   //print_r( $getReceipttest);
                
                      //Canceled
                   if($getRaisetest->paid_status !=3 && $getRaisetest->paid_status !=4 && $getRaisetest->paid_status !=5  ){
                    $actions->prepend("<a style='padding:2px' href='/admin/raisetest/$key/cancel' data-toggle='cancel' title=' Cancel'><i class='fa fa-ban'></i></a>");
                  }
                  
                    if($getRaisetest->paid_status == 1){
                      if($getRaisetest->paid_status !=3 && $getRaisetest->paid_status !=4 && $getRaisetest->paid_status !=5  ){
                        //Report Generation
                    $actions->prepend("<a style='padding:2px' href='/admin/raisetest/$key/report_generation' data-toggle='report' title='Report Collection'><i class='fa fa-bug'></i></a>");
                    }
                  }
                   if($getRaisetest->specimen_collected == 1){
                    if($getRaisetest->paid_status !=3 && $getRaisetest->paid_status !=4 && $getRaisetest->paid_status !=5  ){
                       //Receipt Collection 
                    $actions->prepend("<a style='padding:2px' href='/admin/raisetest/$key/receipt_collection' data-toggle='receipt' title='Receipt Collection'><i class='fa fa-money'></i></a>");
                  }
                    }
                    
                    $actions->disableDelete();
                    $key = $actions->getKey();
                    $RaiseTestStatus = RaiseTest::find($key);
                    $value = $RaiseTestStatus->status;
                    $paid_status = $RaiseTestStatus->paid_status;
                    if($value !=2){
                     $actions->append("<a style='padding:2px' href=''><i class='fa fa-trash delete' id='$key' data-toggle='deleted' title='Delete'></i></a>");
                    }
                     //Sample Collection
                    if($paid_status !=3 && $paid_status !=4 && $paid_status !=5 ){
                    $actions->prepend("<a style='padding:2px' href='/admin/raisetest/$key/sample_collection' data-toggle='sample' title='Sample Collection'><i class='fa fa-paper-plane'></i></a>");
                  }

              });
            $grid->tools(function ($tools) {
                 $tools->batch(function ($batch) {
                   $batch->disableDelete();
                });
            });
             $grid->disableRowSelector();
             // Neelamegam_29/08/2018_CommonDelete
             Admin::script('
                $( document ).ready(function(){
                    $(".delete").click(function(){
                        var id = $(this).prop("id");
                        var url = "CommonDelete/" + id;
                        var fullurl = window.location.pathname.split("/"); 
                        var uri_segment = fullurl[2];

                        swal({
                          title: "DeleteConfirm",
                          type: "warning",
                          showCancelButton: true,
                          confirmButtonColor: "#DD6B55",
                          confirmButtonText: "Confirm",
                          closeOnConfirm: false,
                          cancelButtonText: "Cancel"
                        },
                        function(){
                            $.ajax({

                                method: "post",
                                url: url,
                                data: {
                                    id:id,
                                    _token:LA.token,
                                    uri_segment:uri_segment,
                                },
                                success: function (data) {
                                    $.pjax.reload("#pjax-container");

                                    if (typeof data === "object") {
                                        if (data.status) {
                                            swal(data.message, "", "success");
                                        } else {
                                            swal(data.message, "", "error");
                                        }
                                    }
                                }
                            });
                        });            
                    });
                });
            ');
              $grid->created_at();
        });
    }

    public function getvalue(){ 
      $data = "";
       $subcategory = DB::table('patients_details')->where('status',1)->get();
        foreach($subcategory as $val){
          $data .= "<option value=".$val->id.">".$val->name." ".$val->phone."</option>"; }
        // dd($data);
      return response()->json($data);

    }
    public function getTest($id){
      $data = '';
      if($id== 'doctor'){
          $subcategory = DB::table('lab_tests')->where('status',1)->orderBy('name','asc')->get();
        
        foreach($subcategory as $val){
          $data .= "<option value=".$val->id.">".$val->name."</option>";
        }
      }else{
      $subcategory = DB::table('labs_available_test')
          ->join('lab_tests', 'lab_tests.id', '=', 'labs_available_test.lab_tests_id')
          ->where('labs_available_test.lab_id', $id)
          ->where('labs_available_test.status', '1')
          ->orderBy('lab_tests.name','asc')
          ->get();
        if(count($subcategory)!=0){
          foreach($subcategory as $val){
            $data .= "<option value=".$val->lab_tests_id.">".$val->name."</option>";
          }
        }else{
          $subcategory = DB::table('lab_tests')->where('status',1)->orderBy('name','asc')->get();
        
        foreach($subcategory as $val){
          $data .= "<option value=".$val->id.">".$val->name."</option>";
        }
        }
      }
       
      return $data;

    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {  
        return Admin::form(RaiseTest::class, function (Form $form) {
          $form->setId('raise_form');
           
                // dd(Admin::user()->user_type_id);
                $form->hidden('user_type')->value(Admin::user()->user_type_id);
                if(Admin::user()->user_type_id == '1'){

                // $form->select('raised_by','Raised By')->options([0=>'',1=>'Lab',2=>'Doctor']);
                $form->select('doctor_id','Reference Doctor')->options(Doctors::all()->pluck('name', 'id'));
                 $form->hidden('lab_id')->value(Admin::user()->linked_id);
                $form->select('patients_id','Patient')->options(Patients::where('status',1)->pluck('name','id'));
                $form->html('<a style="display:none;color:white; width:100px;" id="newpatient" class="btn btn-success" href="/admin/patients/create">New patient</a>');
                /*if(Route::getCurrentRoute()->getName() == 'raisetest.edit')
                $form->multipleSelect('lab_tests_id','Tests')->options();
                else*/
                $form->multipleSelect('lab_tests_id','Tests')->options(LabsTest::where('status',1)->orderBy('name','asc')->pluck('name','id'));
                $form->multipleSelect('package_id','Package')->options(Package::where('status',1)->orderBy('package_name','asc')->pluck('package_name','id'));
                $form->text('diagonsis','Diagonsis')->attribute(['id'=>'diagonsis']);
                $form->text('notes','Notes')->attribute(['id'=>'notes']);
                $states = [
                        'off'  => ['value' => 0, 'text' => 'Inactive', 'color' => 'danger'],
                        'on' => ['value' => 1, 'text' => 'Active', 'color' => 'success'],
                        ];
                $form->switch('status','Status')->states($states)->default('1'); 

                }else if(Admin::user()->user_type_id == '2'){

                $form->select('patients_id','Patient')->options(Patients::where('status',1)->pluck('name', 'id'));
                $form->html('<a style="display:none;color:white; width:100px;" id="newpatient" class="btn btn-success" href="/admin/patients/create">New patient</a>');
                /*if(Route::getCurrentRoute()->getName() == 'raisetest.edit')
                $form->multipleSelect('lab_tests_id','Tests')->options();
                else*/
                $form->multipleSelect('lab_tests_id','Tests')->options(LabsTest::where('status',1)->orderBy('name','asc')->pluck('name','id'));
                $form->multipleSelect('package_id','Package')->options(Package::where('status',1)->orderBy('package_name','asc')->pluck('package_name','id'));
                $form->text('diagonsis','Diagonsis')->attribute(['id'=>'diagonsis']);
                $form->text('notes','Notes')->attribute(['id'=>'notes']);
                $states = [
                        'off'  => ['value' => 0, 'text' => 'Inactive', 'color' => 'danger'],
                        'on' => ['value' => 1, 'text' => 'Active', 'color' => 'success'],
                        ];
                $form->switch('status','Status')->states($states)->default('1'); 

                }else if(Admin::user()->user_type_id == '' ||  Admin::user()->user_type_id == null){
                
                $form->select('raised_by','Raised By')->options([0=>'',1=>'Lab',2=>'Doctor']);
                $form->select('doctor_id','Doctor')->options(Doctors::all()->pluck('name', 'id'));
                $form->select('lab_id','Lab')->options(Labs::all()->pluck('name', 'id'));
                $form->select('patients_id','Patient')->options(Patients::all()->pluck('name', 'id'));
                $form->html('<a style="display:none;color:white; width:100px;" id="newpatient" class="btn btn-success" href="/admin/patients/create">New patient</a>');
                // if(Route::getCurrentRoute()->getName() == 'raisetest.edit')
                // $form->multipleSelect('lab_tests_id','Tests')->options();
                // else
                $form->multipleSelect('lab_tests_id','Tests')->options(LabsTest::where('status',1)->orderBy('name','asc')->pluck('name','id'));
                $form->multipleSelect('package_id','Package')->options(Package::where('status',1)->orderBy('package_name','asc')->pluck('package_name','id'));
                $form->text('diagonsis','Diagonsis')->attribute(['id'=>'diagonsis']);
                $form->text('notes','Notes')->attribute(['id'=>'notes']);
                $states = [
                        'off'  => ['value' => 0, 'text' => 'Inactive', 'color' => 'danger'],
                        'on' => ['value' => 1, 'text' => 'Active', 'color' => 'success'],
                        ];
                $form->switch('status','Status')->states($states)->default('1'); 

                }

               
                // dd($form->raised_by);

            

               
              

            // ->tab('New Patient', function (Form $form) {
            //     $form->text('getPatientname','Patient Name');
            //     $form->text('getPatientmobile','Mobile');
            //     $form->text('getPatientemail','Email');
            //     $form->select('getPatientgender','Gender')->options([0=>'Female',1=>'Male',2=>'Non Binary']);
            //     $form->select('getPatientblood_group','Blood Group')->options(Blood::all()->pluck('blood_group','id'));

            //    });
                     $form->saving(function (Form $form) {

                      //echo "test"; print_r($form->lab_tests_id);
                    //die;
					
                      $admin_role = Auth::guard('admin')->user();
                     // dd($admin_role->linked_id);
                      $amount=array();
                       $prices=0;
                       $packageAmounts=0;
                       $raised_by='';

                         
                         if($form->raised_by==1){
                          $raised_by=1;
                         } else if($form->raised_by==2){
                          $raised_by=0;
                         } else{
                            $raised_by = $form->raised_by;
                         }

                        // for RaiseTest doctor_referer_name
                         $referenceDoctorName = DB::table('doctors_details')->where('id', $form->doctor_id)->pluck('name')->first();
                         // for RaiseTest doctor_referer_name
                         $referenceLabName = DB::table('labs_table')->where('id', $form->lab_id)->pluck('name')->first();
                         $tests_id =$form->lab_tests_id;                            
                         foreach($tests_id as $testid) {
                          if($testid !=''){
                            // dd($testid);
                            if($form->raised_by==1){
                              $getAmount = DB::table('labs_available_test')
                                        ->join('lab_tests', 'lab_tests.id', '=', 'labs_available_test.lab_tests_id')
                                        ->where('lab_tests.id', $testid)
                                        ->where('labs_available_test.lab_id', $form->lab_id)->first();
                              if($getAmount !="" || $getAmount !=null)
                              $amount[] = ($getAmount->amount!='' && $getAmount->amount!= null)?$getAmount->amount:$getAmount->price;
                            }else if($form->raised_by==2){
                              $amount[] = DB::table('lab_tests')->where('id', $testid)->pluck('price')->first();
                            }else{
                                // dd( );
                              $getAmount = DB::table('labs_available_test')
                                        ->join('lab_tests', 'lab_tests.id', '=', 'labs_available_test.lab_tests_id')
                                        ->where('lab_tests.id', $testid)
                                        ->where('labs_available_test.lab_id',$admin_role->linked_id)->first();
                              if($getAmount){
                                $amount[] = $getAmount->amount;
                              }else{
                                 $getAmounts = DB::table('lab_tests') ->where('id', $testid)->get();
                                    foreach ($getAmounts as $value) {
                                      $amount[] = $value->price;
                                    }
                              }
                             
                            }
                          }
                         }
                        // dd($amount);
                         // Status
                         if($form->status == "on"){
                            $statusValue =1;
                         }else{
                            $statusValue =0;
                         }
                            foreach ($amount as $price) {
                              $prices += $price;
                            }

                            $testId = array();
                            $packageAmount = array();

                            foreach ($form->package_id as $value) {
                                 $test = Package::find($value);
                                 $availablePackage = LabsAvaTest::where('lab_tests_id',$value)->where('lab_id',$form->lab_id)->where('test_type','group')->first();
                                 // dd($test->amount);
                                 if($test){
                                    $testId[]=$test->tests_id;
                                    $packageAmount[]=(isset($availablePackage) && $availablePackage->amount != NULL && $availablePackage->amount != '')?$availablePackage->amount:$test->amount;
                                  }
                            }
                            foreach ($packageAmount as $value) {
                              $packageAmounts += $value; 
                            }
                            $result = array(); 
                             // dd($testId);
                            foreach ($testId as $key => $value) { 
                             
                              if (is_array($value)) { 
                                $result = array_merge($result, array_flatten($value)); 

                              } else { 
                                $result[$key] = $value; 
                              } 
                            }

                           $lab_tests_ids = array_filter($form->lab_tests_id);
                           $newarray = array_merge($result,$lab_tests_ids);
                           if($form->doctor_id){
                            // dd("hi");
                            $doctorId = $form->doctor_id;
                           }else{
                            $doctorId = 0;
                           } 

                           if($form->lab_id){
                            // dd("hi");
                            $labId = $form->lab_id;
                           }else{
                            $labId = 0;
                           }
                           // dd($prices+$packageAmounts);
                           if($admin_role->user_type_id ==1){
                            // dd("hi");
                              $raise_test = new RaiseTest;
                              $raise_test->patients_id = $form->patients_id;                              
                              $raise_test->lab_id = $admin_role->linked_id;
                              $raise_test->doctor_id = $doctorId;
                              $raise_test->lab_tests_id = array_filter($newarray);
                              $raise_test->original_amount = $prices+$packageAmounts;
                              $raise_test->raised_by = 1;
                              $raise_test->doctor_referer_name = $referenceDoctorName;
                              $raise_test->doctor_referer_id = $doctorId;
                              $raise_test->lab_referer_id = $admin_role->linked_id;
                              $raise_test->diagonsis = $form->diagonsis;   
                              $raise_test->notes = $form->notes;    
                              $raise_test->status = $statusValue;     
                              $raise_test->save();
                               $receiptRaiseId = $raise_test->id; 
                              $doctor_id = 0;
                              if($form->doctor_id !=""){
                                $doctor_id = $form->doctor_id;
                              }
                              $doctor_patient = DocPatient::updateOrCreate(
                                ['patient_id' => $form->patients_id,'lab_id' => $admin_role->linked_id], ['doctor_id'=>$doctor_id,'updated_at' => date('Y-m-d H:i:s')]
                                );
                           }else{
                            if($form->raised_by==1){
                               $doctor_id = 0;
                              if($form->doctor_id !=""){
                                $doctor_id = $form->doctor_id;
                              }else{
                                $doctor_id = 0;
                              }
                              $raise_test = new RaiseTest;
                              $raise_test->patients_id = $form->patients_id;                              
                              $raise_test->lab_id = $form->lab_id;
                              $raise_test->doctor_id =$doctor_id;
                              $raise_test->lab_tests_id = array_filter($newarray);
                              $raise_test->original_amount = $prices+$packageAmounts;
                              $raise_test->raised_by = 1;
                              $raise_test->doctor_referer_name = $referenceDoctorName;
                              $raise_test->doctor_referer_id = $doctor_id;
                              $raise_test->lab_referer_id = $labId;
                              $raise_test->diagonsis = $form->diagonsis;   
                              $raise_test->notes = $form->notes;    
                              $raise_test->raised_for = 1;   
                              $raise_test->status = $statusValue;    
                              $raise_test->save();

                              // for Receipt table raise_test_id
                              $receiptRaiseId = $raise_test->id; 
                              $doctor_id = 0;
                              if($form->doctor_id !=""){
                                $doctor_id = $form->doctor_id;
                              }
                              $doctor_patient = DocPatient::updateOrCreate(
                                ['patient_id' => $form->patients_id,'lab_id' => $form->lab_id], ['doctor_id'=>$doctor_id,'updated_at' => date('Y-m-d H:i:s')]
                                );
                            
                              /*$doctor_patient = new DocPatient;
                              print_r($doctor_patient);die;
                              dd($doctor_patient);
                                if($form->doctor_id !=""){
                                $doctor_patient->doctor_id = $form->doctor_id;
                              }
                              $doctor_patient->patient_id = $form->patients_id;                              
                              $doctor_patient->lab_id = $form->lab_id;                             
                              $doctor_patient->save();   */                          
                            }else if($form->raised_by==2){
                              $raise_test = new RaiseTest;
                              $raise_test->patients_id = $form->patients_id;
                              $raise_test->lab_id = $form->lab_id; 
                              $raise_test->doctor_id = $form->doctor_id; 
                              $raise_test->doctor_referer_name = $referenceDoctorName;
                              $raise_test->doctor_referer_id = $form->doctor_id;                             
                              $raise_test->lab_tests_id = array_filter($newarray);
                              $raise_test->original_amount = $prices+$packageAmounts;
                              $raise_test->raised_by = 2;                              
                              $raise_test->diagonsis = $form->diagonsis;               
                              $raise_test->notes = $form->notes;  
                              $raise_test->lab_referer_id = $labId;                     
                              $raise_test->lab_referer_name = $referenceLabName;                
                              $raise_test->raised_for = 0;                              
                              $raise_test->status = $statusValue;                              
                              $raise_test->save();
                              $receiptRaiseId = $raise_test->id;  
                              $doctor_patient = DocPatient::updateOrCreate(
                                ['patient_id' => $form->patients_id,'doctor_id' => $form->doctor_id], ['lab_id'=> NULL,'updated_at' => date('Y-m-d H:i:s')]
                                );
                              /*$doctor_patient = new DocPatient;
                              $doctor_patient-> = $form->doctor_id;                              
                              $doctor_patient->patient_id = $form->patients_id;                              
                              $doctor_patient->save();                            */
                            }
                           }
                          
                           // dd($raise_test->id);

                           $test = RaiseTest::where('id',$raise_test->id)->where('status',1)->first();        
                            // dd($test->id);
                            if(!empty($test->id))
                            {                     
                            $notification = new Notification();
                            $notification->type             = 'test';
                            $notification->lab_id           =  ($test->lab_id) ? $test->lab_id : 0;
                            $notification->doctor_id        =  ($test->doctor_id) ? $test->doctor_id : 0;
                            $notification->patient_id       =  ($test->patients_id) ? $test->patients_id : 0;
                            $notification->raise_test_id    =  $raise_test->id;
                            $notification->read_status      =  0;
                            $notification->save();
                            }
                            
                            $tests_ids =array_filter($form->lab_tests_id);     
                            
                             foreach($tests_ids as $testid) {
                                    if($form->raised_by==1){
                                      $getdata = DB::table('labs_available_test')
                                            ->join('lab_tests', 'lab_tests.id', '=', 'labs_available_test.lab_tests_id')
                                            ->where('lab_tests.id', $testid)
                                            ->where('labs_available_test.lab_id', $form->lab_id)->first();

                                      if($getdata){
                                        $amount = ($getdata->amount != '' && $getdata->amount!=null)?$getdata->amount:$getdata->price;
                                        $ref_start_range = $getdata->range;
                                      $ref_end_range = $getdata->range_end;
                                      }else{
                                         $getdata = DB::table('lab_tests')->where('id', $testid)->first();
                                      $amount = $getdata->price;
                                       $ref_start_range = $getdata->reference_range;
                                      $ref_end_range = $getdata->reference_endrange;
                                      }
                                    }
                                    else{
                                      $getdata = DB::table('lab_tests')->where('id', $testid)->first();
                                      $amount = $getdata->price;
                                      $ref_start_range = $getdata->reference_range;
                                      $ref_end_range = $getdata->reference_endrange;
                                   }
                                   $receipt_table = new Receipt;
                                   $receipt_table->raise_test_id = $receiptRaiseId;
                                   $receipt_table->tests_id = $testid;
                                   $receipt_table->amount = $amount;
                                    $unit_id =LabsTest::find($testid)->measurement;
                                    $units_count = Units::where('id',$unit_id)->get();
                                  if($units_count->count() !=0){
                                     $receipt_table->units = $units_count->first()->name;
                                    }else{
                                     $receipt_table->units = '-nill-' ;
                                    }                                                                 
                                   $receipt_table->reference_range = $ref_start_range;
                                   $receipt_table->reference_endrange = $ref_end_range;
                                   $specimen_count = LabsCategory::where('lab_test_id',$testid)->where('status',1)->get();
                                  if($specimen_count->count() !=0)
                                           $specimen_id = $specimen_count->first()->id ;
                                  else
                                     $specimen_id = NULL ;
                                   $receipt_table->labtest_sub_name = $specimen_id;
                                   $receipt_table->test_type = "single";
                                   $receipt_table->save();
                              }

                               foreach($form->package_id as $packageId) {
                                  $testIds = Package::find($packageId);
                                   if($testIds){
                                  //echo "<pre>";print_r($testIds->tests_id);die;
                                  foreach ($testIds->tests_id as $value) {
                                    if($value !='' && $value != NULL){
                                  $receipt_table = new Receipt;
                                  $receipt_table->raise_test_id = $receiptRaiseId;
                                  $receipt_table->tests_id = $value;
                                  //print_r(LabsTest::find($value)->first());
                                  $unit_idList =LabsTest::find($value);
                                  if($unit_idList['measurement']!= NULL && $unit_idList['measurement'] != ''){
                                    $unit_id = $unit_idList['measurement'];
                                      $units_count = Units::where('id',$unit_id)->get();
                                    if($units_count->count() !=0){
                                       $receipt_table->units = $units_count->first()->name;
                                      }else{
                                       $receipt_table->units = '-nil-' ;
                                      }   
                                    }else{
                                      $receipt_table->units = '-nil-' ;
                                    }
                                 // $receipt_table->units = '-nil-' ;
                                   $receipt_table->test_type = $packageId;
                                   $receipt_table->reference_range =LabsTest::where('id',$value)->pluck('reference_range')->first();
                                   $receipt_table->reference_endrange =LabsTest::where('id',$value)->pluck('reference_endrange')->first();
                                   $specimen_count = LabsCategory::where('lab_test_id',$value)->where('status',1)->get();
                                  if($specimen_count->count() !=0)
                                           $specimen_id = $specimen_count->first()->id ;
                                  else
                                     $specimen_id = NULL ;
                                   $receipt_table->labtest_sub_name = $specimen_id;

                                   
                                   $receipt_table->save();
                                 }
                                    }
                                  }

                               }
                           //   die;
                                                   
                                                
                          $title = "Livigro - Raise test";
                          // $msg = "New test raised.. RaiseTest ID-{$raise_test->id}";
                          if($form->doctor_id)
                          $doctorName = DB::table('doctors_details')
                                            ->where('id', $form->doctor_id)
                                            ->pluck('name')->first();
                          $msg = "{doctorName} has assigned a test for  patient name";
                          $msg1 = "Self has assigned a test for  patient name";
                          
                          if($form->lab_id !=''){
                          $getLabfcm = get_fcm_key($form->lab_id,'lab'); 
                          if($getLabfcm != null || $getLabfcm != ''){                           
                            $send = send_push_notification($getLabfcm, $msg1, $title); 
                           }
                         }
                           if($form->doctor_id !=''){
                             $getDoctorfcm = get_fcm_key($form->doctor_id,'doctor'); 
                              if($getDoctorfcm != null || $getDoctorfcm != ''){
                            $send1 = send_push_notification($getDoctorfcm, $msg, $title);
                          } 
                           }                               
                         
                          $patientPhone = DB::table('patients_details')
                                            ->where('id', $form->patients_id)
                                            ->pluck('phone')->first();
                          $labMobile = DB::table('labs_table')
                                            ->where('id', $form->lab_id)
                                            ->pluck('mobile')->first();
                          $patientName = DB::table('patients_details')
                                            ->where('id', $form->patients_id)
                                            ->pluck('name')->first();

                          if($labMobile != null && $labMobile != ''){
                             if($patientName)
                            $patientmsg = "Your  Livigro Booking ID : {$raise_test->id} for {$patientName} has been placed sucessfully";
                            if(substr($labMobile,0,1) == '+'){
                              send_sms(substr($labMobile,3), $patientmsg);
                            }else{
                              send_sms($labMobile, $msg);
                            }
                          }
                          if($patientPhone != null && $patientPhone != ''){
                            if($patientName)
                            $patientmsg = "Hi {$patientName},    Livigro Booking ID : {$raise_test->id} has been placed sucessfully";
                            if(substr($patientPhone,0,1) == '+'){
                              send_sms(substr($patientPhone,3), $patientmsg);
                            }else{
                              send_sms($patientPhone, $msg);
                            }
                            
                          }                          
                          $success = new MessageBag([
                                'title'   => 'Raise Test',
                                'message' => 'Saved Successfully',
                            ]);

                          //Need to send the push notificatio and put entry in notification table
                          // regarding the Test to Lab
//                          $lab_fcm = get_fcm_key($form->patients_id,'lab');
                          //send_push_notification($lab_fcm,'test',$raise_test->id);
                /*          Notification::create([
                            'type'=> 'test',
                            'patient_id' => $form->patients_id,
                            'lab_id' => $form->lab_id,
                            'raise_test_id' => $raise_test->id,
                            'doctor_id' => $form->doctor_id,
                          ]);*/

                  return back()->with(compact('success'));

                    });

                  // $form->saved(function (Form $form) {
                 

                  // $test = RaiseTest::where('id',$form->model()->id)->where('status',1)->first();           
                  // if(!empty($test->id))
                  // {                     
                  // $notification = new Notification();
                  // $notification->type             = 'Raise test';
                  // $notification->lab_id           =  $lab_id;
                  // $notification->doctor_id        =  ($test->doctor_id)? $test->doctor_id : 0;
                  // $notification->patient_id       =  ($test->patients_id) ? $test->patients_id : 0;
                  // $notification->raise_test_id    =  $raise_test->id;
                  // $notification->save();
                  // }

                  // });




        
             //   $form->ignore(['getPatientname','getPatientmobile','getPatientemail','getPatientgender','getPatientblood_group']);
            // Neelamegam_27/08/2018
              Admin::script('
                      $( document ).ready(function(){
                         var fullurl = window.location.pathname.split("/"); 
                        var uri_segment = fullurl[4];
                        var user_type = $(".user_type").val();
                        if(uri_segment != "edit"){
                          $("select.doctor_id").closest(".form-group").css({"display": "none"});
                           $("select.lab_id").closest(".form-group").css({"display": "none"});
                           $("select.patients_id").closest(".form-group").css({"display": "none"});
                           $("select.lab_tests_id").closest(".form-group").css({"display": "none"});
                           $(".diagonsis").closest(".form-group").css({"display": "none"});
                           $(".notes").closest(".form-group").css({"display": "none"});
                           $(".package_id").closest(".form-group").css({"display": "none"});
                          }else{
                            if($("select.raised_by").val() == 2){
                               $("select.lab_id").closest(".form-group").css({"display": "none"});
                            }
                          }

                          if(user_type == 1 || user_type == 2){
                            $("select.doctor_id").closest(".form-group").css({"display": "block"});
                               $("select.lab_id").closest(".form-group").css({"display": "block"});
                               $("#newpatient").css({"display": "block"});
                               $("select.patients_id").closest(".form-group").css({"display": "block"});
                               $("select.lab_tests_id").closest(".form-group").css({"display": "block"});
                                $(".diagonsis").closest(".form-group").css({"display": "block"});
                                $(".notes").closest(".form-group").css({"display": "block"});
                                $(".package_id").closest(".form-group").css({"display": "block"});
                          }
                
                        $("select.lab_id").change(function(e){
                             var lab_id = $(".lab_id").val();
                             var raised_by = $(".raised_by").val();
                             var lab_tests_id=$(".lab_tests_id");
                            lab_tests_id.empty();
                             $.get("/admin/raisetest/getTest/" + lab_id, function(data) {                              
                              lab_tests_id.append(data);
                          });

                          });         
                         
                            });
                        $("select.raised_by").change(function(){
                          $("#small").remove();         
                            $("select.raised_by").closest(".form-group").removeClass("has-error").addClass("has-success");
                                       
                          var raised_by = $(".raised_by").val();
                          if(raised_by == "2"){
                             var lab_tests_id=$(".lab_tests_id");
                            lab_tests_id.empty();
                             $.get("/admin/raisetest/getTest/doctor", function(data) {

                              lab_tests_id.append(data);
                              });
                            }
                            if($("select.raised_by").val()==1){
                               $("select.doctor_id").closest(".form-group").css({"display": "block"});
                               $("select.lab_id").closest(".form-group").css({"display": "block"});
                               $("#newpatient").css({"display": "block"});
                               $("select.patients_id").closest(".form-group").css({"display": "block"});
                               $("select.lab_tests_id").closest(".form-group").css({"display": "block"});
                                $(".diagonsis").closest(".form-group").css({"display": "block"});
                                $(".notes").closest(".form-group").css({"display": "block"});
                                $(".package_id").closest(".form-group").css({"display": "block"});
                                  $("label[for=doctor_id]").text("Reference Doctor");
                                  $("label[for=lab_id]").text("Lab");
                            }
                            else if($("select.raised_by").val()==2 ){
                              $("select.doctor_id").closest(".form-group").css({"display": "block"});
                               $("select.lab_id").closest(".form-group").css({"display": "block"});
                               $("#newpatient").css({"display": "block"});
                               $("select.patients_id").closest(".form-group").css({"display": "block"});
                               $("select.lab_tests_id").closest(".form-group").css({"display": "block"});
                                $(".diagonsis").closest(".form-group").css({"display": "block"});
                                $(".notes").closest(".form-group").css({"display": "block"});
                                $(".package_id").closest(".form-group").css({"display": "block"});
                                $("label[for=lab_id]").text("Reference Lab");
                                $("label[for=doctor_id]").text("Doctor");
                            } else {
                               $("select.doctor_id").closest(".form-group").css({"display": "block"});
                               $("select.lab_id").closest(".form-group").css({"display": "block"});
                               $("#newpatient").css({"display": "block"});
                               $("select.patients_id").closest(".form-group").css({"display": "block"});
                               $("select.lab_tests_id").closest(".form-group").css({"display": "block"});
                                $(".diagonsis").closest(".form-group").css({"display": "block"});
                                $(".notes").closest(".form-group").css({"display": "block"});
                                $(".package_id").closest(".form-group").css({"display": "block"});
                            }
                               
                          });
                    $("li.next a,button[type=submit]").click(function(){
                    var user_type = $(".user_type").val();
                    if(user_type == 1){
                    var patient = $("select.patients_id").val();
                    var ltest = $("select.lab_tests_id").val(); 
                      if(patient =="" || ltest ==""){
                        $("select.patients_id").closest(".form-group").addClass("has-error");
                        $("select.lab_tests_id").closest(".form-group").addClass("has-error");
                        return false;
                      }else{
                       return true;
                      }
                    }
                            if(user_type!=1 || user_type == null){
                            var rsi = $("select.raised_by").val();
                            if(rsi == 2){
                var doc = $("select.doctor_id").val();
                var patient = $("select.patients_id").val();
                var ltest = $(".lab_tests_id").val();

                if(doc =="" || patient =="" || ltest ==""){
                    var error="<small id=small class=help-block>Please Select</small>";
                    $("#small").remove();                          
                    $("select.doctor_id").closest(".form-group").addClass("has-error");
                    $("select.patients_id").closest(".form-group").addClass("has-error");
                    $("select.lab_tests_id").closest(".form-group").addClass("has-error");
                   $("select.doctor_id").change(function(){
                    $("select.doctor_id").closest(".form-group").removeClass("has-error").addClass("has-success");
                    });
                   $("select.patients_id").change(function(){
                    $("select.patients_id").closest(".form-group").removeClass("has-error").addClass("has-success");
                    });
                   $("select.lab_tests_id").change(function(){
                    $("select.lab_tests_id").closest(".form-group").removeClass("has-error").addClass("has-success");
                     $("select.raised_by").closest(".nav-tabs-custom").children("ul.nav-tabs").children("li.active").removeClass("bv-tab-error").addClass("bv-tab-sucess"); 
                    });
                    $("select.raised_by").trigger("change");
                    return false;
                } 

                            } else if (rsi == 1){

                            var doc = $("select.doctor_id").val();
                            var patient = $("select.patients_id").val();
                            var ltest = $("select.lab_tests_id").val(); 
                            var lab = $("select.lab_id").val(); 

                            if(patient =="" || ltest =="" || lab ==""){

                                    // $("select.doctor_id").closest(".form-group").addClass("has-error");
                                    $("select.patients_id").closest(".form-group").addClass("has-error");
                                    $("select.lab_tests_id").closest(".form-group").addClass("has-error");
                                    $("select.lab_id").closest(".form-group").addClass("has-error");
                                     $("select.raised_by").closest(".nav-tabs-custom").children("ul.nav-tabs").children("li.active").addClass("bv-tab-error").removeClass("bv-tab-sucess");
                                   // $("select.doctor_id").change(function(){
                                   //  $("select.doctor_id").closest(".form-group").removeClass("has-error").addClass("has-success");
                                   //  });
                                   $("select.patients_id").change(function(){
                                    $("select.patients_id").closest(".form-group").removeClass("has-error").addClass("has-success");
                                    });
                                   $("select.lab_tests_id").change(function(){
                                    $("select.lab_tests_id").closest(".form-group").removeClass("has-error").addClass("has-success");
                                    });
                                   $("select.lab_id").change(function(){
                                    $("select.lab_id").closest(".form-group").removeClass("has-error").addClass("has-success");
                                     $("select.raised_by").closest(".nav-tabs-custom").children("ul.nav-tabs").children("li.active").removeClass("bv-tab-error").addClass("bv-tab-sucess"); 
                                    });
                                    $("select.raised_by").trigger("change");
                                    return false;
                                    } 
                                                          
                            }else{
                              var error="<small id=small class=help-block>Please Select</small>";
                            $("#small").remove();                          
                            $("select.raised_by").closest(".form-group").addClass("has-error");
                            $("select.raised_by").closest(".col-sm-8").append(error);
                            $("select.raised_by").closest(".nav-tabs-custom").children("ul.nav-tabs").children("li.active").addClass("bv-tab-error").removeClass("bv-tab-sucess");
                            return false;
                            } 
                             if($("#getPatientname").val() !=""){
                                    var phone_number = $("#getPatientmobile").val();
                                    if(phone_number==""){
                                     var error="<small id=small class=help-block>The Mobile Number is Required with Country code</small>";
                                      $("#small").remove();                          
                                      $("#getPatientmobile").closest(".form-group").addClass("has-error");
                                    $("#getPatientmobile").closest(".col-sm-8").append(error);
                                    $("#getPatientmobile").closest(".nav-tabs-custom").children("ul.nav-tabs").children("li.active").addClass("bv-tab-error").removeClass("bv-tab-sucess");
                                     return false;
                                    }

                                  }

                              }
                             });
                            
              
                      $("#getPatientmobile").blur(function(){
                     $("#small").remove();    
                    $("#getPatientmobile").closest(".form-group").addClass("has-error");
                    var phone_number = $("#getPatientmobile").val();
                    var firstVal = $("#getPatientmobile").val().charAt(0);
                    var numbericCheck = $("#getPatientmobile").val();
                     if((phone_number!="")&&(firstVal!= "+")) {
                        var error="<small id=small class=help-block>Required Country code</small>";
                        $("#small").remove();
                        $("#getPatientmobile").closest(".nav-tabs-custom").children("ul.nav-tabs").children("li.active").addClass("bv-tab-error").removeClass("bv-tab-sucess");            
                        $("#getPatientmobile").closest(".form-group").addClass("has-error");
						$("#getPatientmobile").closest(".col-sm-8").append(error);
                      } else if((phone_number!="")&&(firstVal== "+")&&(!$.isNumeric(numbericCheck))) {
                         var error="<small id=small class=help-block>Mobile number must contain only numeric</small>";
                         $("#small").remove();
                         $("#getPatientmobile").closest(".nav-tabs-custom").children("ul.nav-tabs").children("li.active").addClass("bv-tab-error").removeClass("bv-tab-sucess");            
                         $("#getPatientmobile").closest(".form-group").addClass("has-error");
                         $("#getPatientmobile").closest(".col-sm-8").append(error);
                      } else {
                        var url = "phonevalidation";
                        var token = $("input[name=_token]").val();
                        var fullurl = window.location.pathname.split("/"); 
                        var uri_segment = fullurl[3];                          
                            $.ajax({
                                type:"post",
                                dataType: "json",
                                url: url,                       
                                data: {
                                    _token: token,
                                    phone_number: phone_number,
                                    uri_segment : uri_segment,
                                },                        
                                success: function( responses ){
                                    console.log(responses);
                                    if(responses.phone_id == 0){                                          
                                        var error="<small id=small class=help-block>  The Mobile Number is Already Exists</small>";
                                        $("#small").remove();                                       
                                        $("#getPatientmobile").closest(".form-group").addClass("has-error");
                                        $("#getPatientmobile").closest(".col-sm-8").append(error);
                                    }
                                },
                                complete: function(data) {
                                   
                                   if(data.responseJSON.message == "Mobile Number Already Exist")
                                   {
                                     $("#getPatientmobile").closest(".form-group").addClass("has-error");
                                   }
                                   else
                                   {
                                    $("#getPatientmobile").closest(".form-group").removeClass("has-error").addClass("has-success");
                                   }
                                    
                                 },
                            });
                       
						} 
                   
                    });
                        

                          // validation
                          

                          // Tooltip
                           $("[data-toggle=sample]").tooltip();
                           $("[data-toggle=report]").tooltip();
                           $("[data-toggle=receipt]").tooltip();
                           $("[data-toggle=cancel]").tooltip();   
                           $("[data-toggle=deleted]").tooltip();   

                          var fullurl = window.location.pathname.split("/"); 
                        var uri_segment = fullurl[4];
                        if(uri_segment != "edit"){
                        var url = "getvalue";
                        var patients_id=$(".patients_id");
                        patients_id.empty();
                        var token = $("input[name=_token]").val();
                        var fullurl = window.location.pathname.split("/"); 
                        var uri_segment = fullurl[3];                          
                            $.ajax({
                                type:"post",
                                dataType: "json",
                                url: url,                       
                                data: {
                                    _token: token,
                                },                        
                                success: function( responses ){
                                // console.log(responses);  
                                // alert(responses);                 
                                patients_id.append(responses);
                                  
                                },
                               
                            });
                      }

                  ');
        });
    }
}
