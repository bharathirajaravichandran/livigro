<?php

namespace App\Admin\Controllers;

use App\Models\Cities;
use App\Models\States;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class AddCitiesController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Cities::class, function (Grid $grid) {

            $grid->model()->orderBy('id', 'desc');
            $grid->id('ID')->sortable();
            $grid->column('name','City Name');
            $grid->column('state_id','State Name')->display(function ($title) {
                return  States::where('id', $this->state_id)
                    ->pluck('name')->first();
                });
             // $grid->model()->orderBy('id', 'asec');
                $grid->actions(function ($actions) {
                         $actions->disableDelete();
                  });
                $grid->tools(function ($tools) {
                     $tools->batch(function ($batch) {
                       $batch->disableDelete();
                    });
                });
                 $grid->disableRowSelector();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Cities::class, function (Form $form) {

            $form->select('state_id','State')->options(States::all()->pluck('name','id'))->rules('required');
            $form->text('name','City Name')->rules('required');

        });
    }
}
