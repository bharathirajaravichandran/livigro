<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ReportToPatient extends Mailable
{
    use Queueable, SerializesModels;

    protected $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.report_email')->with([
                        'link' => $this->data['link'],
                    ])->attachData(base64_decode($this->data['pdf']), $this->data['name'], [
                'mime' => 'application/pdf',
            ]);
       /* return $this->view('email.report_email');*/
    }
}
