<?php

namespace App\Exports;
use Excel;
use App\Models\RaiseTest;
use App\Models\Receipt;
use App\Models\Package;
use App\Models\LabsAvaTest;
use App\Models\LabTestGroup;
use App\User;
use Request;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
class UserDataExcelExpoter implements FromView
{
    public function view(): View
    {
        //return User::all();
        $id= Request::segment(3);
        $labtest = RaiseTest::find($id);
        $labtests = RaiseTest::where('id',$id)->get();
        $raise_test_id = $id;        
        $patient = $labtest->getPatient;
        $receipt_data = array();
        $receipt_data_package = Receipt::where('raise_test_id',$raise_test_id)
                             ->with('testData')
                             ->get()->toArray();
                   
        foreach ($receipt_data_package as $key => $value){
          if($value['test_type']!='' && $value['test_type'] != NULL){
            if($value['test_type'] == 'single'){
              $receipt_data['single'.$value['tests_id']] = $value;
              $receipt_data['single'.$value['tests_id']]['name'] = $value['test_data']['name'];
              $receipt_data['single'.$value['tests_id']]['test_name'] = $value['test_data']['name'];
            }else{
              if(isset($value['test_data']) && $value['test_data'] != ''){
                $packageDetails = Package::where('id',$value['test_type'])->first();
                $packageAmount = LabsAvaTest::where('lab_tests_id',$value['test_type'])->where('lab_id',$labtest->lab_id)->where('test_type','group')->first();
                $receipt_data[$value['test_type']]['id'] = $value['test_type'];
                $receipt_data[$value['test_type']]['test_type'] = $value['test_type'];
                $receipt_data[$value['test_type']]['packageName'] = $packageDetails->package_name;
                $receipt_data[$value['test_type']]['amount'] = (isset($packageAmount) && $packageAmount->amount != null && $packageAmount->amount!='')?$packageAmount->amount:$packageDetails->amount;
                foreach ($packageDetails->profile_id as $key => $val) {
                    $packageTestDetails = LabTestGroup::where('id',$val)->first()->toArray();
                    $receipt_data[$value['test_type']]['package'][$packageTestDetails['id']]['id'] = $packageTestDetails['id'];
                    $receipt_data[$value['test_type']]['package'][$packageTestDetails['id']]['groupName'] = $packageTestDetails['name'];
                     $receipt_data[$value['test_type']]['package'][$packageTestDetails['id']]['showName'] = $packageTestDetails['show_profile_name'];
                    foreach($packageTestDetails['tests_id'] as $k => $v){
                      $testDetails = Receipt::where('raise_test_id',$id)->where('tests_id',$v)->with('testData')->first();
                      $receipt_data[$value['test_type']]['package'][$packageTestDetails['id']]['testList'][$v] =  $testDetails;
                    }
                }
              }
            }
          }
        }             
        $params = ['labtest'=>$labtest,'patient'=>$patient,'receipt_data'=>$receipt_data];
        return view('labtest.labtest_detail',compact('labtest','patient','receipt_data'));


    }
 }
