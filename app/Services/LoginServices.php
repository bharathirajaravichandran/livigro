<?php 
namespace App\Services;		
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use DB;
use App\Models\Doctors;
use App\Models\Hospital;
use App\Models\Labs;
use App\Models\DocPatient;
use App\Models\Patients;
use App\Models\LabsTest;
use App\Models\LabsAvaTest;
use App\Models\RaiseTest;
use App\Models\WorkingTime;
use App\Models\Report;
use App\Models\Notification;
use App\Models\Receipt;
use App\Models\DocLab;
use App\Models\Country;
use App\Models\Blood;
use App\Models\LabsCategory;
use JWTAuth;
use App\Models\CartTest;



class LoginServices { 
	/**
	 * [getAuthUser description]
	 * @param  [type] $token [description]
	 * @return [type]        [description]
	 */
	public function getAuthUser($token){		
		$user = JWTAuth::toUser($token);
		return $user;
	}

	/**
	 * Refresh the JWT token using the expired one
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function refreshToken(Request $request) {
		$token = $request->token;
		try {
			if (! $user = JWTAuth::parseToken()->authenticate()) {
				return response()->json(['user_not_found'], 404);
			}
		}catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
			$refreshedToken = JWTAuth::refresh($token);
			$token = $refreshedToken;
			return response()->json(compact('token'));
		}
		return response()->json(['user not fount'],404);
	}

	/**
	 * this function used to get the user id from the Token
	 * @param  [type] $token [description]
	 * @return [type]        [description]
	 */
	private function getUserId($token) {
		$user = $this->getAuthUser($token);
		$user_detailed = $this->getDetailedData($user);
		return $user_detailed->id;
	}
	/**
	 * Get Detailed data from Doctor,Lab and patient Table
	 * @param  [type] $user [description]
	 * @return [type]       [description]
	 */
	private function getDetailedData($user) {
		if($user['user_type_id'] == 2) { //Doctor
			$details = Doctors::find($user['linked_id']);
		}elseif($user['user_type_id'] == 1){ //lab
			$details = Labs::find($user['linked_id']);
		}elseif($user['user_type_id'] == 3){ //Patients
			$details = Patients::find($user['linked_id']);
		}
		return $details;
	}
	/**
	 * Login Request
	 * We have called this method from ApiController login method
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function login(Request $request){
		/*error_log(print_R($request->all(),true),3,'/var/www/html/error.log');*/
		if($request->phone == '' || $request->password == ''){			
			$response['status']  = config('resource.status_401');
			$response['message'] = config('resource.parameter_missing');
			return response()->json($response,config('resource.status_200'));
			$response['userIsBlocked'] = 1;
		 }
		 $user_check = User::where('username',$request->phone)
		                   ->where('user_type_id',1)->get();
		 $user_check_labtable = Labs::where('mobile',$request->phone)->get();
       if($user_check->count() !=0 && $user_check_labtable->count() !=0){
       	  $user_checking = User::where('username',$request->phone)
		                       ->where('user_type_id',1)->first();		                      
       	         if(!password_verify($request->password,$user_checking->password)){              
		          $response['message']    = config('resource.password_wrong');
		          $response['status'] = config('resource.status_420');
		          $response['userIsBlocked'] = 1;
		          return response()->json($response,config('resource.status_200'));
       	         }/*elseif($user_checking->status == 0){
       	          $response['message']    = config('resource.inactive_user');
		          $response['status'] = config('resource.status_420');
		          return response()->json($response,config('resource.status_200'));
       	         }elseif($user_checking->status == 2){
                  $response['message']    = config('resource.blocked_user');
		          $response['status'] = config('resource.status_420');
		          return response()->json($response,config('resource.status_200'));
       	         }*/      	  
            }else{
            	$response['message']    = config('resource.user_doesnt_exists');
		        $response['status'] = config('resource.status_420');
		        $response['userIsBlocked'] = 1;
		        return response()->json($response,config('resource.status_200'));            	
            }
		$credentials = array(
				'username' => $request->phone,
				'password' => $request->password,
				'user_type_id' =>1
				);
		$token = null;
		try {
			if (!$token = JWTAuth::attempt($credentials)) {
				return response()->json([
						'response' => config('resource.error'),
						'message' => config('resource.user_doesnt_exists'),
						'status' => config('resource.status_420'),
						'userIsBlocked' => 1						
				]);
			}
		} catch (JWTAuthException $e){
			return response()->json([
					'response' => config('resource.error'),
					'message' => config('resource.failed_to_create_token'), 
					'status' => config('resource.status_420'),
					'userIsBlocked' => 1
			]);
		}			
		$user = $this->getAuthUser($token);
		$data = $this->getDetailedData($user);
		/*if($data == null || $data == '')
		   {
					$response['status'] = config('resource.status_400');
				    $response['message'] = config('resource.user_doesnt_exists');
				    return response()->json($response,config('resource.status_200'));
		   }*/
		//update fcmkey
		  if($request->filled('fcm_key')){
			$user = User::where('id',$user['id'])->first();			
			$user->fcm_key = ($request->fcm_key) ? $request->fcm_key : 0;
			$user->save();
		   }
		//update end
		//LiviGro Login Process
		//TODO need to check the settings
		//When settings is enabled need to store the log
		$value = DB::table('fc_WSAuth')->first()->enable_log;
		if($value)   {   
			storelivigroactivity($user,config('resource.login_process'));
		}
		return response()->json([
				'response' => config('resource.success'),
				'status' => config('resource.status_200'),
				'userIsBlocked' => ($data->status ==1) ? 0 : 1,				
				'data' => $data,
				'token' => $token,
				'result' => [
				'token' => $token,
				],
		]);

	}

	public function Doctorlogin(Request $request){
		$credentials = array(
				'username' => $request->phone,
				'password' => $request->password,
				'user_type_id' =>2
				);
		$token = null;
		try {
			if (!$token = JWTAuth::attempt($credentials)) {
				return response()->json([
						'response' => config('resource.error'),
						'message' => config('resource.user_doesnt_exists'),
						'status' => config('resource.status_420'),
						'userIsBlocked' => 1
				]);
			}
		} catch (JWTAuthException $e){
			return response()->json([
					'response' => config('resource.error'),
					'message' => config('resource.failed_to_create_token'), 
					'status' => config('resource.status_420'),
					'userIsBlocked' => 1
			]);
		}		
		$user = $this->getAuthUser($token);
		$data = $this->getDetailedData($user);
		   //update fcmkey
		     if($request->filled('fcm_key')){
			$users = User::where('id',$user['id'])->first();			
			$users->fcm_key = $request->fcm_key;
			$users->save();
                 }        
	        $detailed_data['fcm_key'] = $user->fcm_key; 
	       //fcm key get end

		//update end
		//LiviGro Login Process
		//TODO need to check the settings
		//When settings is enabled need to store the log
		$value = DB::table('fc_WSAuth')->first()->enable_log;
		if($value)   {   
			storelivigroactivity($user,config('resource.login_process'));
		}
		return response()->json([
				'response' => config('resource.success'),
				'status' => config('resource.status_200'),
				'data' => $data,
				'userIsBlocked' =>($data->status ==1) ? 0 : 1,
				'token' => $token,
				'result' => [
				'token' => $token,
				],
		]);

	}
	/**
	 * [checkUserExist description]
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function checkUserExist(Request $request){
		/*error_log(print_R($request->all(),true),3,'/var/www/html/error.log');*/
		$phone = $request->phone;
		$type = $request->type;
		if($phone !='' && $type !=''){
			$user = User::where('username',$phone)->where('user_type_id',$type)->get();						
			$lab_user = Labs::where('mobile',$phone)->get();			
			if($lab_user->count() ==0 || $user->count() ==0){
				
				    $response['status'] = config('resource.status_400');
				    $response['message'] = config('resource.user_doesnt_exists');
				    $response['userIsBlocked'] = 1 ;				    
				    return response()->json($response,config('resource.status_200'));
			}				
			$user = User::where('username',$phone)->where('user_type_id',$type)->first();	
			/*if($user->status == 0){
       	          $response['message']    = config('resource.inactive_user');
		          $response['status']     = config('resource.status_420');
		          return response()->json($response,config('resource.status_200'));
       	         }elseif($user->status == 2){
                  $response['message']    = config('resource.blocked_user');
		          $response['status']     = config('resource.status_420');
		          return response()->json($response,config('resource.status_200'));
       	         }*/

			if($user){
				$token = null;
				try {
					if (!$token = JWTAuth::fromUser($user)) {
						return response()->json([
								'response' => config('resource.error'),
								'message' => config('resource.invalid_email_or_password'),
								'userIsBlocked' => 1 								
						]);
					}
				} catch (JWTAuthException $e) {
					return response()->json([
							'response' => config('resource.error'),
							'message' => config('resource.failed_to_create_token'),
							'userIsBlocked' => 1							
					]);
				}
				$detailed_data = $this->getDetailedData($user);	
				if($detailed_data == null || $detailed_data == ''){
					$response['status'] = config('resource.status_400');
				    $response['message'] = config('resource.user_doesnt_exists');
				    $response['userIsBlocked'] = 1;
				    return response()->json($response,config('resource.status_200'));
				}
				$detailed_data['aadhar_images']  = '';
				$detailed_data['gst_certificate_images'] = '';
				$detailed_data['nabl_certificates'] = '';
				$detailed_data['signatures'] = '';   
			 //fcm key get start
	         if($request->filled('fcm_key')){
	     	 $user->fcm_key   = $request->fcm_key;
	     	 $user->save(); }
	         $detailed_data['fcm_key'] = $user->fcm_key; 
	        //fcm key get end      
				if($detailed_data['aadhar_image'] !='' && $detailed_data['aadhar_image'] !='null' && isset($detailed_data['aadhar_image']))
				{   
					$aadhar_image  = getarraylastimages($detailed_data['aadhar_image']);       
					$detailed_data['aadhar_images'] = $aadhar_image ;
				}
				if($detailed_data['gst_certificate_image'] !='' && $detailed_data['gst_certificate_image'] !='null' && isset($detailed_data['gst_certificate_images']))
				{
					$gst_certificate_image = getarraylastimages($detailed_data['gst_certificate_image']) ;
					$detailed_data['gst_certificate_images'] = $gst_certificate_image;
				}
				if($detailed_data['nabl_certificate'] !='' && $detailed_data['nabl_certificate'] !='null' && isset($detailed_data['nabl_certificate']))
				{
					$nabl_certificate = getarraylastimages($detailed_data['nabl_certificate']) ;
					$detailed_data['nabl_certificates'] = $nabl_certificate;
				}  	 
				if($detailed_data['signature'] !='' && $detailed_data['signature'] !='null' && isset($detailed_data['signature']) )
				{ 
					$signature = getarraylastimages($detailed_data['signature']) ;
					$detailed_data['signatures'] = urlencode($signature);			
				}
				$response['status']  = config('resource.status_200');
				$response['message'] = config('resource.user_exists');
				$response['data'] = $detailed_data;
				$response['userIsBlocked'] = ($detailed_data->status ==1) ? 0 : 1;
				$response['token'] = $token;
				$value = DB::table('fc_WSAuth')->first()->enable_log;
				if($value){   
					storelivigroactivity($user,config('resource.user_exist_process'));				
				}
				return response()->json($response,config('resource.status_201'));		

			}else
			{
				$response['status'] = config('resource.status_400');
				$response['message'] = config('resource.user_doesnt_exists');
				$response['userIsBlocked'] = 1 ;
				return response()->json($response,config('resource.status_200'));
			}		
		}else
		{
			$response['status']  = config('resource.status_401');
			$response['message'] = config('resource.parameter_missing');
			$response['userIsBlocked'] = 1 ;
			return response()->json($response,config('resource.status_401'));
		}
	}

/**
	 * [checkUserExist description]
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function checkDoctorExist(Request $request){		
		/*error_log(print_R($request->all(),true),3,'/var/www/html/error.log');*/
		$phone = $request->phone;
		$type = $request->type;
		if($phone !='' && $type !=''){
			$user = User::where('username',$phone)->where('user_type_id',$type)->get();						
			$lab_user = Doctors::where('mobile',$phone)->get();			
			if($lab_user->count() ==0 || $user->count() ==0){				
				    $response['status'] = config('resource.status_400');
				    $response['message'] = config('resource.user_doesnt_exists');
				    $response['userIsBlocked'] = 1 ;				    
				    return response()->json($response,config('resource.status_200'));
			}						
			$user = User::where('username',$phone)->where('user_type_id',$type)->first();			
			if($user){
				$token = null;
				try {
					if (!$token = JWTAuth::fromUser($user)) {
						return response()->json([
								'response' => config('resource.error'),
								'message' => config('resource.invalid_email_or_password'),
								'userIsBlocked' => 1 								
						]);
					}
				} catch (JWTAuthException $e) {
					return response()->json([
							'response' => config('resource.error'),
							'message' => config('resource.failed_to_create_token'),
							'userIsBlocked' => 1							
					]);
				}
				$detailed_data = $this->getDetailedData($user);					
				if($detailed_data == null || $detailed_data == ''){
					$response['status'] = config('resource.status_400');
				    $response['message'] = config('resource.user_doesnt_exists');
				    $response['userIsBlocked'] = 1;
				    return response()->json($response,config('resource.status_200'));
				}				
				$detailed_data['aadhar_image'] = '';
				$detailed_data['profile_image'] = '';
				$detailed_data['signature'] = '';   
			 //fcm key get start
	         if($request->filled('fcm_key')){
	     	 $user->fcm_key   = $request->fcm_key;
	     	 $user->save(); }
	         $detailed_data['fcm_key'] = $user->fcm_key; 
	        //fcm key get end      
				if(isset($detailed_data['aadhar_image']) && $detailed_data['aadhar_image'] !='' && $detailed_data['aadhar_image'] !='null')
				{   
					$aadhar_image  = getarraylastimages($detailed_data['aadhar_image']);       
					$detailed_data['aadhar_image'] = $aadhar_image ;
				}				
				if(isset($detailed_data['profile_image']) && $detailed_data['profile_image'] !='' && $detailed_data['profile_image'] !='null')
				{
					$profile_image = getarraylastimages($detailed_data['profile_image']) ;
					$detailed_data['profile_image'] = $profile_image;
				}  	 
				if(isset($detailed_data['signature']) && $detailed_data['signature'] !='' && $detailed_data['signature'] !='null')
				{ 
					$signature = getarraylastimages($detailed_data['signature']) ;
					$detailed_data['signature'] = urlencode($signature);			
				}
				$response['status']  = config('resource.status_200');
				$response['message'] = config('resource.user_exists');
				$response['data'] = $detailed_data;
				$response['userIsBlocked'] = ($detailed_data->status ==1) ? 0 : 1;
				$response['token'] = $token;
				$value = DB::table('fc_WSAuth')->first()->enable_log;
				if($value){   
					storelivigroactivity($user,config('resource.user_exist_process'));				
				}
				return response()->json($response,config('resource.status_201'));		

			}else
			{
				$response['status'] = config('resource.status_400');
				$response['message'] = config('resource.user_doesnt_exists');
				$response['userIsBlocked'] = 1 ;
				return response()->json($response,config('resource.status_200'));
			}		
		}else
		{
			$response['status']  = config('resource.status_401');
			$response['message'] = config('resource.parameter_missing');
			$response['userIsBlocked'] = 1 ;
			return response()->json($response,config('resource.status_401'));
		}
	}

	public function signUp(Request $request) {
		/*error_log(print_R($request->all(),true),3,'/var/www/html/error.log'); */
		 $user_type = $request->user_type;
		 if($request->filled('user_type')){
			 if($user_type !=1 && $user_type !=2  && $user_type !=3)
			 { 
			 	 $response['status']  = config('resource.status_401');
				 $response['message'] = config('resource.type_wrong');
				 $response['userIsBlocked'] = 1 ;
				return response()->json($response,config('resource.status_401'));
			 }	
		 }	 
		$validator = Validator::make($request->all(), [
				'username' => 'required|integer',
				'user_type' => 'required',
		]);
		if ($validator->fails()) {
			$response['message'] = $validator->messages();
			$response['status'] = config('resource.status_400');
			$response['userIsBlocked'] = 1 ;
			return response()->json($response,config('resource.status_200'));
		}
		
		$phone  = $request->username;

		if($user_type == '2') { 
			//TODO need to create one record in user table in detail table
			$detail_data = new Doctors();
			$role_id = 2;
			$detail_data->gender=1;			
		}elseif($user_type == '1'){
			$detail_data = new Labs();
			$role_id = 3;			
		}elseif($user_type == '3'){
			$detail_data = new Patients();
			$role_id = 4;
		}
		$detail_data->mobile = $phone;
		$detail_data->status = 1; // Always user is inactive status
		$detail_data->save();



		$user = new User();
		$user->username = $phone;
		$user->linked_id = $detail_data->id;
		$user->user_type_id = $user_type;
		if($request->filled('fcm_key')){
			$user->fcm_key = ($request->fcm_key) ? $request->fcm_key: '';
		}if($request->filled('device_id')){
			$user->device_id   = ($request->device_id) ? $request->device_id :'';
		}if($request->filled('device_type')){
			$user->device_type = ($request->device_type) ? $request->device_type :'';
		}		
		$user->status = 1;
		$user->save(); 
		//default add test lab  
		$tests = LabsTest::limit(150)->where('status',1)->get(); 
		if($tests){  
			foreach ($tests as $key => $value) {		 	     	   
				$labs = new LabsAvaTest();
				$labs->lab_id        = $detail_data->id;
				$labs->lab_tests_id  = $value->id ;
				$labs->amount        = $value->price;
				$labs->range         = $value->reference_range;
				$labs->range_end     = $value->reference_endrange;
				$labs->specimen      = '';
				$labs->status        = 1 ;
				$labs->test_type     = 'single';
				$labs->save(); 				 	   
			}	
		}    		  
		//default add test lab 
		DB::table('admin_role_users')
			->insert(['role_id'=>$role_id,'user_id'=>$user->id]);
		if($user_type == 1) //lab
			WorkingTime::create(['lab_id'=>$detail_data->id]);
		elseif($user_type == 2) //doctor
			WorkingTime::create(['doctor_id'=>$detail_data->id]);
		$token = null;
		try {
			if (!$token = JWTAuth::fromUser($user)) {
				return response()->json([
						'response' => config('resource.error'),
						'message' => config('resource.invalid_email_or_password'),
						'status' => config('resource.status_400'),
						'userIsBlocked' => 1						
				]);
			}
		} catch (JWTAuthException $e) {
			return response()->json([
					'response' => config('resource.error'),
					'message'  => config('resource.failed_to_create_token'),
					'userIsBlocked' => 1
			]);
		}

		$data['data']    = $detail_data->toArray();
		$data['message'] = config('resource.signup_success'); 
		$data['status']  = config('resource.status_200');
		$data['userIsBlocked'] = ($detail_data->status ==1) ? 0 : 1;
		$data['token']    = $token;
		$value = DB::table('fc_WSAuth')->first()->enable_log;
		if($value)   {   
			storelivigroactivity($user,config('resource.signup_process'));
		}
		return response()->json($data,config('resource.status_200'));
	} 


}



