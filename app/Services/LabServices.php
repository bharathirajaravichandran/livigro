<?php 
namespace App\Services;	
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use DB;
use App\Models\Doctors;
use App\Models\Hospital;
use App\Models\Labs;
use App\Models\DocPatient;
use App\Models\Patients;
use App\Models\LabsTest;
use App\Models\LabsAvaTest;
use App\Models\RaiseTest;
use App\Models\WorkingTime;
use App\Models\Report;
use App\Models\Notification;
use App\Models\Receipt;
use App\Models\DocLab;
use App\Models\Country;
use App\Models\Blood;
use App\Models\LabsCategory;
use App\Models\Contacts;
use App\Models\Units;
use App\Models\LabTestGroup;
use App\Models\Package;
use App\Models\Countries;
use App\Models\States;
use App\Models\Cities;
use App\Models\FAQ;
use App\Models\Subscription;
use JWTAuth;
use App\Models\CartTest;
use Excel;
use PDF;
use App\Admin\Extension\ExcelExpoter;
use Illuminate\Support\Facades\Mail;
use App\Mail\ReportToPatient;

use App\Exports\UserDataExcelExpoter;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class LabServices {


	/**
	 * [getAuthUser description]
	 * @param  [type] $token [description]
	 * @return [type]        [description]
	 */
	public function getAuthUser($token){
		$user = JWTAuth::toUser($token); 
		return $user;
	}

	/**
	 * Refresh the JWT token using the expired one
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function refreshToken(Request $request) {
		$token = $request->token;
		try {

			if (! $user = JWTAuth::parseToken()->authenticate()) {
				return response()->json([config('resource.user_not_found')],config('resource.status_404'));
			}

		} catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

			$refreshedToken = JWTAuth::refresh($token);
			$token = $refreshedToken;
			return response()->json(compact('token'));
		}
		return response()->json([config('resource.user_not_found')],config('resource.status_404'));
	}

	/**
	 * this function used to get the user id from the Token
	 * @param  [type] $token [description]
	 * @return [type]        [description]
	 */
	private function getUserId($token) {
		$user = $this->getAuthUser($token);
		$user_detailed = $this->getDetailedData($user);
		return $user_detailed->id;
	}

	/**
	 * this function used to get the user status from the Token
	 * @param  [type] $token [description]
	 * @return [type]        [description]
	 */
	private function getUserStatus($token) {				
			$user = $this->getAuthUser($token);
			$user_detailed = $this->getDetailedData($user);
			if($user_detailed->status == 1)
              return 0 ;
			else
			  return 1 ;
	}

	/**
	 * Get Detailed data from Doctor,Lab and patient Table
	 * @param  [type] $user [description]
	 * @return [type]       [description]
	 */
	private function getDetailedData($user) {

		if($user['user_type_id'] == 2) { //Doctor
			$details = Doctors::find($user['linked_id']);
		}elseif($user['user_type_id'] == 1){ //lab
			$details = Labs::find($user['linked_id']);
		}elseif($user['user_type_id'] == 3){ //Patients
			$details = Patients::find($user['linked_id']);
		}
		return $details;
	}
	/**
	 * Get All test from database
	 * @return [type] [description]
	 */
	public function alltest() {
		$tests = LabsTest::where('status',1)->get();
		if($tests){
			$response['data'] = $tests;
			$response['status'] = config('resource.status_200');
		}
		else{
			$response['data'] = config('resource.record_not_found');
			$response['status'] = config('resource.status_400');
		}
		return response()->json($response,config('resource.status_200'));

	}

	/**
	 * Get all Lab Test
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function lab_alltest(Request $request) {		
		/*error_log(print_r($request->all(),true),3,'/var/www/html/error.log');*/
		/* $test3 = LabsAvaTest::where('lab_id',$request->lab_id)->get();
		   if(count($test3) ==0)
		   { 
		   $response['msg'] = 'No Record';                     
		   $response['status'] = 201;
		   return response()->json($response,201); 
		   }*/			 
		$lab_id = $this->getUserId($request->token);
		$test1 = LabsTest::where('status',1)->orderBy('name','asc')->jsonPaginate();
		$test2 = LabsAvaTest::where('lab_id',$lab_id)->where('status',1)->get(); 
		//package test id get
		$packagetest_id = LabsAvaTest::where('lab_id',$lab_id)
		                             ->where('test_type','group')
		                             ->pluck('lab_tests_id')
		                             ->toArray();
		 if(count($packagetest_id) !=0)
		 {    $package_test_available = Package::whereNotIn('id',$packagetest_id)->get()->toArray();
		 }else{
		 	  $package_test_available = Package::all()->toArray();
		 }
/* dd($package_test_available);*/
		//package test id get		
		if(count($test2) ==0)
		{ 
			$serach_key = null;
			if($request->input('serach_key')){                               
				$serach_key = $request->input('serach_key');
			}                             
			$test1 = LabsTest::where(function ($query) use($serach_key){
					$query->Where('name','ILIKE',"%{$serach_key}%");					
					})
			->where('status',1)
			->orderBy('name','asc')
			->jsonPaginate(); 
			$response['data'] = $test1;                     
			$response['status'] = config('resource.status_200');
			$response['userIsBlocked'] = $this->getUserStatus($request->token);
			return response()->json($response,config('resource.status_200')); 
		}
		$test_ids = array();
		foreach($test2 as $key=>$tes)
		{ 
			$test_ids[] = $tes->lab_tests_id ;
		}       
		/*$testing =  LabsTest::whereNotIn('id',$test_ids)->jsonPaginate();*/ 
		$serach_key = null;
		if($request->input('serach_key')){                               
			$serach_key = $request->input('serach_key');
		}
		$testing =  LabsTest::whereNotIn('id',$test_ids)->where('status',1);   
		$testing =  $testing->where(function ($query) use($serach_key){
				$query->where('name','ILIKE',"%{$serach_key}%");				
				})		
		->orderBy('name','asc')
		->jsonPaginate();      
		if(count($testing) !=0)
		{
			$response['data'] = $testing;
			$response['status'] = config('resource.status_200');
			$response['userIsBlocked'] = $this->getUserStatus($request->token);
		}
		else
		{
			$response['msg'] = config('resource.no_record');
			$response['status'] = config('resource.status_400');
			$response['userIsBlocked'] = $this->getUserStatus($request->token);
		}

		//TODO need to check the condition
		$user = $this->getAuthUser($request->token);
		$value = DB::table('fc_WSAuth')->first()->enable_log;
		if($value)   {
			storelivigroactivity($user,config('resource.Get_Labs_Availabe_Test')); 
		}
		return response()->json($response,config('resource.status_200'));
	}

	/**
	 * Update the Lab test data
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
/*	public function lab_alltest_update(Request $request) {		
		error_log(print_r($request->all(),true),3,'/var/www/html/error.log');
		$lab_id = $this->getUserId($request->token);
		$tests = LabsTest::where('id',$request->lab_test_id)->where('status',1)->first(); 		
		if($tests)
		{      
			$values = LabsAvaTest::where('lab_tests_id',$tests->id)
				->where('lab_id',$lab_id)
				->where('status',1)
				->get();

			if(count($values) ==0)
			{   
				$deactive_data = LabsAvaTest::where('lab_tests_id',$tests->id)
				->where('lab_id',$lab_id)
				->where('status',0)
				->get();
				if($deactive_data->count() !=0)
				{
                        LabsAvaTest::where([['lab_tests_id',$tests->id],['lab_id',$lab_id]])
			               ->update(['status'=>1]);
			            $response['msg'] = config('resource.updated_successfully');
						$response['status'] = config('resource.status_200');
						$response['userIsBlocked'] = $this->getUserStatus($request->token);
						return response()->json($response,config('resource.status_200'));	
				}
              
				$labs = new LabsAvaTest();
				$labs->lab_id        = $lab_id;
				$labs->lab_tests_id  = $request->lab_test_id ;
				$labs->amount        = $tests->price;
				$labs->range         = $tests->reference_range;
				$labs->range_end     = $tests->reference_endrange;
				$labs->specimen      = '';
				$labs->status        = 1 ;
				$labs->save();
				
				$response['msg']      = config('resource.added_successfully');
				$response['status'] = config('resource.status_200');
				$response['userIsBlocked'] = $this->getUserStatus($request->token);				
				$user = $this->getAuthUser($request->token);
				$value = DB::table('fc_WSAuth')->first()->enable_log;
				if($value){
					storelivigroactivity($user,config('resource.update_lab_their_own_test_data'));
				}
				return response()->json($response,config('resource.status_200'));
			}else
			{ 				
				LabsAvaTest::where([['lab_tests_id',$tests->id],['lab_id',$lab_id]])
			               ->update(['status'=>0]);				  
				$value = DB::table('fc_WSAuth')->first()->enable_log;
				if($value)   {   
					storelivigroactivity($user,config('resource.lab_removed_their_available_test'));
				}                              
				$response['msg']      = config('resource.removed_successfully');                
				$response['status']    = config('resource.status_200');
				$response['userIsBlocked'] = $this->getUserStatus($request->token);
				return response()->json($response,config('resource.status_200'));                   
			}                      
		}else
		{
			$response['msg'] = config('resource.no_record');
			$response['status'] = config('resource.status_400');
			$response['userIsBlocked'] = $this->getUserStatus($request->token);
			return response()->json($response,config('resource.status_200'));
		}              
	}*/


	/**
	 * Update the Lab test data
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
 public function lab_alltest_update(Request $request) {	 	
		error_log(print_r($request->all(),true),3,'/var/www/html/error.log');
		if($request->type !='single' && $request->type !='group')
			 { 
			 	 $response['status']  = config('resource.status_401');
				 $response['message'] = config('resource.type_wrong');
				 $response['userIsBlocked'] = $this->getUserStatus($request->token);
				return response()->json($response,config('resource.status_401'));
			 }	
		$lab_id = $this->getUserId($request->token);
	//group test add method start		
	if($request->type == 'group')
	{ 
          $tests = Package::where('id',$request->lab_test_id)->where('status',1)->first();          
        if($tests)
		 {      
			$values = LabsAvaTest::where('lab_tests_id',$tests->id)
				->where('lab_id',$lab_id)
				->where('status',1)
				->get();
			if(count($values) ==0)
			{   
				$deactive_data = LabsAvaTest::where('lab_tests_id',$tests->id)
				->where('lab_id',$lab_id)
				->where('status',0)
				->get();
				if($deactive_data->count() !=0)
				{
                        LabsAvaTest::where([['lab_tests_id',$tests->id],['lab_id',$lab_id]])
			               ->update(['status'=>1]);
			            $response['msg'] = config('resource.added_successfully');
						$response['status'] = config('resource.status_200');
						$response['userIsBlocked'] = $this->getUserStatus($request->token);
						return response()->json($response,config('resource.status_200'));	
				}
              
				$labs = new LabsAvaTest();
				$labs->lab_id        = $lab_id;
				$labs->lab_tests_id  = $request->lab_test_id;              
				$labs->amount        = $tests->amount;
				$labs->test_type     = 'group';			    
				$labs->lab_package_id= $request->lab_test_id;			    
				$labs->specimen      = '';
				$labs->status        = 1 ;
				$labs->save();

				/* $response['data']   = $labs;*/
				$response['msg']      = config('resource.added_successfully');
				$response['status'] = config('resource.status_200');
				$response['userIsBlocked'] = $this->getUserStatus($request->token);
				//TODO need to check the condition
				$user = $this->getAuthUser($request->token);
				$value = DB::table('fc_WSAuth')->first()->enable_log;
				if($value){
					storelivigroactivity($user,config('resource.update_lab_their_own_test_data'));
				}
				return response()->json($response,config('resource.status_200'));
			}else
			{ 				
				LabsAvaTest::where([['lab_tests_id',$tests->id],['lab_id',$lab_id]])
			               ->update(['status'=>0]);				  
				$value = DB::table('fc_WSAuth')->first()->enable_log;
				if($value){   
					storelivigroactivity($user,config('resource.lab_removed_their_available_test'));
				}                              
				$response['msg']      = config('resource.removed_successfully');                
				$response['status']    = config('resource.status_200');
				$response['userIsBlocked'] = $this->getUserStatus($request->token);
				return response()->json($response,config('resource.status_200'));                   
			}                      
		}else
		  {
			$response['msg'] = config('resource.no_record');
			$response['status'] = config('resource.status_400');
			$response['userIsBlocked'] = $this->getUserStatus($request->token);
			return response()->json($response,config('resource.status_200'));
		  } 
	}
	// group test add method end

		$tests = LabsTest::where('id',$request->lab_test_id)->where('status',1)->first();		
		if($tests)
		{      
			$values = LabsAvaTest::where('lab_tests_id',$tests->id)
				->where('lab_id',$lab_id)
				->where('status',1)
				->get();
			if(count($values) ==0)
			{   
				$deactive_data = LabsAvaTest::where('lab_tests_id',$tests->id)
				->where('lab_id',$lab_id)
				->where('status',0)
				->get();
				if($deactive_data->count() !=0)
				{
                        LabsAvaTest::where([['lab_tests_id',$tests->id],['lab_id',$lab_id]])
			               ->update(['status'=>1]);
			            $response['msg'] = config('resource.added_successfully');
						$response['status'] = config('resource.status_200');
						$response['userIsBlocked'] = $this->getUserStatus($request->token);
						return response()->json($response,config('resource.status_200'));	
				}
              
				$labs = new LabsAvaTest();
				$labs->lab_id        = $lab_id;
				$labs->lab_tests_id  = $request->lab_test_id ;             
			    $labs->amount        = $tests->price; 
			    $labs->range         = $tests->reference_range;
				$labs->range_end     = $tests->reference_endrange; 
				$labs->test_type     = 'single';				
				$labs->specimen      = '';
				$labs->status        = 1 ;
				$labs->save();

				/* $response['data']   = $labs;*/
				$response['msg']      = config('resource.added_successfully');
				$response['status'] = config('resource.status_200');
				$response['userIsBlocked'] = $this->getUserStatus($request->token);
				//TODO need to check the condition
				$user = $this->getAuthUser($request->token);
				$value = DB::table('fc_WSAuth')->first()->enable_log;
				if($value){
					storelivigroactivity($user,config('resource.update_lab_their_own_test_data'));
				}
				return response()->json($response,config('resource.status_200'));
			}else
			{ 				
				LabsAvaTest::where([['lab_tests_id',$tests->id],['lab_id',$lab_id]])
			               ->update(['status'=>0]);				  
				$value = DB::table('fc_WSAuth')->first()->enable_log;
				if($value){   
					storelivigroactivity($user,config('resource.lab_removed_their_available_test'));
				}                              
				$response['msg']      = config('resource.removed_successfully');                
				$response['status']    = config('resource.status_200');
				$response['userIsBlocked'] = $this->getUserStatus($request->token);
				return response()->json($response,config('resource.status_200'));                   
			}                      
		}else
		{
			$response['msg'] = config('resource.no_record');
			$response['status'] = config('resource.status_400');
			$response['userIsBlocked'] = $this->getUserStatus($request->token);
			return response()->json($response,config('resource.status_200'));
		}              
	}





	/**
	 * [lab_test_selectlist description]
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function lab_test_selectlist(Request $request) {  		
				/*error_log(print_r($request->all(),true),3,'/var/www/html/error.log');*/
		$lab_id = $this->getUserId($request->token);
		//TODO need to check the condition
		$user = $this->getAuthUser($request->token);                
		//all categories get
		$datas = array();
		if($request->search_tag)
		{ $test1 = LabsTest::where('search_tags','ILIKE','%'.$request->search_tag.'%')
			                  ->where('status',1)  
			                  ->orderBy('name','asc')                   
				              ->jsonPaginate();     
			if($test1){
				$datas =  $test1 ;
			}
		}

		$test1 = LabsTest::where('status',1)->orderBy('name','asc')->get();     
		$new_array = '';
		$category_data = array();
		foreach($test1 as $key=>$tes)
		{ 
			if($tes->search_tags)
			{
				$new_array.= $tes->search_tags.',';
			}

		} 
		if($new_array)
		{
			foreach (explode(',',$new_array) as $key => $value)
			{
				if(!in_array($value,$category_data) && $value)
				{
					$category_data[] = $value ;
				}
			}                

		}
		///all category get
		$test1 = LabsTest::where('status',1)->orderBy('name','asc')->get();
		$test2 = LabsAvaTest::where('lab_id',$lab_id)->where('status',1)->get();
		if(count($test2) ==0)
		{   $response['msg']    = config('resource.no_record');
			$response['status'] = config('resource.status_201');
			$response['userIsBlocked'] = $this->getUserStatus($request->token);
			$value = DB::table('fc_WSAuth')->first()->enable_log;
			if($value)   {   
				storelivigroactivity($user,config('resource.no_record'));
			}
			return response()->json($response,config('resource.status_201')); 
		}
		$test_ids = array();
		foreach($test2 as $key=>$tes)
		{ 
			$test_ids[] = $tes->lab_tests_id ;
		}     
		if($request->search_tags){
			$search_keyword = $request->search_tags;

			$testing = LabsTest::with(['labs_available_test' => function ($query) use ($lab_id){
					$query->where('labs_available_test.lab_id', $lab_id);
					$query->where('labs_available_test.status',1);
					}])->whereIn('id',$test_ids)
			       ->where('name','ILIKE','%'.$search_keyword.'%')
			       ->where('status',1)
			       ->orderBy('name','asc')
				   ->jsonPaginate();                   
		}else{
			$testing =  LabsTest::with(['labs_available_test' => function ($query) use ($lab_id){
					$query->where('labs_available_test.lab_id', $lab_id);
					$query->where('labs_available_test.status', 1);
					}])->whereIn('id',$test_ids)
			           ->where('status',1)
			           ->orderBy('name','asc')
			           ->jsonPaginate();  
		}   
		 //get group package data
		$lab_group_package = LabsAvaTest::where('lab_id',$lab_id)
		                     ->where('test_type','!=','single')
		                     ->where('status',1)		                     
		                     ->get()->toArray();
		//get group package data   
		if(count($testing) !=0)
		{                    
			$response['data'] = ($request->search_tag) ? $datas : $testing;
			$response['package_data']  = $lab_group_package;
			$response['category']      = $category_data;
			$response['userIsBlocked'] = $this->getUserStatus($request->token);
			$response['status'] = config('resource.status_200');
		}
		else{
			$response['msg'] = config('resource.no_record');
			$response['status'] = config('resource.status_201');
			$response['userIsBlocked'] = $this->getUserStatus($request->token);
		}
		$value = DB::table('fc_WSAuth')->first()->enable_log;
		if($value){   
			storelivigroactivity($user,config('resource.lab_test_based_on_the_search_tag'));
		}
		return response()->json($response,config('resource.status_200'));
	} 


		/**
	 * Get all Lab group Test
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function getgrouppackage(Request $request) {			
		$lab_id = $this->getUserId($request->token);
		$test1 = LabsTest::where('status',1)->jsonPaginate();
		$lab_package_test = LabsAvaTest::where('lab_id',$lab_id)
		                     ->where('test_type','!=','single')
		                     ->where('status',1)
		                     ->get(); 	
        //get group package id
		$lab_package_id = LabsAvaTest::where('lab_id',$lab_id)
		                     ->where('test_type','!=','single')
		                     ->where('status',1)		                     
		                     ->pluck('lab_tests_id')->toArray();
		//get group package id               
   
		if(count($lab_package_test) ==0)
		{  
			$package_test_available = Package::where('status',1)->where('tests_id','!=','')->orderBy('package_name','asc')->jsonPaginate(); 
			$serach_key = null;
			if($request->input('search_key')){                               
				$search_key = $request->input('search_key');
				$package_test_available = Package::where('package_name','ILIKE',"%{$search_key}%")
				                                 ->where('status',1)
				                                 ->where('tests_id','!=','')
				                                 ->orderBy('package_name','asc')
				                                 ->jsonPaginate();
			} 
			if(count($package_test_available) == 0){
			$response['msg']    = config('resource.no_record');
			$response['status'] = config('resource.status_201');
			$response['userIsBlocked'] = $this->getUserStatus($request->token);
			return response()->json($response,config('resource.status_200')); 
		    }  
			$response['data'] = $package_test_available;                     
			$response['status'] = config('resource.status_200');
			$response['userIsBlocked'] = $this->getUserStatus($request->token);
			return response()->json($response,config('resource.status_200')); 
		}    
       $package_test_available = Package::whereNotIn('id',$lab_package_id)->where('tests_id','!=','')->where('status',1)->jsonPaginate();
		$serach_key = null;
		if($request->input('search_key')){                               
			$search_key = $request->input('search_key');
			$package_test_available = Package::where('package_name','ILIKE',"%{$search_key}%")
			                                 ->whereNotIn('id',$lab_package_id)
			                                 ->where('status',1)
			                                 ->where('tests_id','!=','')
			                                 ->orderBy('package_name','asc')
			                                 ->jsonPaginate();
		}   
		if(count($package_test_available) == 0){
			$response['msg']    = config('resource.no_record');
			$response['status'] = config('resource.status_201');
			$response['userIsBlocked'] = $this->getUserStatus($request->token);
			return response()->json($response,config('resource.status_200')); 
		}
		$response['data']   = $package_test_available;                     
		$response['status'] = config('resource.status_200');
		$response['userIsBlocked'] = $this->getUserStatus($request->token);
		return response()->json($response,config('resource.status_200')); 
	}


	/**
	 * Get all Lab group Test
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function getLabgrouppackage(Request $request) {					
		$lab_id = $this->getUserId($request->token);	
        //get group package id
		$lab_package_id = LabsAvaTest::where('lab_id',$lab_id)
	                     ->where('test_type','!=','single')
	                     ->where('status',1)		                     
	                     ->pluck('lab_tests_id')->toArray();
		//get group package id      		
        $package_test_available = Package::whereIn('id',$lab_package_id)->where('tests_id','!=','')->where('status',1)->orderBy('package_name','asc')->jsonPaginate();
		$serach_key = null;
		if($request->input('search_key')){                               
			$search_key = $request->input('search_key');
			$package_test_available = Package::where('package_name','ILIKE',"%{$search_key}%")
			                                 ->whereIn('id',$lab_package_id)
			                                 ->where('status',1)
			                                 ->where('tests_id','!=','')
			                                 ->orderBy('package_name','asc')
			                                 ->jsonPaginate();
		}   
		if(count($package_test_available) == 0){
			$response['msg']    = config('resource.no_record');
			$response['status'] = config('resource.status_201');
			$response['userIsBlocked'] = $this->getUserStatus($request->token);
			return response()->json($response,config('resource.status_200')); 
		}
		//get available data
		foreach ($package_test_available as $key => $value) {
	              $labavailable = LabsAvaTest::where('lab_id',$lab_id)
                                ->where('lab_tests_id',$value->id)
                                ->where('test_type','group')
                                ->where('status',1)->first();
			$value->labavailable_table_id     = isset($labavailable->id) ? $labavailable->id : 0 ;
			$value->labavailable_table_amount = isset($labavailable->amount) ? $labavailable->amount : 0;
			# code...

		}
		//get available data
		$response['data']   = $package_test_available;                     
		$response['status'] = config('resource.status_200');
		$response['userIsBlocked'] = $this->getUserStatus($request->token);
		return response()->json($response,config('resource.status_200')); 
	}

	/**
	 * [lab_test_categories description]
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function lab_test_categories(Request $request) {   
	/*error_log(print_r($request->all(),true),3,'/var/www/html/error.log');  */
		$test1 = LabsTest::where('status',1)->get();               
		$new_array = '';
		$data = array();
		foreach($test1 as $key=>$tes)
		{ 
			if($tes->search_tags)
			{
				$new_array.= $tes->search_tags.',';
			}

		} 
		if($new_array)
		{
			foreach (explode(',',$new_array) as $key => $value)
			{
				if(!in_array($value,$data) && $value)
				{
					$data[] = $value ;
				}
			} 
			$response['data']   = $data;
			$response['status'] = config('resource.status_200');
			$response['userIsBlocked'] = $this->getUserStatus($request->token);
			return response()->json($response,config('resource.status_200'));

		}else
		{
			$response['msg']    = config('resource.status_no_record');
			$response['status'] = config('resource.status_400');
			$response['userIsBlocked'] = $this->getUserStatus($request->token);
			return response()->json($response,config('resource.status_400'));

		}
	}    

	/**
	 * ProfileUpdate function
	 *
	 * @param Request $request
	 * @return void
	 */
	public function profileUpdate(Request $request) {
		error_log(print_r($request->all(),true),3,'/var/www/html/error.log'); 
		      $user_type = $request->user_type;		
			 if($user_type !=1 && $user_type !=2  && $user_type !=3)
			 { 
			 	 $response['status']  = config('resource.status_401');
				 $response['message'] = config('resource.type_wrong');
				 $response['userIsBlocked'] = $this->getUserStatus($request->token);
				return response()->json($response,config('resource.status_401'));
			 }	
		 
		/*error_log(print_r($_REQUEST,true),3,'/var/www/html/error.log');*/
		$user_id = $this->getUserId($request->token);		
		$user_id = $user_id;
		$exclude_inputs = array('user_id','user_type','token','profile_image');
		if($user_type == 1) { // labs
			$detailed_data = Labs::find($user_id);
			//mobile number check
			   /*$user_mobile = User::where('mobile',$request->mobile)
			                      ->where('linked_id','!=',$detailed_data->id)->get();
			   if($user_mobile->count() !=0){			   	    
			   	    $response['data']   = config('resource.mobile_exists');
					$response['status'] = config('resource.status_400');
					return response()->json($response,config('resource.status_200'));
			   }*/
			//mobile number check
			$mobile = $detailed_data->mobile;
			if($request->password)
			{
				$user_password = User::where('linked_id',$detailed_data->id)
					->where('user_type_id',1)
					->first();
				$user_password->password = bcrypt($request->password);
				$user_password->name     = $request->name ;
				$user_password->save();
			}

			foreach($request->all() as $key=>$value) {
				if(!in_array($key,$exclude_inputs))
					$detailed_data->$key = $value;
			}
			$detailed_data->mobile = $mobile ;
			if($request->filled('profile_image'))
			{   
				if($request->profile_image != '""')
				{ 					
					//single image                   
					$profile_images = file_upload($request->profile_image,$user_type,$detailed_data->mobile);
					$detailed_data->profile_image = $profile_images;

					//admin user table images update
					$user_avatar = User::where('linked_id',$detailed_data->id)
										->where('user_type_id',1)
										->first();
					$user_avatar->avatar = $profile_images ;					
					$user_avatar->save();
					//admin user table images update
				}             
			} 
			if($request->filled('aadhar_image'))
			{ 
				// multi image            
				$aadhar_images =  file_upload($request->aadhar_image,$user_type,$detailed_data->mobile);
				($detailed_data->aadhar_image !='null' && $detailed_data->aadhar_image !='') ? $old_image = $detailed_data->aadhar_image : 
					$old_image = $aadhar_images;
				$detailed_data->aadhar_image = imageJson($old_image,$aadhar_images);
			}

			if($request->filled('gst_certificate_image'))
			{ 

				$gst_certificate_images =  file_upload($request->gst_certificate_image,$user_type,$detailed_data->mobile);
				($detailed_data->gst_certificate_image !='null' && $detailed_data->gst_certificate_image !='') ? $old_image = $detailed_data->gst_certificate_image : 
					$old_image = $gst_certificate_images;    
				$detailed_data->gst_certificate_image = imageJson($old_image,$gst_certificate_images);    

			}

			if($request->filled('nabl_certificate'))
			{  
				// multi image            
				$nabl_certificates =  file_upload($request->nabl_certificate,$user_type,$detailed_data->mobile);    
				($detailed_data->nabl_certificate) ? $old_image = $detailed_data->nabl_certificate : 
					$old_image = $nabl_certificates;         
				$detailed_data->nabl_certificate = imageJson($old_image,$nabl_certificates);         
			}

			if($request->filled('signature')) 
			{
				// multi image            
				$signatures =  file_upload($request->signature,$user_type,$detailed_data->mobile);
				($detailed_data->signature) ? $old_image = $detailed_data->signature : 
					$old_image = $signatures; 
				$detailed_data->signature = imageJson($old_image,$signatures);
			}
		}else if($user_type == 2)
		{ //doctor
			$detailed_data = Doctors::find($user_id);
			foreach($request->all() as $key=>$value) {
				if(!in_array($key,$exclude_inputs))
					$detailed_data->$key = $value;
			}

			
			$mobile = $detailed_data->mobile;
			if($request->password)
			{
				$user_password = User::where('linked_id',$detailed_data->id)
					->where('user_type_id',2)
					->first();
				$user_password->password = bcrypt($request->password);
				$user_password->name     = $request->name ;
				$user_password->save();
			}
			$detailed_data->mobile = $mobile ;

			if($request->filled('profile_image'))
			{   
				if($request->profile_image != '""')
				{ 
					//single image                   
					$profile_images = file_upload($request->profile_image,$user_type,$detailed_data->mobile) ;            
					$detailed_data->profile_image = $profile_images;
				}             
			} 
			if($request->filled('aadhar_image'))
			{ 
				// multi image            
				$aadhar_images =  file_upload($request->aadhar_image,$user_type,$detailed_data->mobile);
				($detailed_data->aadhar_image !='null' && $detailed_data->aadhar_image !='') ? $old_image = $detailed_data->aadhar_image : 
					$old_image = $aadhar_images;
				$detailed_data->aadhar_image = imageJson($old_image,$aadhar_images);
			}


		}else if($user_type == 3)
		{ //patients

		}         

		$detailed_data->save();
		$arr = $detailed_data->toArray();
		$final = array_filter($arr,function($var){ return !is_null($var); });
		if(@$final['aadhar_image'])             
			$final['aadhar_image'] = getarraylastimages($final['aadhar_image']);
		if(@$final['gst_certificate_image'])
			$final['gst_certificate_image'] = getarraylastimages($final['gst_certificate_image']);
		if(@$final['nabl_certificate'])
			$final['nabl_certificate'] = getarraylastimages($final['nabl_certificate']);
		if(@$final['signature'])
			$final['signature'] = urlencode(getarraylastimages($final['signature']));       
		$response['data']   = $final;
		$response['status'] = config('resource.status_200');
		$response['userIsBlocked'] = $this->getUserStatus($request->token);
		$user = $this->getAuthUser($request->token);
		$value = DB::table('fc_WSAuth')->first()->enable_log;
		if($value)   {   
			storelivigroactivity($user,config('resource.profile_update_process'));
		}      
		return response()->json($response,config('resource.status_200'));
	}

	/**
	 * Profile Completion function
	 *
	 * @param Request $request
	 * @return void
	 */
	public function getProfileCompletion(Request $request){
		/*error_log(print_r($request->all(),true),3,'/var/www/html/error.log');*/
		$user_id = $this->getUserId($request->token);
		//   $user_id = $request->user_id;
		$user_type = $request->user_type;		
		//lab details
		$lab_module_activate_data = array('name', 'address', 'city', 'state', 'lat', 'lan', 'zip',
			 'promoters_name', 'profile_image', 'phone', 'email', 'mobile', 'aadhar_id', 'aadhar_image',
			 'password', 'home_collection', 'gst_registered','gst_number','gst_certificate_image',
			 'nabl_certified', 'nabl_id', 'signature', 'area');
		//lab details
        //doctor details
		$doc_module_activate_data = array('name','profile_image','state','mcid','registered',
				'phone','email','aadhar_no','aadhar_image','city');
		//doctor details
		$month_test_count = 0 ;
		if($user_type ==1 ){ //lab
			$detail = Labs::find($user_id);
			if($detail)
				foreach($detail->toArray() as $key=>$dat){
					if(in_array($key,$lab_module_activate_data))
						$original_data[$key] = $dat;
				}
			//test complete count
			$test_count = RaiseTest::where('lab_id',$user_id)->where('paid_status',1)->get();
			$month_test_count = $test_count->count();
			//test complete count

		}elseif($user_type == 2){//doctor
			$detail = Doctors::find($user_id);
			if($detail)
				foreach($detail->toArray() as $key=>$dat){
					if(in_array($key,$doc_module_activate_data))
						$original_data[$key] = $dat;
				}
		}elseif($user_type == 3){//patient
			$detail = Patients::find($user_id);
		}
		if(empty($detail)) {
			$response['data'] = config('resource.record_not_found');
			$response['status'] = config('resource.status_400');
			$response['userIsBlocked'] = $this->getUserStatus($request->token);
			return response()->json($response,config('resource.status_200'));
		}
		$subscription = Subscription::where('user_id',$user_id)->where('user_type',1)->get();
		$expire_date = "0";
		if($subscription->count() !=0){
			     $end_date = new Carbon($subscription->first()->plan_end_date);
                 $expire_date = $end_date->format('d/m/Y');                		     
		   }		 
		$profile_completness = ProfileComplete($original_data);
		$response['data'] = $profile_completness;
	$response['notification_count'] = Notification::where([['lab_id',$user_id],['read_status',0]])->count();
		$response['month_test_count'] = $month_test_count.' tests have been processed this month' ;
		$response['subscription_expire'] = $expire_date ;
		$response['status'] = config('resource.status_200');
		$response['userIsBlocked'] = $this->getUserStatus($request->token);
		$user = $this->getAuthUser($request->token);
		$value = DB::table('fc_WSAuth')->first()->enable_log;
		if($value)   {   
			storelivigroactivity($user,config('resource.profile_completion_request'));
		}
		return response()->json($response,config('resource.status_200'));

	}

	/**
	 * get patient list
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */

	public function getPatientList(Request $request) {
		if($request->user_type =='')
		{
            $response['msg'] = config('resource.parameter_missing');
			$response['status'] = config('resource.status_201');
			$response['userIsBlocked'] = $this->getUserStatus($request->token);
			return response()->json($response,config('resource.status_201'));
		}
		/*error_log(print_r($request->all(),true),3,'/var/www/html/error.log');*/
		$user_id = $this->getUserId($request->token);		
		//   $user_id = $request->user_id;		
		$user_type = $request->user_type;
		 if($request->filled('user_type')){
			 if($user_type !=1 && $user_type !=2 )
			 { 
			 	 $response['status']  = config('resource.status_401');
				 $response['msg'] = config('resource.type_wrong');
				 $response['userIsBlocked'] = $this->getUserStatus($request->token);
				return response()->json($response,config('resource.status_401'));
			 }	
		 }	 
		$serach_key = null;
		if($request->filled('search_key'))
			$serach_key = $request->search_key;

		if($user_type == 1 ){ //lab
			$detail = Labs::find($user_id);
			$de = Labs::where('id',$user_id)->count();            
			if($de==0)
			{
				$response['msg'] = config('resource.no_record');
				$response['status'] = config('resource.status_201');  
				$response['userIsBlocked'] = $this->getUserStatus($request->token);      
				return response()->json($response,config('resource.status_201'));
			}
			$patients = $detail->patients()->where(function ($query) use($serach_key)
					{
					$query->where('phone','ILIKE',"%{$serach_key}%")
					     ->orWhere('name','ILIKE',"%{$serach_key}%");
					})
			/*->distinct('id')*/
			->where('patients_details.name','!=',null)
			->where('patients_details.status',1)
			->orderBy('patients_details.id','desc')
		    ->jsonPaginate();  

		}elseif($user_type == 2){//doctor
			$user_id = $this->getUserId($request->token);
			$detail = Doctors::find($user_id);
			$patients = $detail->patients()->where(function ($query) use($serach_key){
					$query->where('phone','ILIKE',"%{$serach_key}%")
					->orWhere('name','ILIKE',"%{$serach_key}%");					
					})
			     /*->distinct('id')*/
			     ->orderBy('patients_details.id','DESC')
				 ->jsonPaginate();

		}elseif($user_type == 3){//patient
			$detail = Patients::find($user_id);
		}
		$final_data = array();
		if($patients){
			foreach($patients as $patient){
				//patient create time
				$mytime = Carbon::now();
				$time = new Carbon($patient->created_at);
				$shift_end_time = new Carbon($mytime->toDateTimeString());
				    $val = $time->diffForHumans($shift_end_time); 
				   $resul = str_replace("before","ago",$val);
				//patient create time

				//raise test done on
		         $raisetest_check =RaiseTest::where('patients_id',$patient->patient_id)
		                            ->orderBy('id','DESC')
		                            ->limit(1)->get();
		                           
		         if($raisetest_check->count() !=0){	
		         $raisetest_check =RaiseTest::where('patients_id',$patient->patient_id)
		                            ->orderBy('id','DESC')
		                            ->limit(1)
		                            ->first();			                           		
				$time1 = new Carbon($raisetest_check->created_at);
				$shift_end_time1 = new Carbon($mytime->toDateTimeString());
				$val1 = $time1->diffForHumans($shift_end_time1); 
				$raise_test_on = str_replace("before","ago",$val1);
				//raise test done on

				//last test done on                				
				$time2 = new Carbon($raisetest_check->updated_at);
				$shift_end_time2 = new Carbon($mytime->toDateTimeString());
				   $val2 = $time2->diffForHumans($shift_end_time2); 
				   $last_test_on = str_replace("before","ago",$val2);
				//
               }
				$RaiseTest=RaiseTest::where('patients_id',$patient->patient_id)->orderBy('id','DESC')->limit(1)->get();
				$RaiseTest->count();
				if($RaiseTest->count() == 0){
					$resul = 'Activated on '.$resul;
				}
				else{					
					if($user_type == 1){
					    $raisetest_check =RaiseTest::where('patients_id',$patient->patient_id)
		                            ->orderBy('id','DESC')
		                            ->limit(1)
		                            ->first();			
						if($raisetest_check->remaining_amount != null && $raisetest_check->remaining_amount ==0){
							$resul = 'Last test done on '.$last_test_on;
						}else{ 
							$resul = 'Raise test done on '.$raise_test_on;
						}					  
					}
					else{
					  $resul = 'Last consulted on '.$resul;
					}
				}
				$patient->dates = $resul;
				$final_data = $patient ;
			}    }

		if(count($patients) ==0)
		{
			$response['msg'] = config('resource.no_record');
			$response['status'] = config('resource.status_201');
			$response['userIsBlocked'] = $this->getUserStatus($request->token);
			return response()->json($response,config('resource.status_201'));                                   
		}
		$response['data'] = $patients;
		$response['status'] = config('resource.status_200');
		$response['message'] = config('resource.success');
		$response['userIsBlocked'] = $this->getUserStatus($request->token);
		$user = $this->getAuthUser($request->token);
		$value = DB::table('fc_WSAuth')->first()->enable_log;
		if($value)   {   
			storelivigroactivity($user,config('resource.Get_patient_List_Request'));
		}
		return response()->json($response,config('resource.status_201'));
	}


	/**
	 * get patient profile
	 *
	 * @param Request $request
	 * @return void
	 */
	public function getPatientProfile(Request $request) {
		/*error_log(print_r($request->all(),true),3,'/var/www/html/error.log');*/
		if($request->patient_id =='')
		{
            $response['msg'] = config('resource.parameter_missing');
			$response['status'] = config('resource.status_201');
			$response['userIsBlocked'] = $this->getUserStatus($request->token);
			return response()->json($response,config('resource.status_201'));
		}
		$patient_id = $request->patient_id;
		$patient = Patients::where('id',$patient_id)->get();		
		if($patient->count() !=0)
		{   //only for doctor side
			$patient = Patients::find($patient_id);
			$doctor_id = $this->getUserId($request->token);
			$doctor_data = User::where('linked_id',$doctor_id)->first();			
			if($doctor_data['user_type_id'] == 2){
			     $mytime = Carbon::now();
			     $time = new Carbon($patient->created_at);
				 $shift_end_time = new Carbon($mytime->toDateTimeString());
				 $val = $time->diffForHumans($shift_end_time);
				 $resul = str_replace("before","ago",$val);   
			     $RaiseTest=RaiseTest::where('doctor_id',$doctor_id)
			                        ->where('patients_id',$patient->id)->get();
				 $RaiseTest->count();
				if($RaiseTest->count() == 0)
					$resul = 'Activated on '.$resul;
				else
					$resul = 'Last consulted '.$resul;
				//raised data get start
				$RaiseTest=RaiseTest::where('doctor_id',$doctor_id)->where('patients_id',$request->patient_id)->get();				
				if($RaiseTest->count() != 0)
					$patient->raised_data = $RaiseTest->toArray() ;
			    else
                    $patient->raised_data = 'No Raised' ;
                 //raised data get end
				$patient->dates = $resul;
				//get status create date and consulted date
             }
             //only for doctor side

			$response['data'] = $patient->toArray();
			$response['status'] = config('resource.status_200');
			$response['userIsBlocked'] = $this->getUserStatus($request->token);
		}
		else{
			$response['data'] = config('resource.record_not_found');
			$response['status'] = config('resource.status_400');
			$response['userIsBlocked'] = $this->getUserStatus($request->token);
		}
		$user = $this->getAuthUser($request->token);
		$value = DB::table('fc_WSAuth')->first()->enable_log;
		if($value)   {   
			storelivigroactivity($user,config('resource.Get_patient_List_Request'));
		}
		return response()->json($response,config('resource.status_200'));

	}

	/**
	 *Update insert Patient Info Function
	 * @param Request $request
	 * @return void
	 */
	public function updatePatientInfo(Request $request) { 		
		error_log(print_r($request->all(),true),3,'/var/www/html/error.log');  
		$user_id = $this->getUserId($request->token);
		$user = $this->getAuthUser($request->token);	
		//   $user_id = $request->user_id;
		$user_type = $request->user_type; 
		if($request->filled('user_type')){
			 if($user_type !=1 && $user_type !=2)
			 { 
			 	 $response['status']  = config('resource.status_401');
				 $response['msg'] = config('resource.type_wrong');
				 $response['userIsBlocked'] = $this->getUserStatus($request->token);
				return response()->json($response,config('resource.status_401'));
			 }	
		 }	
		$exclude_inputs = array('user_id','user_type','patient_id','token','profile_image'); 
		if($request->filled('patient_id'))
		{                   
			$patient_count = Patients::where('id',$request->patient_id)->count();
			$patient = Patients::find($request->patient_id);            
			if($patient_count == 0){ $patient = new Patients; 
				foreach($request->all() as $key=>$value) {
					if(!in_array($key,$exclude_inputs))
						$patient->$key = $value;
				}
			}else{
				foreach($request->all() as $key=>$value) {
					if(!in_array($key,$exclude_inputs))
						$patient->$key = $value;
				}

			} 
		}
		else{                
			$patient = new Patients;
			foreach($request->all() as $key=>$value) {
				if(!in_array($key,$exclude_inputs))
					$patient->$key = $value;
			}
		}
	if($request->reference_doctor_id == 'null' || $request->reference_doctor_id =='') {		
		$patient->reference_doctor_id = 0;
		$patient->reference_doctor_name = NULL;
		$doctor_id = NULL;	
	}else{		
		$doctor_data = Doctors::find($request->reference_doctor_id);
		$patient->reference_doctor_id = $doctor_data->id;
		$patient->reference_doctor_name = $doctor_data->name;
		$doctor_id = $doctor_data->id;
	}
		if($request->filled('profile_image'))
			{				
				if($request->profile_image != '""')
					   	    { 					   	    			   	    	
				$patient->profile_image = file_upload($request->profile_image,$user_type,$user->phone);
			                }		
			                             
			}
		$patient->save();
		if($user_type == 1) {			
			$DocPatient_count = DocPatient::where('lab_id',$user_id)
			                               ->where('patient_id',$patient->id)                              
			                               ->get();
			if($DocPatient_count->count() ==0 )
			  { /*dd($patient->id);*/
			    	DocPatient::create(['lab_id' => $user_id,'patient_id' => $patient->id,'doctor_id' => $doctor_id]);
			  }else
			  { /*dd('old');*/
			     /*DocPatient::where(['lab_id',$lab_id],['patient_id',$patient->id],['doctor_id',$doctor_id])
			               ->update(['doctor_id'=>$doctor_id]);*/
			     DocPatient::where([['lab_id',$user_id],['patient_id',$patient->id]])->update(['doctor_id'=>$doctor_id]);
			  }
              //send sms doctor number 
			  if(isset($request->reference_doctor_id) && $request->reference_doctor_id !='' && $request->reference_doctor_id !='null'){			  	
			  	    $doctors = Doctors::find($request->reference_doctor_id);
			        send_sms(substr($doctors->mobile,3),'Hi you referred by patient '.$patient->name.'');
			        //send mail start
			        if($doctors->email !='' && $doctors->email !=null){	
			           $data['to']           = $doctors->email; 
                       $data['subject']      = "New patient referred"; 					
                       $data['patient_name'] = $patient->name; 					
						Mail::send('email.newdoctorRefer',$data, function($message) use ($data)
					      {
					          $message->to($data['to']); 					        
					          $message->subject($data['subject']);
					          $message->cc(config('mail.MailCcFunction'));
					      });

					}
					//send mail end
		          }		         
		      //send sms doctor number 

		}else{
		$DocPatient_count = DocPatient::where('doctor_id',$user_id)->where('patient_id',$patient->id)->get();
			if($DocPatient_count->count() ==0)
				DocPatient::create(['doctor_id' => $user_id,'patient_id' => $patient->id]);
		}
		$response['data'] = $patient->toArray();
		$response['status'] = config('resource.status_200');
		$response['userIsBlocked'] = $this->getUserStatus($request->token);

		$value = DB::table('fc_WSAuth')->first()->enable_log;
		if($value)   {   
			storelivigroactivity($user,config('resource.update_patient_info_request')); 
		}	
		return response()->json($response,config('resource.status_200'));

		//}
}
/**
 * Show All Lab Test function
 *
 * @param Request $request
 * @return void
 */
public function showAllLabTest(Request $request){
	/*error_log(print_r($request->all(),true),3,'/var/www/html/error.log');*/
	$lab_id = $this->getUserId($request->token);
	//   $lab_id = $request->lab_id;
	$tests = Labs::find($lab_id)->tests()->get()->toArray();
	if($tests){
		$response['data'] = $tests;
		$response['userIsBlocked'] = $this->getUserStatus($request->token);
		$response['status'] = config('resource.status_200');
	}
	else{
		$response['data'] = config('resource.record_not_found');
		$response['userIsBlocked'] = $this->getUserStatus($request->token);
		$response['status'] = config('resource.status_400');
	}
	$user = $this->getAuthUser($request->token);
	$value = DB::table('fc_WSAuth')->first()->enable_log;
	if($value)   {    
		storelivigroactivity($user,config('resource.show_all_labs_test'));
	}
	return response()->json($response,config('resource.status_200'));

}

/**
 * Show Test Based on the Category function
 *
 * @param Request $request
 * @return void
 */
public function showTestBaseOnCategory(Request $request){
	/*error_log(print_r($request->all(),true),3,'/var/www/html/error.log');*/
	$category = $request->category;
	$lab_id = $this->getUserId($request->token);
	//$lab_id = $request->lab_id;
	$tests = Labs::find($lab_id)->tests()->where('search_tags','ILIKE',"%{$category}%")->get()->toArray();
	//$tests = LabsTest::where('search_tags','ILIKE',"%{$category}%")->get()->toArray();
	if($tests){
		$response['data'] = $tests;
		$response['userIsBlocked'] = $this->getUserStatus($request->token);
		$response['status'] = config('resource.status_200');
	}
	else{
		$response['msg'] = config('resource.no_record');
		$response['userIsBlocked'] = $this->getUserStatus($request->token);
		$response['status'] = config('resource.status_201');
	}
	$user = $this->getAuthUser($request->token);
	$value = DB::table('fc_WSAuth')->first()->enable_log;
	if($value)   {   
		storelivigroactivity($user,config('resource.show_lab_test_based_on_the_category'));  
	}
	return response()->json($response,config('resource.status_200'));
} 


/**
 * Raise Test function
 *
 * @param Request $request
 * @return void
 */
public function RaiseTest(Request $request){ 

error_log(print_r($request->all(),true),3,'/var/www/html/error.log'); 
	$user_type = $request->user_type;
	//$user_id = $request->user_id;
	$user_id = $request->user_id;
	$patient_id = $request->patient_id;
	if($request->filled('lab_id')) {
		$lab_id = $request->lab_id;
		$doc_id = $user_id;
	}
	else {
		$lab_id = $user_id;
		$doc_id = null;
	}
	// $doctor_referer_name = '';
	if($request->filled('doctor_referer_name'))
		$doctor_referer_name = $request->doctor_referer_name;	
	//$tests_id1 = json_decode($request->tests_id); //array Format Previously it came from Mobile side
	//Now need to get the record from database
	$cart_test_single = CartTest::where([
			['user_id',$user_id],
			['patient_id',$patient_id],
			['test_type','single'],
			['user_type',$user_type]])->pluck('test_id')->toArray(); 
	$cart_test_package = CartTest::where([
			['user_id',$user_id],
			['patient_id',$patient_id],
			['test_type','!=','single'],
			['user_type',$user_type]])->groupBy('test_type')->pluck('test_type')->toArray();

    $package_test_id = CartTest::where([
			['user_id',$user_id],
			['patient_id',$patient_id],
			['test_type','!=','single'],
			['user_type',$user_type]])->pluck('test_id')->toArray();

     $package_amount = 0 ; 	
     $amount = 0 ;
	 $lab_estimate_amount = 0 ;
    if(count($cart_test_package)){
    	foreach($cart_test_package as $testid){
    		$package_test_check = LabsAvaTest::where('lab_tests_id',$testid)
    		                                     ->where('lab_id',$user_id)
    		                                     ->where('test_type','group')
    		                                     ->where('status',1)
    		                                     ->get();
    		if($package_test_check->count() !=0){
    			$package_amount += $package_test_check->first()->amount;    	       
    		}else{
    		$package_test_available = Package::where([['id',$testid]])->get();
			if($package_test_available->count() !=0){
				$package_amount += Package::find($testid)->amount;
			}
		  }
		}
    }    

	$tests_id = $cart_test_single;	
	$lab_tests_image = ''; // image field
	if(count($tests_id)) {		
		foreach($tests_id as $testid){
			$test_available = LabsAvaTest::where([['lab_id',$lab_id],['lab_tests_id',$testid]])->get();
			if($test_available->count() !=0){
				$lab_estimate_amount += LabsAvaTest::where([['lab_id',$lab_id],['lab_tests_id',$testid]])
				->first()->amount;
			}else{
                $lab_estimate_amount += LabsTest::find($testid)->price;
			}
		}		     
	}else{		
		//To update the test image
		//For muliti image need to implement that data
		if($request->filled('lab_tests_image'))
			$lab_tests_image = file_upload($request->lab_tests_image,$user_type,'raise');
	}
	$amount = $package_amount + $lab_estimate_amount;
	//save array values using double quatitions
	$implode_array = implode(",",array_merge($tests_id,$package_test_id));
	$explode_array = explode(',',$implode_array);	
	//save array values using double quatitions

	if($user_type == 1) {// lab
		$Patients_name = Patients::find($patient_id);
		$getdoctor_referer_id = @$Patients_name->reference_doctor_id ;
		$getdoctor_referer_name = @$Patients_name->reference_doctor_name ;
		$raised_by = 1; // for lab
		//$amount = $lab_estimate_amount ;
		/*if($request->filled('doctor_referer_name'))
			$doctor_referer_name = $request->doctor_referer_name;*/
	}
	elseif($user_type == 2){ // doctor
		$raised_by = 0; //doctor
		//$amount = $amount ;
	}	

	$raise_test = RaiseTest::create([
			'doctor_id' => $getdoctor_referer_id,
			'lab_id' => $lab_id,
			'doctor_referer_id' => $getdoctor_referer_id ,
			'doctor_referer_name' => $getdoctor_referer_name ,
			'lab_tests_id' => ($explode_array) ? $explode_array : '',
			'patients_id' => $patient_id,
			'raised_by' => $raised_by,
			'diagonsis' => ($request->diagonsis) ? $request->diagonsis : '',
			'notes'   => ($request->notes) ? $request->notes : '',
			'lab_tests_image' => ($lab_tests_image) ? $lab_tests_image :'',
			'original_amount' => $amount,
			'received_amount' => 0,
			'discount_price' => 0,
			/*'remaining_amount' => $amount,*/
			'status' => 1,
	         ]);	
		$doctor_patient = DocPatient::updateOrCreate(
        ['patient_id' => $patient_id,'lab_id' => $lab_id], ['lab_id' => $lab_id,'doctor_id'=>$getdoctor_referer_id,'updated_at' => date('Y-m-d H:i:s')]
        );
        /*$doctor_patient = new DocPatient;
        $doctor_patient->doctor_id = $getdoctor_referer_id;
      $doctor_patient->patient_id = $patient_id;                              
      $doctor_patient->lab_id = $lab_id;    
      $doctor_patient->created_at = date('Y-m-d H:i:s'); 
      $doctor_patient->updated_at = date('Y-m-d H:i:s');    

      $doctor_patient->save();*/

	//update patient info   
	if($request->filled('patient_id'))     
		$data =Patients::find($patient_id);
	$data->updated_at = Carbon::now();
	$data->save();

	//TODO Need to delete the data in cartTestTable
	if($request->filled('lab_id'))
		CartTest::where([['user_id',$lab_id],['patient_id',$patient_id],['user_type',1]])->delete();
	else
		CartTest::where([['user_id',$user_id],['patient_id',$patient_id],['user_type',1]])->delete();

	//TODO need to insert the record in Receipt table
	/*$receiptAmount = 0 ;
	if(count($tests_id)) {
		foreach($tests_id as $testid) {
			if($user_type == 1){
				$test_available = LabsAvaTest::where([['lab_id',$lab_id],['lab_tests_id',$testid]])->get();
			  if($test_available->count() !=0){			  	  
				$receiptAmount = LabsAvaTest::where([['lab_id',$lab_id],['lab_tests_id',$testid]])->first()->amount;				   
			     }else{			     	
			     	$receiptAmount = LabsTest::find($testid)->price;			     	
			     }
			}else{	$receiptAmount = LabsTest::find($testid)->price; }
			Receipt::create([
					'raise_test_id'=>$raise_test->id,
					'tests_id'=>$testid,
					'amount' => $receiptAmount
			]);
		}
	}*/

//single test added in receipt table
	$lab_estimate_amount = 0 ;
	if(count($tests_id)) {		
		foreach($tests_id as $testid){
			$test_available = LabsAvaTest::where([['lab_id',$lab_id],['lab_tests_id',$testid]])->get();
			if($test_available->count() !=0){
				$lab_estimate_amount = LabsAvaTest::where([['lab_id',$lab_id],['lab_tests_id',$testid]])
				->first()->amount;
			}else{
                $lab_estimate_amount = LabsTest::find($testid)->price;
			}
			$specimen_count = LabsCategory::where('lab_test_id',$testid)->where('status',1)->get();
			if($specimen_count->count() !=0){
               $specimen_id = $specimen_count->first()->id ;
			}
			else{
			   $specimen_id = NULL ;
			}
			$lab_count =LabsTest::where('id',$testid)->get();		    
		    if($lab_count->count() !=0 && is_numeric($lab_count->first()->measurement)){	
            $units_count = Units::where('id',$lab_count->first()->measurement)->get();
            if($units_count->count() !=0){
             $units = $units_count->first()->name;
            }else{
             $units = '-nil-' ;
            }}else{
             $units = '-nil-' ;
            }           
			Receipt::create([
					'raise_test_id'=>$raise_test->id,
					'tests_id'=>$testid,
					'labtest_sub_name'=> $specimen_id,
					'amount' => $lab_estimate_amount,
					'reference_range' => (isset($test_available->first()->range)) ? $test_available->first()->range :0,
				'reference_endrange' =>(isset($test_available->first()->range_end)) ? $test_available->first()->range_end:0,
					'units' => $units,   
					'test_type' => 'single'
			]);
		}		
	}
//single test added in receipt table

//pulk test added in receipt table
	 $package_amount = '0';
	 $package_total_amount = '0' ;
	 if(count($cart_test_package)){
    	foreach($cart_test_package as $testid){
    	       $package_test_available = LabsAvaTest::where('lab_tests_id',$testid)
    		                                     ->where('lab_id',$user_id)
    		                                     ->where('test_type','group')
    		                                     ->where('status',1)
    		                                     ->get();
			if($package_test_available->count() !=0){
				$package_amount = $package_test_available->first()->amount;
			}
			 //insert package test in receipt table
			$package_test_it = Package::find($testid)->tests_id;
			 foreach ($package_test_it as $key => $value) {
			 	if($key==0){
			 		$package_total_amount = $package_amount ;
			 	}else{
                    $package_total_amount = '0' ;
			 	}
			 	$specimen_count = LabsCategory::where('lab_test_id',$value)->where('status',1)->get();
				if($specimen_count->count() !=0){
	               $specimen_id = $specimen_count->first()->id ;
				}
				else{
				   $specimen_id = NULL ;
				}
		    $lab_count =LabsTest::where('id',$value)->get();		    
		    if($lab_count->count() !=0){		    
            $units_count = Units::where('id',$lab_count->first()->measurement)->get();
            if($units_count->count() !=0){
             $units = $units_count->first()->name;
            }else{
             $units = '-nill-' ;
            }}else{
             $units = '-nill-' ;
            }
             	Receipt::create([
						'raise_test_id'=>$raise_test->id,
						'tests_id'=>$value,
						'labtest_sub_name'=> $specimen_id,
						'amount' => $package_total_amount,
						'units' =>  $units,
				'reference_range' => (isset($package_test_available->first()->range)) ? $package_test_available->first()->range :0,
				'reference_endrange' =>(isset($package_test_available->first()->range_end)) ? $package_test_available->first()->range_end:0,
						'test_type' => $testid
				]);
			 }
			 //insert package test in receipt table				
		}
    } 
//pulk test added in receipt table
      
      //save notification start
    	$test = RaiseTest::where('id',$raise_test->id)->where('status',1)->first();           
		if(!empty($test->id))
		{                     
			$notification = new Notification();
			$notification->type             = 'test';
			$notification->lab_id           =  $lab_id;
			$notification->doctor_id        =  ($test->doctor_id !=0 || $test->doctor_id !=null) ? $test->doctor_id : $test->doctor_referer_id ;
			$notification->patient_id       =  ($test->patients_id) ? $test->patients_id : 0;
			$notification->raise_test_id    =  $raise_test->id;
			$notification->read_status      =  0;
			$notification->save();			
		}
      //save notification end

	  $getLabfcm = get_fcm_key($lab_id,'lab');
	  $title = "Livigro -  New test raised";
     /* $msg = " Your raised test id - ".$raise_test->id."";*/
     $patient_name = getPatientdetails($test->patients_id,'name');
      $msg   = "Your Livigro Booking ID : ".$raise_test->id." for ".$patient_name." has been placed sucessfully" ; 
      if($getLabfcm != null && $getLabfcm != ''){                           
        $send = send_push_notification($getLabfcm, $msg, $title); 
       }
       if($doc_id !=''){
         $getDoctorfcm = get_fcm_key($doc_id,'doctor'); 
          if($getDoctorfcm != null && $getDoctorfcm != ''){
		        $send1 = send_push_notification($getDoctorfcm, $msg, $title);
		      } 
       }                               
     
      $patientPhone = DB::table('patients_details')
                        ->where('id', $patient_id)
                        ->pluck('phone')->first();
      if($patientPhone != null && $patientPhone != ''){
      	$patient_name = getPatientdetails($test->patients_id,'name');
        $msg   = "Your Livigro Booking ID : ".$raise_test->id." for ".$patient_name." has been placed sucessfully" ; 
        send_sms(substr($patientPhone,3),$msg);
      }         

	$response['data'] = $raise_test->toArray();
	$response['status'] = config('resource.status_200');
	$response['userIsBlocked'] = $this->getUserStatus($request->token);
	$user = $this->getAuthUser($request->token);
	$value = DB::table('fc_WSAuth')->first()->enable_log;
	if($value)   {   
		storelivigroactivity($user,config('resource.raise_test_function')); 
	}
	return response()->json($response,config('resource.status_200'));

}

/**
 * Lab Can Update their test Data function
 *
 * @param Request $request
 * @return void
 */
public function labupdatetheirtestdata(Request $request){
	//   $lab_id = $request->lab_id;
	$lab_id = $this->getUserId($request->token);
	$test_id = $request->test_id;
	$range = $request->range;
	$specimen = $request->specimen;
	$price = $request->price;
	$updated_test = LabsAvaTest::where([['lab_id',$lab_id],['lab_tests_id',$test_id]])->update(['range'=>$range,
			'amount'=>$price,'specimen'=>$specimen]);
	$response['data'] = $updated_test;
	$response['status'] = config('resource.status_200');
	$response['userIsBlocked'] = $this->getUserStatus($request->token);
	$user = $this->getAuthUser($request->token);
	$value = DB::table('fc_WSAuth')->first()->enable_log;
	if($value)   {   
		storelivigroactivity($user,config('resource.lab_update_their_data'));
	}
	return response()->json($response,config('resource.status_200'));

}

/**
 * Update Specimen Collected function
 *
 * @param Request $request
 * @return void
 */
public function specimencollected(Request $request) {
	/*error_log(print_r($request->all(),true),3,'/var/www/html/error.log');*/
	//$lab_id = $request->lab_id;
	$lab_id = $this->getUserId($request->token);
	$test_id = $request->test_id;
	$individual_test_id = (json_decode($request->individual_test_id)); // array 1=>Blood
	//dd($request);
	if(count($individual_test_id) !=0)
	{
		foreach($individual_test_id as $key=>$value)
			LabsAvaTest::where([['lab_id',$lab_id],['lab_tests_id',$key]])->update(['specimen'=>$value]);
		//TODO if new test addded we need to add in raise test table;
		RaiseTest::where('id',$test_id)->update([
				'specimen_collected'=>1,
				'lab_tests_id'=>json_encode(array_keys($individual_test_id))]);
	}else
	{
		$response['status']  = config('resource.status_201');
		$response['msg'] = config('resource.no_record') ;
		$response['userIsBlocked'] = $this->getUserStatus($request->token);
		return response()->json($response,config('resource.status_201'));
	}

	$response['status'] = config('resource.status_200');
	$response['msg'] = 'Specimen Updated';
	$response['userIsBlocked'] = $this->getUserStatus($request->token);
	$user = $this->getAuthUser($request->token);
	$value = DB::table('fc_WSAuth')->first()->enable_log;
	if($value)   {   
		storelivigroactivity($user,config('resource.specimen_collected_api_call'));
	}
	return response()->json($response,config('resource.status_200'));
}

//Lab can cancel the raised test
/**
 * Cancel Test function
 *
 * @param Request $request
 * @return void
 */
public function Canceltest(Request $request){
	error_log(print_r($request->all(),true),3,'/var/www/html/error.log');

	//$lab_id = $request->lab_id;
	$lab_id = $this->getUserId($request->token);
	$test_id = $request->test_id;
	$reason = ($request->reason !='') ? $request->reason :0 ;
	$status = $request->status;
	RaiseTest::where('id',$test_id)->update([
			'paid_status'=>$status,
			'other_reason_for_cancel'=>$reason
	]);

  $labs_data = Labs::find($lab_id);
  $raisetest_data = RaiseTest::find($test_id);
  $patient_data = Patients::find($raisetest_data->patients_id);
  $patientPhone = $patient_data->phone;

  //email and sms start
        //lab sms
        if($labs_data->mobile != null && $labs_data->mobile != ''){                         
          $msg = "Your Livigro Cancellation for Booking ID : ".$test_id." has been Cancelled  successfully";
          $labMobile = $labs_data->mobile ;
            if(substr($labMobile,0,1) == '+'){
              send_sms(substr($labMobile,3), $msg);
            }else{
              send_sms($labMobile, $msg);
            }
          }
          //lab sms
        //patient sms
          if($patientPhone != null && $patientPhone != ''){ 
          $msg = "Hi, Livigro Cancellation for Booking ID : ".$test_id."  has been Cancelled  successfully";
            if($patient_data->name !=''){
            $msg = "Hi ".$patient_data->name.",    Livigro Cancellation for Booking ID : ".$test_id."  has been Cancelled  successfully ";
             }
            if(substr($patientPhone,0,1) == '+'){
              send_sms(substr($patientPhone,3), $msg);
            }else{
              send_sms($patientPhone, $msg);
            }            
          }  
        //patient sms         
            $patientEmail = $patient_data->email;
            $labEmail     = $labs_data->email;
            $data['to'] = $patientEmail; 
            $data['lab_to'] = $labEmail; 
            $data['subject'] ='Test Cancel';
            $data['lab_subject'] ='Test Cancel';
            $data['patientName'] = $patient_data->name;        
            $data['raise_id'] = $test_id;             
            if($patientEmail !=''){
              Mail::send('email.cancelpatient',$data, function($message) use ($data)
                  {
                      $message->to($data['to']); 
                      $message->subject($data['subject']);
                      $message->cc(config('mail.MailCcFunction'));
                  });

            }
            if($labEmail !=''){             
              Mail::send('email.cancellab',$data, function($message) use ($data)
                  {
                      $message->to($data['lab_to']); 
                      $message->subject($data['lab_subject']);
                      $message->cc(config('mail.MailCcFunction'));
                  });

            }
            //email and sms end

	$response['status']  = config('resource.status_200');
	$response['msg'] = config('resource.test_has_been_successful'); 
	$response['userIsBlocked'] = $this->getUserStatus($request->token);
	$user = $this->getAuthUser($request->token);
	$value = DB::table('fc_WSAuth')->first()->enable_log;
	if($value)   {   
		storelivigroactivity($user,config('resource.test_cancel')); 
	}
	return response()->json($response,config('resource.status_200'));
}
//TODO need to show the labs indidual avaiable test
/**
 * Get Labs Individual Test function
 *
 * @param Request $request
 * @return void
 */
public function indidualtest(Request $request){
	/*error_log(print_r($request->all(),true),3,'/var/www/html/error.log');*/
	//$lab_id = $request->lab_id;
	$lab_id = $this->getUserId($request->token);
	$tests = Labs::find($lab_id)->tests()->get();
	$response['data'] = $tests->toArray();
	$response['status'] = config('resource.status_200');
	$response['userIsBlocked'] = $this->getUserStatus($request->token);
	return response()->json($response,config('resource.status_200'));
}

//1.Need to show the booked test 
//1.1 show both doctor raised test and walk in test in lab module
//1.2 need to show the status, 1.specimen collected 2. amount due 3. test completed
/**
 * Doctor Raised Test to Lab function
 *
 * @param Request $request
 * @return void
 */
public function raisedtesttolab(Request $request){
	/*error_log(print_r($request->all(),true),3,'/var/www/html/error.log');*/
	//   $lab_id = $request->lab_id;
	$lab_id = $this->getUserId($request->token);
	$lab = Labs::find($lab_id);
	$raised_test = $lab->total_raised_test()->with('getLab','getPatient','getDoctor')->get();
	foreach($raised_test as $key=>$raise) {
		$tests_id = $raise->lab_tests_id;
		$test_data = [];
		foreach($tests_id as $test_id)
			$test_data[] = LabsTest::find($test_id);
		$raised_test[$key]->getLabTest = $test_data;
	}
	if(count($raised_test) ==0){
		$response['msg'] = config('resource.no_record') ;
        $response['status']  = config('resource.status_201');
        $response['userIsBlocked'] = $this->getUserStatus($request->token);		
		return response()->json($response,config('resource.status_201'));
	}
	$response['data'] = $raised_test;
	$response['status'] = config('resource.status_200');
	$response['userIsBlocked'] = $this->getUserStatus($request->token);
	$user = $this->getAuthUser($request->token);
	$value = DB::table('fc_WSAuth')->first()->enable_log;
	if($value)   {   
		storelivigroactivity($user,config('resource.all_raised_test')); 
	}
	return response()->json($response,config('resource.status_200'));
}

/**
 * Recipe Update function
 *
 * @param Request $request
 * @return void
 */
public function updateReceipt(Request $request){
	/*error_log(print_r($request->all(),true),3,'/var/www/html/error.log');*/
	$test_id = $request->test_id;
	// $lab_id = $request->lab_id;
	$lab_id = $this->getUserId($request->token);
	$tests_id = json_decode($request->tests_id); // this one should the assosiative array(1=>100,2=>300)
	//dd($tests_id);
	$total_price = $request->total_price;
	$amount_due = $request->due_amount;
	$received_amount=$request->received_amount;
	$paid_amount = $request->paid_amount;
	$payment_mode = $request->payment_mode;
	$status = ($paid_amount == 0) ? 0 : (($paid_amount == $total_price) ? 1 : 2);

	foreach($tests_id as $key=>$value) {
		Receipt::where([['tests_id',$test_id],['raise_test_id',$key]])->update([
				'amount'=>$value
		]);
	}
	RaiseTest::where('id',$test_id)->update([
			'received_amount'=>$received_amount,
			'paid_amount'=>$paid_amount,
			'remaining_amount'=>$amount_due,
			'paid_status'=>$status
	]);
	$response['msg'] = config('resource.receipt_updated'); 
	$response['status'] = config('resource.status_200');
	$response['userIsBlocked'] = $this->getUserStatus($request->token);
	$user = $this->getAuthUser($request->token);
	$value = DB::table('fc_WSAuth')->first()->enable_log;
	if($value)   {   
		storelivigroactivity($user,config('resource.receipt_api_call')); 
	}
	return response()->json($response,config('resource.status_200'));
}

/**
 * Get WORKING TIME function
 *
 * @param Request $request
 * @return void
 */
public function getWorkingTime(Request $request){
	/*error_log(print_r($request->all(),true),3,'/var/www/html/error.log');*/
	$user_type = $request->user_type;
	$user_id = $this->getUserId($request->token);
	if($user_type == 1){
		$data = WorkingTime::where('lab_id',$user_id)->get()->toArray();
	}else{
		$data = WorkingTime::where('doctor_id',$user_id)->get()->toArray();
	}
	$response['data'] = $data;
	$response['status'] = config('resource.status_200');
	$response['userIsBlocked'] = $this->getUserStatus($request->token);
	$user = $this->getAuthUser($request->token);
	$value = DB::table('fc_WSAuth')->first()->enable_log;
	if($value)   {   
		storelivigroactivity($user,config('resource.working_time_api_call'));
	}
	return response()->json($response,config('resource.status_200'));

}
/**
 * Update Working Time function
 *
 * @param Request $request
 * @return void
 */
public function UpdateWorkingTime(Request $request)
{   /*error_log(print_r($request->all(),true),3,'/var/www/html/error.log');*/
	$user_id = $this->getUserId($request->token);
	$user_type = $request->user_type;
	if($user_type == 1) {//lab
		$lab_id = $user_id;
		$doc_id = '';
	}
	else{ // for Doctor
		$doc_id = $user_id;
		$lab_id = '';
	}
	if($request->day_type == 1)
	{
		$condition = ['day_1_open_time'=>$request->open_time,
			'day_1_end_time'=>$request->end_time,
			'day_1_vacation'=>$request->vacation_mode];

	}else if($request->day_type == 2) {
		$condition = ['day_2_open_time'=>$request->open_time,
			'day_2_end_time'=>$request->end_time,
			'day_2_vacation'=>$request->vacation_mode];
	}else if($request->day_type == 3) {
		$condition = ['day_3_open_time'=>$request->open_time,
			'day_3_end_time'=>$request->end_time,
			'day_3_vacation'=>$request->vacation_mode];
	}else if($request->day_type == 4) {
		$condition = ['day_4_open_time'=>$request->open_time,
			'day_4_end_time'=>$request->end_time,
			'day_4_vacation'=>$request->vacation_mode];
	}else if($request->day_type == 5) {
		$condition = ['day_5_open_time'=>$request->open_time,
			'day_5_end_time'=>$request->end_time,
			'day_5_vacation'=>$request->vacation_mode];
	}else if($request->day_type == 6) {
		$condition = ['day_6_open_time'=>$request->open_time,
			'day_6_end_time'=>$request->end_time,
			'day_6_vacation'=>$request->vacation_mode];
	}else if($request->day_type == 7) {
		$condition = ['day_7_open_time'=>$request->open_time,
			'day_7_end_time'=>$request->end_time,
			'day_7_vacation'=>$request->vacation_mode];
	}
	if($user_type == 1)
		$work = WorkingTime::where('lab_id',$lab_id);
	else
		$work = WorkingTime::where('doctor_id',$doc_id);
	if($work->count() !=0){
		$work->update($condition);
		$response['data'] = $work->get()->toArray();
		$response['msg'] = config('resource.working_time_updated_successfully'); 
		$response['status'] = config('resource.status_200');
		$response['userIsBlocked'] = $this->getUserStatus($request->token);
		$user = $this->getAuthUser($request->token);
		$value = DB::table('fc_WSAuth')->first()->enable_log;
		if($value)   {   
			storelivigroactivity($user,config('resource.Working_Time_Update_API_Call'));
		}
		return response()->json($response,config('resource.status_200'));
	}else{
		$response['msg'] = config('resource.no_record');
		$response['status']  = config('resource.status_201');
		$response['userIsBlocked'] = $this->getUserStatus($request->token);
		return response()->json($response,config('resource.status_201'));}         
}

public function export($raise_test)
    {
    //error_reporting(E_ALL);ini_set('display_errors','On');
      //$id = $_REQUEST['id'];

      /*$raise_test_time = new Carbon($raise_test->updated_at);  
      //collection date
      $raise_test->collection_date = $raise_test_time->format('d F Y');
      //collection date

      $Receipt = Receipt::where('raise_test_id',$raise_test_id)->first();
      //report date
      $receipt_time = new Carbon($Receipt->updated_at);
      $raise_test->report_date = $receipt_time->format('d F Y');
      //report date
   
       //test data
        $receipt_data = Receipt::where('raise_test_id',$raise_test_id)->get();
       //testing date
        foreach ($receipt_data as $key => $value) {
              $value->test_name = getLabtestdetails($value->tests_id,'name');              
              $value->test_collection_name = LabsCategorydata($value->labtest_sub_name,'name');              
        }
        */
      $id = $raise_test->id;
      $file_path = storage_path().'/app/public/export/';
      $file_name = 'Report_'.$id.'.pdf';
      $file_name1 = 'Receipt_'.$id.'.pdf';
      libxml_use_internal_errors(true);
      $store_path = [];

      $raisetest = RaiseTest::find($id);
      $lab_data = Labs::where('id',$raisetest->lab_id)->first();
       if(isset($lab_data))
         $sign = $lab_data->signature;
      
      $mpdf = new \Mpdf\Mpdf();
      $mpdf->setAutoBottomMargin = 'stretch';
     // if(!file_exists($filepath.$file_name)){
        //echo "no report";die;
         $mpdf->SetHTMLHeader('<div style="text-align:right;font-weight:bold; font-size:18px;width:95%; margin-bottom:10px;">Order ID : '.$id.'</div>');
        if(isset($sign) && count($sign) > 0 && $sign[0]!=''){
        $signature =end($sign);
        $mpdf->SetHTMLFooter('
        <div style="width:100%;float:right;margin-bottom:0px;text-align:right;">
        <img src="https://livigro-demo.s3.ap-south-1.amazonaws.com/'.$signature.'" style="height: 60px;">
        <h5 style="margin:0;color: #989898">AUTHORISED SIGNATORY</h5>
        </div>

        <div style="width:100%;float:left;margin-bottom:0px;">
        <div style="min-height: 30px;padding: 10px 40px 0 40px;margin-top: 40px;float:left;width:95%;">
        <div style="width: 50%;float: left;"><p style="margin: 0"><strong>www.livigro.com</strong></p></div>
        <div style="width: 50%;float: left;"><p style="margin: 0;text-align: right;">+91 8098094888</p></div>
        </div>
        </div>');
        }else{
        $mpdf->SetHTMLFooter('
        <div style="width:100%;float:left;margin-bottom:0px;">
        <div style="min-height: 30px;padding: 10px 40px 0 40px;margin-top: 40px;float:left;width:95%;">
        <div style="width: 50%;float: left;"><p style="margin: 0"><strong>www.livigro.com</strong></p></div>
        <div style="width: 50%;float: left;"><p style="margin: 0;text-align: right;">+91 8098094888</p></div>
        </div>
        </div>');
        }
      $mpdf->WriteHTML($this->generatePdfFile($id, 'report'));
      $mpdf->Output($file_path.$file_name,\Mpdf\Output\Destination::FILE);

      $mpdf1 = new \Mpdf\Mpdf();
      $mpdf1->setAutoBottomMargin = 'stretch';
      $mpdf1->SetHTMLFooter('
        <div style="width:100%;float:left;margin-bottom:0px;">
        <div style="min-height: 30px;padding: 10px 40px 0 40px;margin-top: 40px;float:left;width:95%;">
        <div style="width: 50%;float: left;"><p style="margin: 0"><strong>www.livigro.com</strong></p></div>
        <div style="width: 50%;float: left;"><p style="margin: 0;text-align: right;">+91 8098094888</p></div>
        </div>
        </div>');
      $mpdf1->WriteHTML($this->generatePdfFile($id, 'receipt'));
      $mpdf1->Output($file_path.$file_name1,\Mpdf\Output\Destination::FILE);


      $store_path['report'] = storage_path().'/app/public/export/'.$file_name;
      $store_path['receipt'] = storage_path().'/app/public/export/'.$file_name1;
     
       return $store_path;     
     
    }

    public function generatePdfFile($id, $type){
      $raise_test = RaiseTest::find($id);
      if($raise_test->receipt_no == '' || $raise_test->receipt_no == 0){
            $raise_test->receipt_no = rand ( 10000 , 99999 );
            $raise_test->save();
        }
      $raise_test_time = new Carbon($raise_test->updated_at);  
      //collection date
      $raise_test->collection_date = $raise_test_time->format('d F Y-h:m A');
      //collection date
      //report id get start
       $report_id_count = Report::where('raise_test_id',$id)->get();
       if($report_id_count->count() !=0){
       	$raise_test->report_id = Report::where('raise_test_id',$id)->first()->id;
       }else{
       	$raise_test->report_id = 0;
       }       
      //report id get end
      $Receipt = Receipt::where('raise_test_id',$id)->first();
      //report date
      $receipt_time = new Carbon($Receipt->updated_at);
      $raise_test->report_date = $receipt_time->format('d F Y-h:m A');
      //report date
      $raise_test->receipt_date = $receipt_time->format('d F Y');
   
       //test data
        //$receipt_data = Receipt::where('raise_test_id',$id)->get();
      $receipt_data_package = Receipt::where('raise_test_id',$id)
                             ->with('testData')
                             ->get()->toArray();

        foreach ($receipt_data_package as $key => $value){
          if($value['test_type'] == 'single'){
            $receipt_data['single'.$value['tests_id']] = $value;
            $receipt_data['single'.$value['tests_id']]['name'] = $value['test_data']['name'];
            $receipt_data['single'.$value['tests_id']]['test_name'] = $value['test_data']['name'];
          }else{
            if(isset($value['test_data']) && $value['test_data'] != ''){
                $packageDetails = Package::where('id',$value['test_type'])->first();
                 $packageAmount = LabsAvaTest::where('lab_tests_id',$value['test_type'])->where('lab_id',$raise_test->lab_id)->where('test_type','group')->first();
                $receipt_data[$value['test_type']]['id'] = $value['test_type'];
                $receipt_data[$value['test_type']]['test_type'] = $value['test_type'];
                $receipt_data[$value['test_type']]['packageName'] = $packageDetails->package_name;
                $receipt_data[$value['test_type']]['amount'] = (isset($packageAmount) && $packageAmount->amount != null && $packageAmount->amount!='')?$packageAmount->amount:$packageDetails->amount;
                foreach ($packageDetails->profile_id as $key => $val) {
                    $packageTestDetails = LabTestGroup::where('id',$val)->first()->toArray();
                    $receipt_data[$value['test_type']]['package'][$packageTestDetails['id']]['id'] = $packageTestDetails['id'];
                    $receipt_data[$value['test_type']]['package'][$packageTestDetails['id']]['groupName'] = $packageTestDetails['name'];
                    $receipt_data[$value['test_type']]['package'][$packageTestDetails['id']]['showName'] = $packageTestDetails['show_profile_name'];
                    foreach($packageTestDetails['tests_id'] as $k => $v){
                      $testDetails = Receipt::where('raise_test_id',$id)->where('tests_id',$v)->with('testData')->first();
                      $receipt_data[$value['test_type']]['package'][$packageTestDetails['id']]['testList'][$v] =  $testDetails;
                    }
                }
              }
          }
        }
       //testing date

        $patient = $raise_test->getPatient;
        $doctor = $raise_test->getDoctor;
        /*foreach ($receipt_data as $key => $value) {
              $value->test_name = getLabtestdetails($value->tests_id,'name');              
              $value->test_collection_name = LabsCategorydata($value->labtest_sub_name,'name');              
        }*/
        $lab_data = Labs::where('id',$raise_test->lab_id)->first();
        $data['raise_test']= $raise_test;
        $data['receipt_data']= $receipt_data;
        $data['patient']= $patient;
        $data['doctor']= $doctor;
        $data['lab_data'] = $lab_data;
        
        if($type =='report'){
            return view('labtest.test_report',compact('data'));
        }else if($type == 'receipt'){
        	return view('labtest.test_receipt',compact('data'));
        }

        //return view('labtest.test_receipt',compact('raise_test,receipt_data,patient'));
    }

/**
 * Generate Report function
 *
 * @param Request $request
 * @return void
 */
public function generateReport(Request $request) {	
ini_set('memory_limit','2048M');
	error_log(print_r($request->all(),true),3,'/var/www/html/error.log');
	$test_id = $request->raise_test_id;
	$test = RaiseTest::find($test_id); 
	//$response['data'] = $test;
	$reportGenerate = $this->export($test);
	if($test ==''){
		$response['msg']    = config('resource.Raise_testing_id_missing');
		$response['status'] = config('resource.status_201');
		$response['userIsBlocked'] = $this->getUserStatus($request->token);
		return response()->json($response,config('resource.status_201'));
	}   
	$test->paid_status = 1 ;	
	$test->save();       

	$report =  new Report();
	$report->raise_test_id = $test_id;
	$report->save(); 
	
    //save notification start
	$report =  new Notification();
	$report->type          = 'report';
	$report->lab_id        = $test->lab_id ;
	$report->doctor_id     = ($test->doctor_id !=0 || $test->doctor_id !=null) ? $test->doctor_id : $test->doctor_referer_id ;
	$report->patient_id    = $test->patients_id ;
	$report->raise_test_id = $test_id;
	$report->read_status   = 0;
	$report->save();       
	 //save notification start

	//$patientDetails = Patients::find($test->patients_id); 
	$patient_email = $test->getPatient->email;
	$lab_email = $test->getLab->email;
	$lab_name = $test->getLab->name;
	
	$code = $patient_email .'_'.$test_id;
      $encode = base64_encode($code);      
      $data['receipt_link'] = url('/').'/receipt/'.$encode;   
      $data['report_link'] = url('/').'/report/'.$encode;  
      $data['pdf'] = $reportGenerate['report']; 
      $data['testId'] =$test_id;
      $data['receipt'] = $reportGenerate['receipt']; 
      $data['name'] = "Report_".$test_id.".pdf"; 
      $data['to'] = $patient_email; 
      $data['lab_to'] = $lab_email; 
      $data['subject'] = 'Report & Receipt To Patient - Order Id #'.$test_id;        
      $data['lab_subject'] = $lab_name.' - Report & Receipt To Patient - Order Id #'.$test_id;        
	if($patient_email !=''){
		//Mail::to($patient_email)->send(new ReportToPatient($data));
		Mail::send('email.report',$data, function($message) use ($data)
	      {
	          $message->to($data['to']);
	          $message->cc(config('mail.MailCcFunction')); 
	          $message->attach($data['pdf']);
	          $message->attach($data['receipt']);
	          $message->subject($data['subject']);
	          
	      });

	}
	if($lab_email !=''){
		//Mail::to($patient_email)->send(new ReportToPatient($data));
		Mail::send('email.report',$data, function($message) use ($data)
	      {
	          $message->to($data['lab_to']); 
	          $message->cc(config('mail.MailCcFunction'));
	          $message->attach($data['pdf']);
	          $message->attach($data['receipt']);
	          $message->subject($data['lab_subject']);
	          
	      });

	}
	if($test->getPatient->phone !=''){
		/*$msg = 'Dowload your Receipt & Report for Order id #'.$test_id.'using the below link %0a Receipt : '.$data['receipt_link'].'%0a %0a Report : '.$data['report_link'].'%0a %0a Thanks %0a Livigro';
		send_sms(substr($test->getPatient->phone,3),$msg);*/
		$msg = "Hi ".$test->getPatient->name.",  Livigro Booking ID :".$test_id." has been Delivered  sucessfully     Report:".$data['report_link']." Receipt : ".$data['receipt_link']." ";
		send_sms(substr($test->getPatient->phone,3),$msg);

	}
	if($test->getLab->mobile !=''){
		/*$msg = $lab_name.' - Dowload your Receipt & Report for Order id #'.$test_id.'using the below link %0a Receipt : '.$data['receipt_link'].'%0a %0a Report : '.$data['report_link'].'%0a %0a Thanks %0a Livigro';		
		send_sms(substr($test->getPatient->mobile,3),$msg);*/
		$msg = " Your  Livigro Booking ID :".$test_id." for ".$test->getPatient->mobile." has been Delivered sucessfully   Report: ".$data['report_link']."  Receipt :".$data['receipt_link']." ";		
		send_sms(substr($test->getPatient->mobile,3),$msg);

	}
	$response['msg']    = config('resource.Report_message'); 
	$response['status'] = config('resource.status_200');
	$response['userIsBlocked'] = $this->getUserStatus($request->token);
	return response()->json($response,config('resource.status_200'));

}


/**
 * get country function
 *
 * @param Request $request
 * @return void
 */
public function getCountry(Request $request) {
	$country = Country::where('flag','!=','')->get();
	//dd($country);
	foreach($country as $countries)
	{
		$countries->flag = 'images/flags/'.$countries->flag;
		$coun[] = $countries;

	}
	$response['data'] = $coun;
	$response['status'] = config('resource.status_200');	
	return response()->json($response,config('resource.status_200'));

}
/**
 * [getLabNotification description]
 * @param  Request $request [description]
 * @return [type]           [description]
 */
public function getLabNotification(Request $request){
	/*error_log(print_r($request->all(),true),3,'/var/www/html/error.log');*/	
	$lab_id = $this->getUserId($request->token);
	$notification = Notification::where([['lab_id',$lab_id]]);
	$data = $notification->orderBy('id','desc')->jsonPaginate();	
	$result = array(); 
	foreach($data as $key=>$da)
	{
		//get lab details
		$lab_name = $lab_photo = '';		 
		$Labs_data = Labs::find($da->lab_id);
		if(isset($Labs_data)){
		  $lab_name = $Labs_data->name;
		  $lab_photo = $Labs_data->profile_image;
		}
		//get lab details
		//get doctor details
		$doctor_name = $doctor_photo = '';	
		$Doctors_data = Doctors::find($da->doctor_id);
		if(isset($Doctors_data)){
		  $doctor_name = $Doctors_data->name;
		  $doctor_photo = $Doctors_data->profile_image;
		}
		//get doctor details
		//get patient details
		$patient_name = $patient_photo = '';
		$Patients_data = Patients::find($da->patient_id);
		if(isset($Patients_data)){
		  $patient_name = $Patients_data->name;
		  $patient_photo = $Patients_data->profile_image;
		}
		//get patient details

		$mytime = Carbon::now();        
		$time = new Carbon($da->created_at);
		$shift_end_time = new Carbon($mytime->toDateTimeString());
		$val = $time->diffForHumans($shift_end_time);
		$resul = str_replace("before","ago",$val);
		if($patient_name == ''){
			$da->message = 'Your subscription pack will be expire';
		}elseif($da->type == 'test')
		{
            $da->message = 'Has assigned a test for' ;
		}else{
            $da->message = 'Report completed for' ;
		}
		$da->date = $resul ;
		$da->patient_name  = $patient_name ;
		$da->patient_photo = $patient_photo ;
		$da->doctor_name   = $doctor_name ;
		$da->doctor_photo  = $doctor_photo ;
		$da->lab_name      = $lab_name ;
		$da->lab_photo     = $lab_photo ;
		$result[]          = $da;		
	} 
	$response['data'] = $data;
	$response['notification_count'] = Notification::where([['lab_id',$lab_id],['read_status',0]])->count();
	$response['status'] = config('resource.status_200');
	$response['userIsBlocked'] = $this->getUserStatus($request->token);
	$user = $this->getAuthUser($request->token);
	$value = DB::table('fc_WSAuth')->first()->enable_log;
	if($value)   {   
		storelivigroactivity($user,config('resource.notification_api_call'));
	}
	return response()->json($response,config('resource.status_200'));

}
/**
 * [raise_test_all description]
 * @param  Request $request [description]
 * @return [type]           [description]
 */

public function raise_test_all(Request $request)
     { 
     	//error_log(print_r('testing',true),3,'/var/www/html/error.log');
     	//error_log(print_r($request->all(),true),3,'/var/www/html/error.log');
     	$lab_id = $this->getUserId($request->token);
     	if($request->filled('notification_id'))
        {  
        	Notification::where([['lab_id',$lab_id],['id',$request->notification_id]])->update(['read_status'=>1]);
        }
        if($request->filled('raise_test_id'))
        {
                
        $raise_test_id = $request->raise_test_id;
        $data = RaiseTest::find($raise_test_id);
        if(@count($data) !=0)
        {
                $data = RaiseTest::where('id',$raise_test_id)->with('getPatient')->first();
                $Report = Report::where('raise_test_id',$raise_test_id)->get();      
                $receipt_dat = Receipt::where('raise_test_id',$raise_test_id)->get();
                $recipt_sample = 0;     
                foreach($receipt_dat as $dat_rec) {
                        if($dat_rec->specimen_collected == 1){
                                $recipt_sample = 1;
                                break;
                        }
                }
                 if($Report->count() !=0){
                          $order_id = '#'.$raise_test_id ;
		  $order_status = 'yes' ;}else{
                          $order_id = '#'.$raise_test_id ;
                          $order_status = 'no' ; }

                $data->order_id   = $order_id ;
                $data->order_status   = $order_status ;
                $data->patient_id = 'AW' ;
                $data->speci_collected = ($data->specimen_collected != '') ? $data->specimen_collected : 0 ;
             // $data->receipt_data = ($receipt_dat->specimen_collected != 'null' && $receipt_dat->specimen_collected !='') ? $receipt_dat->specimen_collected : 0 ;    
                $data->receipt_data = $recipt_sample;

                // $RaiseTest_data = RaiseTest::where('lab_id',$lab_id)->get(); 
                // $raise_test_ids = '';  
       //      foreach ($RaiseTest_data as $key => $value) 
       //      {
       //        $raise_test_ids.= implode(',',$value->lab_tests_id).',';
       //      }
       //      $raise_test_ids = trim(implode(',',array_unique(explode(',', $raise_test_ids))), ",");            
                // $data['labs_data'] = LabsTest::whereIn('id',explode(',',$raise_test_ids))->get();    
                $data['labs_data'] = LabsTest::whereIn('id',$data->lab_tests_id)->get();
                $response['data'] = $data;
                $response['status'] = config('resource.status_200');
                $response['userIsBlocked'] = $this->getUserStatus($request->token);
                return response()->json($response,config('resource.status_200'));
        }else
        {
                $response['data'] = 'No Record Found';
                $response['status'] = config('resource.status_201');
                $response['userIsBlocked'] = $this->getUserStatus($request->token);
                $user = $this->getAuthUser($request->token);
                 $value = DB::table('fc_WSAuth')->first()->enable_log;
                if($value)   {
                        storelivigroactivity($user,"All Raised Test");
                }
                return response()->json($response,config('resource.status_201'));
	    }
    }
    else
    {
            $response['data']   = 'Parameter missing';
            $response['status'] = config('resource.status_201');
            $response['userIsBlocked'] = $this->getUserStatus($request->token);
            return response()->json($response,config('resource.status_201'));

    }
   }

/*
public function raise_test_all(Request $request)
{
	error_log(print_r($request->all(),true),3,'/var/www/html/error.log');
	if($request->filled('raise_test_id'))
	{
		$lab_id = $this->getUserId($request->token);
		$raise_test_id = $request->raise_test_id;         
		$data = RaiseTest::find($raise_test_id);
		if(@count($data) !=0)
		{
			$data = RaiseTest::where('id',$raise_test_id)->with('getPatient')->first(); 
			$receipt_dat = Receipt::where('raise_test_id',$raise_test_id)->get(); 
			$recipt_sample = 0;    
			foreach($receipt_dat as $dat_rec) {
				if($dat_rec->specimen_collected == 1){
					$recipt_sample = 1;
					break;
				}
			}
			$data->order_id   = '#'.$data->id ; 
			$data->patient_id = 'AW' ;  
			$data->speci_collected = ($data->specimen_collected != '') ? $data->specimen_collected : 0 ; 
			//   $data->receipt_data = ($receipt_dat->specimen_collected != 'null' && $receipt_dat->specimen_collected !='') ? $receipt_dat->specimen_collected : 0 ;  
			$data->receipt_data = $recipt_sample;

			// $RaiseTest_data = RaiseTest::where('lab_id',$lab_id)->get(); 
			// $raise_test_ids = '';  
			//      foreach ($RaiseTest_data as $key => $value) 
			//      {
			//            $raise_test_ids.= implode(',',$value->lab_tests_id).',';
			//      }
			//      $raise_test_ids = trim(implode(',',array_unique(explode(',', $raise_test_ids))), ",");            
			// $data['labs_data'] = LabsTest::whereIn('id',explode(',',$raise_test_ids))->get();      
			$data['labs_data'] = LabsTest::whereIn('id',$data->lab_tests_id)->get();        

			$response['data'] = $data;
			$response['status'] = config('resource.status_200');
			return response()->json($response,config('resource.status_200'));
		}else
		{
			$response['data'] = config('resource.record_not_found');
			$response['status'] = config('resource.status_201');
			$user = $this->getAuthUser($request->token);
			$value = DB::table('fc_WSAuth')->first()->enable_log;
			if($value)   {   
				storelivigroactivity($user,config('resource.all_raised_test')); 
			}
			return response()->json($response,config('resource.status_201'));
		}
	}
	else
	{
		$response['data'] = config('resource.parameter_missing');
		$response['status'] = config('resource.status_201');
		return response()->json($response,config('resource.status_201'));

	}
}*/
/**
 * [lab_receipt_list description]
 * @param  Request $request [description]
 * @return [type]           [description]
 */
public function lab_receipt_list(Request $request)
{ 
    error_log(print_R($request->all(),true),3,'/var/www/html/error.log');
	$raise_test_id = $request->raise_test_id;          
	$data = RaiseTest::find($raise_test_id);
	if(@count($data) !=0)
	{ 


		$data = RaiseTest::where('id',$raise_test_id)->with('getPatient')->first();    
		$data->order_id   = '#'.$data->id ; 
		$data->patient_id = 'AW' ;  


		$receipt_dat = Receipt::where('raise_test_id',$raise_test_id)->get();  
		$amount = $receipt_dat->sum('amount');

		//receipt data amount update raise test table(orginal amount) 
		/*$newamount = RaiseTest::find($raise_test_id);
		$newamount->original_amount = $amount ;
		$newamount->save();*/
		//receipt end

	$receipt_data = Receipt::where('raise_test_id',$raise_test_id)->where('test_type','single')->get();    
		$receipt_list = array();          
		foreach ($receipt_data as $key => $value) 
		{            
			$value->test_name  = getLabtestdetails($value->tests_id,'name');                     
			$receipt_list[] = $value;

		}
		$unique_data = array_unique($receipt_list);       
		// now use foreach loop on unique data
		$receipt_lists = array();
		foreach($unique_data as $val) {
			$receipt_lists[] = $val; 
		}

		$receipt_data_package = Receipt::where('raise_test_id',$raise_test_id)
			                   ->where('test_type','!=','single')
			                   ->where('amount','!=',0)
			                   ->get()->toArray();
		$packagereceipt_list = array();
		if(count($receipt_data_package)){        
			foreach ($receipt_data_package as $key => $value) {							
				$value['test_name']    = Package::find($value['test_type'])->package_name;                    
				$packagereceipt_list[] = $value;
				}	
			}                   		                  

		$data['Total_amount'] = $data->original_amount;     
		$response['data']   = $data;            
		$data['Receipt']    = array_merge($receipt_lists,$packagereceipt_list);             
		$response['status'] = config('resource.status_200');   
		$user = $this->getAuthUser($request->token);
		$value = DB::table('fc_WSAuth')->first()->enable_log;
		if($value)   {   
			storelivigroactivity($user,config('resource.lab_recipt_api_call'));        
		}
		return response()->json($response,config('resource.status_200'));
	}else
	{
		$response['msg']   = config('resource.no_record'); 
		$response['status'] = config('resource.status_201');
		$response['userIsBlocked'] = $this->getUserStatus($request->token);
		return response()->json($response,config('resource.status_201'));
	}
}
/**
 * [lab_test_subcategories description]
 * @param  Request $request [description]
 * @return [type]           [description]
 */
public function lab_test_subcategories(Request $request)
{/*error_log(print_R($request->all(),true),3,'/var/www/html/error.log');*/
  
	$lab_id = $this->getUserId($request->token); 
	$raise_id = $request->raise_test_id;           
	$final_data = array();
	$id_check = RaiseTest::where('id',$raise_id)->get();
	if($id_check->count() ==0){
		$response['msg']   = config('resource.no_record'); 
		$response['status'] = config('resource.status_201');
		$response['userIsBlocked'] = $this->getUserStatus($request->token);
		return response()->json($response,config('resource.status_201'));
	}

	$data= RaiseTest::where('id',$raise_id)->first()->lab_tests_id;  
	if($data ==''){ $response['msg'] = config('resource.No_data_in_raise_table') ; $response['status'] = config('resource.status_201');
	    $response['userIsBlocked'] = $this->getUserStatus($request->token);        
		return response()->json($response,config('resource.status_201'));}           
	$final_data = [];
	foreach ($data as $key => $value){	
		$labs_count = LabsTest::where('id',$value)->get()->count();
		$labs = LabsTest::find($value);
	if($labs_count !=0){
		$labs->labs_subcate = LabsCategory::where('status',1)->get();     
		$valueposition = Receipt::where([['raise_test_id',$request->raise_test_id],
				['tests_id',$value]])->first(); 
		if($valueposition !=''){

			/*$labs->position = ($valueposition->labtest_sub_name !='' && $valueposition->labtest_sub_name !='null') ? $valueposition->labtest_sub_name : 0 ;*/

			if($valueposition->labtest_sub_name !='' && $valueposition->labtest_sub_name !='null'){
             $labs->position = $valueposition->labtest_sub_name ;
			}else{
             $lab_test_position = LabsCategory::where('lab_test_id',$value)->where('status',1)->get();		
			if($lab_test_position->count() !=0){
			 $labs->position = $lab_test_position->first()->id;
			}else{
			 $labs->position = 0 ;	
			    }
			}

		}else{
			$lab_test_position = Package::where('id',$value)->where('status',1)->get();
			if($lab_test_position->count() !=0)
			  $labs->position = $lab_test_position->first()->id;
			else
			  $labs->position = 0 ;
		}
		$final_data[]  = $labs ;
	}  
 }          
	$response['data'] = $final_data ;                         
	$response['status'] = config('resource.status_200');
	$response['userIsBlocked'] = $this->getUserStatus($request->token);        
	return response()->json($response,config('resource.status_200'));
}
/**
 * [add_new_rise_test description]
 * @param Request $request [description]
 */
public function add_new_rise_test(Request $request)
{         
 error_log(print_r($request->all(),true),3,'/var/www/html/error.log');
  if($request->type !='single' && $request->type !='group')
	 { 
	 	 $response['status']  = config('resource.status_201');
		 $response['message'] = config('resource.type_wrong');
		 $response['userIsBlocked'] = 1 ;
		return response()->json($response,config('resource.status_200'));
	 }	
	 $lab_test_id = $request->lab_test_id; 
	if($request->type == 'single')
	{	 
		$lab_id = $this->getUserId($request->token); 	 
        $amount_price = 0;
        $labtest_count = LabsAvaTest::where('lab_id',$lab_id)->where('lab_tests_id',$lab_test_id)->get();
        if($labtest_count->count() == 0){
          $test_data = LabsTest::find($lab_test_id);
          $amount_price = $test_data->price;
        }else{
          $test_data = LabsAvaTest::where('lab_id',$lab_id)->where('lab_tests_id',$lab_test_id)->first();
          $amount_price = $test_data->amount;   
        }
		$raise_id = $request->raise_test_id;
		$RaiseTest = RaiseTest::find($raise_id);
		$old_test_id = $RaiseTest->lab_tests_id;
		array_push($old_test_id,$lab_test_id);       
		$RaiseTest->lab_tests_id = $old_test_id ;
		$RaiseTest->original_amount = $RaiseTest->original_amount+ $amount_price;
		$RaiseTest->save();
		$test_data->labs_subcate = LabsCategory::all();	

			$specimen_count = LabsCategory::where('lab_test_id',$lab_test_id)->where('status',1)->get();
				if($specimen_count->count() !=0){
	               $specimen_id = $specimen_count->first()->id ;
				}
				else{
				   $specimen_id = NULL ;
				}
			 //insert package test in receipt table			 	
		    $lab_count =LabsTest::where('id',$lab_test_id)->get();		    
		    if($lab_count->count() !=0){		    
            $units_count = Units::where('id',$lab_count->first()->measurement)->get();
            if($units_count->count() !=0){
             $units = $units_count->first()->name;
            }else{
             $units = '-nill-' ;
            }}else{
             $units = '-nill-' ;
            }
		Receipt::create([
				'raise_test_id' => $raise_id,
				'tests_id'  => $lab_test_id,
				'amount' =>  $test_data->amount,
				'labtest_sub_name' =>  $specimen_id,
				'units' =>  $units,
				'test_type' =>'single',
		]);
	  $response['data'] = array($test_data); 
	}else{
		   $lab_id = $this->getUserId($request->token);
		   $amount_price = 0 ;
		   $package = LabsAvaTest::where('lab_tests_id',$lab_test_id)
                                 ->where('lab_id',$lab_id)
                                 ->where('test_type',$request->type)
                                 ->where('status',1)
                                 ->get();
       		   
	       if($package->count() != 0){		          
		          $amount_price = $package->first()->amount;
		          $package_tests_id = package::find($lab_test_id);
	              }
	           
	     if(empty($package->first()->lab_tests_id)){
			$response['msg']    = config('resource.no_record'); 
		    $response['status'] = config('resource.status_201');
		    $response['userIsBlocked'] = $this->getUserStatus($request->token);
		    return response()->json($response,config('resource.status_200'));
		}		
        $lab_test_id = $package_tests_id->tests_id; 			
		$raise_id = $request->raise_test_id;
		$RaiseTest = RaiseTest::find($raise_id);
		$old_test_id = $RaiseTest->lab_tests_id;
		$merge_id = array_merge($old_test_id,$lab_test_id);       
		$RaiseTest->lab_tests_id = $merge_id ;
		$RaiseTest->original_amount = $RaiseTest->original_amount + $amount_price ;
		$RaiseTest->save();			
		foreach($package_tests_id->tests_id as $key =>$testid){			
    	       $package_test_available = LabsAvaTest::where('lab_tests_id',$request->lab_test_id)
    		                                     ->where('lab_id',$lab_id)
    		                                     ->where('test_type',$request->type)
    		                                     ->where('status',1)
    		                                     ->get();  
				if($package_test_available->count() !=0)
				{
					$package_amount = $package_test_available->first()->amount;
				}
			    if($key==0){
			 		$package_total_amount = $package_amount ;
			 	}else{
                    $package_total_amount = '0' ;
			 	}
			 	$specimen_count = LabsCategory::where('lab_test_id',$testid)->where('status',1)->get();
				if($specimen_count->count() !=0){
	               $specimen_id = $specimen_count->first()->id ;
				}
				else{
				   $specimen_id = NULL ;
				}
			 //insert package test in receipt table			 	
		    $lab_count =LabsTest::where('id',$testid)->get();		    
		    if($lab_count->count() !=0){		    
            $units_count = Units::where('id',$lab_count->first()->measurement)->get();
            if($units_count->count() !=0){
             $units = $units_count->first()->name;
            }else{
             $units = '-nill-' ;
            }}else{
             $units = '-nill-' ;
            }
            //CREATE TEST TEST PACKAGE
             	Receipt::create([
						'raise_test_id'=>$raise_id,
						'tests_id'=>$testid,
						'labtest_sub_name'=> $specimen_id,
						'amount' => $package_total_amount,
						'units' =>  $units,
				'reference_range' => (isset($package_test_available->first()->range)) ? $package_test_available->first()->range :0,
				'reference_endrange' =>(isset($package_test_available->first()->range_end)) ? $package_test_available->first()->range_end:0,
						'test_type' => $request->lab_test_id
				]);			 
			//CREATE TEST TEST PACKAGE			
		}		
	}
	$response['msg']  = config('resource.data_added_successfully'); 	                
	$response['status'] = config('resource.status_200'); 
	$response['userIsBlocked'] = $this->getUserStatus($request->token);       
	return response()->json($response,config('resource.status_200'));     
}
/**
 * [change_collected_confirm_test description]
 * @param  Request $request [description]
 * @return [type]           [description]
 */

public function change_collected_confirm_test(Request $request)
{
	//$response['requesttt'] = $request->raise_test_id;
    error_log(print_r($request->all(),true),3,'/var/www/html/error.log');    
	if($request->filled('raise_test_id') && $request->filled('type'))
	{
		if($request->type == 'collected')
			$data = RaiseTest::find($request->raise_test_id);
		if($request->type == 'receipt')
			$data = Receipt::where('raise_test_id',$request->raise_test_id);

		if($data->count() !=0)
		{
			if($request->type == 'collected'){
				$data->specimen_collected = 1 ;
				$data->save(); }
			if($request->type == 'receipt') { 
				$raise_test_id = $request->raise_test_id;
        		$raise = RaiseTest::find($raise_test_id);
        		$amount_received = ($request->received_amount != '')?$request->received_amount:'0' ;
        		$discount_price  = ($request->discount_price != '')?$request->discount_price: '0' ;
        		$input_amount = $amount_received ; 
		        $remaining_amount = $raise->original_amount - $input_amount ; 
		         if($raise->original_amount < $input_amount){
		         	$response['msg']    = 'Please check your input amount';
		         	$response['status'] = config('resource.status_201');
					$response['userIsBlocked'] = $this->getUserStatus($request->token);
					return response()->json($response,config('resource.status_201'));
		         }
		         $set_discount_price = $discount_price;
		         $set_remaining_amount = $remaining_amount - $discount_price;
		         $set_amount_received = $input_amount;
		          if($raise->received_amount != null || $raise->received_amount != 0 )
		              $set_amount_received = $raise->received_amount+ $input_amount;
		          if($raise->discount_price != null || $raise->discount_price != 0 )
		              $set_discount_price = $raise->discount_price + $discount_price;
		          if($raise->remaining_amount != null || $raise->remaining_amount != 0 )
		             $set_remaining_amount = $raise->original_amount - $set_amount_received - $set_discount_price;

		        $mode = $request->payment_mode;        
		        if($set_remaining_amount >= 0 ){
		          $raise->discount_price = $set_discount_price;
		          $raise->remaining_amount = $set_remaining_amount;
		          $raise->received_amount = $set_amount_received;
		          $raise->mode_of_payment = $mode;
		          if(($raise->received_amount+$raise->discount_price) == $raise->original_amount)
		            $raise->paid_status = '1';

		          $raise->save();
		      }
		      if($raise->remaining_amount == 0 && ($raise->received_amount+$raise->discount_price) == $raise->original_amount){
		      	Receipt::where('raise_test_id',$request->raise_test_id)->update(['specimen_collected'=>1]);
		      }
		          $data = Receipt::where('raise_test_id',$request->raise_test_id)->get();
			}
             

			$response['msg']    = 'Updated Successfully';
			$response['data']   =  $data;

			if($request->type == 'receipt')
			 	$response['raise_table_data'] = RaiseTest::find($request->raise_test_id);			 
			$response['status'] =  config('resource.status_200');
			$response['userIsBlocked'] = $this->getUserStatus($request->token);
			return response()->json($response,config('resource.status_200'));
		}
		else{
			$response['msg'] = 'No Record Found';
			$response['status'] = config('resource.status_201');
			$response['userIsBlocked'] = $this->getUserStatus($request->token);
			return response()->json($response,config('resource.status_201'));
		}
	}else
	{
		$response['msg'] = 'No Record Found';
		$response['status'] = config('resource.status_201');
		$response['userIsBlocked'] = $this->getUserStatus($request->token);
		return response()->json($response,config('resource.status_201'));
	}
}


/*
public function change_collected_confirm_test(Request $request)
{ 
	if($request->filled('raise_test_id') && $request->filled('type'))
	{  
		if($request->type == 'collected')
			$data = RaiseTest::find($request->raise_test_id);
		if($request->type == 'receipt') 
			$data = Receipt::where('raise_test_id',$request->raise_test_id);
		if($data->count() !=0)
		{
			if($request->type == 'collected'){                                                                  
				$data->specimen_collected = 1 ;
				$data->save(); }
			if($request->type == 'receipt') {                       

				//subtraction amount 
				if($request->filled('received_amount')){
					$data = RaiseTest::find($request->raise_test_id);
					if( $data->original_amount > $request->received_amount){
						$subtraction_amount = $data->original_amount - $request->received_amount ;
						Receipt::where('raise_test_id',$request->raise_test_id)->update(['specimen_collected'=>0]);   
					}else{
						$subtraction_amount = 0; 
						Receipt::where('raise_test_id',$request->raise_test_id)->update(['specimen_collected'=>1]);                                 
					}

					$data->remaining_amount = $subtraction_amount ;
					$data->received_amount = $request->received_amount;
					$data->save();
				  if($subtraction_amount == 0){                          
                                        Receipt::where('raise_test_id',$request->raise_test_id)->update(['specimen_collected'=>1]); }else{                                              
                                        Receipt::where('raise_test_id',$request->raise_test_id)->update(['specimen_collected'=>0]);             

					/*RaiseTest::where('id',$request->raise_test_id)->update(['received_amount'=>$request->received_amount]);				}
				//subtraction amount

				RaiseTest::where('id',$request->raise_test_id)->update(['received_amount'=>$request->received_amount]);
				$data = Receipt::where('raise_test_id',$request->raise_test_id)->get();
			}
			$response['msg']    = config('resource.updated_successfully');
			$response['data']   =  $data;
			$response['status'] =  config('resource.status_200');
			return response()->json($response,config('resource.status_200'));
		}
		else{
			$response['msg'] = config('resource.no_record');
			$response['status'] = config('resource.status_201');
			return response()->json($response,config('resource.status_201'));
		}
	}else
	{
		$response['msg'] = config('resource.no_record');
		$response['status'] = config('resource.status_201');
		return response()->json($response,config('resource.status_201'));
	}
}


*/
/**
 * receipt data amount update
 * @param  Request $request [description]
 * @return [type]           [description]
 */
public function updateReceiptamount(Request $request)
{      /* error_log(print_r($request->all(),true),3,'/var/www/html/error.log'); */ 
	if($request->filled('receipt_id') && $request->filled('amount'))
	{    
		$data = Receipt::find($request->receipt_id);
		$check_data = Receipt::where('id',$request->receipt_id)->get();
		if($check_data->count() !=0)
		{     
			//Receipt table update amount                 
			$data->amount = $request->amount ;
			$data->save();
			//rise table update amount
			RaiseTest::where('id',$data->raise_test_id)->update([
					'received_amount'=>$request->amount,                   
			]);

			$response['msg']    = config('resource.receipt_updated');
			$response['data']   = $data ;
			$response['status'] = config('resource.status_200');
			$response['userIsBlocked'] = $this->getUserStatus($request->token);
			return response()->json($response,config('resource.status_200'));
		}else
		{
			$response['msg'] = config('resource.no_record');
			$response['status'] = config('resource.status_201');
			$response['userIsBlocked'] = $this->getUserStatus($request->token);
			return response()->json($response,config('resource.status_201'));

		}
	}else
	{
		$response['msg'] = config('resource.no_record');
		$response['status'] = config('resource.status_201');
		$response['userIsBlocked'] = $this->getUserStatus($request->token);
		return response()->json($response,config('resource.status_201'));
	}
}

/**
 * [add_new_test_lab description]
 * @param Request $request [description]
 */
public function add_new_test_lab(Request $request)
{         
   /*error_log(print_r($request->all(),true),3,'/var/www/html/error.log');*/
	if($request->raise_test_id =='')
	{
       $response['msg'] = 'parameter missing';
	   $response['status'] = config('resource.status_201');
	   $response['userIsBlocked'] = $this->getUserStatus($request->token);
	   return response()->json($response,config('resource.status_200'));
	}
    $lab_id = $this->getUserId($request->token);    
	$raise_id = $request->raise_test_id;
	$old_test_id = RaiseTest::find($raise_id)->lab_tests_id;                   
	$lab_ava_test = LabsAvaTest::where('lab_id',$lab_id)->where('status',1)->whereNotIn('lab_tests_id',$old_test_id)->pluck('lab_tests_id');  
	$lab_test_data = LabsTest::whereIn('id',$lab_ava_test)->where('status',1)->jsonPaginate();  	
	if($request->filled('search'))
	{  
      $lab_test_data = LabsTest::whereIn('id',$lab_ava_test)
                               ->where('name','ILIKE','%'.$request->search.'%')
                               ->where('status',1)
                               ->jsonPaginate(); 
	}
	if(count($lab_test_data) ==0){
        $response['msg']    = config('resource.no_record');
		$response['status'] = config('resource.status_201');
		$response['userIsBlocked'] = $this->getUserStatus($request->token);
		return response()->json($response,config('resource.status_200'));
	}	     
	$response['data'] = $lab_test_data;                    
	$response['status'] = config('resource.status_200');   
	$response['userIsBlocked'] = $this->getUserStatus($request->token);
	$user = $this->getAuthUser($request->token);
	$value = DB::table('fc_WSAuth')->first()->enable_log;
	if($value)   {   
		storelivigroactivity($user,config('resource.add_new_test_lab'));
	} 
	return response()->json($response,config('resource.status_200'));     
}

/**
 * [add_new_test_lab description]
 * @param Request $request [description]
 */
public function AddNewPackageSamplepage(Request $request)
{         
   /*error_log(print_r($request->all(),true),3,'/var/www/html/error.log');*/
	if($request->raise_test_id =='')
	{
       $response['msg'] = 'parameter missing';
	   $response['status'] = config('resource.status_201');
	   $response['userIsBlocked'] = $this->getUserStatus($request->token);
	   return response()->json($response,config('resource.status_200'));
	}
    $lab_id = $this->getUserId($request->token);    
	$raise_id = $request->raise_test_id;
	$old_test_id = RaiseTest::where('id',$raise_id)->get();   
	if($old_test_id->count() ==0)
	{
        $response['msg']    = config('resource.no_record');
		$response['status'] = config('resource.status_201');
		$response['userIsBlocked'] = $this->getUserStatus($request->token);
		return response()->json($response,config('resource.status_200'));
	}	
	$old_group_id = Receipt::where('raise_test_id',$raise_id)                                
						   ->where('test_type','!=','single')
						   ->pluck('test_type');
	$lab_ava_test = LabsAvaTest::where('lab_id',$lab_id)->where('status',1)
	                           ->where('test_type','group')
	                           ->whereNotIn('lab_tests_id',$old_group_id)	                           
	                           ->pluck('lab_tests_id');
	$lab_test_data = Package::whereIn('id',$lab_ava_test)->where('status',1)->jsonPaginate();  	
	if($request->filled('search'))
	{  
      $lab_test_data = Package::whereIn('id',$lab_ava_test)
                               ->where('package_name','ILIKE','%'.$request->search.'%')
                               ->where('status',1)
                               ->jsonPaginate(); 
	}
	if(count($lab_test_data) ==0){
        $response['msg']    = config('resource.no_record');
		$response['status'] = config('resource.status_201');
		$response['userIsBlocked'] = $this->getUserStatus($request->token);
		return response()->json($response,config('resource.status_200'));
	}	     
	$response['data'] = $lab_test_data;                    
	$response['status'] = config('resource.status_200');   
	$response['userIsBlocked'] = $this->getUserStatus($request->token);
	$user = $this->getAuthUser($request->token);
	$value = DB::table('fc_WSAuth')->first()->enable_log;
	if($value)   {   
		storelivigroactivity($user,config('resource.add_new_test_lab'));
	} 
	return response()->json($response,config('resource.status_200'));     
}

/**
 * [lab_test_id_subcategories description]
 * @param  Request $request [description]
 * @return [type]           [description]
 */
public function lab_test_id_subcategories(Request $request){          
	$lab_test_id = $request->lab_test_id;         
	$data = LabsCategory::where('lab_test_id',$lab_test_id)->get();
	if(@count($data) !=0)
	{  
		$response['data'] = $data ;                  
		$response['status'] = config('resource.status_200');
		$response['userIsBlocked'] = $this->getUserStatus($request->token);        
		return response()->json($response,config('resource.status_200'));
	}else
	{
		$response['msg'] = config('resource.no_record');
		$response['status'] = config('resource.status_201');
		$response['userIsBlocked'] = $this->getUserStatus($request->token);
		return response()->json($response,config('resource.status_201'));
	}
}

/**
 * [lab_add_update_collectiondata description]
 * @param  Request $request [description]
 * @return [type]           [description]
 */
public function lab_add_update_collectiondata(Request $request)
{       

  error_log(print_r($request->all(),true),3,'/var/www/html/error.log');
    $lab_id = $this->getUserId($request->token);
	$lab_test_id = $request->lab_test_id;  
	$rise_id = $request->rise_id;  
	$subcate_name = $request->subcate_name;  

	$receipt_data = Receipt::where('tests_id',$lab_test_id)
		->where('raise_test_id',$rise_id)                                
		->first();
	$receipt_d = Receipt::where('tests_id',$lab_test_id)
		->where('raise_test_id',$rise_id)
		->get();                            
	if($receipt_d->count() !=0)
	{  	 
	  $count = LabsAvaTest::where('lab_id',$lab_id)->where('lab_tests_id',$lab_test_id)->get();		
		if($count->count() !=0){
		    $data = LabsAvaTest::where('lab_id',$lab_id)->where('lab_tests_id',$lab_test_id)->first();
	     }else{
	     	$data = LabsTest::find($lab_test_id);
	     } 			
		$receipt_data->labtest_sub_name =  ($subcate_name) ? $subcate_name : '';
		if($receipt_d->first()->test_type == 'single'){ 
		$receipt_data->amount = ($data->amount!= null || $data->amount !='') ? $data->amount : '0';
		}
$receipt_data->reference_range    = (isset($data->range)) ? $data->range :$data->reference_range;
$receipt_data->reference_endrange = (isset($data->range_end)) ? $data->range_end :$data->reference_endrange;
		$receipt_data->save();	   
		$response['msg'] = config('resource.updated_successfully');           
		$response['status'] = config('resource.status_200'); 
		$response['userIsBlocked'] = $this->getUserStatus($request->token);       
		return response()->json($response,config('resource.status_200'));
	}else
	{   
		$data = LabsTest::find($lab_test_id);
		$receipt_data = new  Receipt();
		$receipt_data->raise_test_id = ($rise_id) ? $rise_id : '';
		$receipt_data->tests_id =  ($lab_test_id) ? $lab_test_id : '';
		$receipt_data->labtest_sub_name =  ($subcate_name) ? $subcate_name : '';
		$receipt_data->amount = (@$data->price != null || @$data->price != '') ? @$data->price : '0';
		$receipt_data->reference_range    = ($data->reference_range) ? $data->reference_range :0;
		$receipt_data->reference_endrange = ($data->reference_endrange) ? $data->reference_endrange :0;
		/*$receipt_data->result = '';	
		$receipt_data->units  = '';	*/
		$receipt_data->save();
		$response['msg']    = config('resource.added_successfully') ;                   
		$response['status'] = config('resource.status_200'); 
		$response['userIsBlocked'] = $this->getUserStatus($request->token);       
		return response()->json($response,config('resource.status_200'));
	}         
}
/**
 * [getReceipt description]
 * @param  Request $request [description]
 * @return [type]           [description]
 */
public function getReceipt(Request $request){
	$tests_id = $request->tests_id;
	$receipt = Receipt::where('raise_test_id',$tests_id)->with('testData')->get();
	$response['data'] = $receipt;
	$response['status'] = config('resource.status_200');
	$response['userIsBlocked'] = $this->getUserStatus($request->token);
	return response()->json($response,config('resource.status_200'));
}
/**
 * [getlabgenerateReport description]
 * @param  Request $request [description]
 * @return [type]           [description]
 */
public function getlabgenerateReport(Request $request){
/*error_log(print_r($request->all(),true),3,'/var/www/html/error.log'); */ 
	if($request->filled('raise_test_id')) 
	{  
		$raise_test_id = $request->raise_test_id;
		/*$data = new Report();
		$data->raise_test_id = $raise_test_id ;
		$data->save();*/
         $order_raised_date     = '';
         $patient_data          = '';

		$Receipt = Receipt::where('raise_test_id',$raise_test_id);
		if($Receipt->count() !=0)
		{
			$RaiseTest = RaiseTest::where('id',$raise_test_id)->with('getPatient')->first();
			$Report = Report::where('raise_test_id',$raise_test_id)->first();   
			if(!empty($RaiseTest->getPatient)){			         
			$RaiseTest->getPatient->patient_id = 'AW'.$RaiseTest->getPatient->id;  
			/*$response['Receipt']   = $Receipt->toArray(); */


			//oreder rised date          
			$RaiseTest->getPatient->date  = Carbon::parse($RaiseTest->getPatient->updated_at)->format('M d, Y - h:m A');
			$RaiseTest->getPatient->date  = str_replace("-","at",$RaiseTest->getPatient->date);            
			//order rised date end
			$order_raised_date = $RaiseTest->getPatient->date;
			$patient_data      = $RaiseTest->getPatient ;
			}   

			//sample collected data
			  $sample_collected  = Carbon::parse($Receipt->get()->first()->updated_at)->format('M d, Y - h:m A');
			  $sample_collected  = str_replace("-","at",$sample_collected);
			//sample collected data

            //report date
			  $report = Carbon::now('Asia/Kolkata')->format('M d, Y - h:m A');
		      $report = str_replace("-","at",$report);
		    //report date
			

			foreach ($Receipt->orderBy('id')->get() as $key => $value) 
			{   $LabsCategory=LabsCategory::where('id',$value->labtest_sub_name)->get();
				if($LabsCategory->count() !=0){$LabsCategoryname =$LabsCategory[0]->name;}else{ $LabsCategoryname =''; }
				   $test_id  = $value->tests_id; 
                  $test_name_check =  LabsTest::where('id',$test_id)->get();
                  if($test_name_check->count() !=0){                  	
                   $test_name = LabsTest::find($test_id)->name; 
                  }else{
                  	$test_name = ''; 
                  }	
				$value->test_name = $test_name;
				$value->labtest_subcategory_name = $LabsCategoryname;
				$value->all_units = Units::orderBy('id','asc')->get()->toArray();
				$Receipts[] = $value;
			}   
			$user_id = $this->getUserId($request->token);
			$user = User::where('linked_id',$user_id)->first();
			$detailed_data = $this->getDetailedData($user);

			if($detailed_data['signature'] !='' && $detailed_data['signature'] !='null' && isset($detailed_data['signature']) )
			{ 
				$signature = getarraylastimages($detailed_data['signature']) ;
				$detailed_data['signatures'] = urlencode($signature) ;  
			}

			$patient_email = $RaiseTest->getPatient->email;
	        $code = $patient_email .'_'.$RaiseTest->id;
            $encode = base64_encode($code);      
            $report_link_pdf = url('/storage/').'/export/Report_'.$RaiseTest->id.'.pdf';           
            $receipt_link_pdf = url('/storage/').'/export/Receipt_'.$RaiseTest->id.'.pdf';       

			$response['order_raised_date'] = $order_raised_date;
			$response['reportedon']        = $report;
			$response['sample_collected']  = $sample_collected;
			$response['receipt_link_pdf']  = $receipt_link_pdf;
			$response['report_link_pdf']   = $report_link_pdf;
			$response['patient_data']      = $patient_data;
			$response['Receipt_data']      = $Receipts;
			$response['signature']         = ($detailed_data['signatures'] !='') ? $detailed_data['signatures']: '' ;
			$response['status'] = config('resource.status_200');
			$response['userIsBlocked'] = $this->getUserStatus($request->token);
			return response()->json($response,config('resource.status_200'));
		}else
		{
			$response['msg'] = config('resource.no_record');
			$response['status'] = config('resource.status_201');
			$response['userIsBlocked'] = $this->getUserStatus($request->token);
			return response()->json($response,config('resource.status_201'));
		}
	}else
	{
		$response['msg'] = config('resource.no_record');
		$response['status'] = config('resource.status_201');
		$response['userIsBlocked'] = $this->getUserStatus($request->token);
		return response()->json($response,config('resource.status_201'));

	}         

}
/**
 * [editGentrateReport description]
 * @param  Request $request [description]
 * @return [type]           [description]
 */
public function editGentrateReport(Request $request){
	/*error_log(print_r($request->all(),true),3,'/var/www/html/error.log');*/
	$lab_id = $this->getUserId($request->token);
	if($request->filled('id'))
	{
		$data = Receipt::where('id',$request->id)->get();
		if($data->count() ==0){
          $response['msg']    = config('resource.no_record');              
		  $response['status'] = config('resource.status_201');
		  $response['userIsBlocked'] = $this->getUserStatus($request->token);
		  return response()->json($response,config('resource.status_201'));
		}

		$data = Receipt::find($request->id);
		if($data)
		{
			/*$data->amount = $request->end_amount ;*/		
			$data->reference_range    = $request->start_amount ;
			$data->reference_endrange = $request->end_amount ;
			$data->result = $request->result ;
			$data->units  = $request->units ;
			$data->save();
	    }

		/*$raiseTest = RaiseTest::find($data->raise_test_id);
		if($raiseTest)
			$raiseTest->original_amount = $request->end_amount ;
		$raiseTest->save();*/

		$LabsTest = LabsAvaTest::where('lab_id',$lab_id)->where('lab_tests_id',$data->tests_id)->first();
		if($LabsTest){
			/*$LabsTest->amount    = $request->result ;*/
			$LabsTest->range     = $request->start_amount ;
			$LabsTest->range_end = $request->end_amount ;
			$LabsTest->status    = 1 ;
		    $LabsTest->save();
		}
		

		$response['msg']    = config('resource.updated_successfully');
		$response['data']   = $data;
		$response['status'] = config('resource.status_200');
		$response['userIsBlocked'] = $this->getUserStatus($request->token);
		return response()->json($response,config('resource.status_200'));
		/* }*/
	}else
	{
		$response['msg']    = config('resource.no_record');              
		$response['status'] = config('resource.status_201');
		$response['userIsBlocked'] = $this->getUserStatus($request->token);
		return response()->json($response,config('resource.status_201'));
	}

}

/**
 * [clinicProfile description]
 * @param  Request $request [description]
 * @return [type]           [description]
 */
public function clinicProfile(Request $request){
	$data = Hospital::create($request->all());
	$response['msg']    = config('resource.updated_successfully');
	$response['data']   =  $data ;                
	$response['status'] = config('resource.status_200');
	$response['userIsBlocked'] = $this->getUserStatus($request->token);
	return response()->json($response, config('resource.status_200'));
}
/**
 * [raise_user_list description]
 * @param  Request $request [description]
 * @return [type]           [description]
 */
public function raise_user_list(Request $request)
{   
	error_log(print_r($request->all(),true),3,'/var/www/html/error.log');
	$lab_id = $this->getUserId($request->token);  $term = $request->search_tag;
	if($request->Type == 'walkin'){ $id = 1;  }else{ $id = 0;} 
	$carbon = Carbon::today();
	$timestamp = $carbon->timestamp ;			
	$today_date = $carbon->format('Y-m-d H:i:s');
	 if($request->from_date !='' && $request->to_date !=''){ $to_date   = $request->to_date ;
				  $from_date = $request->from_date ;
		}
		/*else{  $to_date   = $today_date ; $from_date = $today_date ; }    */
	if($request->search_tag != '')
	{        $datas = array();        
		if($request->Type == 'walkin' || $request->type == 'walkin')
		{      
			if($request->from_date !='' && $request->to_date !=''){
			$data = RaiseTest::where('lab_id',$lab_id) 
				/*->where('other_reason_for_cancel',null)*/
				->where('doctor_referer_id', '0')                        
				->with(array('getPatient' => function($query) use ($term,$to_date,$from_date)
							{  
							$query->where('name','ILIKE','%'.$term.'%' );                              
							$query->orwhere('phone','ILIKE','%'.$term.'%' );                              
							$query->whereDate('updated_at', '>=', $from_date);
							$query->whereDate('updated_at', '<=', $to_date);                          
							}))->orderBy('id','desc')
			->jsonPaginate();  
		}else{
			$data = RaiseTest::where('lab_id',$lab_id) 
				/*->where('other_reason_for_cancel',null)*/
				->where('doctor_referer_id', '0')                        
				->with(array('getPatient' => function($query) use ($term)
							{  
							$query->where('name','ILIKE','%'.$term.'%' );
							$query->orwhere('phone','ILIKE','%'.$term.'%' );                              
							}))->orderBy('id','desc')
			->jsonPaginate(); 
		}

		}else{ 
			if($request->from_date !='' && $request->to_date !=''){
					$data = RaiseTest::where('lab_id',$lab_id) 
						/*->where('other_reason_for_cancel',null)*/
						->where('doctor_referer_id', '!=', '0')                        
						->with(array('getPatient' => function($query) use ($term,$to_date,$from_date)
									{                                
									$query->where('name','ILIKE','%'.$term.'%' );
									$query->orwhere('phone','ILIKE','%'.$term.'%' );
									$query->whereDate('updated_at', '>=', $from_date);
									$query->whereDate('updated_at', '<=', $to_date);                          
									}))->orderBy('id','desc')
					->jsonPaginate(); 
				}else{
					$data = RaiseTest::where('lab_id',$lab_id) 
						/*->where('other_reason_for_cancel',null)*/
						->where('doctor_referer_id', '!=', '0')                        
						->with(array('getPatient' => function($query) use ($term)
									{                                
									$query->where('name','ILIKE','%'.$term.'%' );
									$query->orwhere('phone','ILIKE','%'.$term.'%' );
									}))->orderBy('id','desc')
					->jsonPaginate(); 
				}
		}
		if($data)
		{           
			foreach ($data as $key => $value)
			{                     
				if($value->getPatient)
				{ 
					$datas[] = $value  ;
				}
			}
		}else
		{
			$datas = '';
		}
        if(count($data) ==0)
	   {  
	  	$response['msg'] = config('resource.no_record');
		$response['status'] = config('resource.status_201');
		$response['userIsBlocked'] = $this->getUserStatus($request->token);
		return response()->json($response,config('resource.status_200'));
       }
		/*$response['data']   = $datas;
		$response['status'] = config('resource.status_200');
		$response['userIsBlocked'] = $this->getUserStatus($request->token);
		return response()->json($response,config('resource.status_200'));*/

	}else
	{   
		if($request->type == 'walkin' || $request->Type == 'walkin')
		{    
			if($request->from_date !='' && $request->to_date !=''){
			$data = RaiseTest::where('lab_id',$lab_id) 
				/*->where('other_reason_for_cancel',null)*/
				->where('doctor_referer_id','0')                        
				->with(array('getPatient' => function($query) use ($to_date,$from_date)
							{                                
							$query->whereDate('updated_at', '>=', $from_date);
							$query->whereDate('updated_at', '<=', $to_date);                          
							}))
				->orderBy('id','desc')
			->jsonPaginate(); 
			}else{
				$data = RaiseTest::where('lab_id',$lab_id) 
				/*->where('other_reason_for_cancel',null)*/
				->where('doctor_referer_id','0')                        
				->with(array('getPatient'))
				->orderBy('id','desc')
			->jsonPaginate();
			}
		}else{ 
			if($request->from_date !='' && $request->to_date !=''){
			$data = RaiseTest::where('lab_id',$lab_id) 
				/*->where('other_reason_for_cancel',null)*/
				->where('doctor_referer_id', '!=','0')                        
				->with(array('getPatient' => function($query) use ($to_date,$from_date)
							{                                
							$query->whereDate('updated_at', '>=', $from_date);
							$query->whereDate('updated_at', '<=', $to_date);                          
							}))
				->orderBy('id','desc')
			->jsonPaginate(); 
			}else{
				$data = RaiseTest::where('lab_id',$lab_id) 
				/*->where('other_reason_for_cancel',null)*/
				->where('doctor_referer_id', '!=', '0')                        
				->with(array('getPatient'))
				->orderBy('id','desc')
			->jsonPaginate(); 
			}
		}
	}  

	if(count($data) !=0)
	{         
		$people = array();
		$datas = array();
		$i = 0 ;
		foreach($data as $key=>$dat)
		{               
			//if($dat->getPatient !=''){

				$people[$i] = $dat->patients_id;
				if($dat->getPatient)
				{              
					$dat->getPatient->date  = Carbon::parse($dat->getPatient->updated_at)->format('M d, Y - h:m A');
					$dat->getPatient->date  = str_replace("-","at",$dat->getPatient->date);
				}
				if($request->Type != 'walkin' || $request->type != 'walkin')
				{ 
					if($dat->getPatient)
					{
						//$salutation = (getdoctordetails($dat->doctor_id,'salutation') != null || getdoctordetails($dat->doctor_id,'salutation') !='' )?getdoctordetails($dat->doctor_id,'salutation'):'';
						//$dat->getPatient->doctor_name  = $salutation.' '.getdoctordetails($dat->doctor_id,'name');
						$dat->getPatient->doctor_name  = $dat->doctor_id;
					}
				}        //progress status get start
				$progress_status  = 1 ;
				$progress_msg     = config('resource.test_raised');
				$remaining_amount = $dat->remaining_amount; 
				$full_amount = $dat->original_amount;
				if($full_amount > $remaining_amount)
					$raise_table = 1;
				else
					$raise_table = 0;

				$Receipt_count = Receipt::where('specimen_collected',1)
					->where('raise_test_id',$dat->id)                                     
					->get();
				$Receipt_count = $raise_table;
				$report_table = Report::where('raise_test_id',$dat->id)->get();
				$report_table = $report_table->count();               
				if($dat->paid_status == 1 && $report_table !=0)
				{
					$progress_status = 4 ;
					$progress_msg    = config('resource.test_completed') ;
				}
				elseif($raise_table == 1 && $dat->specimen_collected == 1 && $dat->remaining_amount !=null)
				{
					$progress_status = 3 ;  
					if($dat->remaining_amount ==0 && $dat->remaining_amount !=null)
					{              $remain_amount = config('resource.ready_to_generate');
					}else{
						if($dat->received_amount !=0 && $dat->remaining_amount !=null)
							$amount = $dat->remaining_amount ;
						else
							$amount = $dat->remaining_amount;
						$remain_amount = 'Payment due Rs '.$amount; 
					}
					$progress_msg     =  $remain_amount ;
				}elseif($dat->specimen_collected == 1)
				{
					$progress_status = 2 ;
					$progress_msg    = config('resource.test_collected'); 
				}
                if($dat->paid_status == 3 || $dat->paid_status == 4 || $dat->paid_status == 5)
				{
					$progress_status = 5 ;
					$progress_msg    = config('resource.test_cancelled');
				}
				//progress status get end
				$dat->progress_bar = $progress_status ;
				$dat->progress_msg = $progress_msg ;
				$datas[$i]  = $dat ;                     
				$i++ ;
			//}          

		}        
		//error_log(print_r($datas,true),3,'/var/www/html/error.log');
		$response['count']   = count($datas);
		$response['data']   = $datas;
		$response['status'] = config('resource.status_200');
		$response['userIsBlocked'] = $this->getUserStatus($request->token);
		return response()->json($response,config('resource.status_200'));

	}
	else{

		$response['msg'] = config('resource.no_record');
		$response['status'] = config('resource.status_201');
		$response['userIsBlocked'] = $this->getUserStatus($request->token);
		return response()->json($response,config('resource.status_201'));
	}        

}


/**
 * [saveNotification description]
 * @param  Request $request [description]
 * @return [type]           [description]
 */
public function saveNotification(Request $request)
{   $lab_id = $this->getUserId($request->token);
	if($request->filled('raise_test_id'))
	{
		$test = RaiseTest::where('id',$request->raise_test_id)->where('status',1)->first();           
		if(!empty($test->id))
		{                     
			$notification = new Notification();
			$notification->type             = 'test';
			$notification->lab_id           =  $lab_id; 
			$notification->doctor_id        =  ($test->doctor_id !=0 || $test->doctor_id !=null) ? $test->doctor_id : $test->doctor_referer_id ;
			$notification->patient_id       =  ($test->patients_id) ? $test->patients_id : 0;
			$notification->raise_test_id    =  $test->id;
			$notification->read_status      =0;
			$notification->save();
			$response['msg']    = 'Updated Notification Successfully' ;
			$response['status'] = config('resource.status_200');
			$response['userIsBlocked'] = $this->getUserStatus($request->token);
			return response()->json($response,config('resource.status_200'));
		}else
		{ 
			$response['msg']    = config('resource.no_record');
			$response['status'] = config('resource.status_201');
			$response['userIsBlocked'] = $this->getUserStatus($request->token);
			return response()->json($response,config('resource.status_201'));
		}
	}else
	{
		$response['msg']    = config('resource.parameter_missing') ;
		$response['status'] = config('resource.status_201');
		$response['userIsBlocked'] = $this->getUserStatus($request->token);
		return response()->json($response,config('resource.status_201'));
	}

}
/**
 * [getReport description]
 * @param  Request $request [description]
 * @return [type]           [description]
 */
public function getReport(Request $request){
	$user_type = $request->user_type;
	$created_at =$request->created_at;
	$lab_tests_id = $request->lab_tests_id;
	$patients_id = $request->patients_id;
	$user_id = $this->getUserId($request->token);
	$data = RaiseTest::where('doctor_id',$user_id)->where('status',1)->where('created_at',$created_at)->get();

	$response['data'] = $data;
	$response['status'] = config('resource.status_200');
	$response['userIsBlocked'] = $this->getUserStatus($request->token);
	$user = $this->getAuthUser($request->token);
	$value = DB::table('fc_WSAuth')->first()->enable_log;
	if($value)   {   
		storelivigroactivity($user,config('resource.get_doctor_raised_test'));   
	}
	return response()->json($response,config('resource.status_200'));
}
/** 
 * [getRaisedTestDetail description]
 * @param  Request $request [description]
 * @return [type]           [description]
 */
public function getRaisedTestDetail(Request $request){
	/*error_log(print_r($request->all(),true),3,'/var/www/html/error.log');*/
	$test_id1 = $request->test_id;
	$data = RaiseTest::find($test_id1)->with('getLab','getPatient','getDoctor')->first();
	foreach($data->getPatient->test_assigned()->get() as $key=>$patient_raise_list){
		//dd($patient_raise_list);
		if($patient_raise_list->id != $data->id)
			foreach($patient_raise_list->lab_tests_id as $test_id) {
				//dd($test_id);
				$test_da[$test_id][$key]['result'] = Receipt::where([['tests_id',$test_id],['raise_test_id',$patient_raise_list->id]])->first()['result'];
				//dd($test_da);
				$test_da[$test_id][$key]['unit'] = Receipt::where([['tests_id',$test_id],['raise_test_id',$patient_raise_list->id]])->first()['result'];
				$test_da[$test_id][$key]['test_taken_at'] = $data->updated_at;
			}
	}
	foreach($data->lab_tests_id as $key1=>$tes_id){
		$test_data[$key1]['result'] = Receipt::where([['tests_id',$tes_id],['raise_test_id',$test_id1]])->get()[0]->result;
		$test_data[$key1]['unit'] = Receipt::where([['tests_id',$tes_id],['raise_test_id',$test_id1]])->get()[0]->units;
		$test_data[$key1]['name'] = LabsTest::find($tes_id)->name;
		if(!empty($test_da[$tes_id])) {
			$test_data[$key1]['previous_result'] = config('resource.yes');
			$test_data[$key1]['previous_data'] = $test_da[$tes_id];
		}else{
			$test_data[$key1]['previous_result'] = config('resource.no');
			$test_data[$key1]['previous_data'] = '';
		}

	}
	$data->getLabTest = $test_data;
	$response['data'] = $data;
	$response['status'] = config('resource.status_200');
	$response['userIsBlocked'] = $this->getUserStatus($request->token);
	return response()->json($response,config('resource.status_200'));
}

/**
 * [getCommonData description]
 * @param  Request $request [description]
 * @return [type]           [description]
 */
public function getCommonData(Request $request)
{    
	error_log(print_r($request->all(),true),3,'/var/www/html/error.log');
	     $type = $request->type;
		 if($type =='')
		 {  
		 	 $response['status']  = config('resource.status_401');
			 $response['message'] = config('resource.type_wrong');
			 $response['userIsBlocked'] = $this->getUserStatus($request->token);
			return response()->json($response,config('resource.status_401'));
		 }if($request->country_code =='')
		 {  
		 	 $response['message']    = config('resource.parameter_missing') ;
			 $response['status'] = config('resource.status_201');
			 $response['userIsBlocked'] = $this->getUserStatus($request->token);
			 return response()->json($response,config('resource.status_200'));
		 }	
	if($request->type == 'doctor')
	{
		$data = Doctors::where('name','!=','')->orderBy('name','asc')->where('status',1)->get(); 
		if($request->filled('doctor_id'))
			$data = Doctors::find($request->doctor_id);
	}
	elseif($request->type == 'city')
	{                    
		$data = DB::table('statelist')->select('state')->orderBy('state','asc')->DISTINCT()->get();
		if($request->filled('state_name'))
			$data = DB::table('statelist')->where('state',$request->state_name)->get();
	}
	elseif($request->type == 'blood')
	{
		$data = Blood::orderBy('blood_group','asc')->where('status',1)->get();  
		if($request->filled('blood_id'))
			$data = Blood::find($request->blood_id);                    
	}
	elseif($request->type == 'all')
	{     
		$data_doctor = Doctors::where('name','!=','')->where('status',1)->orderBy('name','asc')->get();
		$data_state = DB::table('statelist')->select('state')->orderBy('state','asc')->DISTINCT()->get();
		$contry_id = Countries::where('phonecode',$request->country_code)->get();
		if($contry_id){
			$contry_id  = Countries::where('phonecode',$request->country_code)->first()->id;
			$data_state = States::where('country_id',$contry_id)->orderBy('name','asc')->DISTINCT()->get();
		}
		$data_blood = Blood::orderBy('blood_group','asc')->where('status',1)->get();                            

		if($request->filled('doctor_id')){
			$data_doctor = Doctors::find($request->doctor_id);
		}
		if($request->filled('state_name')){
			$state_id = States::where('name',$request->state_name)->first()->id;
			$city_name = Cities::where('state_id',$state_id)->orderBy('name','asc')->DISTINCT()->get();
		}
		if($request->filled('blood_id')){
			$data_blood = Blood::find($request->blood_id); 
		}
        $response['default_state'] ='Tamil Nadu';
        $response['default_city']  ='Madurai';
		$response['blood']     = $data_blood  ;
		$response['doctor']    = $data_doctor ;
		$response['state']     = $data_state  ;
		if($request->filled('state_name'))			
			$response['city']         = $city_name  ;
		$response['status']    = config('resource.status_200');
		$response['userIsBlocked'] = $this->getUserStatus($request->token);
		return response()->json($response,config('resource.status_200'));
	}
	if($data)
	{
		$response['data'] = $data;
		$response['status'] = config('resource.status_200');
		$response['userIsBlocked'] = $this->getUserStatus($request->token);
		return response()->json($response,config('resource.status_200'));
	}else{ $response['msg'] = config('resource.no_record'); $response['status'] = config('resource.status_201'); 
	$response['userIsBlocked'] = $this->getUserStatus($request->token);
	return response()->json($response,config('resource.status_201')); }

}


/**
 * [updateRangeOwnTest description]
 * @param  Request $request [description]
 * @return [type]           [description]
 */
public function updateRangeOwnTest(Request $request) {
/*error_log(print_r($request->all(),true),3,'/var/www/html/error.log');*/
	$lab_id = $this->getUserId($request->token);
	$test_id = $request->test_id;
	$price = $request->price;
	$start_range = $request->range;
	$end_range = $request->range_end;

	$labdata = LabsAvaTest::find($test_id);
	$labdata->amount    = $price ;
	$labdata->range     = $start_range ;
	$labdata->range_end = $end_range ;
	$labdata->save();

	$response['status']  = config('resource.status_200');
	$response['message'] = config('resource.lab_test_updated_successfully');
	$response['userIsBlocked'] = $this->getUserStatus($request->token);
	return response()->json($response,config('resource.status_201'));
}

/**
 * [insertTestCart or Delete description]
 * @param  Request $request [description]
 * @return [type]           [description]
 */
public function insertTestCart(Request $request) {

	error_log(print_r($request->all(),true),3,'/var/www/html/error.log'); 
	$user_id = $this->getUserId($request->token);	
	$AvaTest_check_amount = LabsAvaTest::where('lab_id',$user_id)->where('lab_tests_id',$request->test_id)
	                                   ->where('status',1)->first();
if($request->test_type == 'single')
    {	
	  $AvaTest_check_count = LabsAvaTest::where('lab_id',$user_id)->where('lab_tests_id',$request->test_id)
	                                   ->where('status',1)->get();
	 if($AvaTest_check_count->count() ==0)
	{
        $response['status']  = config('resource.status_202');
		$response['msg'] = config('resource.lab_price_update');
		$response['userIsBlocked'] = $this->getUserStatus($request->token);
		return response()->json($response,config('resource.status_200'));
	}
	if($AvaTest_check_amount->amount == '' || $AvaTest_check_amount->amount == 0 || $AvaTest_check_amount->amount == null)
	{
        $response['status']  = config('resource.status_202');
		$response['msg'] = config('resource.lab_price_update');
		$response['userIsBlocked'] = $this->getUserStatus($request->token);
		return response()->json($response,config('resource.status_200'));
	}
   }

		

	$user_type = $request->user_type;
	$patient_id = $request->patient_id;

    if($request->test_type == 'single')
    {
       	$count = CartTest::where([['user_id',$user_id],['patient_id',$patient_id],['user_type',$user_type],['test_type',$request->test_type],['test_id',$request->test_id]])->count();
	if($count <= 0 ){
		$response['count'] = 0;
		CartTest::create([
			'user_id' => $user_id,
			'patient_id' => $patient_id,
			'user_type' => $user_type,
			'test_id' => $request->test_id,
			'test_type' => $request->test_type
		]);
		//$response['message'] = "No Data in Cart";
		$response['status'] = config('resource.status_200');
		//return response()->json($response,200);
	}else{
		CartTest::where([['user_id',$user_id],['patient_id',$patient_id],['user_type',$user_type],['test_id',$request->test_id],['test_type',$request->test_type]])->delete();
		$response['status'] = config('resource.status_201');
	}	
    }elseif($request->test_type == 'group'){
    	$gettest_count = Package::where('id',$request->test_id)->where('status',1)->get();
    	if($gettest_count->count() ==0){    		
	    	$response['status']  = config('resource.status_201');
			$response['msg']     = config('resource.id_wrong');
			$response['userIsBlocked'] = $this->getUserStatus($request->token);
			return response()->json($response,config('resource.status_200'));
    	}  
    	$gettest_id = Package::find($request->test_id);     	  	
    	$grouptestid = $gettest_id->tests_id ;     	
    	if($grouptestid ==null){
            $response['status']  = config('resource.status_201');
			$response['msg']     = config('resource.please_insert_id');
			$response['userIsBlocked'] = $this->getUserStatus($request->token);
			return response()->json($response,config('resource.status_200'));
    	}  

    	$count = CartTest::where([['user_id',$user_id],['patient_id',$patient_id],['user_type',$user_type],['test_type',$gettest_id->id]])->count();     	
    	if($count <= 0 ){        	  		
    		foreach($grouptestid as $value){
    			 CartTest::create([
			'user_id' => $user_id,
			'patient_id' => $patient_id,
			'user_type' => $user_type,
			'test_id' => $value,
			'test_type' => $request->test_id
		     ]);
    	  }
    	  $response['status'] = config('resource.status_200');
    	}else{    		
    		CartTest::where([['user_id',$user_id],['patient_id',$patient_id],['user_type',$user_type],['test_type',$gettest_id->id]])->delete();
		    $response['status'] = config('resource.status_201');
    	}

    }


	//
	$cart_test_id = CartTest::where([['user_id',$user_id],['patient_id',$patient_id],['user_type',$user_type]])->pluck('test_id')->toArray();
	//Get Lab Tests
	if($request->filled('lab_id')){
		if($request->test_type == 'single'){
           $AvaTest = LabsAvaTest::where('lab_id',$request->lab_id)->where('status',1);
		}else{
		   $gettest_id = Package::find($request->group_id);
    	   $grouptestid = $gettest_id->tests_id;
           $AvaTest = LabsAvaTest::where('status',1)->whereIn('lab_id',implode(',',$grouptestid));
		}		
	}
	else{
		$AvaTest = LabsAvaTest::where('lab_id',$user_id)->where('status',1);
	}
	/*if($request->filled('search_tag'))
		$AvaTest = $AvaTest->where('search_tags','ILIKE','%'.$request->search_tag.'%');*/
	$AvaTest_dat = $AvaTest->pluck('lab_tests_id')->toArray();

	if($request->search_tags){
		$search_keyword = $request->search_tags;
		$AvaTest_data = LabsTest::whereIn('id',$AvaTest_dat)
			->where('name','ILIKE','%'.$search_keyword.'%')
			->where('status',1)
			->jsonPaginate(); }else{
				$AvaTest_data = LabsTest::where('status',1)->whereIn('id',$AvaTest_dat)->jsonPaginate(); }
	$newAvaTest = [];
	foreach($AvaTest_data as $avatest) {
		if(in_array($avatest->id,$cart_test_id))
			$avatest->ischecked = true;
		else
			$avatest->ischecked = false;
		$newAvaTest[] = $avatest; 
	}

	if(count($newAvaTest) == 0) {
		$response['status']  = config('resource.status_201');
		$response['message'] = config('resource.no_test_found');
		$response['userIsBlocked'] = $this->getUserStatus($request->token);
		return response()->json($response,config('resource.status_200'));
	}
	$test1 = LabsTest::where('status',1)->get();    
	$new_array = '';
	$category_data = array();
	foreach($test1 as $key=>$tes)
	{ 
		if($tes->search_tags)
		{
			$new_array.= $tes->search_tags.',';
		}

	} 
	if($new_array)
	{
		foreach (explode(',',$new_array) as $key => $value)
		{
			if(!in_array($value,$category_data) && $value)
			{
				$category_data[] = $value ;
			}
		}                

	}
	$response['data'] = $AvaTest_data;	
	$response['category'] = $category_data;
	$response['count'] = count($cart_test_id);
	$response['userIsBlocked'] = $this->getUserStatus($request->token);
	return response()->json($response,config('resource.status_200'));
}

/**
 * for doctor module need to send the lab_id
 * @param  Request $request [description]
 * @return [type]           [description]
 */
public function getCartTestDetail(Request $request){
	/*error_log(print_r($request->all(),true),3,'/var/www/html/error.log');*/
	$user_id = $this->getUserId($request->token);
	$user_type = $request->user_type;
	$patient_id = $request->patient_id;
	$count = CartTest::where([['user_id',$user_id],['patient_id',$patient_id],['user_type',$user_type]])->count();
	if($count <= 0 ){
		$response['count'] = 0;
		//$response['message'] = "No Data in Cart";
		$response['status'] = config('resource.status_201');
		//return response()->json($response,200);
	}
	$cart_test_id = CartTest::where([['user_id',$user_id],['patient_id',$patient_id],['user_type',$user_type]])->pluck('test_id')->toArray();
	//Get Lab Tests
	if($request->filled('lab_id'))
		$AvaTest = LabsAvaTest::where('lab_id',$request->lab_id)->where('status',1);
	else
		$AvaTest = LabsAvaTest::where('lab_id',$user_id)->where('status',1);
	/*if($request->filled('search_tag'))
		$AvaTest = $AvaTest->where('search_tags','ILIKE','%'.$request->search_tag.'%');*/
	$AvaTest_dat = $AvaTest->pluck('lab_tests_id')->toArray();

	if($request->search_name !=''){
		$search_keyword = $request->search_name;
		$AvaTest_data = LabsTest::whereIn('id',$AvaTest_dat)
			->where('name','ILIKE','%'.$search_keyword.'%')
			->where('status',1)
			->orderBy('name','asc')
			->jsonPaginate(); }elseif($request->search_tag !=''){
				$search_keyword = $request->search_tag;
		     $AvaTest_data = LabsTest::whereIn('id',$AvaTest_dat)
			->where('search_tags','ILIKE','%'.$search_keyword.'%')
			->where('status',1)
			->orderBy('name','asc')
			->jsonPaginate();
			}else{
				$AvaTest_data = LabsTest::whereIn('id',$AvaTest_dat)->where('status',1)->orderBy('name','asc')->jsonPaginate();
				 }
	$newAvaTest = [];
	foreach($AvaTest_data as $avatest) {
		if(in_array($avatest->id,$cart_test_id))
			$avatest->ischecked = true;
		else
			$avatest->ischecked = false;
		$newAvaTest[] = $avatest; 
	}

	if(count($newAvaTest) == 0) {
		$response['status'] = config('resource.status_201');
		$response['message'] = config('resource.no_test_found');
		$response['userIsBlocked'] = $this->getUserStatus($request->token);
		return response()->json($response,config('resource.status_200'));
	}
	$test1 = LabsTest::where('status',1)->orderBy('name','asc')->get();    
	$new_array = '';
	$category_data = array();
	foreach($test1 as $key=>$tes)
	{ 
		if($tes->search_tags)
		{
			$new_array.= $tes->search_tags.',';
		}

	} 
	if($new_array)
	{
		foreach (explode(',',$new_array) as $key => $value)
		{
			if(!in_array($value,$category_data) && $value)
			{
				$category_data[] = $value ;
			}
		}                

	}

    //bulk group test data
      $final_data = array(); 
     /* $bulk_group_test = Package::where('status',1)->get();*/
    
      $bulk_group_test = DB::table('labs_available_test')
            ->join('package', 'labs_available_test.lab_tests_id', '=', 'package.id')  
           /* ->select('package.*', 'package.package_name')   */       
            ->where('labs_available_test.lab_id',$user_id)
            ->where('labs_available_test.test_type','group')
            ->where('labs_available_test.status',1)
            ->where('package.tests_id','!=','')
            ->where('package.status',1)
            ->orderBy('package.package_name','asc')
            ->get()->toArray();          

      $cart_test_package_id = CartTest::where([['user_id',$user_id],['patient_id',$patient_id],['user_type',$user_type]])->where('test_type','!=','single')->pluck('test_type')->toArray();     
      /*dd($cart_test_package_id);*/
     /* dd($cart_test_package_id);*/
	//bulk group test data
	foreach ($bulk_group_test as $key => $value) {
		  if(in_array($value->id,$cart_test_package_id))
			$value->ischecked = true;
		  else
			$value->ischecked = false;
         $final_data[] = $value ;		
	}
	//cart test count
	$cart_test_count = CartTest::where([['user_id',$user_id],['patient_id',$patient_id],['user_type',$user_type]])->get();
	//cart test count

	$response['bulk_group'] = $bulk_group_test;
	$response['data']       = $AvaTest_data;	
	$response['category']   = $category_data;
	$response['count']      = $cart_test_count->count();
	$response['status']     = config('resource.status_200');
	$response['userIsBlocked'] = $this->getUserStatus($request->token);
	return response()->json($response,config('resource.status_200'));


}

/**
 * [onclickCart description]
 * @param  Request $request [description]
 * @return [type]           [description]
 */

//single package test

/*public function onclickCart(Request $request) {	
            $user_id = $this->getUserId($request->token);
			$user_type = $request->user_type;
			$patient_id = $request->patient_id;
			$cart_data = CartTest::where([['user_id',$user_id],['patient_id',$patient_id],['user_type',$user_type]])->pluck('test_id')->toArray();			

			if(count($cart_data) <= 0 ){
				$response['status'] = config('resource.status_201');
				$response['message'] = "No Data Found";
				return response()->json($response,config('resource.status_200'));
			}else{
				$total_amount = LabsAvaTest::where('lab_id',$user_id)
				                           ->whereIn('lab_tests_id',$cart_data)
				                           ->where('status',1)
				                           ->sum('amount');
                

                $single_test_data = CartTest::where([['user_id',$user_id],['patient_id',$patient_id],['user_type',$user_type],['test_type','single']])->pluck('test_id')->toArray();
				$ava_test =  LabsTest::with(['labs_available_test' => function ($query) use ($user_id){
					$query->where('labs_available_test.lab_id', $user_id);
					$query->where('labs_available_test.status', 1);
					}])
				    ->where('status',1)
				    ->whereIn('id',$single_test_data)
			        ->jsonPaginate(); 
			   

			    
			        $group_test_data = CartTest::where([['user_id',$user_id],['patient_id',$patient_id],['user_type',$user_type]])->where('test_type','!=','single')->pluck('test_id')->toArray();  

			       
			        $group_test = CartTest::where([['user_id',$user_id],['patient_id',$patient_id],['user_type',$user_type]])->where('test_type','!=','single')->first();
			                       
                        
			        $group_data =  LabsTest::with(['labs_available_test' => function ($query) use ($user_id)
			        {
					   $query->where('labs_available_test.lab_id', $user_id);
					   $query->where('labs_available_test.status',1);
					}])
					->where('status',1)
					->whereIn('id',$group_test_data)->get();		 
			        $final_data = array();			       
			        if(count($group_data) !=0){
                            foreach ($group_data as $key => $value) {
                          $value->group_test_name = LabTestGroupData($group_test->test_type,'name');
                          $value->group_test_id = LabTestGroupData($group_test->test_type,'id');
                          $final_data[] = $value ;
                            }
			        }
			    $response['total_amount'] = $total_amount;
				$response['data']       = $ava_test;				
				$response['group_data'] = $group_data;				
				$response['status'] = config('resource.status_200');
				return response()->json($response,config('resource.status_200'));
			}
}*/




public function onclickCart(Request $request) {
$user_id = $this->getUserId($request->token);
$user_type = $request->user_type;
$patient_id = $request->patient_id;
$cart_data = CartTest::where([['user_id',$user_id],['patient_id',$patient_id],['user_type',$user_type]])->pluck('test_id')->toArray();

$cart_data_single = CartTest::where([['user_id',$user_id],['patient_id',$patient_id],['user_type',$user_type],['test_type','single']])->pluck('test_id')->toArray();	

//group test amount 
$cart_data_package = CartTest::where([['user_id',$user_id],['patient_id',$patient_id],['user_type',$user_type],['test_type','!=','single']])->groupBy('test_type')->pluck('test_type')->toArray();

$total_amount_package = LabsAvaTest::select(DB::raw('sum(cast(amount as double precision))'))			   
			                           ->whereIn('lab_tests_id',$cart_data_package)
			                           ->where('lab_id',$user_id)
			                           ->where('test_type','!=','single')
			                           ->where('status',1)
			                           ->get();	

//$response['$total_amount_package']= $total_amount_package;			                     
$total_amount_package = (isset($total_amount_package[0]->sum)) ? $total_amount_package[0]->sum : 0;
/*dd($total_amount_package);*/
//group test amount
 $response['$user_id'] = $user_id;
if(count($cart_data) <= 0 ){
$response['status'] = config('resource.status_201');
$response['message'] = "No Data Found";
$response['userIsBlocked'] = $this->getUserStatus($request->token);
return response()->json($response,config('resource.status_200'));
}else{
/*$total_amount = LabsAvaTest::where('lab_id',$user_id)
                           ->whereIn('lab_tests_id',$cart_data)
                           ->where('status',1)
                           ->sum('amount');*/

        $total_amount = LabsAvaTest::select(DB::raw('sum(cast(amount as double precision))'))
        					->where('lab_id',$user_id)
                           ->whereIn('lab_tests_id',$cart_data_single)
                           ->where('test_type','single')
                           ->where('status',1)
                           ->get();	
                  // $response['$total_amount']= $total_amount;
        $total_amount = (isset($total_amount[0]->sum)) ? $total_amount[0]->sum : 0;        
        $total_amount = $total_amount + $total_amount_package;
      //single test data get stat
        $single_test_data = CartTest::where([['user_id',$user_id],['patient_id',$patient_id],['user_type',$user_type],['test_type','single']])->pluck('test_id')->toArray();
		$ava_test =  LabsTest::with(['labs_available_test' => function ($query) use ($user_id){
		$query->where('labs_available_test.lab_id', $user_id);
		$query->where('labs_available_test.status', 1);
		}])->where('status',1)
		   ->whereIn('id',$single_test_data)
		   ->get()->toArray();   
	 //single test data get end 

        //group package
        $group_test_data = CartTest::where([['user_id',$user_id],['patient_id',$patient_id],['user_type',$user_type]])->where('test_type','!=','single')->pluck('test_id')->toArray();  
        /*$package_test_array = LabsTest::whereIn('id',$group_test_data)->get()->toArray();*/ 
        //group package

      
        $group_test = CartTest::where([['user_id',$user_id],['patient_id',$patient_id],['user_type',$user_type]])->where('test_type','!=','single')->get();      
         
         //group test data in labstest table start               
        $group_data =  LabsTest::with(['labs_available_test' => function ($query) use ($user_id)
        {
		   $query->where('labs_available_test.lab_id', $user_id);
		   $query->where('labs_available_test.status',1);
		}])
		->where('status',1)
		->whereIn('id',$group_test_data)->get();
		//group test data in labstest table end	


        $final_data = $final_data1 =array();	       
        if(count($group_data) !=0){
                     foreach ($group_data as $key => $value) { 
                        $final_data[0]['name'] = /*LabTestGroupData($group_test->test_type,'name')*/'hi';            
                        $final_data[0]['group_package_array'][] = $value ;
                            }
        }	

        foreach($ava_test as $k =>$val){           
        $final_data1[] = $val;
        $final_data1[$k]['group_package_array'] = [];
        }	
               
       /* $cart_data = DB::table('cart_test')
			->join('labs_available_test', 'labs_available_test.lab_tests_id', '=', 'cart_test.test_id')
			->join('lab_tests', 'lab_tests.id', '=', 'cart_test.test_id')
			->select(DB::raw('labs_available_test.*, lab_tests.*, cart_test.test_type, cart_test.test_id'))
			->where([['cart_test.user_id',$user_id],['labs_available_test.lab_id',$user_id],['cart_test.patient_id',$patient_id],['cart_test.user_type',$user_type]])
			->get()->toArray();	*/
			 $cart_data = DB::table('cart_test')
			//->join('labs_available_test', 'labs_available_test.lab_tests_id', '=', 'cart_test.test_id')
			->join('lab_tests', 'lab_tests.id', '=', 'cart_test.test_id')
			->select(DB::raw('lab_tests.*, cart_test.test_type, cart_test.test_id'))
			->where([['cart_test.user_id',$user_id],['cart_test.patient_id',$patient_id],['cart_test.user_type',$user_type]])
			->get()->toArray();		
	//$response['$cart_data'] =$cart_data;
			$finaldata = array();
			if(count($cart_data) >= 0 ){
				foreach($cart_data as $key => $value){
					if($value->test_type !='single'){						
					$finaldata[$value->test_type]['name'] = Package::find($value->test_type)->package_name;
						$finaldata[$value->test_type]['test_type'] = 'group';	

						$finaldata[$value->test_type]['id'] = (int)$value->test_type;
						$finaldata[$value->test_type]['group_package_array'][] = $value;
					}
					else{
						$finaldata['single'.$value->test_id] = $value;
						$finaldata['single'.$value->test_id]->group_package_array = [];	
					}

				}

			}
               
                
                $response['total_amount'] = $total_amount;
                /*$response['amount'] = $total_amount;*/
                $response['data']         = array_values($finaldata) ;	
				$response['status'] = config('resource.status_200');
				$response['userIsBlocked'] = $this->getUserStatus($request->token);
				return response()->json($response,config('resource.status_200'));
                
                /*$final_values = array_merge($final_data1, $final_data);	            
                $response['total_amount'] = $total_amount;
				$response['data']         = $final_values ;	
				/*$response['group_data'] = $group_data;*/	
				/*$response['status'] = config('resource.status_200');
				return response()->json($response,config('resource.status_200'));*/
}
}

//group test package

public function onclickCart1(Request $request) {
            $user_id = $this->getUserId($request->token);
			$user_type = $request->user_type;
			$patient_id = $request->patient_id;
			//$cart_data = CartTest::where([['user_id',$user_id],['patient_id',$patient_id],['user_type',$user_type]])->get()->toArray();
			//$cart_data = CartTest::where([['user_id',$user_id],['patient_id',$patient_id],['user_type',$user_type]])->get()->toArray();	
		$cart_data = DB::table('cart_test')
			->join('labs_available_test', 'labs_available_test.lab_tests_id', '=', 'cart_test.test_id')
			->join('lab_tests', 'lab_tests.id', '=', 'cart_test.test_id')
			->select(DB::raw('labs_available_test.*, lab_tests.*, cart_test.test_type, cart_test.test_id'))
			->where([['cart_test.user_id',$user_id],['labs_available_test.lab_id',$user_id],['cart_test.patient_id',$patient_id],['cart_test.user_type',$user_type]])
			->get()->toArray();
			print_r($cart_data );die;
			$finaldata = array();

			if(count($cart_data) >= 0 ){
				foreach($cart_data as $key => $value){
					if($value->test_type !='single'){
						$finaldata[$value->test_type][] = $value;
					}
					else{
						$finaldata['single'.$value->test_id][] = $value;	
					}

				}

			}
           
			    //$response['total_amount'] = $total_amount;
				$response['data']         = array_values($finaldata) ;			
				/*$response['group_data'] = $group_data;*/						
				$response['status'] = config('resource.status_200');
				return response()->json($response,config('resource.status_200'));
			
}

/**
 * [newDoctoradd description]
 * @param  Request $request [description]
 * @return [type]           [description]
 */
public function newDoctoradd(Request $request){ 
	/*error_log(print_r($request->all(),true),3,'/var/www/html/error.log');*/
	if($request->name =='' || $request->mobile =='' || $request->email ==''){
		if($request->name ==''){
			$response['msg']    = config('resource.name_missing');
		    $response['status'] = config('resource.status_201');
		    $response['userIsBlocked'] = $this->getUserStatus($request->token);
		    return response()->json($response,config('resource.status_201'));
		}if($request->mobile ==''){
			$response['msg']    = config('resource.mobile_missing');
		    $response['status'] = config('resource.status_201');
		    $response['userIsBlocked'] = $this->getUserStatus($request->token);
		    return response()->json($response,config('resource.status_201'));
		}/*if($request->email ==''){
			$response['msg']    = config('resource.email_missing');
		    $response['status'] = config('resource.status_201');
		    $response['userIsBlocked'] = $this->getUserStatus($request->token);
		    return response()->json($response,config('resource.status_201'));
		}*/		
	}
	 $mobile_number_check = Doctors::where('mobile',$request->mobile)->get();
	 if($mobile_number_check->count() !=0){
            $response['msg']    = config('resource.mobile_exists');
		    $response['status'] = config('resource.status_201');
		    $response['userIsBlocked'] = $this->getUserStatus($request->token);
		    return response()->json($response,config('resource.status_201'));
	 }
	$lab_id = $this->getUserId($request->token);
	if($lab_id)
	{
		$doctors = new Doctors();
		$doctors->name        = $request->name;
		$doctors->mobile      = $request->mobile;
		$doctors->email       = ($request->email) ? $request->email : '';
		$doctors->status      = 1;
		$doctors->save();
		$doctor_lab = new DocLab(); 
		$doctor_lab->doctor_id   = $doctors->id ;
		$doctor_lab->lab_id      = $lab_id ;		
		$doctor_lab->save();
		//admin_users table insert table 
		$new_doctor = new User();
		$new_doctor->username    = $request->mobile;
		$new_doctor->name        = $request->name;		
		$new_doctor->linked_id   = $doctors->id;
		$new_doctor->user_type_id  = 2;
		$new_doctor->status      = 1;
		//admin_users table insert table 

		//send sms doctor number 
			  if($request->mobile !='' && $request->mobile != null){
			  	    $doctors = Doctors::find($doctors->id);
			        send_sms(substr($doctors->mobile,3),'Hi welcome '.$doctors->name.' by livigro');
			        //send mail start
			        if($doctors->email !='' && $doctors->email !=null){	
			           $data['to']           = $doctors->email; 
                       $data['subject']      = "Welcome livigro"; 					
                       $data['doctor_name']  = $doctors->name; 					
						Mail::send('email.newdoctorwelcome',$data, function($message) use ($data)
					      {
					          $message->to($data['to']); 
					          $message->cc(config('mail.MailCcFunction')); 					        
					          $message->subject($data['subject']);
					      });

					}
					//send mail end
		          }
		//send sms doctor number 

		$response['msg']    = config('resource.added_successfully');
		$response['new_doctor']   =  $doctors;
		$response['all_doctors']   =  Doctors::where('status',1)->get();
		$response['status'] =  config('resource.status_200');
		$response['userIsBlocked'] = $this->getUserStatus($request->token);
		return response()->json($response,config('resource.status_200'));
	}else{
		$response['msg'] = config('resource.parameter_missing');
		$response['status'] = config('resource.status_201');
		$response['userIsBlocked'] = $this->getUserStatus($request->token);
		return response()->json($response,config('resource.status_201'));
	}
}

/**
 * [labForgotPassword description]
 * @param  Request $request [description]
 * @return [type]           [description]
 */
public function labForgotPassword(Request $request){ 
	/*error_log(print_r($request->all(),true),3,'/var/www/html/error.log');*/

	$users = DB::table('admin_users')->where('linked_id',$request->id)->where('status',1)->first(); 
	$user = DB::table('admin_users')->where('linked_id',$request->id)->where('status',1)->get(); 
	if($user->count() ==0){
                $response['status'] = config('resource.status_400');
				$response['msg'] = config('resource.user_doesnt_exists');
				$response['userIsBlocked'] = $this->getUserStatus($request->token);
				return response()->json($response,config('resource.status_200'));
	} 
	if($request->id =='' || $request->password ==''){ $response['msg'] = config('resource.parameter_missing');
		$response['status'] = config('resource.status_201');
		$response['userIsBlocked'] = $this->getUserStatus($request->token);
		return response()->json($response,config('resource.status_201'));}                  
	if($users->user_type_id == 1){                  
		//update password
		DB::table('admin_users')->where('id',$users->id)->update(['password' => bcrypt($request->password)]);        
		$user_password = Labs::find($users->linked_id);                                       
		$user_password->password = bcrypt($request->password);                       
		$user_password->save();
		$response['msg'] = config('resource.updated_successfully');
		$response['status'] = config('resource.status_200');
		$response['userIsBlocked'] = $this->getUserStatus($request->token);
		return response()->json($response,config('resource.status_200'));
	}else{ 
		DB::table('admin_users')->where('id',$users->id)->update(['password' => bcrypt($request->password)]);
		$user_password = Doctors::find($users->linked_id);
		$user_password->password = bcrypt($request->password);                       
		$user_password->save();

		$response['msg'] = config('resource.updated_successfully');
		$response['status'] = config('resource.status_200');
		$response['userIsBlocked'] = $this->getUserStatus($request->token);
		return response()->json($response,config('resource.status_200'));
	}               
	$response['msg'] = config('resource.id_missing');
	$response['status'] = config('resource.status_201');
	$response['userIsBlocked'] = $this->getUserStatus($request->token);
	return response()->json($response,config('resource.status_201'));             

} 

/**
 * [labPatientsorder description]
 * @param  Request $request [description]
 * @return [type]           [description]
 */

	public function labPatientsorder(Request $request){ 
		     error_log(print_r($request->all(),true),3,'/var/www/html/error.log');
		   	 $lab_id     = $this->getUserId($request->token);
	     	 $patient_id = $request->patient_id ;
	     	 if($lab_id !='' && $patient_id !='')
	     	 {      $labdata = Labs::find($lab_id);
	     	 	    $final_data = array();
                    $raise_test_data =RaiseTest::where('lab_id',$lab_id)->where('patients_id',$patient_id)
                                             ->where('status',1)
                                             ->jsonPaginate();
                  if($raise_test_data->count() !=0)
                  {  
                  	foreach($raise_test_data as $datas)
                  	    {	  	
	     	 	    	  $mytime = Carbon::now();
	     	 	    	  $time = new Carbon($datas->created_at);
                          $shift_end_time = new Carbon($mytime->toDateTimeString());
                          $val = $time->diffForHumans($shift_end_time);
                          $resul = str_replace("before","ago",$val);
                          $datas->show_date   = $resul;
                          if($datas->paid_status ==3 || $datas->paid_status ==4 || $datas->paid_status ==5){
                          	$datas->Iscancelled ="1";
                           }else{
                            $datas->Iscancelled ="0";
                          }
                          $final_data[] = $datas ;
                        }                      
                  }else
                  {
                        $response['msg']    = 'Not raised';
				        $response['status'] = config('resource.status_201');
				        $response['userIsBlocked'] = $this->getUserStatus($request->token);
				       return response()->json($response,config('resource.status_201'));
                  }
	     	 	   $response['data']   = $raise_test_data;
				   $response['status'] = config('resource.status_200');
				   $response['userIsBlocked'] = $this->getUserStatus($request->token);
				   return response()->json($response,config('resource.status_200'));	
	     	 }else
	     	 {
	     	 	   $response['msg'] = 'parameter missing';
				   $response['status'] = config('resource.status_201');
				   $response['userIsBlocked'] = $this->getUserStatus($request->token);
				   return response()->json($response,config('resource.status_201'));
	     	 }
	     }



/**
 * [Contacts description]
 * @param  Request $request [description]
 * @return [type]           [description]
 */
public function contacts(Request $request){ 
	/*$data['to'] = 'bharathiraja.rbr@gmail.com';
	$data['from'] = 'bharathiraja.rbr@gmail.com';
	$data['link']  = 'link';
	send_mail('labs/email_template/sample',$data);*/

	/*send_sms('8072737006','Test message bharathi');
	dd('successfully');*/
	/*error_log(print_r($request->all(),true),3,'/var/www/html/error.log');*/
	 $id = $this->getUserId($request->token);
	if($request->message == '' && $request->photo == '' && $request->user_type == '')
	{
                $response['msg'] = 'parameter missing';
				$response['status'] = config('resource.status_201');
				$response['userIsBlocked'] = $this->getUserStatus($request->token);
				return response()->json($response,config('resource.status_201'));
	}
	$users = DB::table('admin_users')->where('linked_id',$id)->where('status',1)->first(); 
	$user = DB::table('admin_users')->where('linked_id',$id)->where('status',1)->get(); 
	if($user->count() ==0){
                $response['status'] = config('resource.status_400');
				$response['msg'] = config('resource.user_doesnt_exists');
				$response['userIsBlocked'] = $this->getUserStatus($request->token);
				return response()->json($response,config('resource.status_200'));
	} 

         if($request->user_type == 1){
         	$lab_id     = $id;
         	$doctor_id = 0;

          }elseif($request->user_type == 2)
          {
          	$lab_id     = 0 ;
         	$doctor_id = $id;
          }else{
            
                $response['status'] = config('resource.status_201');
				$response['msg'] = config('resource.type_wrong');
				$response['userIsBlocked'] = $this->getUserStatus($request->token);
				return response()->json($response,config('resource.status_201'));
          }
          $photo = null;       
          if($request->filled('photo'))
			{   			
				$type= 'contacts/'.$request->user_type;				
				//single image                   
				$images = file_upload($request->photo,$type,$users->username);   
				$photo = $images;
				          
			} 
		  		  		
           $contact_message = new  Contacts();
           $contact_message->lab_id     = $lab_id ;
           $contact_message->doctor_id  = $doctor_id ;
           $contact_message->message    = $request->message ;
           $contact_message->photo      = $photo ;
           $contact_message->status     = 1 ;
           $contact_message->save();

           $response['msg'] = config('resource.submitted_successfully');
		   $response['status'] = config('resource.status_200');
		   $response['userIsBlocked'] = $this->getUserStatus($request->token);
		   return response()->json($response,config('resource.status_200'));
	           
   }

   /**
	 * Get All faq data
	 * @return [type] [description]
	 */
	public function getFaq() {
		$tests = FAQ::where('status',1)->get();
		if($tests){
			$response['data'] = $tests;
			$response['status'] = config('resource.status_200');
		}
		else{
			$response['data'] = config('resource.record_not_found');
			$response['status'] = config('resource.status_400');
		}
		return response()->json($response,config('resource.status_200'));

	}

			 /**
		 *  lab dashboard get doctor details
		 * @param  [type] $doctor [description]
		 * @return [type]         [description]
		 */
	public function get_all_Doctor_details_Dashboard(Request $request) 
	{   	  
        error_log(print_r($request->all(),true),3,'/var/www/html/error.log');
				$carbon = Carbon::today();
				$timestamp = $carbon->timestamp ;			
				$today_date = $carbon->format('Y-m-d H:i:s');
				 if($request->from_date !='' && $request->to_date !=''){ $to_date   = $request->to_date ;
							  $from_date = $request->from_date ;
					}else{  $to_date   = $today_date ; $from_date = $today_date ; }
				$lab_id = $this->getUserId($request->token);   

		if($request->type == 'all')
		{ 

				
			//1.revenue_amount get
			$amounts = RaiseTest::select(DB::raw('sum(cast(received_amount as double precision))'))
									 ->whereDate('created_at', '>=', $from_date) 
									 ->whereDate('created_at', '<=', $to_date)
									 ->where('lab_id',$lab_id)
									 ->where('paid_status',1)
									 //->where('raised_by',1)
									 ->get();			
			$revenue_amount = (isset($amounts[0]->sum)) ? $amounts[0]->sum : 0;
			//1

			//2.patient count ;
			//$patient_count = DocPatient::whereDate('created_at', '>=', $from_date)->whereDate('updated_at','<=', $from_date)->where('lab_id',$lab_id)->get();
			/*$patient_count = DB::table('doctor_patient')
				  ->whereDate('doctor_patient.created_at', '>=', $from_date)
				  ->whereDate('doctor_patient.created_at', '<=', $to_date)
				  ->where('doctor_patient.lab_id',$lab_id)	          
				  ->get();*/
			$patientsList = DB::table('raise_test')
			      ->leftjoin('patients_details', 'patients_details.id', '=', 'raise_test.patients_id')
			      ->select(DB::raw('patients_details.name as name, patients_details.*'))
			      ->where('raise_test.lab_id', '=', $lab_id)
			      ->where('patients_details.status',1)
			      ->whereDate('raise_test.created_at', '>=', $from_date)
				  ->whereDate('raise_test.created_at', '<=', $to_date)
				  ->groupBy('raise_test.patients_id','patients_details.name','patients_details.id')
			      ->get()->toArray(); 
			 //$data['$patientsList'] = $patientsList;
			$patient_count = count($patientsList);
			//$patient_count = $patient_count->count(); 
			//      $patient_count =count($patientsList);
			//2

			//3. Raise test count
			$raise_test = RaiseTest::whereDate('created_at', '>=', $from_date)
									 ->whereDate('created_at', '<=', $to_date)
									 ->where('lab_id',$lab_id)
									 ->where('paid_status',1);
									// ->where('raised_by',1);	   
			$raise_test_count = $raise_test->count();
			//3

			//4 doctors data
			 /*$doctors_datas = DocLab::where('lab_id',$lab_id)->get();*/
			//        

			/*$doctors_datas = DB::table('doctor_lab')->join('doctors_details','doctors_details.id','=','doctor_lab.doctor_id')->where('doctor_lab.lab_id',$lab_id)->whereDate('created_at', '>=', $to_date)->whereDate('updated_at','<=', $from_date)->get()->toArray();*/

			/*$doctors_datas1 = DB::table('doctors_details')
							->join('doctor_patient','doctors_details.id','=','doctor_patient.doctor_id')
							->where(['doctor_patient.lab_id' => $lab_id])
							->whereDate('doctor_patient.created_at', '>=', $from_date)
				  			->whereDate('doctor_patient.created_at', '<=', $to_date)
							->get()
							->toArray();*/
			$doctors_datas1 = DB::table('doctors_details')
							->join('raise_test','doctors_details.id','=','raise_test.doctor_id')
							//->select(DB::raw('doctors_details.*'))
							->where(['raise_test.lab_id' => $lab_id])
							//->where('raised_by',1)
							->whereDate('raise_test.created_at', '>=', $from_date)
				  			->whereDate('raise_test.created_at', '<=', $to_date)
							->get()
							->toArray();
			$doctors_datas = [];
			
			foreach($doctors_datas1 as $val){
				$doctors_datas[$val->doctor_id]=$val;
			}
			$doctors_datas = array_values($doctors_datas);

			$data['status'] = config('resource.status_200');	
			$data['revenue_amount']             = $revenue_amount;
			$data['patient_count']              = $patient_count;
			$data['test_count']                 = $raise_test_count;		
			$data['doctors_data']               = $doctors_datas;	
			$data['userIsBlocked'] = $this->getUserStatus($request->token);	
			return response()->json($data,config('resource.status_200'));

		}elseif($request->type == 'walkin')
		{ 
			//1.revenue_amount get
			$amounts = RaiseTest::select(DB::raw('sum(cast(received_amount as double precision))'))
									 ->whereDate('created_at', '>=', $from_date)
									 ->whereDate('created_at', '<=', $to_date)
									 ->where('lab_id',$lab_id)
									// ->where('raised_by',1)
									 /*->where('paid_status',1)*/
									 ->where('doctor_referer_name',null)
									 ->get();			
			$revenue_amount = (isset($amounts[0]->sum)) ? $amounts[0]->sum : 0;
			//1

			//2.patient count ;
			/*$patient_count = RaiseTest::whereDate('created_at', '<=', $to_date)->whereDate('updated_at','>=', $from_date)->where('lab_id',$lab_id)->where('doctor_referer_name',0)->where('raised_by',1)
						  ->groupBy('patients_id')->count('patients_id');*/
			/*$patient_count = DB::table('doctor_patient')
				  ->join('patients_details','patients_details.id','=','doctor_patient.patient_id')
				  ->whereDate('doctor_patient.created_at', '>=', $from_date)
				  ->whereDate('doctor_patient.created_at', '<=', $to_date)
				  ->where('doctor_patient.lab_id',$lab_id)	          
				  ->where('patients_details.reference_doctor_id',0)
				  ->get();
			$patient_count = $patient_count->count();*/
			$patientsList = DB::table('raise_test')
			      ->leftjoin('patients_details', 'patients_details.id', '=', 'raise_test.patients_id')
			      ->select(DB::raw('patients_details.name as name, patients_details.*'))
			      ->where('raise_test.lab_id', '=', $lab_id)
			      ->where('patients_details.status',1)
			      ->where('raise_test.doctor_id',0)
			      ->whereDate('raise_test.created_at', '>=', $from_date)
				  ->whereDate('raise_test.created_at', '<=', $to_date)
				  ->groupBy('raise_test.patients_id','patients_details.name','patients_details.id')
			      ->get()->toArray(); 
			 //$data['$patientsList'] = $patientsList;
			$patient_count = count($patientsList);
			//2
			//3. Raise test count
			$raise_test = RaiseTest::whereDate('created_at', '>=', $from_date)
									 ->whereDate('created_at', '<=', $to_date)
									 ->where('lab_id',$lab_id)
									 //->where('raised_by',1)
									 ->where('paid_status',1)	   
									 ->where('doctor_id',0);
									 //->orWhere('doctor_id',0);	 
	
    $data['$lab_id'] =$lab_id;
    $data['$raise_test']=$raise_test;  
			$raise_test_count = $raise_test->count();
   // $raise_test_count = 4;
			//3

			//4 doctors data
			 /*$doctors_datas = DocLab::where('lab_id',$lab_id)->get();*/
			//        

			$doctors_datas = DB::table('doctor_lab')->join('doctors_details','doctors_details.id','=','doctor_lab.doctor_id')->where(['doctor_lab.lab_id' => $lab_id])->get()->toArray();

			$data['status'] = config('resource.status_200');	
			$data['revenue_amount']             = $revenue_amount;
			$data['patient_count']              = $patient_count;
			$data['test_count']                 = $raise_test_count;		
			$data['doctors_data']               = $doctors_datas;	
			$data['userIsBlocked']                = $this->getUserStatus($request->token);	
			return response()->json($data,config('resource.status_200'));

		}elseif($request->type == 'doctor')
		{ 
	  //get today record based on doctor id
		if($request->filled('doctor_id'))
		  { 
		  	
		  	 $doctors_data = Doctors::find($request->doctor_id);
		  	 if(!$doctors_data){
		  	 	$response['msg']    = config('resource.Doctor_id_missing');
		        $response['status'] = config('resource.status_201');
		        $response['userIsBlocked'] = $this->getUserStatus($request->token);
		        return response()->json($response,config('resource.status_200'));
		  	  }		  	
			 $d_id = $request->doctor_id ;			 
			//1. doctors patient count ;
			/*$patient_count = DB::table('doctor_patient') 
				  //->join('doctor_lab','doctor_lab.lab_id','=','doctor_patient.lab_id')				  
				  ->whereDate('doctor_patient.created_at', '>=', $from_date)
				  ->whereDate('doctor_patient.created_at', '<=', $to_date)
				  ->where('doctor_patient.doctor_id',$d_id)	          
				  ->where('doctor_patient.lab_id',$lab_id)	  
				  ->get();			 			 
			$patient_count = $patient_count->count();*/
			$patientsList = DB::table('raise_test')
			      ->leftjoin('patients_details', 'patients_details.id', '=', 'raise_test.patients_id')
			      ->select(DB::raw('patients_details.name as name, patients_details.*'))
			      ->where('raise_test.lab_id', '=', $lab_id)
			      ->where('patients_details.status',1)
			      ->where('raise_test.doctor_id',$d_id)
			      ->whereDate('raise_test.created_at', '>=', $from_date)
				  ->whereDate('raise_test.created_at', '<=', $to_date)
				  ->groupBy('raise_test.patients_id','patients_details.name','patients_details.id')
			      ->get()->toArray(); 
			 //$data['$patientsList'] = $patientsList;
			$patient_count = count($patientsList);
			//2. Raise test count
			$raise_test = RaiseTest::whereDate('created_at', '>=', $from_date)
									 ->whereDate('created_at', '<=', $to_date)
									 ->where('lab_id',$lab_id)
									 ->where('doctor_id',$d_id)
									 //->where('raised_by',1)
									 ->where('paid_status',1)	   
									 ->where('doctor_id','!=',0);
									 //->orWhere('doctor_id','!=',0);	   
			$raise_test_count = $raise_test->count();
			//2

			//3.revenue_amount get
			$amounts = RaiseTest::select(DB::raw('sum(cast(received_amount as double precision))'))
									 ->whereDate('created_at', '>=', $from_date)
									 ->whereDate('created_at', '<=', $to_date)
									 ->where('lab_id',$lab_id)
									 ->where('doctor_referer_id',$d_id)
									 //->where('raised_by',1)
									 ->where('paid_status',1)
									 /*->where('doctor_referer_name',0)*/
									 ->get();			
			$revenue_amount = (isset($amounts[0]->sum)) ? $amounts[0]->sum : 0; 
			//3
			/*dd($revenue_amount);*/

			//4. doctor details         
			//$doctors_datas = DB::table('doctor_lab')->join('doctors_details','doctors_details.id','=','doctor_lab.doctor_id')->where(['doctor_lab.lab_id' => $lab_id])->get()->toArray();
			//4
			/*$doctors_datas1 = DB::table('doctors_details')
							->join('doctor_patient','doctors_details.id','=','doctor_patient.doctor_id')
							->where(['doctor_patient.lab_id' => $lab_id])
							
							->get()
							->toArray();*/
			$doctors_datas1 = DB::table('doctors_details')
							->join('raise_test','doctors_details.id','=','raise_test.doctor_id')
							//->select(DB::raw('doctors_details.*'))
							->where(['raise_test.lab_id' => $lab_id])
							//->where('raised_by',1)

							->get()
							->toArray();
			$data['sdf']= $doctors_datas1;
			$doctors_datas = [];
			foreach($doctors_datas1 as $val){
				$doctors_datas[$val->doctor_id]=$val;
			}
			$doctors_datas = array_values($doctors_datas);



			$data['filed'] = 'yes';
			$data['doctor_id'] = $d_id;
			$data['status'] = config('resource.status_200');       
			$data['revenue_amount']             = $revenue_amount;
			$data['patient_count']              = $patient_count;
			$data['test_count']                 = $raise_test_count;		
			$data['doctors_data']               = $doctors_datas;
			$data['userIsBlocked'] = $this->getUserStatus($request->token);		
			return response()->json($data,config('resource.status_200'));  
		 }
		
	 //get today record based on doctor id

			//get all today record start
			//1.revenue_amount get
			$amounts = RaiseTest::select(DB::raw('sum(cast(received_amount as double precision))'))
									 ->whereDate('created_at', '>=', $from_date)
									 ->whereDate('created_at', '<=', $to_date)
									 ->where('lab_id',$lab_id)
									 //->where('raised_by',1)
									 ->where('paid_status',1)
									 ->where('doctor_id','!=',0)
									 //->orWhere('doctor_id','!=',0)
									 ->get();			
			$revenue_amount = (isset($amounts[0]->sum)) ? $amounts[0]->sum : 0;
			//1

			//2.patient count ;
			/*$patient_count = DocPatient::whereDate('created_at', '<=', $to_date)->whereDate('updated_at','>=', $from_date)->where('lab_id',$lab_id)->get();*/

		   /*$patient_count = DB::table('doctor_patient')
				  ->join('patients_details','patients_details.id','=','doctor_patient.patient_id')
				  ->whereDate('doctor_patient.created_at', '>=', $from_date)
				  ->whereDate('doctor_patient.created_at', '<=', $to_date)
				  ->where('doctor_patient.lab_id',$lab_id)	  
				  ->where('patients_details.reference_doctor_id','!=',0)
				  ->get();

			$patient_count = $patient_count->count();*/
			$patientsList = DB::table('raise_test')
			      ->leftjoin('patients_details', 'patients_details.id', '=', 'raise_test.patients_id')
			      ->select(DB::raw('patients_details.name as name, patients_details.*'))
			      ->where('raise_test.lab_id', '=', $lab_id)
			      ->where('patients_details.status',1)
			      ->where('raise_test.doctor_id','!=',0)
			      ->whereDate('raise_test.created_at', '>=', $from_date)
				  ->whereDate('raise_test.created_at', '<=', $to_date)
				  ->groupBy('raise_test.patients_id','patients_details.name','patients_details.id')
			      ->get()->toArray(); 
			 //$data['$patientsList'] = $patientsList;
			$patient_count = count($patientsList); 
			//2

			//3. Raise test count
			$raise_test = RaiseTest::whereDate('created_at', '>=', $from_date)
									 ->whereDate('created_at', '<=', $to_date)
									 ->where('lab_id',$lab_id)
									// ->where('raised_by',1)	
									 ->where('paid_status',1)   
									 ->where('doctor_id','!=',0);
									 //->orWhere('doctor_id','!=',0);   
			$raise_test_count = $raise_test->count();
			//3

			//4 doctors data
			 /*$doctors_datas = DocLab::where('lab_id',$lab_id)->get();*/
			//        

			/*$doctors_datas1 = DB::table('doctors_details')
							->join('doctor_patient','doctors_details.id','=','doctor_patient.doctor_id')
							->where(['doctor_patient.lab_id' => $lab_id])
							->whereDate('doctor_patient.created_at', '>=', $from_date)
				  			->whereDate('doctor_patient.created_at', '<=', $to_date)
							->get()
							->toArray();*/
			$doctors_datas1 = DB::table('doctors_details')
							->join('raise_test','doctors_details.id','=','raise_test.doctor_id')
							->where(['raise_test.lab_id' => $lab_id])
							//->where('raised_by',1)
							->whereDate('raise_test.created_at', '>=', $from_date)
				  			->whereDate('raise_test.created_at', '<=', $to_date)
							->get()
							->toArray();
			$doctors_datas = [];

			foreach($doctors_datas1 as $val){
				$doctors_datas[$val->doctor_id]=$val;
			}
			$doctors_datas = array_values($doctors_datas);
			/*$doctors_datas = DB::table('doctor_lab')->join('doctors_details','doctors_details.id','=','doctor_lab.doctor_id')->where(['doctor_lab.lab_id' => $lab_id])->get()->toArray();*/
			$data['labid'] = $lab_id;
			$data['docCount'] = count($doctors_datas);
			$data['status'] = config('resource.status_200');	
			$data['revenue_amount']             = $revenue_amount;
			$data['patient_count']              = $patient_count;
			//$data['patient_count']              = $request->doctor_id;
			$data['test_count']                 = $raise_test_count;		
			$data['doctors_data']               = $doctors_datas;
			$data['userIsBlocked'] = $this->getUserStatus($request->token);		
			return response()->json($data,config('resource.status_200'));
			//get all today record end

		
	   }   
		$response['msg']    = config('resource.parameter_missing'); 
		$response['status'] = config('resource.status_201');
		$response['userIsBlocked'] = $this->getUserStatus($request->token);
		return response()->json($response,config('resource.status_201'));
	}


public function getSubscriptionExpiredData(Request $request) 
    {
		$lab_id = $this->getUserId($request->token); 
		if($request->user_type ==''){
	     	 	   $response['msg'] = 'parameter missing';
				   $response['status'] = config('resource.status_201');
				   $response['userIsBlocked'] = $this->getUserStatus($request->token);
				   return response()->json($response,config('resource.status_201'));
	     	 }
		if($request->message_type == 'Expired'){
			//save notification start
                  $notification = Notification::where('lab_id',$lab_id)
                                              ->where('type','Expired')
                                              ->where('read_status',0)
                                              ->first();
                  if($notification){                               
	                  $notification->read_status      =  1;
	                  $notification->save(); 
                  }                  
            //save notification end
		} 
		$subscription = Subscription::where('user_id',$lab_id)->where('user_type',$request->user_type)->get();
		$expire_date = "0";
		if($subscription->count() !=0){
			     $end_date = new Carbon($subscription->first()->plan_end_date);
                 $expire_date = $end_date->format('d/m/Y');                		     
		   }		
		$response['subscription_expire'] = $expire_date ;
		$response['status'] = config('resource.status_200');
		$response['userIsBlocked'] = $this->getUserStatus($request->token);
		$user = $this->getAuthUser($request->token);
		$value = DB::table('fc_WSAuth')->first()->enable_log;
		if($value){   
			storelivigroactivity($user,config('resource.profile_completion_request'));
		}
		return response()->json($response,config('resource.status_200'));
	}

public function exportexcel() 
    {
		return Excel::download(new UserDataExcelExpoter(), 'users.xlsx');
	}


}