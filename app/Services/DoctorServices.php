<?php 
namespace App\Services;		
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use DB;
use App\Models\Doctors;
use App\Models\Hospital;
use App\Models\Labs;
use App\Models\DocPatient;
use App\Models\Patients;
use App\Models\LabsTest;
use App\Models\LabsAvaTest;
use App\Models\RaiseTest;
use App\Models\WorkingTime;
use App\Models\Report;
use App\Models\Notification;
use App\Models\Receipt;
use App\Models\DocLab;
use App\Models\Country;
use App\Models\Blood;
use App\Models\LabsCategory;
use JWTAuth;
use App\Models\CartTest;


class DoctorServices { 
		/**
		 * [getAuthUser description]
		 * @param  [type] $token [description]
		 * @return [type]        [description]
		 */
		public function getAuthUser($token){
			$user = JWTAuth::toUser($token);
			return $user;
		}

		/**
		 * Refresh the JWT token using the expired one
		 * @param  Request $request [description]
		 * @return [type]           [description]
		 */
		public function refreshToken(Request $request) {
			$token = $request->token;
			try {
				if (! $user = JWTAuth::parseToken()->authenticate()) {
					return response()->json(['user_not_found'],config('resource.status_404'));
				}
			}catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
				$refreshedToken = JWTAuth::refresh($token);
				$token = $refreshedToken;
				return response()->json(compact('token'));
			}
			return response()->json(['user not fount'],config('resource.status_404'));
		}

		/**
		 * this function used to get the user id from the Token
		 * @param  [type] $token [description]
		 * @return [type]        [description]
		 */
		private function getUserId($token) {
			$user = $this->getAuthUser($token);
			$user_detailed = $this->getDetailedData($user);	
			if($user_detailed == '' || $user_detailed ==null ){	
				return '';
			}else{
				return $user_detailed->id;
			}
			
		}

		private function getUserStatus($token) {				
			$user = $this->getAuthUser($token);
			$user_detailed = $this->getDetailedData($user);
			if($user_detailed->status == 1)
              return 0 ;
			else
			  return 1 ;
		}
		/**
		 * Get Detailed data from Doctor,Lab and patient Table
		 * @param  [type] $user [description]
		 * @return [type]       [description]
		 */
		private function getDetailedData($user) {

			if($user['user_type_id'] == 2) { //Doctor
				$details = Doctors::find($user['linked_id']);
			}elseif($user['user_type_id'] == 1){ //lab
				$details = Labs::find($user['linked_id']);
			}elseif($user['user_type_id'] == 3){ //Patients
				$details = Patients::find($user['linked_id']);
			}
			return $details;
		}
		
		
		public function getEducationDetails(Request $request){    	
		//$doc_id = $request->doctor_id;	
		$doc_id = $this->getUserId($request->token);
			$doctors_count = Doctors::where('id',$doc_id);		
		if($doctors_count->count() !=0){
			 $doctor_edu = Doctors::find($doc_id)->with('education')->get()->toArray();
		}
		else{
			 $response['msg'] = config('resource.no_record') ;
			 $response['status'] = config('resource.status_201') ;
			 return response()->json($response,config('resource.status_201'));
		 }
		$response['data'] = $doctor_edu;
		$response['status'] = config('resource.status_200');
		return response()->json($response,config('resource.status_200'));

		 }

		 /**
		 * doctor add education method
		 * @param  [type] $doctor [description]
		 * @return [type]       [description]
		 */

		 public function addEducation(Request $request)
		 {
		   if($request->type =="save")
		   {	
			$doctor_id = $this->getUserId($request->token);
			$doctor = Doctors::find($doctor_id);
			$degree = $request->degree;
			$college  = $request->college;
			$year = $request->year;
			$degree_certificate ='';
			if($request->filled('degree_certificate'))
				$degree_certificate = array(file_upload($request->degree_certificate,2,$doctor->mobile));
			$education = Education::create([
					'doctor_id' => $doctor_id,
					'degree'    => $degree,
					'college'  => $college,
					'year' 		=> $year,
					'degree_certificate' => $degree_certificate,
				]);
			
			$response['data'] = $education;
			$response['msg'] =  config('resource.added_successfully');
			$response['status'] = config('resource.status_200');
			return response()->json($response,config('resource.status_200'));
			}
			if($request->type == "add")
			{
			 $response['college'] = College::all();
			 $response['degree']  = Degree::all();
			 $response['year']    = '';
			 $response['status']  = config('resource.status_200');
			 return response()->json($response,config('resource.status_200'));
			}else{
				 $response['msg']    = config('resource.parameter_missing');
				 $response['status'] = config('resource.status_201');
				 return response()->json($response,config('resource.status_201'));
			}

	}

		 /**
		 * doctor add education method
		 * @param  [type] $doctor [description]
		 * @return [type]       [description]
		 */
	public function editEducation(Request $request){
			if($request->type == 'update')
		  {
			$education_id = Education::find($request->edu_id);
			$degree = $request->degree;
			$college  = $request->college;
			$year = $request->year;
			$degree_certificate= '';	
			if($request->filled('degree_certificate') && !empty($request->filled('degree_certificate')))
				   {   
					if($request->profile_image != '" "')
						{ 
						//single image	                
						$degree_certificate = file_upload($request->degree_certificate,2,$education_id->mobile) ;  	        
						}		       
				   } 
			$education_id->degree = $degree;
			$education_id->college = $college;
			$education_id->year = $year;
			$education_id->degree_certificate = $degree_certificate;
			$education_id->save();
			$response['data'] = $education_id;
			$response['status'] = config('resource.status_200');
			 //TODO need to check the condition
			 $user = $this->getAuthUser($request->token);
			 $value = DB::table('fc_WSAuth')->first()->enable_log;
				if($value)  
				{
				storelivigroactivity($user,"Update Lab their own test data");
			 }
			return response()->json($response,config('resource.status_200'));
		 }
		 elseif($request->type == 'get')
		 {
				 $education = Education::find($request->edu_id);
				 $response['data']   = $education;
				 $response['status'] = config('resource.status_200');
				 return response()->json($response,config('resource.status_200'));
		 }
		 else
		 { 
				$response['msg']    = 'No Record';
				$response['status'] = config('resource.status_200');
				return response()->json($response,config('resource.status_200'));
		 }

	}

		 /**
		 * doctor raised test details get
		 * @param  [type] $doctor [description]
		 * @return [type]       [description]
		 */
	public function Docraisedtest(Request $request){	
		$doc_id = $this->getUserId($request->token);
		$doctor = Doctors::find($doc_id);
		$raised_test = $doctor->raised_test()->with('getLab','getPatient')->jsonPaginate();
		$response['data']  = $raised_test;
		$response['status'] = config('resource.status_200');
		$response['userIsBlocked'] = $this->getUserStatus($request->token);
		return response()->json($response,config('resource.status_200'));
	}


	/*public function Labraisedtest(Request $request){
		$lab_id = $this->getUserId($request->token);
		$lab = Labs::find($lab_id);
		$response['data']  = $raised_test;
		$response['status'] = config('resource.status_200');
		return response()->json($response,config('resource.status_200')); 
	}*/



		 /**
		 * get all Generated Order
		 * @param  [type] $doctor [description]
		 * @return [type]       [description]
		 */
	public function getgeneratedorder(Request $request){
		$doc_id = $this->getUserId($request->token);
		$doctor = Doctors::find($doc_id);
		$raised_test = $doctor->raised_test()
							  ->join('test_report', 'raise_test.id', '=', 'test_report.raise_test_id')
							  ->with('getLab','getPatient')->jsonPaginate();							
		$response['data'] = $raised_test;
		$response['status'] = config('resource.status_200');
		$response['userIsBlocked'] = $this->getUserStatus($request->token);
		return response()->json($response,config('resource.status_200'));
	}



		 /**
		 * Contact query Mail send to Support Team
		 * @param  [type] $doctor [description]
		 * @return [type]       [description]
		 */
	public function sendSupportMail(Request $request){
		$user_type = $request->user_type;
		$user_id = $this->getUserId($request->token);

		//$user_id = $request->user_id;
		$description = $request->description;
		$attachment = '';
		if($request->filled('attachment'))
				$attachment =file_upload($request->attachment,'Support','Attachment');
		if($user_type == 1 ){ //lab
				$detail = Labs::find($user_id);
			}elseif($user_type == 2){//doctor
				$detail = Doctors::find($user_id);
			}
		$data['description'] = $description;
		$data['attachment'] = $attachment;
		$name = $detail->name;
		$email = $detail->email;
		Mail::to('balamuruga.subiramanian@dci.in')
		->send(new SupportMail($data));
		$response['status'] = config('resource.status_200');
		$response['message'] = 'Mail Send Succesfully';
		$response['userIsBlocked'] = $this->getUserStatus($request->token);
		return response()->json($response,config('resource.status_200'));

	}

		 /**
		 * getDoctorDashboard doctor details
		 * @param  [type] $doctor [description]
		 * @return [type]       [description]
		 */
	public function getDoctorDashboard(Request $request)
	{
		$from_date = $request->from_date;
		$to_date = '';
		/*$from_date = Carbon::createFromFormat('Y:m:d',$from_date);
		dd($from_date);*/
		if($request->has('to_date'))
			$to_date = $request->to_date;
		//1. Need to get the patient list( count )
		if($to_date) {
			$patient_count = DocPatient::whereDate('created_at', '>=', $from_date)->whereDate('created_at','<=', $to_date)->count();
			$raise_test = RaiseTest::selectRaw('count(*) AS cnt, lab_id')->whereDate('created_at', '>=', $from_date)->whereDate('created_at','<=', $to_date);
		}
		else{
			$patient_count = DocPatient::whereDate('created_at','=',$from_date);
			$raise_test = RaiseTest::selectRaw('count(*) AS cnt, lab_id')->whereDate('created_at','=',$from_date);
		}
			   
		//2. Raise test count
		$raise_test_count = $raise_test->count();
		//3. Raise test count based on the Lab group
		$raise_test_based_on_lab = $raise_test->groupBy('lab_id')->with('getLab')->get();

		$data['patient_count']  = $patient_count;
		$data['test_count'] = $raise_test_count;
		$data['lab_details'] = $raise_test_based_on_lab;
		$data['userIsBlocked'] = $this->getUserStatus($request->token);
		return response()->json($data,config('resource.status_200'));
	}




		/**
		 *  getdoctorDetails
		 * @param  [type] $doctor [description]
		 * @return [type]         [description]
		 */
	public function getdoctorDetails(Request $request)
		 {  
			$doctor_id = $this->getUserId($request->token);     	
			$doctor_datas = Doctors::find($doctor_id);
			if($doctor_datas)
			   {   
				/*$response['data']    = $doctor_datas->with('education')->with('hospitals')->get()->first();       
					$response['blood_group']    = Blood::all();                                                  
					$response['status']  = 200;
					return response()->json($response,200);*/
					$response['data']           = $doctor_datas ;
					$response['education']      = $doctor_datas->education()->get()->toArray();                   
					$response['blood_group']    = Blood::all(); 
					$response['city']           = DB::table('statelist')->select('city_id','city_name')->get();                                                 
					$response['status']  = config('resource.status_200');
					$response['userIsBlocked'] = $this->getUserStatus($request->token);
					return response()->json($response,config('resource.status_200'));
			   }else
			   {
				$response['msg']    = config('resource.no_record');
				$response['status'] = config('resource.status_201');
				$response['userIsBlocked'] = $this->getUserStatus($request->token);
				return response()->json($response,config('resource.status_201'));
			   }
		 }


		/**
		 *  getdoctorDetails
		 * @param  [type] $doctor [description]
		 * @return [type]         [description]
		 */
	  public function getDoctorRegistration(Request $request){	
		 $doc_id = $this->getUserId($request->token);	 
		 $details = Doctors::find($doc_id);
		 if($request->filled('type')){
					  if($request->type == 'get'){
									$data = array();
								foreach($details->get() as $detai){
										  $detai->degree_list = College::all();
										  $data = $detai ;
								}
								$response['data']    = $data;
								$response['status']  = 200;
								$response['userIsBlocked'] = $this->getUserStatus($request->token);
								return response()->json($response,200);
					  }elseif($request->type == 'save'){
							  $registered_certificate ='';
							if($request->filled('registered'))
								$registered_certificate = array(file_upload($request->registered,2,$details->mobile));
							  $details->mcid               = ($request->mcid) ? $request->mcid : 0;
							  $details->register_council   = ($request->register_council) ? $request->register_council : 0;
							  $details->registered         = $registered_certificate ;
							  $details->register_year  = ($request->register_year) ? $request->register_year : 0;
							  $details->save();
							 $response['msg']     = config('resource.updated_successfully');
							 $response['status']  = config('resource.status_200');
							 $response['userIsBlocked'] = $this->getUserStatus($request->token);
							 $response['data']    = $details;				         
							 return response()->json($response,config('resource.status_200'));
					  }
				$response['msg']    = config('resource.parameter_missing'); 
				$response['status'] = config('resource.status_201');
				$response['userIsBlocked'] = $this->getUserStatus($request->token);
				return response()->json($response,config('resource.status_201'));

		  }else{ 
				$response['msg']    = config('resource.parameter_missing'); 
				$response['status'] = config('resource.status_201');
				$response['userIsBlocked'] = $this->getUserStatus($request->token);
				return response()->json($response,config('resource.status_201'));
		  }
		 
	   }

		 /**
		 *  doctor forgot password
		 * @param  [type] $doctor [description]
		 * @return [type]         [description]
		 */
	   public function DoctorForgotPassword(Request $request){    	
		$users = DB::table('admin_users')->where('linked_id',$request->id)->first();  
		if($request->id =='' || $request->password ==''){ $response['msg'] = config('resource.parameter_missing');
			$response['status'] = config('resource.status_201');
			$response['userIsBlocked'] = $this->getUserStatus($request->token);
			return response()->json($response,config('resource.status_201'));}                  
		if($users->user_type_id == 2){                  
			//update password	
			DB::table('admin_users')->where('id',$users->id)->update(['password' => bcrypt($request->password)]);
			$user_password = Doctors::find($users->linked_id);
			$user_password->password = bcrypt($request->password);                       
			$user_password->save();

			$response['msg'] = config('resource.updated_successfully');
			$response['status'] = config('resource.status_200');
			$response['userIsBlocked'] = $this->getUserStatus($request->token);
			return response()->json($response,config('resource.status_200'));
		}               
		$response['msg'] = config('resource.id_missing');
		$response['status'] = config('resource.status_201');
		return response()->json($response,config('resource.status_201'));   
	  } 


		/**
		 *  doctor raised test details
		 * @param  [type] $doctor [description]
		 * @return [type]         [description]
		 */
	public function getDoctorRaisetest(Request $request) {
			$doc_id = $this->getUserId($request->token);
			//all categories get
			//only search use start
			$datas = array();
			if($request->filled('search_tag'))
			{	$test1 = LabsTest::where('search_tags','ILIKE','%'.$request->search_tag.'%')                        
					->jsonPaginate();     
				if($test1)
				{
					$datas =  $test1 ;    

				}
			}
			//only search use end		
			$test1 = LabsTest::jsonPaginate();     
			$new_array = '';
			$category_data = array();
			if($test1->count() !=0)
			{	foreach($test1 as $key=>$tes)
				{ 
					if($tes->search_tags)
					{
						$new_array.= $tes->search_tags.',';
					}

				} 
			}else{
				$response['msg'] = config('resource.no_record');
				$response['status'] = config('resource.status_400');
				$response['userIsBlocked'] = $this->getUserStatus($request->token);
				return response()->json($response,config('resource.status_200'));
			}
			if($new_array)
			{	foreach (explode(',',$new_array) as $key => $value)
				{		if(!in_array($value,$category_data) && $value)
					{	$category_data[] = $value ;	}
				} 
			}

			if($test1->count() !=0)
			{                    
				$response['data'] = ($request->search_tag) ? $datas : $test1;
				$response['category'] = $category_data;
				$response['userIsBlocked'] = $this->getUserStatus($request->token);
				$response['status'] = config('resource.status_200');
			}
			else{
				$response['msg'] = config('resource.no_record');
				$response['status'] = config('resource.status_400');
				$response['userIsBlocked'] = $this->getUserStatus($request->token);
			}
			$value = DB::table('fc_WSAuth')->first()->enable_log;
			if($value)   {   
				storelivigroactivity($user,config('resource.lab_test_based_on_the_search_tag'));
			}
			return response()->json($response,config('resource.status_200'));
			
		}

		/**
		 * doctor Order list
		 * @param  [type] $doctor [description]
		 * @return [type]         [description]
		 */
	public function getDoctorOrderList(Request $request) {			
			$user_id = $this->getUserId($request->token);							
			if($request->filled('from_date') && $request->filled('to_date'))
				$from_date = $request->from_date;
				$to_date   = $request->to_date;
				 //noraml data 
				$detail = RaiseTest::where('doctor_id',$user_id)
								   ->where('raised_by',0)
								   ->groupBy('patients_id')
								   ->with('getPatient')
								   ->get();
				   //noraml data                
				   //search data using date              
				 if($request->filled('from_date') && $request->filled('to_date'))
				   {               $detail = RaiseTest::where('doctor_id',$user_id)
								   ->where('raised_by',0)
								   ->groupBy('patients_id')
								   ->whereDate('created_at', '>=', $from_date)
								   ->whereDate('created_at', '<=', $to_date)
								   ->with('getPatient')
								   ->get();
						
				   }
				   //search data using date
				if($detail->count() ==0){
					$response['msg'] = config('resource.no_record');
					$response['status'] = config('resource.status_400');
					$response['userIsBlocked'] = $this->getUserStatus($request->token);
					return response()->json($response,config('resource.status_200'));}
			
			$final_data = array();
			if($detail){
				foreach($detail as $patient)
				  {
					$mytime = Carbon::now(); $time = new Carbon($patient->created_at);
					$shift_end_time = new Carbon($mytime->toDateTimeString()); $val = $time->diffForHumans($shift_end_time); $resul = str_replace("before","ago",$val); 			
					$patient->dates           = 'Last test done on '.$resul ;
					$patient->complete_status = ($patient->paid_status ==1) ? 'Completed' : 'Pending' ;
					$final_data = $patient ;
				  }
					 }

			if(count($detail) ==0)
			{
				$response['msg'] = config('resource.no_record');
				$response['status'] = config('resource.status_201');
				$response['userIsBlocked'] = $this->getUserStatus($request->token);
				return response()->json($response,config('resource.status_201'));                                   
			}
			$response['data'] = $detail;
			$response['status'] = config('resource.status_200');
			$response['message'] = config('resource.success');
			$response['userIsBlocked'] = $this->getUserStatus($request->token);
			$user = $this->getAuthUser($request->token);
			$value = DB::table('fc_WSAuth')->first()->enable_log;
			if($value)   {   
				storelivigroactivity($user,config('resource.Get_patient_List_Request'));
			}
			return response()->json($response,config('resource.status_201'));
		}

		 /**
		 * doctor Doctor Order detail individual
		 * @param  [type] $doctor [description]
		 * @return [type]         [description]
		 */
	   public function getDoctorOrderdetail(Request $request) {	

			$user_id = $this->getUserId($request->token);							
			if($request->raise_test_id == ''){
			 $response['status'] = config('resource.status_200');
			 $response['message'] = config('resource.success');
			 $response['userIsBlocked'] = $this->getUserStatus($request->token);
			 return response()->json($response,config('resource.status_200'));
			}
				
				//noraml data 	
				$raise_test_id = $request->raise_test_id;	
				$detail = RaiseTest::where('id',$raise_test_id)
								   ->where('doctor_id',1)
								   ->where('raised_by',0)			                   
								   ->with('getPatient')
								   ->with('getReciept')
								   ->first();

			  //order_raised data get       
			  $time = new Carbon($detail->created_at);                  
			  $shift_end_time = new Carbon($time->toDateTimeString());  
			  //order_raised data get              
				   
				//noraml data   
				 $detail->patient_id = 'AW'.$detail->getPatient['id'] ;
				 $detail->order_raised = str_replace(".","at",$shift_end_time->format('F d, Y . h:i:s A'));        
				 
				if($detail->count() ==0){
					$response['msg'] = config('resource.no_record');
					$response['status'] = config('resource.status_400');
					$response['userIsBlocked'] = $this->getUserStatus($request->token);
					return response()->json($response,config('resource.status_200'));}

			if($detail->count() ==0)
			{
				$response['msg'] = config('resource.no_record');
				$response['status'] = config('resource.status_201');
				$response['userIsBlocked'] = $this->getUserStatus($request->token);
				return response()->json($response,config('resource.status_201'));                                   
			}
			$response['data'] = $detail;
			$response['status'] = config('resource.status_200');
			$response['message'] = config('resource.success');
			$response['userIsBlocked'] = $this->getUserStatus($request->token);
			$user = $this->getAuthUser($request->token);
			$value = DB::table('fc_WSAuth')->first()->enable_log;
			if($value)   {   
				storelivigroactivity($user,config('resource.Get_patient_List_Request'));
			}
			return response()->json($response,config('resource.status_201'));
		}

	 /**
	 * [saveNotification description]
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function DoctorsaveNotification(Request $request)
	{
		if($request->filled('raise_test_id'))
		{   $doctor_id = $this->getUserId($request->token); 
			$test = RaiseTest::find($request->raise_test_id);           
			if(!empty($test->id))
			{                     
				$notification = new Notification();
				$notification->type             = 'test';
				$notification->lab_id           =  ($test->lab_id)? $test->lab_id : 0;
				$notification->doctor_id        =  $doctor_id; 
				$notification->patient_id       =  ($test->patients_id) ? $test->patients_id : 0;
				$notification->raise_test_id    =  $test->id;
				$notification->save();
				$response['msg']    = 'Updated Notification Successfully' ;
				$response['status'] = config('resource.status_200');
				$response['userIsBlocked'] = $this->getUserStatus($request->token);
				return response()->json($response,config('resource.status_200'));
			}else
			{ 
				$response['msg']    = config('resource.no_record');
				$response['status'] = config('resource.status_201');
				$response['userIsBlocked'] = $this->getUserStatus($request->token);
				return response()->json($response,config('resource.status_201'));
			}
		}else
		{
			$response['msg']    = config('resource.parameter_missing') ;
			$response['status'] = config('resource.status_201');
			$response['userIsBlocked'] = $this->getUserStatus($request->token);
			return response()->json($response,config('resource.status_201'));
		}

	}
		 /**
		 * doctor notification data get 
		 * @param  [type] $doctor [description]
		 * @return [type]         [description]
		 */

	public function getDoctorNotification(Request $request){     	
			$doctor_id = $this->getUserId($request->token);
			$notification = Notification::where([['doctor_id',$doctor_id]]);
			$data = $notification->with('getPatient','getLab')->latest()->jsonPaginate();     	
			$result = array();       
			foreach($data as $key=>$da)
			{

					  $mytime = Carbon::now();        
					  $time = new Carbon($da->created_at);
					  $shift_end_time = new Carbon($mytime->toDateTimeString());
					  $val = $time->diffForHumans($shift_end_time);
					  $resul = str_replace("before","ago",$val);
					  $result[$key]['id']                    =  $da->id ;
					  $result[$key]['raise_test_id']         =  $da->raise_test_id ;
					  $result[$key]['type']                  =  $da->type ;
					  $result[$key]['lab_id']                =  $da->lab_id ;
					  $result[$key]['doctor_id']             =  $da->doctor_id ;
					  $result[$key]['patient_id']            =  $da->patient_id ;
					  $result[$key]['created_at']            =  $resul ;
					  $result[$key]['patient_name']          =  (!empty($da->getPatient->name)) ? $da->getPatient->name  :'null' ;
					  /*$result[$key]['patient_photo']        =  $da->getPatient->profile_image ;*/
					  /*$result[$key]['doctor_name']          =  (!empty($da->getDoctor->name)) ? $da->getDoctor->name  :'null' ;
					  $result[$key]['doctor_photo']         =  (!empty($da->getDoctor->name)) ? $da->getDoctor->name : 'null' ;*/
					  $result[$key]['lab_name']             = (!empty($da->getLab->name)) ? $da->getLab->name : 'null' ;
					  $result[$key]['lab_photo']            = (!empty($da->getLab->profile_image)) ? $da->getLab->profile_image : 'null' ; 
			}                
			
			$response['data'] = $result;
			$response['status'] = 200;
			$response['userIsBlocked'] = $this->getUserStatus($request->token);
			$user = $this->getAuthUser($request->token);
			 $value = DB::table('fc_WSAuth')->first()->enable_log;
					if($value)   {   
				storelivigroactivity($user,config('resource.notification_api_call'));
			}
			return response()->json($response,200);

		 }


		/**
		 * doctor insert new  test cart
		 * @param  [type] $doctor [description]
		 * @return [type]         [description]
		 */
	public function DoctorinsertTestCart(Request $request)
	 {
		 
		$user_id = $this->getUserId($request->token); 
		if($request->filled('patient_id') && $request->filled('test_id'))
		{  		
			   $cart_count = CartTest::where('user_id',$user_id)
									  ->where('patient_id',$request->patient_id)
									  ->where('patient_id',$request->test_id)
									  ->where('user_type',2)->get();
			   if($cart_count->count() ==0)
			   {
					   $insert_data = new CartTest();
					   $insert_data->user_id    =  $user_id;
					   $insert_data->patient_id =  $request->patient_id;
					   $insert_data->user_type  =  2;
					   $insert_data->test_id    =  $request->test_id;
					   $insert_data->save(); 
					   $response['msg']    = config('resource.added_successfully');
					   $response['status'] = config('resource.status_200');
					   $response['userIsBlocked'] = $this->getUserStatus($request->token);
					   return response()->json($response,config('resource.status_200'));

			   }else{
					   $cart_count = CartTest::where('user_id',$user_id)
									  ->where('patient_id',$request->patient_id)
									  ->where('patient_id',$request->test_id)
									  ->where('user_type',2)->delete();      
					   $response['msg']    = config('resource.removed_successfully');
					   $response['status'] = config('resource.status_201');
					   $response['userIsBlocked'] = $this->getUserStatus($request->token);
					   return response()->json($response,config('resource.status_201'));
			   }
		}else
		{
			$response['msg']    = config('resource.parameter_missing') ;
			$response['status'] = config('resource.status_201');
			$response['userIsBlocked'] = $this->getUserStatus($request->token);
			return response()->json($response,config('resource.status_201'));
		}

	}


	 /**
	 * [get doctor test without added test ]
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function FilterDoctorlab(Request $request)
	{	
			$doctor_id = $this->getUserId($request->token);
			if($doctor_id == '' || $doctor_id ==null ){
				    $response['status'] = config('resource.status_400');
				    $response['message'] = config('resource.user_doesnt_exists');
				    $response['userIsBlocked'] = 1;
				    return response()->json($response,config('resource.status_200'));
			  }				
			$DocLab = DocLab::where('doctor_id',$doctor_id)->get();
			if($DocLab->count() !=0){
			$DocLab_labid = array();
				foreach ($DocLab as $key => $value) {
					$DocLab_labid[]= $value->lab_id ;	        	
				}
				$final_labid = array_unique($DocLab_labid);
				if($request->filled('search_tag'))
				  $data = Labs::where('name','ILIKE','%'.$request->search_tag.'%')->whereNotIn('id',$final_labid)->jsonPaginate();
				else
				  $data = Labs::whereNotIn('id',$final_labid)->jsonPaginate();
			}else{
				if($request->filled('search_tag'))
				  $data = Labs::where('name','ILIKE','%'.$request->search_tag.'%')->jsonPaginate();
				else
				  $data = Labs::jsonPaginate();
			}		

			$final_data = array();
			//opening date get
			foreach ($data as $key => $value) {
				//
				$dt = Carbon::now('Asia/Kolkata');
				$days_name = $dt->getDays();
				$today_name = $dt->format('l');
			$time_data = WorkingTime::where('lab_id',$value->id)->first();	
			$today_opening_time = 'please update time';	
			if($time_data)
				{		
						if($time_data->day_1_open_time !='00:00:00')
					{
						if($days_name[0] == $today_name)
						{   $start_time = Carbon::parse($time_data->day_1_open_time)->format('h:m A');
							$end_time = Carbon::parse($time_data->day_1_end_time)->format('h:m A');

						}elseif($days_name[1] == $today_name){
							$start_time = Carbon::parse($time_data->day_2_open_time)->format('h:m A');
							$end_time = Carbon::parse($time_data->day_2_end_time)->format('h:m A');
						}elseif($days_name[2] == $today_name){
							$start_time = Carbon::parse($time_data->day_3_open_time)->format('h:m A');
							$end_time = Carbon::parse($time_data->day_3_end_time)->format('h:m A');
						}elseif($days_name[3] == $today_name){				
							$start_time = Carbon::parse($time_data->day_4_open_time)->format('h:m A');
							$end_time = Carbon::parse($time_data->day_4_end_time)->format('h:m A');
						}elseif($days_name[4] == $today_name){
							$start_time = Carbon::parse($time_data->day_5_open_time)->format('h:m A');
							$end_time = Carbon::parse($time_data->day_5_end_time)->format('h:m A');
						}elseif($days_name[5] == $today_name){
							$start_time = Carbon::parse($time_data->day_6_open_time)->format('h:m A');
							$end_time = Carbon::parse($time_data->day_6_end_time)->format('h:m A');
						}elseif($days_name[6] == $today_name){
							$start_time = Carbon::parse($time_data->day_7_open_time)->format('h:m A');
							$end_time = Carbon::parse($time_data->day_7_end_time)->format('h:m A');
						}
						  $today_opening_time = $start_time.' to '.$end_time;
					}
				}		 	
				/*dd($dt->format('l jS \\of F Y h:i:s A'));*/					     
				$value->opening_time = $today_opening_time ;
				$final_data[] = $value;
				# code...
			}
			//opening date get

			$user = $this->getAuthUser($request->token);
			$value = DB::table('fc_WSAuth')->first()->enable_log;
			if($value)
			{   
				storelivigroactivity($user,config('resource.update_lab_list_to_doctor'));   
			}
			if(count($data) !=0){ $response['data']   = $data; }else{ $response['msg']   = 'No data'; }
			$response['status'] = config('resource.status_200');
			return response()->json($response,config('resource.status_200'));
		

	}

	/**
 * [doctorLab description]
 * @param  Request $request [description]
 * @return [type]           [description]
 */
public function doctorLab(Request $request)
{
	error_log(print_r($request->all(),true),3,'/var/www/html/error.log');
	$doctor_id = $this->getUserId($request->token);
	//get doctor labs start
	if($request->type == 'mylab')
	{ 
		
		$DocLab = DocLab::where('doctor_id',$doctor_id)->get();
			if($DocLab->count() !=0){
			    $DocLab_labid = array();
				foreach ($DocLab as $key => $value){
					$DocLab_labid[]= $value->lab_id ;	        	
				}
				$final_labid = array_unique($DocLab_labid);
				if($request->filled('search_tag'))
				  $data = Labs::where('name','ILIKE','%'.$request->search_tag.'%')->whereIn('id',$final_labid)->jsonPaginate();
				else
				  $data = Labs::whereIn('id',$final_labid)->jsonPaginate();
			}else{
					$response['msg'] = config('resource.no_record');
					$response['status'] = config('resource.status_201');
					/*$response['userIsBlocked'] = $this->getUserStatus($request->token);*/
					return response()->json($response,config('resource.status_201'));
			}
		if($data->count() ==0){ 
			$response['msg'] = config('resource.no_record');
			$response['status'] = config('resource.status_201');
			$response['userIsBlocked'] = $this->getUserStatus($request->token);
			return response()->json($response,config('resource.status_201'));
		}
		
			$final_data = array();
			//opening date get
			foreach ($data as $key => $value) {				
				$dt = Carbon::now('Asia/Kolkata');
				$days_name = $dt->getDays();
				$today_name = $dt->format('l');
			$time_data = WorkingTime::where('lab_id',$value->id)->first();	
			$today_opening_time = 'please update time';	
			if($time_data)
				{		
						if($time_data->day_1_open_time !='00:00:00')
					{
						if($days_name[0] == $today_name)
						{   $start_time = Carbon::parse($time_data->day_1_open_time)->format('h:m A');
							$end_time = Carbon::parse($time_data->day_1_end_time)->format('h:m A');

						}elseif($days_name[1] == $today_name){
							$start_time = Carbon::parse($time_data->day_2_open_time)->format('h:m A');
							$end_time = Carbon::parse($time_data->day_2_end_time)->format('h:m A');
						}elseif($days_name[2] == $today_name){
							$start_time = Carbon::parse($time_data->day_3_open_time)->format('h:m A');
							$end_time = Carbon::parse($time_data->day_3_end_time)->format('h:m A');
						}elseif($days_name[3] == $today_name){				
							$start_time = Carbon::parse($time_data->day_4_open_time)->format('h:m A');
							$end_time = Carbon::parse($time_data->day_4_end_time)->format('h:m A');
						}elseif($days_name[4] == $today_name){
							$start_time = Carbon::parse($time_data->day_5_open_time)->format('h:m A');
							$end_time = Carbon::parse($time_data->day_5_end_time)->format('h:m A');
						}elseif($days_name[5] == $today_name){
							$start_time = Carbon::parse($time_data->day_6_open_time)->format('h:m A');
							$end_time = Carbon::parse($time_data->day_6_end_time)->format('h:m A');
						}elseif($days_name[6] == $today_name){
							$start_time = Carbon::parse($time_data->day_7_open_time)->format('h:m A');
							$end_time = Carbon::parse($time_data->day_7_end_time)->format('h:m A');
						}
						  $today_opening_time = $start_time.' to '.$end_time;
					}
				}		 	
				/*dd($dt->format('l jS \\of F Y h:i:s A'));*/					     
				$value->opening_time = $today_opening_time ;
				$final_data[] = $value;
				# code...
			}
		//opening date get

		$user = $this->getAuthUser($request->token);
		$value = DB::table('fc_WSAuth')->first()->enable_log;
		if($value)
		{   
			storelivigroactivity($user,config('resource.update_lab_list_to_doctor'));  
		}    
      
		if(count($data) !=0){ $response['data']   = $data; }else{ $response['msg']   = config('resource.no_record'); }
		$response['status'] = config('resource.status_200');
		$response['userIsBlocked'] = $this->getUserStatus($request->token);
		return response()->json($response,config('resource.status_200'));
	}  //get doctor labs end
	
	if($request->type == 'delete')
	{               
		$datas = DocLab::where('lab_id',$request->lab_id)->where('doctor_id',$doctor_id)->get(); 
		 if($datas->count() ==0){
			$response['msg']    = config('resource.no_record');
			$response['status'] = config('resource.status_201');
			$response['userIsBlocked'] = $this->getUserStatus($request->token);
			return response()->json($response,config('resource.status_201')); } 
		$datas = DocLab::where('lab_id',$request->lab_id)->where('doctor_id',$doctor_id)->delete();           
		$user = $this->getAuthUser($request->token);
		$value = DB::table('fc_WSAuth')->first()->enable_log;
		if($value)
		{   
			storelivigroactivity($user,config('resource.update_lab_list_to_doctor'));  
		}
		$response['msg']    = 'Data deleted successfully';
		$response['status'] = config('resource.status_200');
		$response['userIsBlocked'] = $this->getUserStatus($request->token);
		return response()->json($response,config('resource.status_200'));
	}
	if($request->type == 'edit')
	{               
		$datas = DocLab::find($request->doclab_id);
		if($datas ==''){
			$response['msg']    = config('resource.no_record');
			$response['status'] = config('resource.status_201');
			$response['userIsBlocked'] = $this->getUserStatus($request->token);
			return response()->json($response,config('resource.status_201')); }

		$user = $this->getAuthUser($request->token);
		$value = DB::table('fc_WSAuth')->first()->enable_log;
		if($value)
		{   
			storelivigroactivity($user,config('resource.update_lab_list_to_doctor'));  
		}
		$response['data']    = $datas;
		$response['status']  = config('resource.status_200');
		$response['userIsBlocked'] = $this->getUserStatus($request->token);
		return response()->json($response,config('resource.status_200'));
	}
	//get all labs end       

	//    $lab_id = $request->lab_id;
 if($request->lab_id !=''){
	$doctor_id = $this->getUserId($request->token);
	$lab_id    = $request->lab_id;   
	$check_data = DocLab::where('doctor_id',$doctor_id)->where('lab_id',$lab_id)->get();  
	if(count($check_data) == 0){
		$data = new DocLab;
		$data->doctor_id =  $doctor_id;
		$data->lab_id    =  $lab_id ;
		$data->feedback  =  ($request->comment) ? $request->comment : '';
		$data->save();
	}else
	{
        $data = DocLab::where('doctor_id',$doctor_id)->where('lab_id',$lab_id)->first();
        $data->doctor_id =  $doctor_id;
		$data->lab_id    =  $lab_id ;
		$data->feedback  =  ($request->comment) ? $request->comment : '';
		$data->save();

		$response['msg']     = config('resource.already_added'); 
		$response['status']  = config('resource.status_201');
		$response['userIsBlocked'] = $this->getUserStatus($request->token);
		return response()->json($response,config('resource.status_201'));
	}
}

	$user = $this->getAuthUser($request->token);
	$value = DB::table('fc_WSAuth')->first()->enable_log;
	if($value){   
		storelivigroactivity($user,config('resource.update_lab_list_to_doctor'));  
	}
	$response['msg']     = config('resource.added_successfully'); 
	$response['status']  = config('resource.status_200');
	$response['userIsBlocked'] = $this->getUserStatus($request->token);
	if($request->lab_id !='')
	   $response['data']    = $data;       
	return response()->json($response,config('resource.status_200'));
}

	}



