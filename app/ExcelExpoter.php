<?php

namespace App;
use Excel;
use Illuminate\Contracts\View\View;
use App\Admin\RaiseTest;
class ExcelExpoter implements FromView
{

	public function __construct($id){
		$this->id = $id;
	}

    public function view(): View
    {

        $labtest = RaiseTest::find(27);
        return view('labtest.labtest_detail',compact('labtest'));
    }
}