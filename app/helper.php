<?php 
use App\Models\Doctors;
use App\Models\LabsTest;
use App\Models\Patients;
use App\Models\LabTestGroup;
use App\Models\LabsCategory;
use jeremykenedy\LaravelLogger\App\Http\Traits\ActivityLogger;

function file_upload($data,$type,$info){	    
	if($type == 1){
		$type = "Lab";
	}
	else if($type == 2){
		$type = "Doctor";
	}
	    //contacts image upload
	     $path = explode('/',$type) ;
	     
	     if(isset($path[1]))
	     	{ 
	     		$type = 'contacts';
	     	}
		//contacts image 
	$ProfileImagedate = md5(date('Y-m-d H:i:s'));
	$ProfileImageData = base64_decode($data);
	$profileImageFilename = $ProfileImagedate.rand().".png";
	$location = $type.'/'.encrypt($info).'/'.$profileImageFilename;
	$path = \Storage::disk('s3')->put($location, $ProfileImageData);
	//\Storage::put($profileImageFilename, $ProfileImageData);
	return $location;
}

function imageJson($old,$new){
	  if(is_array($old)){ 
	 array_push($old,$new);
	 return $old;
	 }else{	 	
	 $old = [];
	 array_push($old,$new);
	 return $old;
	}

}

function ProfileComplete($data){
	$total_input = count($data);
	$filled_data = array_filter($data, function($var){return !is_null($var);} );
	$percentage = (count($filled_data) / $total_input ) * 100;
	return $percentage;
}

function get_fcm_key($id,$user_type){
	if($user_type == 'doctor'){
		$userType = 2;
	}elseif($user_type == 'lab'){
		$userType = 1;
	}else{
		$userType = 3;
	}
	$query = \DB::table('admin_users')->where([['linked_id',$id],['user_type_id',$userType]])->first();
	$fcm_key = isset($query->fcm_key)?$query->fcm_key:'';

	/*if($user_type == 'doctor'){
		$fcm_key = \DB::table('admin_users')->where([['linked_id',$id],['user_type_id',2]])->first()->fcm_key;
	}elseif($user_type == 'lab'){
		$fcm_key = \DB::table('admin_users')->where([['linked_id',$id],['user_type_id',1]])->first()->fcm_key;
	}else
		$fcm_key = \DB::table('admin_users')->where([['linked_id',$id],['user_type_id',3]])->first()->fcm_key;*/
	return $fcm_key;
}

function getCityName($id){
	if($id)
		$city = \DB::table('statelist')->where('city_id',$id)->first()->city_name;
	else
		$city = '';
	return $city;
}

function getdoctordetails($id,$column_name)
{
	$data = Doctors::find($id);
	return $data->$column_name;
}

function getLabtestdetails($id,$column_name)
{   
	$find_dats = LabsTest::where('id',$id)->get();
     if($find_dats->count() !=0){
	$data = LabsTest::find($id);
	return $data->$column_name;
        }
        $data = '';
        return $data;
}
function getPatientdetails($id,$column_name)
{   
	$find_dats = Patients::where('id',$id)->get();
     if($find_dats->count() !=0){
	$data = Patients::find($id);
	return $data->$column_name;
        }
        $data = '';
        return $data;
}

function LabTestGroupData($id,$column_name)
{   
	$find_data = LabTestGroup::where('id',$id)->get();
     if($find_data->count() !=0){
	   $data = LabTestGroup::find($id);
	   if($column_name =='')
         return $data;
	   else
	     return $data->$column_name;
        }
        $data = '';
        return $data;
}

function LabsCategorydata($id,$column_name)
{   
	$find_data = LabsCategory::where('id',$id)->get();
     if($find_data->count() !=0){
	   $data = LabsCategory::find($id);
	   if($column_name =='')
         return $data;
	   else
	     return $data->$column_name;
        }
        $data = '';
        return $data;
}

function getarraylastimages($column_name)
{ 

	if(is_array($column_name)){
	foreach($column_name as $key => $value)
	{
		$data = $value ;
	}
	return $data;
    }
    $data = '';
    return $data;
}

function send_push_notification($fcm,$msg,$title) {
	//$type test
	//$type  report
	 /*$fcm = 'dPUO-rUcSA4:APA91bHB5zIJNtycxS6L7Q1L34WOotF2eMY5oSJ6JYfjFwxVmq3HNCzhcGKxLodCrxC3VqB_AEJzyiOYFVLvL9NlbfDMVff4jv4MtfLpSKPOVez6mDYg0WFvl_BrbrHmCv3sAHZVneXj';*/ 
	/* $fcm = 'fK5lf3jkTYA:APA91bFxHpPJ7z0V7AE6F4UzgO_W1IO4BJkyOXg2fQlSRWHAJ_UAtfYdOKdc2vxWEaoJvIGXHO_pcgBU9DG2FvNkYevCm1MDZjPfYZnQdrxbFuF1m1lGN2Vit12arXO66cSYy_0ANbDw';*/
	 $path_to_firebase_cm = 'https://fcm.googleapis.com/fcm/send';
                $msg = $msg;
                $title = $title;
                $token = $fcm;
                $sound="1";
                $fields = array(
                                'to' => $token,
                                'priority' => "high",
                                'content_available' => true,
                                'data' => array(                                	
                                	"message" => $msg,
                                	"title"   => $title,
                                	"sound"   => ($sound==1)? "":NULL,
                                	 )
                                );                                                 
                $headers = array('Authorization:key=AIzaSyDhbgs15zBMuagu8I_NFFzYHgKvhCNKP8A',
                                'Content-Type:application/json'
                                );
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $path_to_firebase_cm);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
               $result = curl_exec($ch);                 
                curl_close($ch);

}
function send_push_notification1($fcm,$msg,$title) {
        $fcm = "e-r14KxwD5I:APA91bHr-2Qjs4AlMPVjO3Gw5o2-TCWMD-Q07kwr5SBmzeylbTlx3MCIpQGOkh2ZA0FE-wqdH1Cw9q7xxxDa_rWI3A9Kln_bFhGqerO6Cg7YrRzambIMtqgwEt-zFt1HRwbPkY50gsmR" ;
        $url = "https://fcm.googleapis.com/fcm/send";
		$serverKey = 'AIzaSyDhbgs15zBMuagu8I_NFFzYHgKvhCNKP8A';
		$title = $title;
		$notification = array('title' =>$title , 'body' => $msg, 'sound' => 'default', 'badge' => '1');
		
		$arrayToSend = array('to' => $fcm, 'data' => $notification,'priority'=>'high');
		/*print_r($arrayToSend);*/
		$json = json_encode($arrayToSend);
		$headers = array();
		$headers[] = 'Content-Type: application/json';
		$headers[] = 'Authorization: key='. $serverKey;
		
		$ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);

       /* echo $result = curl_exec($ch);
        die;*/
        curl_exec($ch);
        curl_close($ch); 

}


function send_sms($to_num,$msg) {
	    /*$textLoop = $msg; 
		$surl = "http://api.msg91.com/api/sendhttp.php?sender=MSGIND&route=4&mobiles=" . '91' .$to_num. "&authkey=233686AIlNLHO6BPb5b81384a&encrypt=&country=0&message=";              
        $textloop = urlencode($textLoop);
        $parm = trim($textloop) . "&flash=&unicode=&afterminutes=&response=&campaign=";     
        print_r($surl.$parm);
        die;   
        $curl = curl_init();
            curl_setopt_array($curl, array(
            CURLOPT_URL => $surl . $parm,           
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        print_r($response);
        die;*/
        /*curl_close($curl);*/
                if(!empty($to_num))
                {       $msg = urlencode($msg) ;
                        $curl = curl_init();
                        curl_setopt_array($curl, array(
                                CURLOPT_URL => "http://api.msg91.com/api/v2/sendsms",
                                CURLOPT_RETURNTRANSFER => true,
                                CURLOPT_ENCODING => "",
                                CURLOPT_MAXREDIRS => 10,
                                CURLOPT_TIMEOUT => 30,
                                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                CURLOPT_CUSTOMREQUEST => "POST",
                                CURLOPT_POSTFIELDS => "{ \"sender\": \"MSGIND\", \"route\": \"4\", 
                                \"country\": \"91\", \"sms\": [ { \"message\": \"{$msg}\", \"to\": 
                                [ \"{$to_num}\"] } ] }",
                                CURLOPT_SSL_VERIFYHOST => 0,
                                CURLOPT_SSL_VERIFYPEER => 0, 
                                CURLOPT_HTTPHEADER => array(
                                        "authkey:239999AugU1eecVs5bae0e76",
                                        "content-type: application/json" 
                                ),
                        ));
                        $response = curl_exec($curl);
                        $err = curl_error($curl);
                        /*dd($response);*/
                        curl_close($curl);
                        /*if ($err) {
						  echo "cURL Error #:" . $err;
						} else {
						  echo $response;
						}*/

                }
        }


/*function send_sms($gcmID,$messagePush)
        {
                  $url = 'https://fcm.googleapis.com/fcm/send';
		          $fields = array(	
		            'registration_ids' => array($gcmID),
		            'data' => array(
		                "message" => $messagePush,
		                "image" => "
		                Zoom https---ibin.co-2t1lLdpfS06F.png
		                https://ibin.co/2t1lLdpfS06F.png",
		                "AnotherActivity" => "True"));
			        $fields = json_encode($fields);
			        $headers = array(
			            'Authorization: key='."AAAAqeNYSkk:APA91bG95Ns5K4nKENPH8c9fADUDaSJbFIFlUnh7j5k-PVsugsputMLrmgLr-ucsrMRDPUpblsKUVqvDCsY87BrsylUmjCerEupGIxoCKP2yQfcfcyPT-mRewkjCZaRqn6D-CH6rSBdq8IZ7KfZEjQC38E6MmADnyQ",
			            'Content-Type: application/json'
		        );
		        $ch = curl_init();
		        curl_setopt($ch, CURLOPT_URL, $url);
		        curl_setopt($ch, CURLOPT_POST, true);
		        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		        $result = curl_exec($ch);
		        echo $result;
		        curl_close($ch);
		        return;
        }*/

function storelivigroactivity($user,$desc) {
			if($user['id'] == 1)
				$user_type = 'Lab';
			else
				$user_type = 'Doctor';
			$activity_data['description'] = $desc;
			$activity_data['userType'] = $user_type;
			$activity_data['userId'] = $user['id'];
			$activity_data['route'] = \Request::fullUrl();
			$activity_data['ipAddress'] = \Request::ip();
			$activity_data['userAgent'] = \Request::userAgent();
			$activity_data['locale'] = \Request::header('accept-language');
			$activity_data['methodType'] = \Request::method();
			$activity_data['referer'] = \Request::header('referer');
			ActivityLogger::storeActivity($activity_data);


}


function send_mail($viewpath,$data){
     // start  mail template     
       Mail::send($viewpath,$data, function($message) use ($data)
      {
          $message->from($data['from'],'Welcome');
          $message->to($data['to']); 
          if(isset($data['subject']))                    
              $message->subject($data['subject']);
          if(isset($data['subject']))                    
              $message->subject($data['subject']);
      });
      //end mail template 
}

 ?>
