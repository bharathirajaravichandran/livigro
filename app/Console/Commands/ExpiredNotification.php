<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Subscription;
use App\Models\Labs;
use App\Models\Doctors;
use App\Models\Notification;
use Carbon\Carbon;
use Mail; 

class ExpiredNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ExpiredNotification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
          // dd("Hi");
      $subscription = Subscription::where('plan_end_date',   '=', Carbon::now()->addDays(4)->format('d F Y'))
                                  ->orWhere('plan_end_date', '=', Carbon::now()->addDays(3)->format('d F Y'))
                                  ->orWhere('plan_end_date', '=', Carbon::now()->addDays(2)->format('d F Y'))
                                  ->orWhere('plan_end_date', '=', Carbon::now()->addDays(1)->format('d F Y'))
                                  ->get();
      if(count($subscription)!=0){
        foreach ($subscription as $key => $value) {
            if($value->user_type ==1){
                $labs = Labs::find($value->user_id);
                // $expired_date = $value->plan_end_date->format('d/m/Y');
                if($labs){
                  //save notification start
                  $notification = new Notification();
                  $notification->type             = 'Expired';
                  $notification->lab_id           =  $value->user_id;
                  $notification->doctor_id        =  0;
                  $notification->patient_id       =  0;
                  $notification->raise_test_id    =  0;
                  $notification->read_status      =  0;
                  /*$notification->created_at       =  $value->plan_start_date;
                  $notification->updated_at       =  $value->plan_end_date;*/
                  $notification->save();  
                //save notification end

                 $title = "Livigro - Expired Subscription";
                 $msg = "Your livigro subscription is expire ({$value->plan_end_date})/Renewed  ({$value->plan_start_date} to {$value->plan_end_date}) ";
                 $labPhone = $labs->mobile;
                 if(substr($labPhone,0,1) == '+'){
                      send_sms(substr($labPhone,3), $msg);
                    }else{
                      send_sms($labPhone, $msg);
                    }
                  $labs_email = $labs->email;
                  $data['to'] = $labs_email; 
                  $data['subject'] = 'Subscription Expired';    
                  $data['expired_date'] = $value->plan_end_date;       
                  $data['start_date'] = $value->plan_start_date;       
                   // dd(substr($raise_test->getLab->mobile,3));
                   //Mail::to($patient_email)->send(new ReportToPatient($data));     
                  if($labs_email !=''){
                    Mail::send('email.subscription',$data, function($message) use ($data)
                        {
                            $message->to($data['to']); 
                            $message->subject($data['subject']);
                            $message->cc(config('mail.MailCcFunction'));
                        });

                  }                
              }
            }else  if($value->user_type ==2){
                $doctors = Doctors::find($value->user_id);
                if($doctors){
                  //save notification start
                  $notification = new Notification();
                  $notification->type             = 'Expired';
                  $notification->lab_id           =  0;
                  $notification->doctor_id        =  $value->user_id;
                  $notification->patient_id       =  0;
                  $notification->raise_test_id    =  0;
                  $notification->read_status      =  0;
                  /*$notification->created_at       =  $value->plan_start_date;
                  $notification->updated_at       =  $value->plan_end_date;*/
                  $notification->save(); 
                //save notification end

                 $title = "Livigro - Expired Subscription";
                 $msg = "Hi, Your Subscription has been Expired on {$value->plan_end_date}";
                 $labPhone = $doctors->mobile;
                 if(substr($labPhone,0,1) == '+'){
                      send_sms(substr($labPhone,3), $msg);
                    }else{
                      send_sms($labPhone, $msg);
                    }
                  $labs_email = $doctors->email;
                  $data['to'] = $labs_email; 
                  $data['subject'] = 'Subscription Expired';        
                  $data['expired_date'] = $value->plan_end_date;        
                   // dd(substr($raise_test->getLab->mobile,3));
                   //Mail::to($patient_email)->send(new ReportToPatient($data));     
                  if($labs_email !=''){
                    Mail::send('email.subscription',$data, function($message) use ($data)
                        {
                            $message->to($data['to']); 
                            $message->subject($data['subject']);
                            $message->cc(config('mail.MailCcFunction'));
                        });

                  }
                }                
            }
        }
      }
      echo 'success' ; exit;
    }
}
