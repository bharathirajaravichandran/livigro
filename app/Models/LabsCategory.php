<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\LabsTest;

class LabsCategory extends Model
{
    protected $table = 'lab_test_subcate';

    public function getTest(){
        return $this->belongsTo('App\Models\LabsTest','lab_test_id','id');
    } 
    
}
