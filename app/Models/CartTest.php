<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CartTest extends Model
{
    protected $table = 'cart_test';
    protected $guarded = [];
    public $timestamps = false;

}
