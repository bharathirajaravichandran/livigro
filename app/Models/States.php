<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class States extends Model
{
     protected $table = 'states_table';
     protected $guarded = [];
     public $timestamps = false;
}
