<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cities extends Model
{
     protected $table = 'cities_table';
     protected $guarded = [];
     public $timestamps = false;
}
