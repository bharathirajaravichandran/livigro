<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $table = 'notification';
    protected $guarded = [];
    public function getPatient() {
    	return $this->hasOne('App\Models\Patients','id','patient_id');
    }

    public function getDoctor() {
    	return $this->hasOne('App\Models\Doctors','id','doctor_id');
    }

    public function getLab() {
    	return $this->hasOne('App\Models\Labs','id','lab_id');
    }
}
