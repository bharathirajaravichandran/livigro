<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Receipt extends Model
{
    protected $table = 'receipt_table';
    
      protected $guarded = [];

    public $timestamps = false;

    public function testData() {
    	return $this->belongsTo('App\Models\LabsTest','tests_id');
    }
    public function subcatName() {    	
    	return $this->belongsTo('App\Models\LabsCategory','labtest_sub_name');
    }

    

}
