<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DocHospital extends Model
{
    protected $table = 'hospital_doctor';

    public $timestamps = false;

}
