<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Education extends Model
{
    protected $table = 'education_details';
     protected $guarded = [];

     public function getDegreeCertificateAttribute($degree_certificate)
    {
        if (is_string($degree_certificate)) {
            return json_decode($degree_certificate, true);
        }

        return $degree_certificate;
    }

    public function setDegreeCertificateAttribute($degree_certificate)
    {
        if (is_array($degree_certificate)) {
            $this->attributes['degree_certificate'] = json_encode($degree_certificate);
        }
       
    }

      public function getDoctor()
    {
        return $this->belongsTo('App\Models\Doctors','doctor_id','id');
    }  
    public function getDegreeAttribute($value)
    {
        return \App\Models\Degree::find($value)->name;
    }

    public function getCollegeAttribute($value)
    {
        return \App\Models\College::find($value)->name;
    }
}
