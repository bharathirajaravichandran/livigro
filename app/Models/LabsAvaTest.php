<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class LabsAvaTest extends Model
{
    
    public $timestamps = false;
    protected $table = 'labs_available_test';

    public function test() {
    	return $this->hasOne('App\Models\LabsTest','lab_test_id','id');
    } 
    public function getTestName() {
    	return $this->belongsTo('App\Models\LabsTest','lab_test_id','id');
    }
}
