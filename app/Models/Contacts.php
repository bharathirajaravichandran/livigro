<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contacts extends Model
{
    protected $table = 'contacts'; 
    protected $fillable = ['doctor_id', 'message','photo'];  
    
    public function getLab(){
    	return $this->belongsTo('App\Models\Labs','lab_id');
    } 
}


