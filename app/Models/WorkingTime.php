<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WorkingTime extends Model
{
    protected $table = 'working_time_table';
     protected $guarded = [];

    public $timestamps = false;

      public function openingTime() {
    	$date = \Carbon\Carbon::now();
    	$num = $date->dayOfWeek + 1;
    	$start_date = 'day'.$num.'_open_time';
    	$end_date = 'day'.$num.'_end_time';
    	$time['start_time']  = $this->$start_date;
    	$time['end_time'] = $this->$end_date;
    	return $time;
    }

}
