<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class LabsTest extends Model
{
    
    protected $table = 'lab_tests';

    public function path() {
    	return "/admin/tests/$this->id/show";
    }
     public function labs_subcate() 
    {
        return $this->hasMany('App\Models\LabsCategory','lab_test_id');
    }

    // public function lab_test_group() {
    // 	return $this->hasOne('App\Models\LabTest');
    // }

    public function test_group() {
    	return $this->belongsTo('App\Model\LabTestGroup','group_id');
    }

      public function getGroupIdAttribute($group_id)
    {
        if (is_string($group_id)) {
            return json_decode($group_id, true);
        }

        return $group_id;
    }
	
   public function labs_available_test() {

	return $this->hasMany('App\Models\LabsAvaTest','lab_tests_id','id');
	}
    public function setGroupIdAttribute($group_id)
    {
        if (is_array($group_id)) {
            $this->attributes['group_id'] = json_encode($group_id);
        }
       
    }
}
