<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
      protected $table = 'package';

    public $timestamps = false;

      public function getProfileIdAttribute($profile_id)
    {
        if (is_string($profile_id)) {
            return json_decode($profile_id, true);
        }

        return $profile_id;
    }

    public function setProfileIdAttribute($profile_id)
    {
        if (is_array($profile_id)) {
            $this->attributes['profile_id'] = json_encode($profile_id);
        }
       
    }
     public function getTestsIdAttribute($tests_id)
    {
        if (is_string($tests_id)) {
            return json_decode($tests_id, true);
        }

        return $tests_id;
    }

    public function setTestsIdAttribute($tests_id)
    {
        if (is_array($tests_id)) {
            $this->attributes['tests_id'] = json_encode($tests_id);
        }
       
    }

    // Neelamegam
    public function labs_available_test() {
    return $this->hasMany('App\Models\LabsAvaTest','lab_package_id','id');
    }

}
