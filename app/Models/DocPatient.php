<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DocPatient extends Model
{
    protected $guarded = [];
    protected $table = 'doctor_patient';
}
