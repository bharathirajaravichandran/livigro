<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Hospital extends Model
{
    
	public function path() {
		return "/admin/hospital/$this->id/show";
	}
    protected $table = 'hospitals';
    protected $guarded = [];

    public function doctors() {
    		return $this->belongsToMany('App\Models\Doctors','hospital_doctor','hospital_id','doctor_id');
    }
     // Neelamegam_HospitalsDetailsView_13/08/2018 
    public function getWheelChair() {
    	if($this->wheel_charir == 0)
    		return 'No';
    	elseif($this->wheel_charir == 1)
    		return 'Yes';
    	else
    		return '';
    } 
    public function getPharmacy() {
    	if($this->pharmacy == 0)
    		return 'No';
    	elseif($this->pharmacy == 1)
    		return 'Yes';
    	else
    		return '';
    }
    public function getHouseLab() {
    	if($this->in_house_lab == 0)
    		return 'No';
    	elseif($this->in_house_lab == 1)
    		return 'Yes';
    	else
    		return '';
    }
    public function getCarParking() {
    	if($this->car_parking == 0)
    		return 'No';
    	elseif($this->car_parking == 1)
    		return 'Yes';
    	else
    		return '';
    }
    public function getAmbulanceService() {
    	if($this->ambulance_service == 0)
    		return 'No';
    	elseif($this->ambulance_service == 1)
    		return 'Yes';
    	else
    		return '';
    }
}
