<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Coupans extends Model
{
    protected $table = 'coupans';
     protected $guarded = [];
     
       public function setLabsAttribute($labs)
    {
        if (is_array($labs)) {
            $this->attributes['labs'] = implode(',',$labs);
        }
       
    }
    
    
       public function setDoctorsAttribute($doctors)
    {
        if (is_array($doctors)) {
            $this->attributes['doctors'] = implode(',',$doctors);
        }
       
    }
    
   public function setPatientsAttribute($patients)
    {
        if (is_array($patients)) {
            $this->attributes['patients'] = implode(',',$patients);
        }
       
    }
 
    
 }
