<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class RaiseTest extends Model
{
    

    protected $table = 'raise_test';
    protected $guarded = [];

   public function path() {
     return "/admin/raisetest/$this->id/show";
   }

    public function getLab(){
    	return $this->belongsTo('App\Models\Labs','lab_id');
    }

    public function getPatient() {
    	return $this->belongsTo('App\Models\Patients','patients_id');
    }
    public function age() {
        if($this->attributes['dob'])
            return Carbon::parse($this->attributes['dob'])->age;
        else
            return null;
    }

    public function getStatus() {
        $paid_status = $this->paid_status ;
        if($paid_status == null){
                $result =  'Not Paid';
        }elseif($paid_status == 1){
                $result = 'Paid';
        }elseif($paid_status == 2){
                $result = 'Partialy Paid';
        }elseif($paid_status == 3){
                $result = 'Patient Was Not Reachable';
        }elseif($paid_status == 4){
                $result ='Patient Refused to Take the Test';
        }elseif($paid_status == 5){
                $result = 'lab do not offer the prescribed test';
        }else{
               $result = 'Not Paid';
        }          
        return $result;
    }

/*    public function getSpecimenCollectedAttribute() {
        if($this->attributes['specimen_collected'])
                return 'Yes';
        else
            return 'No';
    }*/

    public function getLabTest() {
    		return $this->belongsTo('App\Models\LabsTest','lab_tests_id');
    }

    public function getDoctor() {
        return $this->belongsTo('App\Models\Doctors','doctor_id');
    } 

   
    // public function whoRaised() {
    //     return $this->raised_by == '0' ? 'Doctor' : 'Lab';
    // }

     public function whoRaised() {
         return $this->raised_by == '1' ? 'Lab' : ($this->raised_by == '0' ? 'Doctor': 'Admin');
    }

    public function raised_test()  {
        return $this->hasMany('App\Models\RaiseTest','patients_id','id');
    }

    public function whoName() {
        return $this->raised_by == '0' ? $this->getDoctor->salutation. ' '. $this->getDoctor->name : $this->getLab->name;
    }

    public function getReciept() {
        return $this->hasMany('App\Models\Receipt', 'raise_test_id', 'id');
    }

    public function getTestStatus(){
        // dd($this->paid_status);
        $value = "";
        $test_status = $this->belongsTo('App\Models\Report','id','raise_test_id')->count();
        if($test_status>0){
            $value = "Completed";
        }else if($this->paid_status!=1 && $this->paid_status!=2 && $this->paid_status!=null){
            $value = "Cancel";
        }else{
            $value = "Pending";
        }
        return $value;
        // return $this->belongsTo('App\Models\Report','id','raise_test_id')->count() > 0 ? "Completed" : "Pending";
    }

    public function getReport() {
        return $this->belongsTo('App\Models\Report','id','raise_test_id');
    }

    public function paidStatus(){    
    $paid_status = $this->paid_status ;
        if($paid_status == null){
                $result =  'Not Paid';
        }elseif($paid_status == 1){
                $result = 'Paid';
        }elseif($paid_status == 2){
                $result = 'Partialy Paid';
        }elseif($paid_status == 3){
                $result = 'Patient Was Not Reachable';
        }elseif($paid_status == 4){
                $result ='Patient Refused to Take the Test';
        }elseif($paid_status == 5){
                $result = 'lab do not offer the prescribed test';
        }else{
               $result = 'Not Paid';
        }          
        return $result;
    }


    public function getLabTestsIdAttribute($lab_tests_id)
    {
        
        if (is_string($lab_tests_id)) {
            return json_decode($lab_tests_id, true);
        }

        return $lab_tests_id;
    }

    public function setLabTestsIdAttribute($lab_tests_id)
    {        
        if (is_array($lab_tests_id)) {
            $this->attributes['lab_tests_id'] = json_encode($lab_tests_id);
           return $this->attributes['lab_tests_id'];
        }
       
    }

}
