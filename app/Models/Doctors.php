<?php

namespace App\Models;
use Auth;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;


class Doctors extends Model
{
    

    protected $table = 'doctors_details';
    protected $guarded = ['phone'];

    public function getGender() {
    	if($this->gender == 0)
    		return 'Female';
    	elseif($this->gender == 1)
    		return 'Male';
    	else
    		return 'Non-Binary';

    }

    public function age() {
        if($this->attributes['dob']!=''){
            return Carbon::parse($this->attributes['dob'])->age;
        }
        else
            return null;
    }

  public function path() {
  	return "/admin/doctors/$this->id/show";
  }
    public function getDate() {
    	 return $this->dob->format('M d Y');
    	$date = \Carbon\Carbon::create($this->dob);
    	return $date->toFormattedDateString();
    }
    // Neelamegam_grid 30/07/2018

    public function getCityName(){
        return $this->belongsTo('App\Models\City','city','city_id');
    }

    public function getHospitals() {
        return $this->belongsToMany('App\Models\Hospital','hospital_doctor','doctor_id','hospital_id');
    }
        
    public function patients() {
    	return $this->belongsToMany('App\Models\Patients','doctor_patient','doctor_id','patient_id');
    }

    public function scopeCheck($query)
    {
         if(Auth::guard('admin')->user()->isRole('administrator')){
            return $query;
         }
         else
        return $query->where('id', Auth::guard('admin')->user()->linked_id);
    }

    public function thisWeekPatient() {
            $minDate = \Carbon\Carbon::now()->startOfWeek()->format('Y-m-d');
            $maxDate = \Carbon\Carbon::now()->endOfWeek()->format('Y-m-d');
        return $this->belongsToMany('App\Models\Patients','doctor_patient','doctor_id','patient_id')->whereDate('patients_details.created_at','>',$minDate)->whereDate('patients_details.created_at','<',$maxDate);
    }

    public function thisWeekLab(){
            $minDate = \Carbon\Carbon::now()->startOfWeek()->format('Y-m-d');
            $maxDate = \Carbon\Carbon::now()->endOfWeek()->format('Y-m-d');
            return $this->labs()->whereDate('labs_table.created_at','>',$minDate)->whereDate('labs_table.created_at','<',$maxDate);
    }

    public function hospitals(){
    	return $this->belongsToMany('App\Models\Hospital','hospital_doctor','doctor_id','hospital_id');
    }

   public function getCityAttribute() {
         if(is_numeric($this->attributes['city']))
         {
            $city = City::where('city_id',$this->attributes['city'])->first();
            if($city!="")
            return $city->city_name;
            else
            return $city ="";
        }
        else
            return $this->attributes['city'];
    }

    // public function getBloodGroup($id) {
    //     return Blood::find($id)->blood_group;
    // }

    public function getBloodGroupAttribute() {

        if(is_numeric($this->attributes['blood_group'])){
             if($this->attributes['blood_group']!=0){
                 return Blood::find($this->attributes['blood_group'])->blood_group;
            }else{
                return $this->attributes['blood_group'];
            }
        }else{
            return $this->attributes['blood_group'];
        }
    }

     

    public function labs(){
    	return $this->belongsToMany('App\Models\Labs','doctor_lab','doctor_id','lab_id');
    }
    
    
    // public function blood_group(){
    //     // dd($this->hasOne('App\Models\Blood','blood_group','blood_group'));
    // 	return $this->hasOne('App\Models\Blood','blood_group','id');
    // }
    

    //TODO need to get the all raised Test to which lab and which patient
    public function raised_test()  {
        return $this->hasMany('App\Models\RaiseTest','doctor_id','id')->where('raised_by',0);
    }

    //  public function getEducationalCertificateAttribute($educational_certificate)
    // {
    //     if (is_string($educational_certificate)) {
    //         return json_decode($educational_certificate, true);
    //     }

    //     return $educational_certificate;
    // }

    // public function setEducationalCertificateAttribute($educational_certificate)
    // {
    //     if (is_array($educational_certificate)) {
    //         $this->attributes['educational_certificate'] = json_encode($educational_certificate);
    //     }
       
    // }

     public function getAadharImageAttribute($aadhar_image)
    {
        if (is_string($aadhar_image)) {
            return json_decode($aadhar_image, true);
        }

        return $aadhar_image;
    }

    public function setAadharImageAttribute($aadhar_image)
    {
        if (is_array($aadhar_image)) {
            $this->attributes['aadhar_image'] = json_encode($aadhar_image);
        }
       
    }

    public function education() {
        return $this->hasMany('App\Models\Education','doctor_id');
    }


   
}
