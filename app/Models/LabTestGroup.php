<?php

namespace App\Models;

use App\Models\LabsTest;
use Illuminate\Database\Eloquent\Model;

class LabTestGroup extends Model
{
    protected $table = 'lab_test_group';

    public function tests() {

    	return $this->hasMany('App\Models\LabsTest','group_id','id');
    }

  

       public function getTestsIdAttribute($test_id)
    {
        if (is_string($test_id)) {
            return json_decode($test_id, true);
        }

        return $test_id;
    }

    public function setTestsIdAttribute($test_id)
    {
        if (is_array($test_id)) {
            $this->attributes['tests_id'] = json_encode($test_id);
        }
       
    }
     public function getTest(){
        return $this->belongsTo('App\Models\LabsTest','tests_id','id');
    }
}
