<?php

namespace App\Models;
use App\Models\Doctors;
use App\Models\Blood;
use App\Models\City;
use Illuminate\Database\Eloquent\Model;
use Auth;
class Labs extends Model
{
    protected $table = 'labs_table';

    public function getHome() {
    	if($this->home_collection == '0')
    		return 'No';
    	else
    		return 'Yes';
    }
    public function path() {               
        if($this->id) {  
           $path = "/admin/labs/$this->id/show";        
           return $path ;
        }else{
          $path = "/admin/labs/"; 
          return $path ; 
        }

    }

    public function getCityAttribute() {
        // dd($this->attributes['city']);
         if(is_numeric($this->attributes['city']))
            return  City::where('city_id',$this->attributes['city'])->first()->city_name;
        else
            return $this->attributes['city'];
    }

    public function getBloodGroupAttribute() {
        if(is_numeric($this->attributes['blood_group']))
             return Blood::find($this->attributes['blood_group'])->blood_group;
         else
            return $this->attributes['blood_group'];
    }

    public function openingTime() {
    	$date = \Carbon\Carbon::now();
    	$num = $date->dayOfWeek + 1;
    	$start_date = 'day'.$num.'_open_time';
    	$end_date = 'day'.$num.'_end_time';
    	$time['start_time']  = $this->$start_date;
    	$time['end_time'] = $this->$end_date;
    	return $time;
    }

    public function workingTime() {
        return $this->hasOne('App\Models\WorkingTime','lab_id');
    }

    public function scopeCheck($query)
    {
         if(Auth::guard('admin')->user()->isRole('administrator')){
            return $query;
         }
         else
        return $query->where('id', Auth::guard('admin')->user()->linked_id);
    }

    public function doctors(){
        return $this->belongsToMany('App\Models\Doctors','doctor_lab','lab_id','doctor_id');
    }

   public function patients() {
        return $this->belongsToMany('App\Models\Patients','doctor_patient','lab_id','patient_id');
    }
    // Neelamegam_grid 30/07/2018

    public function getCityName(){
        return $this->belongsTo('App\Models\City','city','city_id');
    }
/*
    public function patients($serach = null) {
        $patient_data = [];
            foreach($this->doctors()->get() as $doctors) {
                $patients = $doctors->patients()->where(function ($query) use($serach){
                                                $query->where('phone','like',"%{$serach}%")
                                                      ->orWhere('name','like',"%{$serach}%");
                                                 })->get();
                foreach($patients as $patient){
                    $skip = false;
                    foreach($patient_data as $original){
                       // dd($original);
                        if($original['id'] == $patient->id)
                            $skip = true;
                    }
                    if($skip == false)
                     $patient_data[] = $patient->toArray();
                }
            }
            return $patient_data;
    }
    
  */  
      public function patient() {
    	return $this->belongsToMany('App\Models\Patients', 'doctor_patient','patient_id','doctor_id');
    }
    
    
     public function labs(){
    	return $this->belongsToMany('App\Models\Labs','doctor_lab','doctor_id','lab_id');
    }
    //  public function blood_group(){
    //     return $this->belongsToMany('App\Models\Blood','doctor_lab','doctor_id','lab_id');
    // }


    public function total_raised_test() {
         return $this->hasMany('App\Models\RaiseTest','lab_id','id');
    }

    public function raised_test(){
        return $this->total_raised_test()->where('raised_by',1);
    }

    public function test_assigned(){
        return $this->total_raised_test()->where('raised_by',0)->get();
    }

     public function active_raised(){
        return $this->total_raised_test()->where('other_reason_for_cancel',null)->get();
    }
    public function paid_status(){
        return $this->total_raised_test()->where('paid_status',1)->get();
    }
    public function pending_test(){
        return $this->total_raised_test()->where('paid_status',null)->get();
    }
    public function cancel_test(){
        return $this->total_raised_test()->where('paid_status','!=',null)->where('paid_status','!=',1)->get();
    }
     public function getTestStatus(){
        $ids = $this->total_raised_test()->where('paid_status',1)->pluck('id');
        $value = array();
        foreach ($ids as $id) {
            $value[] = Report::where('raise_test_id',$id)->get();
        }
       return count($value);
        // return $this->belongsTo('App\Models\Report','raise_test_id','id');
    }

    // Neelamegam_TestsListDetails_13/08/2018

    public function test_assigneds(){
         return $this->hasMany('App\Models\RaiseTest','lab_id','id');
    }

    // Neelamegam
     public function package(){
        return $this->belongsToMany('App\Models\Package','labs_available_test','lab_id','lab_package_id')
                    ->wherePivot('test_type','!=','single'); 
    }
   
    public function tests(){
        return $this->belongsToMany('App\Models\LabsTest','labs_available_test','lab_id','lab_tests_id')->wherePivot('test_type','single'); 
    }

  /*  public function getTestsAttribute()
    {        
           return $this->attributes['test_type'] = 'single';       
    }
     public function getPackageAttribute()
    {        
           return $this->attributes['test_type'] = '';       
    }
*/
    // public function test(){
    //     return $this->belongsToMany('App\Models\LabsTest','labs_available_test','lab_tests_id','lab_id');
    // }

    public function getSignatureAttribute($signature)
    {
        if (is_string($signature)) {
            return json_decode($signature, true);
        }

        return $signature;
    }

    public function setSignatureAttribute($signature)
    {
        if (is_array($signature)) {
            $this->attributes['signature'] = json_encode($signature);
        }
       
    }
     public function getGstCertificateImageAttribute($gst_certificate_image)
    {
        if (is_string($gst_certificate_image)) {
            return json_decode($gst_certificate_image, true);
        }

        return $gst_certificate_image;
    }

    public function setGstCertificateImageAttribute($gst_certificate_image)
    {
        if (is_array($gst_certificate_image)) {
            $this->attributes['gst_certificate_image'] = json_encode($gst_certificate_image);
        }
       
    }

     public function getNablCertificateAttribute($nabl_certificate)
    {
        if (is_string($nabl_certificate)) {
            return json_decode($nabl_certificate, true);
        }

        return $nabl_certificate;
    }

    public function setNablCertificateAttribute($nabl_certificate)
    {
        if (is_array($nabl_certificate)) {
            $this->attributes['nabl_certificate'] = json_encode($nabl_certificate);
        }
       
    }

    public function getAadharImageAttribute($aadhar_image)
    {
        if (is_string($aadhar_image)) {
            return json_decode($aadhar_image, true);
        }

        return $aadhar_image;
    }

    public function setAadharImageAttribute($aadhar_image)
    {
        if (is_array($aadhar_image)) {
            $this->attributes['aadhar_image'] = json_encode($aadhar_image);
        }
       
    }
    // Neelamegam_LabsListView_13/08/2018
     public function getHomeCollection() {
        if($this->home_collection == 0)
            return 'No';
        elseif($this->home_collection == 1)
            return 'Yes';
        else
            return '';
    }
    public function getGstRegister() {
        if($this->gst_registered == 0)
            return 'No';
        elseif($this->gst_registered == 1)
            return 'Yes';
        else
            return '';
    }

    



}
