<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Blood extends Model
{
    protected $table = 'blood_group';

    public $timestamps = false;

}
