<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DocLab extends Model
{
    protected $table = 'doctor_lab';
    
      protected $guarded = [];

    public $timestamps = false;

    public function getLab()
    {    	
        return $this->belongsTo('App\Models\Labs','lab_id');
    }

}
