<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Countries extends Model
{
     protected $table = 'countries_table';
     protected $guarded = [];
     public $timestamps = false;
}
