<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Blood;
use Auth;
use Carbon\Carbon;
use App\Models\City;
class Patients extends Model
{
    protected $table = 'patients_details';
    protected $guarded = [];


    public function path(){
    	return "/admin/patient/$this->id/show";
    }


    public function age() {
        if($this->attributes['dob']!=''){
            return Carbon::parse($this->attributes['dob'])->age;
        }
        else
            return null;
    }

    // public function getBloodGroup($id) {
    //     return Blood::find($id)->blood_group;
    // }

 public function getCityAttribute() {
         if(is_numeric($this->attributes['city']))
            return  City::where('city_id',$this->attributes['city'])->first()->city_name;
        else
            return $this->attributes['city'];
    }

    public function getBloodGroupAttribute() {
        if(is_numeric($this->attributes['blood_group'])){
        if($this->attributes['blood_group']!=0){
         $blood = Blood::where('id',$this->attributes['blood_group'])->first();
         if ($blood!="")
        return $blood->blood_group;
         else
            return $blood="";
        }else{
             return $this->attributes['blood_group'];
        }
         }else
            return $this->attributes['blood_group'];
    }

    public function getGender() {
    	if($this->gender == 0)
    		return 'Female';
    	elseif($this->gender == 1)
    		return 'Male';
    	else
    		return 'Non-Binary';

    }
    // Neelamegam_grid 30/07/2018

    public function getCityName(){
        return $this->belongsTo('App\Models\City','city','city_id');
    }
   public function scopeCheck($query)
    {
         if(Auth::guard('admin')->user()->isRole('administrator')){
            return $query;
         }
         else
        return $query->where('id', Auth::guard('admin')->user()->linked_id);
    }
    public function doctors(){
        return $this->belongsToMany('App\Models\Doctors','doctor_patient','patient_id','doctor_id');
    }
    public function test_assigned(){
        return $this->hasMany('App\Models\RaiseTest','patients_id','id');
    }
    public function active_raised(){
        return $this->test_assigned()->where('other_reason_for_cancel',null);
    }

    public function total_raised_test() {
         return $this->hasMany('App\Models\RaiseTest','patients_id','id');
    }

     public function raised_test(){
        return $this->total_raised_test()->where('raised_by',1);
    }
     public function test_assigneds(){
         return $this->hasMany('App\Models\RaiseTest','patients_id','id');
    }

    // public function getGridPatients($gridValue){
    //     if(Auth::guard('admin')->user()->id != 1){
    //         // dd('If');
    //         $values = Patients::where('status',1)->get()->paginate(10);
    //     }
    //     else
    //     {
    //         // dd('else');
    //         $values = $gridValue;
    //     }
    //     return $values;
    // }


}
