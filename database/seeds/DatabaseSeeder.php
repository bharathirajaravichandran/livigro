<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Crockett\CsvSeeder\CsvSeeder;
class DatabaseSeeder extends CsvSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	 $this->call([\Encore\Admin\Auth\Database\AdminTablesSeeder::class]);

         $this->call([
         AdminRole::class,
         UserType::class,
         AdminMenu::class,
         AdminRoleMenu::class,
            ]);
        Eloquent::unguard();
         $this->seedFromCSV(base_path('app/country.csv'), 'country');
         $this->seedFromCSV(base_path('app/statelist.csv'), 'statelist');

        
    }
}
class AdminRole extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admin_roles')->insert([
            'name' => 'Doctor',
            'slug' => 'doctor',
        ]);
        DB::table('admin_roles')->insert([
            'name' => 'Lab',
            'slug' => 'lab',
        ]);
        DB::table('admin_roles')->insert([
            'name' => 'Patient',
            'slug' => 'patient',
        ]);
    }
}
class UserType extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_type')->insert([
            'type_name' => 'Administrator',
        ]);
          DB::table('user_type')->insert([
            'type_name' => 'Doctor',
        ]);
          DB::table('user_type')->insert([
            'type_name' => 'Lab',
        ]);  DB::table('user_type')->insert([
            'type_name' => 'patient',
        ]);

        DB::table('fc_WSAuth')->insert([
            'Secret' => '37834234',
            'Password' => '32473892490',
            'enable_log' => 0
        ]);
    }
}
class AdminMenu extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admin_menu')->insert([
            'parent_id' => '0',
            'order' => '0',
            'title' => 'Doctors',
            'icon' => 'fa-user-md',
            'uri' => '/doctors',
        ]);
        DB::table('admin_menu')->insert([
            'parent_id' => '0',
            'order' => '0',
            'title' => 'Patients',
            'icon' => 'fa-heartbeat',
            'uri' => 'patients',
        ]);
        DB::table('admin_menu')->insert([
            'parent_id' => '0',
            'order' => '0',
            'title' => 'Labs',
            'icon' => 'fa-flask',
            'uri' => 'labs',
        ]);
        DB::table('admin_menu')->insert([
            'parent_id' => '0',
            'order' => '0',
            'title' => 'Scaffold',
            'icon' => 'fa-keyboard-o',
            'uri' => 'helpers/scaffold',
        ]);
        DB::table('admin_menu')->insert([
            'parent_id' => '0',
            'order' => '0',
            'title' => 'Hospital',
            'icon' => 'fa-hospital-o',
            'uri' => '/hospitals',
        ]);
        DB::table('admin_menu')->insert([
            'parent_id' => '0',
            'order' => '0',
            'title' => 'LabTests',
            'icon' => 'fa-eyedropper',
            'uri' => 'labtest',
        ]);
        DB::table('admin_menu')->insert([
            'parent_id' => '0',
            'order' => '0',
            'title' => 'DropDowns',
            'icon' => 'fa-bars',
            'uri' => NULL,
        ]);

        DB::table('admin_menu')->insert([
            'parent_id' => '14',
            'order' => '0',
            'title' => 'UserType',
            'icon' => 'fa-user-secret',
            'uri' => 'usertype',
        ]);
          DB::table('admin_menu')->insert([
            'parent_id' => '0',
            'order' => '0',
            'title' => 'Web Service',
            'icon' => 'fa-chrome',
            'uri' => '/webservice',
        ]);
       /*   DB::table('admin_menu')->insert([
            'parent_id' => '14',
            'order' => '0',
            'title' => 'Raise Test',
            'icon' => 'fa-exchange',
            'uri' => 'raisetest',
        ]);*/
          DB::table('admin_menu')->insert([
            'parent_id' => '14',
            'order' => '0',
            'title' => 'Blood Group',
            'icon' => 'fa-tint',
            'uri' => '/blood',
        ]);
          DB::table('admin_menu')->insert([
            'parent_id' => '14',
            'order' => '0',
            'title' => 'Speciality',
            'icon' => '	fa-ambulance',
            'uri' => 'speciality',
        ]);
          DB::table('admin_menu')->insert([
            'parent_id' => '14',
            'order' => '0',
            'title' => 'Education',
            'icon' => 'fa-book',
            'uri' => 'education',
        ]);
          DB::table('admin_menu')->insert([
            'parent_id' => '14',
            'order' => '0',
            'title' => 'Degree',
            'icon' => 'fa-certificate',
            'uri' => '/degree	',
        ]);
          DB::table('admin_menu')->insert([
            'parent_id' => '14',
            'order' => '0',
            'title' => 'College',
            'icon' => 'fa-balance-scale	',
            'uri' => '/college	',
        ]);
           DB::table('admin_menu')->insert([
            'parent_id' => '0',
            'order' => '0',
            'title' => 'Coupons',
            'icon' => 'fa-bell-o',
            'uri' => 'coupans',
        ]);
           DB::table('admin_menu')->insert([
            'parent_id' => '0',
            'order' => '0',
            'title' => 'Logger',
            'icon' => 'fa-bars',
            'uri' => 'activity',
        ]);
             DB::table('admin_menu')->insert([
            'parent_id' => '0',
            'order' => '0',
            'title' => 'Reports',
            'icon' => 'fa-bank',
            'uri' => 'NULL',
        ]);


             DB::table('admin_menu')->insert([
            'parent_id' => '25',
            'order' => '0',
            'title' => 'Doctor Report',
            'icon' => 'fa-briefcase',
            'uri' => 'doctor_report',
        ]);
              DB::table('admin_menu')->insert([
            'parent_id' => '25',
            'order' => '0',
            'title' => 'Lab Report',
            'icon' => ' fa-eyedropper',
            'uri' => 'lab_report',
        ]);

            DB::table('admin_menu')->insert([
            'parent_id' => '25',
            'order' => '0',
            'title' => 'Patient Report',
            'icon' => 'fa-gittip',
            'uri' => 'patient_report',
        ]);
	DB::table('admin_menu')->insert([
            'parent_id' => '0',
            'order' => '28',
            'title' => 'Raise Test',
            'icon' => 'fa-exchange',
            'uri' => 'raisetest',
        ]);
	DB::table('admin_menu')->insert([
            'parent_id' => '0',
            'order' => '29',
            'title' => 'LabTestGroup',
            'icon' => 'fa-anchor',
            'uri' => 'labtestgroup',
        ]);
	DB::table('admin_menu')->insert([
            'parent_id' => '0',
            'order' => '30',
            'title' => 'Specimen',
            'icon' => '	fa-apple',
            'uri' => 'samplespecimens',
        ]);

    }
}
class AdminRoleMenu extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admin_role_menu')->insert([
            'role_id' => '1',
            'menu_id' => '8',
        ]);
        DB::table('admin_role_menu')->insert([
            'role_id' => '1',
            'menu_id' => '9',
        ]);
        DB::table('admin_role_menu')->insert([
            'role_id' => '1',
            'menu_id' => '10',
        ]);
        DB::table('admin_role_menu')->insert([
            'role_id' => '1',
            'menu_id' => '11',
        ]);
        DB::table('admin_role_menu')->insert([
            'role_id' => '1',
            'menu_id' => '12',
        ]);
        DB::table('admin_role_menu')->insert([
            'role_id' => '1',
            'menu_id' => '13',
        ]);
        DB::table('admin_role_menu')->insert([
            'role_id' => '1',
            'menu_id' => '14',
        ]);
        DB::table('admin_role_menu')->insert([
            'role_id' => '1',
            'menu_id' => '15',
        ]);
        DB::table('admin_role_menu')->insert([
            'role_id' => '1',
            'menu_id' => '16',
        ]);
        DB::table('admin_role_menu')->insert([
            'role_id' => '1',
            'menu_id' => '17',
        ]);
    DB::table('admin_role_menu')->insert([
            'role_id' => '1',
            'menu_id' => '18',
        ]);
        DB::table('admin_role_menu')->insert([
            'role_id' => '1',
            'menu_id' => '19',
        ]);
        DB::table('admin_role_menu')->insert([
            'role_id' => '1',
            'menu_id' => '20',
        ]);
         DB::table('admin_role_menu')->insert([
            'role_id' => '1',
            'menu_id' => '21',
        ]);

         DB::table('admin_role_menu')->insert([
            'role_id' => '1',
            'menu_id' => '22',
        ]);
        DB::table('admin_role_menu')->insert([
            'role_id' => '1',
            'menu_id' => '23',
        ]);
         DB::table('admin_role_menu')->insert([
            'role_id' => '1',
            'menu_id' => '24',
        ]);
    }
}
