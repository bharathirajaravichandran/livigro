<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReceiptTableTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receipt_table', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('raise_test_id')->nullable();
            $table->integer('tests_id')->nullable();
            $table->string('labtest_sub_name')->nullable();
            $table->string('amount')->nullable();
            $table->string('reference_range')->nullable();
            $table->string('reference_endrange')->nullable();
            $table->string('result')->nullable();
            $table->string('units')->nullable();
            $table->string('specimen_collected')->nullable();
            $table->timestamps();
            $table->string('test_type')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('receipt_table');
    }
}
