<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkingTimeTableTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('working_time_table', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lab_id')->nullable();
            $table->integer('doctor_id')->nullable();
            $table->time('day_1_open_time')->default('00:00:00')->nullable();
            $table->time('day_1_end_time')->default('00:00:00')->nullable();
            $table->time('day_2_open_time')->default('00:00:00')->nullable();
            $table->time('day_2_end_time')->default('00:00:00')->nullable();
            $table->time('day_3_open_time')->default('00:00:00')->nullable();
            $table->time('day_3_end_time')->default('00:00:00')->nullable();
            $table->time('day_4_open_time')->default('00:00:00')->nullable();
            $table->time('day_4_end_time')->default('00:00:00')->nullable();
            $table->time('day_5_open_time')->default('00:00:00')->nullable();
            $table->time('day_5_end_time')->default('00:00:00')->nullable();
            $table->time('day_6_open_time')->default('00:00:00')->nullable();
            $table->time('day_6_end_time')->default('00:00:00')->nullable();
            $table->time('day_7_open_time')->default('00:00:00')->nullable();
            $table->time('day_7_end_time')->default('00:00:00')->nullable();
            $table->integer('day_1_vacation')->comment('1-yes,0-no')->nullable();
            $table->integer('day_2_vacation')->comment('1-yes,0-no')->nullable();
            $table->integer('day_3_vacation')->comment('1-yes,0-no')->nullable();
            $table->integer('day_4_vacation')->comment('1-yes,0-no')->nullable();
            $table->integer('day_5_vacation')->comment('1-yes,0-no')->nullable();
            $table->integer('day_6_vacation')->comment('1-yes,0-no')->nullable();
            $table->integer('day_7_vacation')->comment('1-yes,0-no')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('working_time_table');
    }
}
