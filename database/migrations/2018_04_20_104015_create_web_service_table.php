<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebServiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('web_service', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name');
            $table->text('description')->nullable();
            // $table->string('name');
            // $table->string('description')->nullable();Neelamegam_pgadmin_DB_20082018
            $table->text('parameter');
            $table->text('success_result');
            $table->text('failure_result');
            $table->integer('status')->comment('0-inactive,1-active,2-deleted');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('web_service');
    }
}
