<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiteSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_setting', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('logo');
            $table->string('mobile');
            $table->string('phone');
            $table->string('email');
            $table->text('address');
            $table->string('footer');
            $table->timestamps();
            $table->text('footer_logo');
            // $table->integer('status')->comment('0-inactive,1-active,2-deleted');
            // $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site_setting');
    }
}
