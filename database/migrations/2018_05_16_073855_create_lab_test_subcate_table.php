<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLabTestSubcateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lab_test_subcate', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lab_test_id');
            $table->string('name');            
            $table->timestamps();
            $table->integer('status')->comment('0-inactive,1-active');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lab_test_subcate');
    }
}
