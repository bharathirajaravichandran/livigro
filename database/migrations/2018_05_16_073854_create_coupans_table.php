<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoupansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('area')->nullable();
            $table->string('labs')->nullable();
            $table->string('doctors')->nullable();
            $table->string('patients')->nullable();
            $table->string('amount')->nullable();
            $table->string('expiry_date')->nullable();
            $table->timestamps();
            $table->integer('status')->comment('0-inactive,1-active');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupans');
    }
}
