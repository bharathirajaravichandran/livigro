<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRaiseTestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('raise_test', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('doctor_id')->nullable();
            $table->integer('lab_id')->nullable();
            $table->integer('patients_id')->nullable();
            $table->string('lab_tests_id')->nullable();
            // $table->integer('lab_tests_id'); Neelamegam_pgadmin_DB_20082018
            $table->dateTime('test_on_date_time')->nullable();
            $table->string('original_amount')->nullable();
            $table->string('paid_amount')->nullable();
            $table->integer('paid_status')->comment('0-not paid ,1 -paid,2-partially paid,3-patient was not reachable,4-patient refused to take the test 5-lab do not offer the prescribed test')->nullable();
            $table->text('other_reason_for_cancel')->nullable();
            $table->integer('raised_by')->comment('0-doctor,1-lab')->nullable();
            $table->integer('specimen_collected')->comment('0-No,1-yes')->nullable();
            $table->string('received_amount')->nullable();
            $table->string('remaining_amount')->nullable();
            $table->string('doctor_referer_name')->nullable()->comment('refer to doctor table')->nullable();
            $table->string('diagonsis')->nullable();
            $table->string('notes')->nullable();
            $table->string('lab_tests_image')->nullable();
            $table->string('mode_of_payment')->nullable();
            $table->timestamps();
            $table->string('discount_price')->nullable();
            $table->integer('status')->comment('0-inactive,1-active,2-deleted')->nullable();
            $table->integer('doctor_referer_id')->nullable();
         
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('raise_test');
    }
}
