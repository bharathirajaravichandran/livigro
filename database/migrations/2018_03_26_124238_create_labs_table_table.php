    <?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLabsTableTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('labs_table', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('address')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('lat')->nullable();
            $table->string('lan')->nullable();
            $table->string('zip')->nullable();
            $table->string('promoters_name')->nullable();
            $table->text('profile_image')->nullable();
            $table->text('description')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('mobile')->nullable();
            $table->string('aadhar_id')->nullable();
            $table->text('aadhar_image')->nullable();
            $table->string('password')->nullable();
            $table->integer('home_collection')->comment('1-yes,0-no')->nullable();
            $table->integer('gst_registered')->comment('1-yes,0-no')->nullable();
            $table->string('gst_number')->nullable();
            $table->text('gst_certificate_image')->comment('image')->nullable();
            $table->string('nabl_certified')->comment('dropdown value')->nullable();
            $table->string('nabl_id')->nullable();
            $table->text("nabl_certificate")->comment('image')->nullable();
            $table->text('signature')->comment('image')->nullable();
            $table->integer('status')->comment('o-inactive,1-active')->nullable();
            $table->timestamps();
            $table->string('area')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('labs_table');
    }
}
