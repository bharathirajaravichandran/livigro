<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLabsAvailableTestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('labs_available_test', function (Blueprint $table) {
             $table->increments('id')->unsigned();
             $table->integer('lab_id')->unsigned();
            $table->integer('lab_tests_id')->unsigned();
            $table->float('amount', 8, 2)->nullable();
            $table->string('range')->nullable();
            $table->string('range_end')->nullable();
            $table->string('specimen')->nullable();
            $table->integer('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('labs_available_test');
    }
}
