    <?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientsDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patients_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('phone')->nullable();
            $table->string('mobile')->nullable();
            $table->text('address')->nullable();
            $table->text('password')->nullable();
            $table->integer('city')->nullable();
            $table->string('state')->nullable();
            $table->string('lat')->nullable();
            $table->string('lan')->nullable();
            $table->string('zip')->nullable();
            $table->string('email')->nullable();
	        $table->string('reference_doctor_id')->nullable();
            $table->integer('gender')->comment('0-female,1-male,2-non binary')->nullable();
            $table->date('dob')->nullable();
            $table->integer('blood_group')->nullable();
            $table->string('height')->nullable();
            $table->string('weight')->nullable();
            $table->string('profile_image')->nullable();
            $table->text('before_statement')->nullable();
            $table->text('area')->nullable();
            $table->integer('status')->comment('1-active,0-inactive,2-deleted')->nullable();
            $table->timestamps();
            $table->integer('created_by')->comment('linked to user table')->nullable();
            $table->integer('updated_by')->comment('linked to user table')->nullable();
            $table->string('reference_doctor_name   ')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patients_details');
    }
}
