<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHospitalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hospitals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->text('address')->nullable();
            $table->string('zipcode')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('lat')->nullable();
            $table->string('lan')->nullable();
            $table->integer('24_7_emerg_ward')->nullable();
            $table->string('clinic_reg_no')->nullable();
            $table->integer('wheel_charir')->nullable();
            $table->integer('in_house_lab')->nullable();
            $table->integer('pharmacy')->nullable();
            $table->integer('car_parking')->nullable();
            $table->integer('ambulance_service')->nullable();
            $table->integer('patient_beds')->nullable();
            $table->timestamps();
            $table->integer('status')->comment('0-inactive,1-active,2-deleted');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hospitals');
    }
}
