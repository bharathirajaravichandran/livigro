<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLabTestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lab_tests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->text('description')->nullable();
            $table->text('reference_range')->nullable();
            $table->text('reference_endrange')->nullable();
            $table->string('price')->nullable();
            $table->string('equipment1')->nullable();
            $table->string('equipment2')->nullable();
            $table->string('equipment3')->nullable();
            $table->string('equipment4')->nullable();
            $table->string('equipment5')->nullable();
            $table->string('equipment6')->nullable();
            $table->string('equipment7')->nullable();
            $table->string('equipment8')->nullable();
            $table->string('equipment9')->nullable();
            $table->string('equipment10')->nullable();
            $table->string('estimated_price')->nullable();
            $table->string('measurement')->nullable();
            $table->string('default_value1')->nullable();
            $table->string('default_value2')->nullable();
            $table->string('default_value3')->nullable();
            $table->text('search_tags')->nullable();
            $table->string('speciman')->nullable();            
            $table->timestamps();
            $table->text('group_id')->nullable();
            $table->integer('status')->comment('0-inactive,1-active,2-deleted');
            $table->integer('price_edit_status')->comment('0-Non-Editable,1-Editable);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lab_tests');
    }
}
