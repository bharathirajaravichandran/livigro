<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDoctorsDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctors_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('salutation')->nullable();
            $table->string('name')->nullable();
            $table->string('phone')->nullable();
            $table->string('mobile')->nullable();
            $table->string('email')->nullable();
            $table->string('dob')->nullable();
            $table->string('password')->nullable();
            $table->string('lat')->nullable();
            $table->string('lng')->nullable();
            $table->integer('city')->nullable();
            $table->string('state')->nullable();
            $table->string('zip')->nullable();
            $table->string('mcid')->nullable();
            $table->integer('gender')->comment('0-female,1-male.2-non-binary')->nullable();
            $table->integer('blood_group')->nullable();
            $table->string('register_council')->nullable(); 
            $table->text('registered')->comment('multimage')->nullable();
            $table->integer('specialization')->nullable();
            $table->string('yearofexperience')->nullable();
            $table->string('register_year')->nullable();
          //  $table->text('educational_qualification')->nullable();
          //  $table->text('educational_certificate')->comment('multiimage');
            // $table->string('availability_day')->nullable();
            // $table->string('availability_timing')->nullable();
          //  $table->text('certificate_course')->comment('mulitiimage');
            $table->text('signature')->comment('image field')->nullable();
            $table->text('address')->nullable();
            $table->text('profile_image')->nullable();
            $table->text('aadhar_no')->comment('may be image field')->nullable();
            $table->text('aadhar_image')->comment('multiimage')->nullable();
            $table->integer('status')->comment('0-inactive,1-active,2-deleted');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doctors_details');
    }
}
