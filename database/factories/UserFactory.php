<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => str_random(10),
     ];
});

$factory->define(App\Models\LabsTest::class,function(Faker $faker) {
	return [
		'name' => $faker->name,
		'description' => $faker->text,
		'price' => $faker->amPm,
		'estimated_price' => $faker->amPm,
		'measurement' => $faker->text,
		'reference_range' => $faker->text,
		'default_value1' => $faker->text,
		'default_value2' => $faker->text,
		'default_value3' => $faker->text,
		'search_tags' => $faker->word,
		'created_at' => $faker->dateTime,
		'updated_at' => $faker->dateTime,
 	];
});

$factory->define(App\Models\Hospital::class,function(Faker $faker) {
	return	[
		'name' => $faker->name,
		'address' => $faker->address,
		'zipcode' => $faker->postcode,
		'city'    => $faker->city,
		'state'   => $faker->country,
		'lat'  => $faker->latitude,
		'lan' => $faker->longitude,
		'day1_start_time' => $faker->time('H:i:s','now'),
		'day2_start_time' => $faker->time('H:i:s','now'),
		'day3_start_time' => $faker->time('H:i:s','now'),
		'day4_start_time' => $faker->time('H:i:s','now'),
		'day5_start_time' => $faker->time('H:i:s','now'),
		'day6_start_time' => $faker->time('H:i:s','now'),
		'day7_start_time' => $faker->time('H:i:s','now'),
		'day1_end_time' => $faker->time('H:i:s','now'),
		'day2_end_time' => $faker->time('H:i:s','now'),
		'day3_end_time' => $faker->time('H:i:s','now'),
		'day4_end_time' => $faker->time('H:i:s','now'),
		'day5_end_time' => $faker->time('H:i:s','now'),
		'day6_end_time' => $faker->time('H:i:s','now'),
		'day7_end_time' => $faker->time('H:i:s','now'),
		'created_at' => $faker->dateTime,
		'updated_at' => $faker->dateTime,
		'created_by' => 1,
		'updated_by' => 1,

	];
});

$factory->define(App\Models\Doctors::class,function(Faker $faker){
	return [
		'salutation' => $faker->randomElement(array('Mr','Mrs','Miss')),
		'first_name' => $faker->firstName,
		'last_name' => $faker->lastName,
		'phone'   => $faker->phoneNumber,
		'mobile' => $faker->phoneNumber,
		'email'  => $faker->safeEmail,
		'dob'  => $faker->date(),
		'password'  => str_random(10),
		'lat' => $faker->latitude,
		'lng'  => $faker->longitude,
		'city'  => $faker->city,
		'state' => $faker->country,
		'zip'  => $faker->postcode,
		'mcid'  => str_random(20),
		'gender' => rand(0,2),
		'martial_status' => rand(0,1),
		'blood_group' => $faker->randomElement(array('A1+ve','A +ve','O +ve','B+ve','A -ve')),
		'register_council' => $faker->word,
		'register_date' => $faker->date(),
		'specialization' => $faker->text,
		'educational_qualification' => $faker->text,
		'joined_date' => $faker->date(),
		'signature' => $faker->image(),
		'address' => $faker->address,
		'profile_image' => $faker->image(),
		'aadhar_no' =>  mt_rand(100000000000, 999999999999),
		'status' => rand(0,1),
		'created_at' => $faker->dateTime,
		'updated_at' => $faker->dateTime,
		'created_by' =>1 ,
		'updated_by' =>1

	];
});

$factory->define(App\Models\Labs::class,function(Faker $faker){
	return [
		'name' => $faker->name,
		'address' => $faker->address,
		'city' => $faker->city,
		'state' => $faker->country,
		'lat' => $faker->latitude,
		'lan' => $faker->longitude,
		'zip' => $faker->postcode,
		'profile_image' => $faker->image(),
		'gst_number' => mt_rand(100000000000, 999999999999),
		'gst_certificate_image' => json_encode(array($faker->image())),
		'gst_registered' => rand(0,1),
		'nabl_certified' => $faker->randomElement(array('test','test1')),
		'description' => $faker->text,
		'phone' => $faker->phoneNumber,
		'mobile' => $faker->phoneNumber,
		'signature' => json_encode(array($faker->image())),
		'nabl_id' => str_random(15),
		'email' => $faker->safeEmail,
		'aadhar_id' => mt_rand(100000000000, 999999999999),
		'password' => str_random(10),
		'aadhar_image' => json_encode(array($faker->image())),
		'home_collection' => rand(0,1),
		'status' => rand(0,1),
		'nabl_certificate' => json_encode(array($faker->image())),
		'created_at' => $faker->dateTime,
		'updated_at' => $faker->dateTime,
	];
});

$factory->define(App\Models\Patients::class,function(Faker $faker) {
	return [
		'name' => $faker->name,
		// 'email' => $faker->safeEmail,
		'phone' => $faker->phoneNumber,
		'mobile' => $faker->phoneNumber,
		'address' => $faker->address,
		'city' => $faker->city,
		'state' => $faker->country,
		'lat' => $faker->latitude,
		'lan' => $faker->longitude,
		'zip' => $faker->postcode,
		'gender' => rand(0,2),
		'dob' => $faker->date(),
		'blood_group' => $faker->randomElement(array('A1+ve','A +ve','O +ve','B+ve','A -ve')),
		'height' => $faker->randomDigit,
		'weight' => $faker->randomDigit,
		'profile_image' => $faker->image(),
		'before_statement' => $faker->text,
		'created_at' => $faker->dateTime,
		'updated_at' => $faker->dateTime,
		'created_by' =>1,
		'updated_by' => 1,
	];
});

$factory->define(App\Models\DocPatient::class,function(Faker $faker) {
	return [
		'doctor_id' => rand(1,51),
		'patient_id' => rand(1,50),
		'created_at' => $faker->dateTime,
		'updated_at' => $faker->dateTime,
	];
});

$factory->define(App\Models\DocLab::class,function(Faker $faker){
	return[
		'doctor_id' => rand(1,51),
		'lab_id' => rand(1,51),
	];
});

$factory->define(App\Models\DocHospital::class,function(Faker $faker){
	return[
		'doctor_id' => rand(1,51),
		'hospital_id' => rand(1,51),
	];
});

$factory->define(App\Models\RaiseTest::class,function(Faker $faker){
	return[
			'doctor_id' => rand(1,50),
			'lab_id'    => rand(1,50),
			'patients_id'  => rand(1,50),
			'lab_tests_id'  => rand(1,50),
			'test_on_date_time' => $faker->dateTime,
			'original_amount' => rand(100,400),
			'paid_status' => rand(0,1),
			'raised_by' => rand(0,1),
			'received_amount' => rand(100,400),
			'created_at' => $faker->dateTime,
			'updated_at' => $faker->dateTime,
			'created_by' =>1,
			'updated_by' => 1,
	];
});

$factory->define(App\Models\LabsAvaTest::class,function(Faker $faker){
	return[
		'lab_id' => rand(1,50),
		'lab_tests_id' => rand(1,53),
	];
});

$factory->define(App\Models\Report::class,function(Faker $faker){
	return [
		'raise_test_id' => rand(1,150),
		'test_result' => $faker->text,
		'test_default1' => $faker->text,
		'test_default2' => $faker->text,
		'test_default3' => $faker->text,
		'measurement_units' => $faker->randomElement(array('mg','ml','g','kg','unit')),
		'specimen' => $faker->text,
		'created_at' => $faker->dateTime,
		'updated_at' => $faker->dateTime,
		'created_by' =>1,
		'updated_by' => 1,
	];
});
