<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('report/{id}','\App\Admin\Controllers\LabsController@generatePatientView');
Route::get('receipt/{id}','\App\Admin\Controllers\LabsController@generateReceiptView'); 
Route::post('phonevalidation','\src\Controllers\UserController@getphone');
Route::get('admin/raisetest/getTest/{id}', '\App\Admin\Controllers\RaiseTestController@getTest');
Route::get('admin/raisetest/getvalue', '\App\Admin\Controllers\RaiseTestController@getvalue');
Route::get('admin/mytest/getTest/{id}', '\App\Admin\Controllers\MyLabController@getTest');
Route::get('admin/mypackage/getTest/{id}', '\App\Admin\Controllers\MyPackageController@getTest');

Route::get('admin/exportexcel/{id}','\App\Admin\Controllers\ApiController@exportexcel');
