<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Route::group(['middleware' => ['activity']], function () {
Route::post('login','\App\Admin\Controllers\ApiController@login');

Route::post('signup','\App\Admin\Controllers\ApiController@signUp');
Route::post('checkuserexists','\App\Admin\Controllers\ApiController@checkUserExist');
Route::post('checkDoctorExist','\App\Admin\Controllers\DocApiController@checkDoctorExist');
Route::get('getCountry','\App\Admin\Controllers\ApiController@getCountry');
Route::get('getimagepath','\App\Admin\Controllers\ApiController@getImagePath');


Route::group(['middleware' => ['jwt.refresh','jwt.auth']], function () {
	
Route::post('getCommonData','\App\Admin\Controllers\ApiController@getCommonData');

Route::post('profileupdate','\App\Admin\Controllers\ApiController@profileUpdate');

Route::post('profilecompletion','\App\Admin\Controllers\ApiController@getProfileCompletion');

Route::post('getPatientList','\App\Admin\Controllers\ApiController@getPatientList');

Route::post('getPatientProfile','\App\Admin\Controllers\ApiController@getPatientProfile');

Route::post('updatePatientInfo','\App\Admin\Controllers\ApiController@updatePatientInfo');

Route::post('getlabtests','\App\Admin\Controllers\ApiController@showAllLabTest');

Route::post('showTestBaseOnCategory','\App\Admin\Controllers\ApiController@showTestBaseOnCategory');

Route::post('labupdatetheirtestdata','\App\Admin\Controllers\ApiController@labupdatetheirtestdata');

Route::post('raisetest','\App\Admin\Controllers\ApiController@RaiseTest');

Route::post('raisedtesttolab','\App\Admin\Controllers\ApiController@raisedtesttolab');

Route::post('Canceltest','\App\Admin\Controllers\ApiController@Canceltest');

Route::post('specimencollected','\App\Admin\Controllers\ApiController@specimencollected');

Route::post('individualtest','\App\Admin\Controllers\ApiController@indidualtest');

Route::post('updateReceipt','\App\Admin\Controllers\ApiController@updateReceipt');

Route::post('getWorkingTime','\App\Admin\Controllers\ApiController@getWorkingTime');

Route::post('UpdateWorkingTime','\App\Admin\Controllers\ApiController@UpdateWorkingTime');

Route::post('getLabNotification','\App\Admin\Controllers\ApiController@getLabNotification');

Route::post('getReceipt','\App\Admin\Controllers\ApiController@getReceipt');

Route::post('clinicProfile','\App\Admin\Controllers\ApiController@clinicProfile');

Route::post('doctorLab','\App\Admin\Controllers\DocApiController@doctorLab'); 

Route::post('getReport','\App\Admin\Controllers\ApiController@getReport');

//Route::get('getCountry','\App\Admin\Controllers\ApiController@getCountry');

//Route::post('checkuserexists','\App\Admin\Controllers\ApiController@checkUserExist');

Route::get('alltest','\App\Admin\Controllers\ApiController@alltest');

Route::post('lab_alltest','\App\Admin\Controllers\ApiController@lab_alltest');

Route::post('lab_alltest_update','\App\Admin\Controllers\ApiController@lab_alltest_update');

Route::post('lab_test_selectlist','\App\Admin\Controllers\ApiController@lab_test_selectlist');

Route::post('lab_test_categories','\App\Admin\Controllers\ApiController@lab_test_categories');

Route::post('getRaisedTestDetail','\App\Admin\Controllers\ApiController@getRaisedTestDetail');

Route::post('getEducationDetails','\App\Admin\Controllers\DocApiController@getEducationDetails');

Route::post('addEducation','\App\Admin\Controllers\DocApiController@addEducation');

Route::post('editEducation','\App\Admin\Controllers\DocApiController@editEducation');

Route::post('Docraisedtest','\App\Admin\Controllers\DocApiController@Docraisedtest');

Route::post('Labraisedtest','\App\Admin\Controllers\DocApiController@Labraisedtest');


Route::post('getgeneratedorder','\App\Admin\Controllers\DocApiController@getgeneratedorder');

Route::post('doctordashboard','\App\Admin\Controllers\DocApiController@getDoctorDashboard');

Route::post('sendSupportMail','\App\Admin\Controllers\DocApiController@sendSupportMail');

Route::post('raise_test_all','\App\Admin\Controllers\ApiController@raise_test_all');

Route::post('raise_user_list','\App\Admin\Controllers\ApiController@raise_user_list');


Route::post('get_all_Doctor_details_Dashboard','\App\Admin\Controllers\ApiController@get_all_Doctor_details_Dashboard'); 



Route::post('lab_test_subcategories','\App\Admin\Controllers\ApiController@lab_test_subcategories');

Route::post('add_new_test_lab','\App\Admin\Controllers\ApiController@add_new_test_lab');

Route::post('lab_test_id_subcategories','\App\Admin\Controllers\ApiController@lab_test_id_subcategories');

Route::post('lab_add_update_collectiondata','\App\Admin\Controllers\ApiController@lab_add_update_collectiondata');
Route::post('lab_receipt_list','\App\Admin\Controllers\ApiController@lab_receipt_list');

Route::post('add_new_rise_test','\App\Admin\Controllers\ApiController@add_new_rise_test');

Route::post('change_collected_confirm_test','\App\Admin\Controllers\ApiController@change_collected_confirm_test');

Route::post('update_receipt_amount','\App\Admin\Controllers\ApiController@updateReceiptamount');

Route::post('getlabgenerateReport','\App\Admin\Controllers\ApiController@getlabgenerateReport');

Route::post('editGentrateReport','\App\Admin\Controllers\ApiController@editGentrateReport');

Route::post('saveNotification','\App\Admin\Controllers\ApiController@saveNotification');

Route::post('updateRangeOwnTest','\App\Admin\Controllers\ApiController@updateRangeOwnTest');

Route::post('insertTestCart','\App\Admin\Controllers\ApiController@insertTestCart');

Route::post('getCartTestDetail','\App\Admin\Controllers\ApiController@getCartTestDetail');

Route::post('onclickCart','\App\Admin\Controllers\ApiController@onclickCart');

Route::post('getdoctorDetails','\App\Admin\Controllers\DocApiController@getdoctorDetails');

Route::post('getDoctorNotification','\App\Admin\Controllers\DocApiController@getDoctorNotification');

Route::post('getDoctorRegistration','\App\Admin\Controllers\DocApiController@getDoctorRegistration');

Route::post('newDoctoradd','\App\Admin\Controllers\ApiController@newDoctoradd');

Route::post('labForgotPassword','\App\Admin\Controllers\ApiController@labForgotPassword');

Route::post('generateReport','\App\Admin\Controllers\ApiController@generateReport');

Route::post('labPatientsorder','\App\Admin\Controllers\ApiController@labPatientsorder');

Route::post('DoctorForgotPassword','\App\Admin\Controllers\DocApiController@DoctorForgotPassword');

Route::post('getDoctorRaisetest','\App\Admin\Controllers\DocApiController@getDoctorRaisetest');

Route::post('getDoctorOrderList','\App\Admin\Controllers\DocApiController@getDoctorOrderList');

Route::post('getDoctorOrderdetail','\App\Admin\Controllers\DocApiController@getDoctorOrderdetail');

Route::post('DoctorsaveNotification','\App\Admin\Controllers\DocApiController@DoctorsaveNotification');

Route::post('DoctorinsertTestCart','\App\Admin\Controllers\DocApiController@DoctorinsertTestCart');

Route::post('Doctorlogin','\App\Admin\Controllers\DocApiController@Doctorlogin'); 

Route::post('FilterDoctorlab','\App\Admin\Controllers\DocApiController@FilterDoctorlab');

Route::post('contacts','\App\Admin\Controllers\ApiController@contacts');

Route::post('getgrouppackage','\App\Admin\Controllers\ApiController@getgrouppackage');

Route::post('getLabgrouppackage','\App\Admin\Controllers\ApiController@getLabgrouppackage');

Route::post('AddNewPackageSamplepage','\App\Admin\Controllers\ApiController@AddNewPackageSamplepage');

Route::get('getFaq','\App\Admin\Controllers\ApiController@getFaq');

Route::post('getSubscriptionExpiredData','\App\Admin\Controllers\ApiController@getSubscriptionExpiredData');

});

//});


Route::post('refreshtoken','\App\Admin\Controllers\ApiController@refreshToken');

Route::post('testingsms','\App\Admin\Controllers\ApiController@testingsms');




















