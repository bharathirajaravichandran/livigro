  <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css')}}"/>
    <!--End of plugin styles-->
    <!--Page level styles-->
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/wizards.css')}}"/>

                                    <!-- BEGIN FORM WIZARD WITH VALIDATION -->
                                    <form id="commentForm" method="post" action="#" class="validate">
                                        <div id="rootwizard">
                                            <ul class="nav nav-pills">
                                                <li class="nav-item m-t-15">
                                                    <a class="nav-link" href="#tab1" data-toggle="tab">
                                                        <span class="userprofile_tab1">1</span>User
                                                        profile</a>
                                                </li>
                                                <li class="nav-item m-t-15">
                                                    <a class="nav-link" href="#tab2" data-toggle="tab">
                                                        <span class="userprofile_tab2">2</span>Profile
                                                        details</a>
                                                </li>
                                                <li class="nav-item m-t-15">
                                                    <a class="nav-link" href="#tab3"
                                                       data-toggle="tab"><span>3</span>Finish</a>
                                                </li>
                                            </ul>
                                            <div class="tab-content m-t-20">
                                                <div class="tab-pane" id="tab1">
                                                    <div class="form-group">
                                                        <label for="userName" class="control-label">User
                                                            name *</label>
                                                        <input id="userName" name="username" type="text"
                                                               placeholder="Enter your name"
                                                               class="form-control required">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="email" class="control-label">Email
                                                            *</label>
                                                        <input id="email" name="email"
                                                               placeholder="Enter your Email"
                                                               type="text"
                                                               class="form-control required email">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="password" class="control-label">Password
                                                            *</label>
                                                        <input id="password" name="password" type="password"
                                                               placeholder="Enter your password"
                                                               class="form-control required">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="confirm" class="control-label">Confirm
                                                            Password
                                                            *</label>
                                                        <input id="confirm" name="confirm" type="password"
                                                               placeholder="Confirm your password "
                                                               class="form-control required">
                                                    </div>
                                                    <ul class="pager wizard pager_a_cursor_pointer">
                                                        <li class="previous">
                                                            <a><i class="fa fa-long-arrow-left"></i>
                                                                Previous</a>
                                                        </li>
                                                        <li class="next">
                                                            <a>Next <i class="fa fa-long-arrow-right"></i>
                                                            </a>
                                                        </li>
                                                        <li class="next finish" style="display:none;">
                                                            <a>Finish</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="tab-pane" id="tab2">
                                                    <div class="form-group">
                                                        <label for="name1" class="control-label">First name
                                                            *</label>
                                                        <input id="name1" name="val_first_name"
                                                               placeholder="Enter your First name"
                                                               type="text"
                                                               class="form-control required">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="surname1" class="control-label">Last
                                                            name *</label>
                                                        <input id="surname1" name="lname" type="text"
                                                               placeholder=" Enter your Last name"
                                                               class="form-control required">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="email">Gender</label>
                                                        <select class="custom-select form-control"
                                                                id="gender"
                                                                title="Select an account type...">
                                                            <option>Select</option>
                                                            <option>MALE</option>
                                                            <option>FEMALE</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="address">Address *</label>
                                                        <input id="address" name="val_address" type="text"
                                                               class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="age" class="control-label">Age *</label>
                                                        <input id="age" name="val_age" type="text"
                                                               maxlength="3"
                                                               class="form-control required number">
                                                    </div>
                                                    <ul class="pager wizard pager_a_cursor_pointer">
                                                        <li class="previous">
                                                            <a><i class="fa fa-long-arrow-left"></i>
                                                                Previous</a>
                                                        </li>
                                                        <li class="next">
                                                            <a>Next <i class="fa fa-long-arrow-right"></i>
                                                            </a>
                                                        </li>
                                                        <li class="next finish" style="display:none;">
                                                            <a>Finish</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="tab-pane" id="tab3">
                                                    <div class="form-group">
                                                        <label>Home number *</label>
                                                        <input type="text" class="form-control" id="phone1"
                                                               name="phone1"
                                                               placeholder="(999)999-9999">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Personal number *</label>
                                                        <input type="text" class="form-control" id="phone2"
                                                               name="phone2"
                                                               placeholder="(999)999-9999">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Alternate number *</label>
                                                        <input type="text" class="form-control" id="phone3"
                                                               name="phone3"
                                                               placeholder="(999)999-9999">
                                                    </div>
                                                    <div class="form-group">
                                                        <span>Terms and Conditions *</span>
                                                        <br>
                                                        <label class="custom-control custom-checkbox wizard_label_block">
                                                            <input type="checkbox" id="acceptTerms"
                                                                   name="acceptTerms"
                                                                   class="custom-control-input">
                                                            <span class="custom-control-label"></span>
                                                            <span class="custom-control-description custom_control_description_color">I agree with the Terms and Conditions.</span>
                                                        </label>

                                                    </div>
                                                    <ul class="pager wizard pager_a_cursor_pointer">
                                                        <li class="previous">
                                                            <a><i class="fa fa-long-arrow-left"></i>
                                                                Previous</a>
                                                        </li>
                                                        <li class="next">
                                                            <a>Next <i class="fa fa-long-arrow-right"></i>
                                                            </a>
                                                        </li>
                                                        <li class="next finish" style="display:none;">
                                                            <a>Finish</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="myModal" class="modal fade" role="dialog">
                                            <div class="modal-dialog">
                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header">

                                                        <h4 class="modal-title">Wizard</h4>
                                                        <button type="button" class="close"
                                                                data-dismiss="modal">&times;</button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>You Submitted Successfully.</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default"
                                                                data-dismiss="modal">
                                                            OK
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                
             <script type="text/javascript" src="{{asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/twitter-bootstrap-wizard/js/jquery.bootstrap.wizard.min.js')}}"></script>
    <!--End of plugin scripts-->
    <!--Page level scripts-->
    <script type="text/javascript" src="{{asset('assets/js/pages/wizard.js')}}"></script>