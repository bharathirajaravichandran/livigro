<style type="text/css">
    /*Panel tabs*/
.panel-tabs {
    position: relative;
    bottom: 30px;
    clear:both;
    border-bottom: 1px solid transparent;
}

.panel-tabs > li {
    float: left;
    margin-bottom: -1px;
}

.panel-tabs > li > a {
    margin-right: 2px;
    margin-top: 4px;
    line-height: .85;
    border: 1px solid transparent;
    border-radius: 4px 4px 0 0;
    color: black;
}

.panel-tabs > li > a:hover {
    border-color: transparent;
    color: black;
    background-color: transparent;
}

.panel-tabs > li.active > a,
.panel-tabs > li.active > a:hover,
.panel-tabs > li.active > a:focus {
    color: blue;
    cursor: default;
    -webkit-border-radius: 2px;
    -moz-border-radius: 2px;
    border-radius: 2px;
    background-color: rgba(255,255,255, .23);
    border-bottom-color: transparent;
}/*Panel tabs*/
.panel-tabs {
    position: relative;
    bottom: 30px;
    clear:both;
    border-bottom: 1px solid transparent;
}

.panel-tabs > li {
    float: left;
    margin-bottom: -1px;
}

.panel-tabs > li > a {
    margin-right: 2px;
    margin-top: 4px;
    line-height: .85;
    border: 1px solid transparent;
    border-radius: 4px 4px 0 0;
    color: black;
}

.panel-tabs > li > a:hover {
    border-color: transparent;
    color:black;
    background-color: transparent;
}

.panel-tabs > li.active > a,
.panel-tabs > li.active > a:hover,
.panel-tabs > li.active > a:focus {
    color: red;
    cursor: default;
    -webkit-border-radius: 2px;
    -moz-border-radius: 2px;
    border-radius: 2px;
    background-color: rgba(255,255,255, .23);
    border-bottom-color: transparent;
}
</style>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">{{ $doctor->salutation}} @if($doctor->name){{$doctor->name }} @else Nil @endif  </h3>
                    <span class="pull-right">
                        <!-- Tabs -->
                        <ul class="nav panel-tabs">
                            <li class="active">
                                <a href="#tab1" data-toggle="tab">Doctors Details</a>
                            </li>
                            <li>
                                <a href="#tab2" data-toggle="tab">Patient List</a>
                            </li>
                            <li>
                                <a href="#tab3" data-toggle="tab">Lab List</a>
                            </li>
                            <li>
                                <a href="#tab4" data-toggle="tab">Hospital List</a>
                            </li>
                        </ul>
                    </span>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab1">
                            <div class="row">
              
                                 <div class="col-md-3" style="visibility: hidden;">test</div>
                            <div class=" col-md-6"> 
                             <div class="container-fluid well span6">
	<div class="row-fluid">
        <div class="span2" >
			<div class=" col-md-3">
                <!-- Neelamegam_DoctorsDetailsView_13/08/2018 -->
                @if($doctor->profile_image!="") 
                <img src="{!! \Illuminate\Support\Facades\Storage::disk(config('admin.upload.disk'))->url($doctor->profile_image) !!}" alt="" class="img-rounded img-responsive" />
                @else
                    <img src="" />    
                @endif
                                </div></div>
                                 <div class="span8">
            <h3>@if($doctor->name){{$doctor->name }} @else Nil @endif</h3>
            <h6>Mobile: @if($doctor->mobile){{$doctor->mobile}} @else Nil @endif</h6>
            <h6>Email: @if($doctor->email){{$doctor->email}} @else Nil @endif</h6>
            <h6>Date Of Birth: @if($doctor->dob){{$doctor->dob}} @else Nil @endif</h6>
            <h6>Age: @if($age == null || $age == 0) 0 @else {{ $age }} @endif</h6>
            <h6>Blood Group:  @if($bg){{$bg}} @else Nil @endif</h6>
            <h6>Specialization:  @if($sp){{$sp}} @else Nil @endif</h6>
            <h6>Year Of Experience:  @if($doctor->yearofexperience){{$doctor->yearofexperience}} @else Nil @endif</h6>
            
        </div>
                            </div>
            </div>
             </div>     
                    
              </div>
                        </div>
                        <div class="tab-pane" id="tab2">
                            @include('patients_list')
                        </div>
                        <div class="tab-pane" id="tab3">
                            @include('labs_list')

                        </div>
                        <div class="tab-pane" id="tab4">
                            @include('hospital_list')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
     <div class="row">
        @if(count($raised_test) > 0)
             @include('raised_test')
        @endif
    </div>