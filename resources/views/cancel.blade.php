<div class="panel panel-default">
	<div class="panel-heading">
		<strong>Sample</strong>
	</div>
	@if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>Whoops! Something went wrong!</strong>

        <br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
	<div class="panel-body">
		<form  action="{{route('cancelsave')}}" method="post" class="form-horizontal">
			{!! csrf_field() !!}
		  	<input type="hidden" value="{{$id}}" name="raise_test_id" />
		    <div class="checkbox">
			  <label><input type="radio" @if($status == 3) checked="checked" @endif name="check" value="3">Patient Was Not Reachable</label>
			</div>
			<div class="checkbox">
			  <label><input type="radio" @if($status == 4) checked="checked" @endif name="check" value="4">Patient Refused to Take the Test</label>
			</div>
			<div class="checkbox">
			  <label><input type="radio" @if($status == 5) checked="checked" @endif name="check" value="5">Lab Do Not Offer the Prescribed Test</label>
			</div>
			<div>
				<span id="cancelerror" style="color: red"></span>
			</div>
			
		  <button type="submit" id="cancel" name="cancel" class="btn btn-primary submitbtn-fix">Submit</button>
		</form>
	</div>
</div>

<style type="text/css">
	.submitbtn-fix{
		margin-top: 20px;
	}
</style>
<script type="text/javascript">
	$(document).ready(function() {
		$("#cancel").click(function(){
	// alert("hi");
   			 var cancelvalue = $("input[name='check']:checked").val();
            if(cancelvalue!=3 && cancelvalue!=4 && cancelvalue!=5){
            	$("#cancelerror").text("please select");
            	return false;
            }
		});
	});
</script>