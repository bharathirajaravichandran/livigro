<?php
//echo "<pre>";
//print_r($doctorsList);?>    
<section class="content" >
    <div class="row">
        <div class="col-md-12">
            <div class="box" style="padding: 25px;">
                <div class="box-header">
                </div>
                <div class="box-body table-responsive no-padding">
                <div class="form-group  ">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-search"></i></span>  
                            <input type="text" id="search" name="search" value="" class="form-control search" placeholder="Search" />   
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">  
                        </div>
                </div>
                 <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Mobile Number</th>
                                <th>Email</th>
                                <th>Title</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $( document ).ready(function(){ 
         $("#search").keypress(function(){
            var url = "searchresult"; 
            var token = $("input[name=_token]").val();
            var search = $("#search").val();                   
                $.ajax({
                    type:"post",
                    dataType: "json",
                    url: url,                       
                    data: {
                        _token: token,
                        search: search,                                
                    },                        
                    success: function( responses ){
                        if(responses)
                        $("tbody").html(responses);
                        else
                         $("tbody").html("<tr><td colspan=5> <h5 style='text-align:center'>No Data Found</h5></td></tr>");
                    },
                });
         });
    });
</script>

   
 				 
	