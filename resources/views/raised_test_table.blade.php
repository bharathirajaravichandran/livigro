

                 <div class="card-body">
                    <div class="col-md-12">
                        <div class="">
                                <div class="pull-sm-right">
                                    <div class="tools pull-sm-right"></div>
                                </div>
                            </div>
                        <table id="example" class="table display nowrap" >
                            <thead>
                            <tr>
                                <th>Raise Test ID</th>
                                <th>Patient Name</th>
                                 @if(!isset($labs))
                                    <th>Lab Name</th>
                                @endif
                                <th>Speciemen Collected</th>
                                <th>Amount</th>
                                <th>Test On Date</th>
                                <th>Test Status</th>
                                <th>Paid Status</th>
                            </tr>
                            </thead>
                            <tbody>
								@if($raised_test->count()>0)
									@foreach($raised_test as $raise_test)
										<tr>
											@if($raise_test->getTestStatus() == 'Completed')
											 <td><a href="{{$raise_test->path()}}">{{$raise_test->id }}</a></td>
											@else 
													<td>{{$raise_test->id }}</td>
											@endif
											<td><a href="{{$raise_test->getPatient->path()}}"> {{$raise_test->getPatient->name }}</a></td>
											@if(!isset($labs))
												 <td><a href="{{$raise_test->getLab->path()}}">{{$raise_test->getLab->name}}</a></td>
											@endif
											<td>@if($raise_test->specimen_collected ==0 || $raise_test->specimen_collected == null || $raise_test->specimen_collected =='')
                                                 No
                                                @else
                                                 Yes
                                                @endif
                                                </td>
											<td>₹{{$raise_test->original_amount}}</td>
											<td>{{$raise_test->updated_at}}</td>
											<td>{{$raise_test->getTestStatus()}}</td>
											<td>{{$raise_test->paidStatus()}}</td>
										</tr>
									@endforeach
                                @endif

                            </tbody>
                        </table>
                    </div>
                </div>
    
            
