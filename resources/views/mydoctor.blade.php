<?php
//echo "<pre>";
//print_r($doctorsList);?>    
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                </div>
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Doctor Name</th>
                                <th>Specialization</th>
                                <th>Blood Group</th>
                                <th>Phone number</th>
                                <th>Email</th>
                                <th>City</th>
                                <th>State</th>
                                <th>Status</th>
                                <th>Test id</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($doctorsList as $doctor)
                                    <tr>
                                        <td>{{ $doctor->id }}</td>
                                        <td>{{ $doctor->salutation.' '.$doctor->name }}</td>
                                        <td>{{ $doctor->specialization }}</td>
                                        <td>{{ DB::table('blood_group')->where('id', $doctor->blood_group)->pluck('blood_group')->first() }}</td>
                                        <td>{{ $doctor->mobile }}</td>
                                        <td>{{ $doctor->email }}</td>
                                        <td>{{ $doctor->city }}</td>
                                        <td>{{ $doctor->state }}</td>
                                        <td>{{ $doctor->status }}</td>
                                        <td> <?php
                                            $doctorTestArr = str_replace(array('{','}'), '', $doctor->test_name);
                                            $testList = explode(',',$doctorTestArr);
                                            
                                            $data = array();
                                            foreach($testList as $v){
                                                $data[] = '<a href="raisetest/'.$v.'/show" title="View">'.$v.'</a> ';
                                            }
                                            echo implode(', ', $data);
                                            ?>
                                                
                                            </td>
                                        
                                            <?php
                                            /*$doctorTestArr = str_replace(array('{','}'), '', $doctor->patient_list);
                                            $testList = explode(',',$doctorTestArr);
                                            
                                            $data = array();
                                            foreach(array_unique($testList) as $v){
                                                $data[] = '<a href="patients/'.$v.'/edit" title="View">'.$v.'</a> ';
                                            }
                                            echo implode(', ', $data);*/
                                            ?>
                                                
                                             
                                        <td><a href="doctors/{{ $doctor->id }}/edit" title="View"><i class="fa fa-edit"></i></a></td>
                                        
                                    </tr>
                                
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

   
 				 
	