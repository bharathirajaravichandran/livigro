<!-- <script type="text/javascript" src="{{asset('assets/vendors/slimscroll/js/jquery.slimscroll.min.js')}}"></script> -->

	 <link type="text/css" rel="stylesheet" href="{{asset('assets/css/components.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/custom.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/layouts.css')}}" />
    <!-- <link type="text/css" rel="stylesheet" href="#" id="skin_change"/> -->
	 <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/c3/css/c3.min.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/toastr/css/toastr.min.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/switchery/css/switchery.min.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/new_dashboard.css')}}"/>

      <div class="outer">
        <div class="inner bg-container">
            <div class="row">
            	  <div class="col-md-4 col-12">
                    <a style="color:white;" href="#">
                            <div class="bg-primary top_cards">
                                <div class="row icon_margin_left">
                                    <div class="col-lg-5 col-5 icon_padd_left">
                                        <div class="float-left">
                											<span class="fa-stack fa-sm">
                											<i class="fa fa-circle fa-stack-2x"></i>
                											<i class="fa fa-user fa-stack-1x fa-inverse text-primary sales_hover"></i>
                											</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-offset-1 icon_padd_right">
                                        <div class="cards_content">
                                            <span class="number_val" >{{$data['patient_count']}}</span><!-- <i
                                                    class="fa fa-long-arrow-up fa-2x"></i> -->
                                            <br/>
                                            <span class="card_description">Patients</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                          </a>
                        </div>
                 <div class="col-md-4 col-12">
                    <a style="color:white;" href="#">
                            <div class="bg-success top_cards">
                                <div class="row icon_margin_left">
                                    <div class="col-lg-5 col-5 icon_padd_left">
                                        <div class="float-left">
											<span class="fa-stack fa-sm">
											<i class="fa fa-circle fa-stack-2x"></i>
											<i class="fa fa-flask fa-stack-1x fa-inverse text-primary sales_hover"></i>
											</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-offset-1 icon_padd_right">
                                        <div class="cards_content">
                                            <span class="number_val" >{{$data['labs_count']}}</span><!-- <i
                                                    class="fa fa-long-arrow-up fa-2x"></i> -->
                                            <br/>
                                            <span class="card_description">Labs</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                          </a>
                        </div>
                 <div class="col-md-4 col-12">
                    <a style="color:white;" href="#">
                            <div class="bg-warning top_cards">
                                <div class="row icon_margin_left">
                                    <div class="col-lg-5 col-5 icon_padd_left">
                                        <div class="float-left">
											<span class="fa-stack fa-sm">
											<i class="fa fa-circle fa-stack-2x"></i>
											<i class="fa fa-hospital-o  fa-stack-1x fa-inverse text-primary sales_hover"></i>
											</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-offset-1 icon_padd_right">
                                        <div class="cards_content">
                                            <span class="number_val" >{{$data['hospital_count']}}</span><!-- <i
                                                    class="fa fa-long-arrow-up fa-2x"></i> -->
                                            <br/>
                                            <span class="card_description">Hospital</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                      </a>
                </div>
            <div class="row">
                <div class="col-lg-4 col-12 m-t-35">
                    <div class="card">
                        <div class="card-header bg-white">
                          Completed Raised Test ( {{ $data['completed_test'] }} )
                        </div>
                        <div class="card-body">
                           <table id='nt-example1' class="table display nowrap" >
                                <thead>
                                    <tr>
                                        <th>Test Name</th>
                                        <th>Pateint Name</th>
                                        <th>Lab Name</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($data['completed_raised_test'] as $completed)
                                    <tr>
                                       <td> <a href="/admin/test/{{ $completed->id }}/show" >
                                       			{{$completed->getLabTest()[0]->name}}
                                       		</a> 
                                   		</td>
                                   		<td>
                                   			<a href="/admin/patients/{{ $completed->getPatient()[0]->id }}/show" >
                                       			{{$completed->getPatient()[0]->name}}
                                       		</a> 
                                   		</td>
                                   		<td>
                                   			<a href="/admin/labs/{{ $completed->getLab()[0]->id }}/show" >
                                       			{{$completed->getLab()[0]->name}}
                                       		</a>
                                   		</td>
                                       
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-12 m-t-35">
                    <div class="card">
                        <div class="card-header bg-white">
                          Pending Raised Test ( {{ $data['count_not_completed'] }} )
                        </div>
                        <div class="card-body">
                           <table  class="table display nowrap" >
                                <thead>
                                    <tr>
                                        <th>Test Name</th>
                                        <th>Pateint Name</th>
                                        <th>Lab Name</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($data['pending_raised_test'] as $completed)
                                    <tr>
                                       <td> 
                                       			{{$completed->getLabTest()[0]->name}}
                                       		
                                   		</td>
                                   		<td>
                                   			<a href="/admin/patients/{{ $completed->getPatient()[0]->id }}/show" >
                                       			{{$completed->getPatient()[0]->name}}
                                       		</a> 
                                   		</td>
                                   		<td>
                                   			<a href="/admin/labs/{{ $completed->getLab()[0]->id }}/show" >
                                       			{{$completed->getLab()[0]->name}}
                                       		</a>
                                   		</td>
                                       
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-12 m-t-35">
                    <div class="card">
                        <div class="card-header bg-white">
                          This Week Patient List ( {{ count($data['new_Week_ patient']) }} )
                        </div>
                        <div class="card-body">
                        	<table  class="table display nowrap" >
                                <tbody>
                                	@foreach($data['new_Week_ patient'] as $patient)
                                	<tr>
                                		<td>
                                			<a href="{{$patient->path()}}">
                                				{{$patient->name}}</a>
                                		</td>
                                	</tr>
                                	@endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>

    <script type="text/javascript" src="{{asset('assets/vendors/raphael/js/raphael.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/d3/js/d3.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/c3/js/c3.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/toastr/js/toastr.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/switchery/js/switchery.min.js')}}"></script>
    
    <script type="text/javascript" src="{{asset('assets/vendors/jquery_newsTicker/js/newsTicker.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/countUp.js/js/countUp.min.js')}}"></script>
    <!--end of plugin scripts-->
    <script type="text/javascript" src="{{asset('assets/js/pages/new_dashboard.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/custom.js')}}"></script>
    <!-- <script type="text/javascript" src="{{asset('assets/js/components.js')}}"></script> -->
<!--  -->
<!-- <script type="text/javascript" src="{{asset('assets/js/pages/fixed_menu.js')}}"></script> -->

