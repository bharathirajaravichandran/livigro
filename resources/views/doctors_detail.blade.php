<style type="text/css">
    /*Panel tabs*/
.panel-tabs {
    position: relative;
    bottom: 30px;
    clear:both;
    border-bottom: 1px solid transparent;
}

.panel-tabs > li {
    float: left;
    margin-bottom: -1px;
}

.panel-tabs > li > a {
    margin-right: 2px;
    margin-top: 4px;
    line-height: .85;
    border: 1px solid transparent;
    border-radius: 4px 4px 0 0;
    color: #ffffff;
}

.panel-tabs > li > a:hover {
    border-color: transparent;
    color: #ffffff;
    background-color: transparent;
}

.panel-tabs > li.active > a,
.panel-tabs > li.active > a:hover,
.panel-tabs > li.active > a:focus {
    color: #fff;
    cursor: default;
    -webkit-border-radius: 2px;
    -moz-border-radius: 2px;
    border-radius: 2px;
    background-color: rgba(255,255,255, .23);
    border-bottom-color: transparent;
}/*Panel tabs*/
.panel-tabs {
    position: relative;
    bottom: 30px;
    clear:both;
    border-bottom: 1px solid transparent;
}

.panel-tabs > li {
    float: left;
    margin-bottom: -1px;
}

.panel-tabs > li > a {
    margin-right: 2px;
    margin-top: 4px;
    line-height: .85;
    border: 1px solid transparent;
    border-radius: 4px 4px 0 0;
    color: #ffffff;
}

.panel-tabs > li > a:hover {
    border-color: transparent;
    color: #ffffff;
    background-color: transparent;
}

.panel-tabs > li.active > a,
.panel-tabs > li.active > a:hover,
.panel-tabs > li.active > a:focus {
    color: #fff;
    cursor: default;
    -webkit-border-radius: 2px;
    -moz-border-radius: 2px;
    border-radius: 2px;
    background-color: rgba(255,255,255, .23);
    border-bottom-color: transparent;
}
</style>
<div class="container">
    <div class="row">
        <div class="col-md-9">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">{{ $doctor->salutation}} {{$doctor->first_name }} {{ $doctor->last_name }} </h3>
                    <span class="pull-right">
                        <!-- Tabs -->
                        <ul class="nav panel-tabs">
                            <li class="active">
                                <a href="#tab1" data-toggle="tab">Doctors Details</a>
                            </li>
                            <li>
                                <a href="#tab2" data-toggle="tab">Patient List</a>
                            </li>
                            <li>
                                <a href="#tab3" data-toggle="tab">Lab List</a>
                            </li>
                            <li>
                                <a href="#tab4" data-toggle="tab">Hospital List</a>
                            </li>
                        </ul>
                    </span>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab1">
                            <div class="row">
              
                            <div class=" col-md-9"> 
                              <div class="well well-sm">
                            <div class="row">
                                <div class="col-sm-6 col-md-4">
                                    <img src="{{ $doctor->profile_image }}" alt="" class="img-rounded img-responsive" />
                                </div>
                                <div class="col-sm-6 col-md-8">
                                    <h4>
                                        {{ $doctor->salutation}} {{$doctor->first_name }} {{ $doctor->last_name }}</h4>
                                    <small><cite title="San Francisco, USA">{{ $doctor->state}},{{ $doctor->city}} <i class="glyphicon glyphicon-map-marker">
                                    </i></cite></small>
                                    <p>
                                        <i class="glyphicon glyphicon-envelope"></i> {{$doctor->email}}
                                        <br />
                                        <i class="glyphicon glyphicon-gift"></i> {{$doctor->dob}}
                                        <br>
                                        <i class="glyphicon glyphicon-heart"></i> {{$doctor->blood_groups()}}
                                        <br>
                                        <i class="glyphicon glyphicon-book"></i> {{$doctor->educational_qualification}}
                                        </p>
                                    <!-- Split button -->
                                    
                                </div>
                            </div>
            </div>
             </div>     
                    
              </div>
                        </div>
                        <div class="tab-pane" id="tab2">
                            @include('patients_list')
                        </div>
                        <div class="tab-pane" id="tab3">
                            @include('labs_list')

                        </div>
                        <div class="tab-pane" id="tab4">
                            @include('hospital_list')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>