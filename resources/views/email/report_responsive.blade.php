<div style=""><p style="text-align:center;">Please review the following booking details and feel free to contact us for any support at +91 8098094888</p></div>
<br>
<h3 style="color:#EF2D7F; text-align:center"> Booking Confirmation</h3>

<table style="margin-left:25%; width:60%;">
	<tr>
		<td style="color:#00A2C8">Booking ID</td>
		<td>{{$raise_test->id}}</td>
	</tr>
	<tr>
		<td style="color:#00A2C8">Patient's ID</td>
		<td>{{$raise_test->getPatient->id}}</td>
	</tr>
	<tr>
		<td style="color:#00A2C8">Name</td>
		<td>{{$raise_test->getPatient->name}}</td>
	</tr>
	<tr>
		<td style="color:#00A2C8">Time & date</td>
		<td><?php echo date('g:ia',strtotime($raise_test->updated_at)); ?> and <?php echo date('d-M-Y',strtotime($raise_test->updated_at)); ?></td>
	</tr>
	<tr>
		<td style="color:#00A2C8">Address</td>
		<td>@if(@$raise_test->getPatient->city)  {{ @$raise_test->getPatient->area}} {{ @$raise_test->getPatient->address}} {{ @$raise_test->getPatient->city}} {{ @$raise_test->getPatient->state}} @else Nil @endif</td>
	</tr>
	<tr>
		<td style="color:#00A2C8">Phone No</td>
		<td>{{$raise_test->getPatient->phone}}</td>
	</tr>
	<tr>
		<td style="color:#00A2C8">Payment Status</td>
		<td>{{$raise_test->original_amount}} / Amount Due</td>
	</tr>
</table>
<br>
<br>
<div style="text-align:center;">
	<img src="{{url('/').'/images/logo.png'}}" style="width: 80px;margin:0px 0 0 0;">
</div>