<?php 
use App\Models\LabsTest;
use App\Models\LabsCategory;
//echo "<pre>";
//print_r($receipt_data);die;
?>
<div class="panel panel-default">
	<div class="panel-heading">
		<strong>Report</strong><p>Order Id : {{@$labtest->id }}   Patient Id: {{@$labtest->getPatient->id}}</p>
				
	</div>
	<div class="panel-body">
		<div class="box-body">
		
		<div class="row">
			<div class="col-md-8 col-sm-8">
				<h4><strong>Patient Details</strong></h4>
				 <div class="col-md-4">
				  @if(@$patient->profile_image!="") 
					<img src="{!! \Illuminate\Support\Facades\Storage::disk(config('admin.upload.disk'))->url(@$labtest->getPatient->profile_image) !!}" class="img-responsive" style="max-width:150px;max-height:175px;">
					@else
					<img src="" />
					@endif
				</div> 
				<div class="col-md-4">
					<p>Name: @if(@$patient->name) {{ @$patient->name}} @else Nil @endif</p>
					<p>Mobile: @if(@$patient->phone) {{ @$patient->phone}} @else Nil @endif</p>
					<p>Email: @if(@$patient->email) {{ @$patient->email}} @else Nil @endif</p>
					<p>Blood Group: @if(@$patient->blood_group) {{ @$patient->blood_group }} @else Nil @endif</p>
					<p>Date of Birth: @if(@$patient->dob) {{ @$patient->dob }} @else Nil @endif</p>
					<p>Age: @if(@$patient->age()) {{ @$patient->age() }} Years @else 0 year @endif</p>
					<p>Address: @if(@$patient->city)  {{ @$patient->area}} {{ @$patient->address}} {{ @$patient->city}} {{ @$patient->state}} @else Nil @endif</p> 
				</div>
			</div>
			<div class="col-md-4 col-sm-4">
			<h4> <strong>Lab Details</strong> </h4>
			<p>Name: @if(@$labtest->getLab->name){{@$labtest->getLab->name }} @else Nil @endif</p>
			<p>Mobile: @if(@$labtest->getLab->mobile){{@$labtest->getLab->mobile }} @else Nil @endif</p>
			<p>Address: @if(@$labtest->getLab->area || @$labtest->getLab->address || @$labtest->getLab->city || @$labtest->getLab->state){{@$labtest->getLab->area}} {{@$labtest->getLab->address}} {{@$labtest->getLab->city}} {{@$labtest->getLab->state}} @else Nil @endif</p>
			
			</div>
		</div>
		<div class="row">
			<p>Diagonis : @if(@$labtest->diagonsis) {{@$labtest->diagonsis}} @else Nil @endif</p>
			<p>Notes :@if(@$labtest->notes) {{@$labtest->notes}} @else Nil @endif </p>
		</div>
		
		<div class="row">
			<h1>Report Details</h1>

			<div class="table-responsive">
			   <table class="table table-stripped">
			   	<tr>
				   	<th>Test Name</th>
				   	<th>Sample Collected</th>
				   	<th>Test Result</th>
				   	<th>Measurement</th>
				   	<th>Price</th>
				   	<th>Reference</th>
				   	<!-- <th>Reference End Range</th> -->
				</tr>
			@foreach (@$receipt_data as $recipt)
			@if($recipt['test_type'] == 'single')
			<tr style="font-weight: bold">
				<td>{{ @$recipt['name'].$recipt['tests_id'] }}</td>
				<td> @if($recipt['labtest_sub_name']){{ @LabsCategory::find($recipt['labtest_sub_name'])->name }} @else Nil @endif</td>
				<td> {{ @$recipt['result'] }} </td>
				<td> {{ @$recipt['units'] }} </td>
				<td> {{ @$recipt['amount'] }} </td>
				<td> {{ @$recipt['reference_range'] }} - {{ @$recipt['reference_endrange'] }} </td>
				<!-- <td> {{ @$recipt['reference_endrange'] }} </td> -->
			</tr>
			@else
			<tr style="font-weight: bold">
				<td colspan="4">{{ @$recipt['packageName'] }}</td>
				<td colspan="3">{{ @$recipt['amount'] }}</td>
			</tr>
			@foreach($recipt['package'] as $value)
			@if($value['showName'] == 1)
				<tr style="font-weight: bold">
					<td colspan="7">{{ @$value['groupName'] }}</td>
				</tr>
				@endif
				@foreach($value['testList'] as $v)
				@if($v['testData']['name'] != '')
				<tr>
					<td style="padding-left:30px;">{{ @$v['testData']['name'].$v['tests_id']}}</td>
					<td> @if($v->labtest_sub_name){{ @LabsCategory::find($v->labtest_sub_name)->name }}@else Nil @endif</td>
					<td> {{ ($v['result']!='')?$v['result']:'-'}} </td>
					<td> {{ ($v['units']!='')?$v['units']:'-' }} </td>
					<td> - </td>
					<td> {{ $v['reference_range'] }} -  {{ $v['reference_endrange'] }} </td>
					<!-- <td> {{ $v['reference_endrange'] }} </td> -->
				</tr>
				@endif
			@endforeach
			@endforeach
			@endif
			@endforeach
		
		</table>
	</div>
				
			
		</div>
		<div class="row">
		</div>

			{{-- <div class="row">
			<div class="table-responsive">
			   <table class="table table-stripped">
			   	<tr>
			   		<td>Test Name</td>
			   		<td>{{@$labtest->getLabTest->name }}</td>
			   	</tr>
			   	<tr>
			   		<td>Raised By ({{ $labtest->whoRaised() }})</td>
			   		<td>{{ @$labtest->whoName() }}</td>
			   	</tr>
			   	<tr>
			   		<td>Test Handled In</td>
			   		<td>{{ @$labtest->getLab->name }}</td>
			   	</tr>
			   	<tr>
			   		<td>Measurement Units</td>
			   		<td>{{ @$labtest->getReport->measurement_units }}</td>
			   	</tr>
			   	<tr>
			   		<td>Result</td>
			   		<td>{{ @$labtest->getReport->test_result }}</td>
			   	</tr>
			   	<tr>
			   		<td>Result1</td>
			   		<td>{{ @$labtest->getReport->test_default1 }}</td>
			   	</tr>
			   	<tr>
			   		<td>Result2</td>
			   		<td>{{ @$labtest->getReport->test_default1 }}</td>
			   	</tr>
			   	<tr>
			   		<td>Specimen</td>
			   		<td>{{ @$labtest->getReport->specimen }}</td>
			   	</tr>


			   </table>

			</div>
		</div> --}}
		</div>
		<div class="row"> 
			<div class="col-md-8 col-sm-8">
				<div class="col-md-2">
				<form method="post" action="/admin/export">
					{!! csrf_field() !!}
					<input type="hidden" name="id" value="{{@$labtest->id }}" >
					<button formtarget="_blank" class='btn btn-success' >Receipt</button>
				</form>
				</div>
				<div class="col-md-2">
				<form method="post" action="{{route('reportgenerate')}}">
				{!! csrf_field() !!}
				<input type="hidden" name="raise_test_id" value="{{@$labtest->id }}" />
				<button formtarget="_blank" type="submit" class="btn btn-success" id="generate">Report</button>
				</form>
				</div>
				<div class="col-md-2">
				<form method="post" action="/admin/sendreportmail">
				{!! csrf_field() !!}
				<input type="hidden" name="raise_test_id" value="{{@$labtest->id }}" >
				<button class="btn btn-success" type="submit"> Send Report </button>
			    </form>
			    </div>
			    <div class="col-md-4">
			    <form method="get" action="/admin/exportexcel/{{@$labtest->id}}">
				{!! csrf_field() !!}
				<input type="hidden" name="raise_test_id" value="{{@$labtest->id }}" >
				<button class="btn btn-success" type="submit">Export Excel</button>
			    </form>
		        </div>
		</div>
	 </div> 
	</div>

	
</div>
<script >
	function exportPdf(id){
						$.ajax({
					            type: 'GET',
					            url: '/admin/export',
					            data: {id: id},
					            success: function( msg ) {
					            	window.open(msg,'_blank');
					            	$.ajax({
					            		type:'GET',
					            		url:'/admin/filedelete',
					            		data:{id:id},
					            		success:function(msg){
					            			console.log('file Deleted');
					            		}	
					            	});
					            }
        				});
	}
</script>