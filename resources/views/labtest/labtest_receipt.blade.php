<!DOCTYPE html>
<html lang="en-US" class="no-js no-svg">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
</head>
<body style="height: 100%;margin: 0 auto;font-family: 'Roboto', sans-serif;background:#f8f9fa;display: table;width: 100%;">
<div class="col-md-12">
    <div class="box">
        <div class="box-body">

<div style="padding: 40px 40px 0 40px;width:94%;float:left;">
<div style="width:100%;display: block;float: left;clear: both;">
    <form method="get" action="/admin/download">
                {!! csrf_field() !!}
                <input type="hidden" name="id" value="{{@$data['raise_test']->id }}" >
                <button class="btn btn-success" type="submit"> Download Receipt </button>
                </form>
    <div style="width: 50%;float: right;"><p style="margin: 0;text-align: right;">{{ $data['patient']->name}}  |  Payments Received  |  Livigro </p></div>
</div>  
<div style="clear: both"></div>
<div style="width:100%;display: block;float: left;clear: both;margin: 30px 0 0 0;">
    <div style="width: 50%;float: left;">
        <div><h3 style="display: inline-block;font-weight: 600;color: #1a9fc5">POWERED <span style="color: #dd3e7d">BY</span></h3></div>
        <img src="{{url('/').'/images/logo.png'}}" style="width: 80px;margin:0px 0 0 0;">
    </div>
    <div style="width: 50%;float: left;text-align: right;">
        <div style="float: right;">
        <strong style="font-size: 18px;float: right; width: 100%;">Receipt No : {{ $data['raise_test']->receipt_no }}</strong>
        <ul style="list-style-type: none;margin: 5px 0px 0 0px;padding: 0; float: right;">
            <li style="padding: 3px 0px;display:block; font-size: 16px;"><span style="position: relative;top:-5px;"> <strong style="font-size: 16px;float: right; width: 100%;">Lab ID : {{ $data['lab_data']->id }}</strong></span></li>
            <li style="padding: 3px 0px;display:block; font-size: 16px;"><span style="position: relative;top:-5px;">@if($data['lab_data']->name){{ $data['lab_data']->name }}@else Nil @endif</span></li>
            <?php if($data['lab_data']->email != '' || $data['lab_data']->email != null){ ?>
            <li style="padding: 3px 0px;display:block; font-size: 16px;"><img src="{{url('/').'/images/ph_icon.png'}}" > <span style="position: relative;top:-5px;">{{ $data['lab_data']->email }}</span></li>
        <?php } ?>
        <?php if($data['lab_data']->mobile != '' || $data['lab_data']->mobile != null){ ?>
            <li style="padding: 3px 0px;display:block; font-size: 16px;"><img src="{{url('/').'/images/ph_icon1.png'}}" > <span style="position: relative;top:-5px;">{{ $data['lab_data']->mobile }}</span></li>
        <?php } ?>
            <?php if($data['lab_data']->phone != '' || $data['lab_data']->phone != null){ ?>
            <li style="padding: 3px 0px;display:block; font-size: 16px;"><img src="{{url('/').'/images/ph_icon2.png'}}" > <span style="position: relative;top:-5px;">@if($data['lab_data']->phone){{ $data['lab_data']->phone }}@else Nil @endif </span></li>
            <?php } ?>
        </ul>
    </div>
</div>
</div>
<div style="clear: both"></div>

<div style="border:2px solid #000;margin: 15px 0 15px 0;"></div>

<div style="text-align: center;display:block;float:left;width:100%;">
    <h2 style="font-size: 20px;text-transform: uppercase;width:40%; margin:0 auto;border-bottom: 1px solid #676565;font-family: 'Roboto', sans-serif;">Payment receipt</h2>
</div>
<div style="clear: both"></div>
<div style="width: 100%;margin-top: 20px;float:left;">
    <div style="width: 58%;float: left;">
        <div style="width: 100%;float: left;">
            <div style="padding: 4px 0;font-size: 18px;float:left;font-weight:normal;margin-bottom:0px;width:40%;">Payment Date </div>
            <div style="padding: 4px 0;font-size: 18px;font-weight:bold;float:left;display:inline-block;border-bottom:1px solid #999;margin-bottom:0px;width:58%;">: {{ $data['raise_test']->receipt_date }}</div>            
        </div>
        <div style="width: 100%;float: left;">
            <div style="padding: 4px 0;font-size: 18px;float:left;font-weight:normal;margin-bottom:0px;width:40%;">Order ID </div>
            <div style="padding: 4px 0;font-size: 18px;font-weight:bold;float:left;display:inline-block;border-bottom:1px solid #999;margin-bottom:0px;width:60%;">: {{ $data['raise_test']->id }}</div>            
        </div>
        <div style="width: 100%;float: left;">
            <div style="padding: 4px 0;font-size: 18px;float:left;font-weight:normal;margin-bottom:0px;width:40%;">Payment Mode </div>
            <div style="padding: 4px 0;font-size: 18px;font-weight:bold;float:left;display:inline-block;border-bottom:1px solid #999;margin-bottom:0px;width:60%;">: {{ isset($data['raise_test']->mode_of_payment)?$data['raise_test']->mode_of_payment:'Cash' }}</div>            
        </div>

    </div>
    <div style="width: 40%;float: left;text-align: center;padding-top: 5px;">
        <h3 style="margin:0;margin-bottom:15px;">Amount Received</h3>
        <h2 style="margin:0;color: #1a9fc5;font-weight: 600;">₹{{ $data['raise_test']->received_amount }}</h2>
    </div>
</div>
<div style="clear: both"></div>
<div style="margin-top: 10px;float:left;width:100%;">
    <h1 style="margin: 10px 0 0 0;font-size: 20px;font-weight: 600;color: #0e475a;float:left;width:100%;">{{ $data['patient']->name}}</h1>
     <?php
        $gender = $address = '';
        if($data['patient']->gender == '1')
            $gender = 'Male';
        else if($data['patient']->gender == '1')
            $gender = 'Female';
        else
            $gender = "Others";
        if($data['patient']->address != '')
            $address .= $data['patient']->address.',';
        else if($data['patient']->city != '')
            $address .= $data['patient']->city.',';
        else if($data['patient']->state != '')
            $address .= $data['patient']->state.',';
        else if($data['patient']->zip != '')
            $address .= $data['patient']->zip.'.';
    ?>
    <h3 style="margin: 0;font-size: 16px;font-weight: 200;color: #1a9fc5;float:left;width:100%;">{{ $data['patient']->age() }} Years, {{ $gender }},</h3>
    <p style="margin: 0;font-size: 16px;font-weight: 200;float:left;width:100%;">{{ $address }}</p>
    <h5  style="margin: 0;font-size: 16px;font-weight: 200;color: #de3f7e;float:left;width:100%;">Mobile No: {{ $data['patient']->phone }}</h5>

</div>
<div style="clear: both"></div>
<div style="margin-top: 30px;float:left;width:100%;">
    <div style="width:100%;float:left;margin-bottom:30px;">
        <table style="width:100%; " cellspacing="0"  cellpadding='0'  repeat_header='1'>
        <thead >
            <tr style="padding: 5px 10px;background: #1a9fc5;color: #fff;">
            <td style="width:70%; padding: 5px 10px; color: #fff; font-size:15px; font-weight:bold;">Test Names</td>
            <td style="width:30%: padding: 5px 10px; color: #fff; font-size:15px; font-weight:bold;">Rate</td>
            </tr>
        </thead>
        <tbody>
            @foreach($data['receipt_data'] as $key => $value)
            @if($value['test_type'] == 'single')
                <tr style="background-color: #fff;">
                    <td style="width:70%; padding: 5px 10px; color: #000; font-size:14px; font-weight:bold; ; border: 1px solid #ddd;">{{ $value['name']}}</td>
                    <td style="width:30%; padding: 5px 10px; color: #000; font-size:14px; font-weight:bold; border: 1px solid #ddd;">₹{{ $value['amount'] }}</td>
                </tr>
                <tr style="background-color: #fff; padding:5px;" ><td style="padding:5px;" colspan="2"></td></tr>
            @else
                <tr>
                    <td style="width:70%; padding: 5px 10px; color: #000; font-size:14px; font-weight:bold; ; border: 1px solid #ddd;">{{ $value['packageName']}}</td>
                    <td style="width:30%; padding: 5px 10px; color: #000; font-size:14px; font-weight:bold; border: 1px solid #ddd;">₹{{ $value['amount'] }}</td>
                </tr>
                @foreach($value['package'] as $val)
                @if($val['showName'] == 1)
                <tr>
                    <td colspan="2" style="padding: 10px 15px 10px 5px;color: #000;font-weight:bold; font-size:14px; border:1px solid #ddd; border-top:none; border-bottom:none;"> 

                    {{ $val['groupName']}}</td>
                </tr>
                <?php /*@else
                <tr>
                    <td colspan="4" style="padding: 10px 15px 10px 5px;color: #000;font-weight:bold; font-size:14px;"></td>
                </tr>*/ ?>
                @endif
                @foreach($val['testList'] as $v)
                @if($v['testData']['name'] != '')
                    <tr style="background-color: #fff;">
                    <td style="width:70%; padding: 5px 10px; color: #000; font-size:14px; font-weight:normal;border: 1px solid #ddd;">{{ $v['testData']['name']}}</td>
                    
                    <td style="width:30%; padding: 5px 10px; color: #000; font-size:14px; font-weight:normal;border: 1px solid #ddd; ">
                     -
                    </td>
                </tr>
                @endif
                @endforeach
                @endforeach
                <tr style="background-color: #fff; padding:5px;" ><td style="padding:5px;" colspan="2"></td></tr>
            @endif
            @endforeach
                <tr>
                    <td style="width:70%; padding: 5px 10px; color: #000; font-size:14px; font-weight:bold; ; border: 1px solid #ddd;">Sub total</td>
                    <td style="width:30%; padding: 5px 10px; color: #000; font-size:14px; font-weight:bold; border: 1px solid #ddd;">₹{{ $data['raise_test']->original_amount }}</td>
                </tr>
                @if($data['raise_test']->discount_price != 0)
                <tr>
                    <td style="width:70%; padding: 5px 10px; color: #000; font-size:14px; font-weight:bold; ; border: 1px solid #ddd;"> Discount Amount</td>
                    <td style="width:30%; padding: 5px 10px; color: #000; font-size:14px; font-weight:bold; border: 1px solid #ddd;">₹{{ $data['raise_test']->discount_price }}</td>
                </tr>
                @endif
                <tr>
                    <td style="width:70%; padding: 5px 10px; color: #000; font-size:14px; font-weight:bold; ; border: 1px solid #ddd;">Total</td>
                    <td style="width:30%; padding: 5px 10px; color: #000; font-size:14px; font-weight:bold; border: 1px solid #ddd;">₹{{ $data['raise_test']->received_amount }}</td>
                </tr>

        </tbody>
        </table>
       
    </div>

</div>

<div style="clear: both"></div>

</div>
</div> <!-- box-body -->
</div> <!-- box -->
</div>
</body>
</html>