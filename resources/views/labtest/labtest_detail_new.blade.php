<html>
<link rel="stylesheet" href="http://belongcare.com/vendor/laravel-admin/AdminLTE/bootstrap/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="http://belongcare.com/assets/vendors/select2/css/select2.min.css"/>
    <link type="text/css" rel="stylesheet" href="http://belongcare.com/assets/css/pages/tables.css"/>
    <link type="text/css" rel="stylesheet" href="http://belongcare.com/assets/css/pages/dataTables.bootstrap.css"/>
<body>
<div class="contanier">
<div class="panel panel-default">
	<div class="panel-heading">
		<strong>Report</strong>
	</div>
	<div class="pull-right">
		<form method="get" action="{{ route('export') }}">
			{{ csrf_field() }}
			<input type="hidden" name="id" value="{{$labtest->id}}" />
			<button id="download_report" class="btn btn-success">Download</button>
		</form>
	</div>
	<div class="panel-body">
		<div class="box-body">
	<div class="row">
			<div class="col-md-8 col-sm-8">
				<h4><strong>Patient Details</strong></h4>
				 <div class="col-md-4">
				  @if( $labtest->getPatient->profile_image!="") 
					<img src="/storage/{{$labtest->getPatient->profile_image}}"" class="img-responsive" style="max-width:200px;">
					@else
					<img src="" />
					@endif
				</div> 
				<div class="col-md-4">
					<p>Name: @if( @$labtest->getPatient->name) {{  @$labtest->getPatient->name}} @else - @endif</p>
					<p>Mobile: @if( @$labtest->getPatient->mobile) {{  @$labtest->getPatient->mobile}} @else - @endif</p>
					<p>Email: @if( @$labtest->getPatient->email) {{  @$labtest->getPatient->email}} @else - @endif</p>
					<p>Blood Group: @if( @$labtest->getPatient->blood_group) {{  @$labtest->getPatient->blood_group }} @else Nil @endif</p>
					<p>Date of Birth: @if( @$labtest->getPatient->dob) {{  @$labtest->getPatient->dob }} @else - @endif</p>
					<p>Address: @if( @$labtest->getPatient->city)  {{  @$labtest->getPatient->area}} {{  @$labtest->getPatient->address}} {{  @$labtest->getPatient->city}} {{  @$labtest->getPatient->state}} @else - @endif</p> 
				</div>
			</div>
			<div class="col-md-4 col-sm-4">
			<h4> <strong>Lab Details</strong> </h4>
			<p>Name: @if(@$labtest->getLab->name){{@$labtest->getLab->name }} @else Nil @endif</p>
			<p>Mobile: @if(@$labtest->getLab->mobile){{@$labtest->getLab->mobile }} @else - @endif</p>
			<p>Address: @if(@$labtest->getLab->area || @$labtest->getLab->address || @$labtest->getLab->city || @$labtest->getLab->state){{@$labtest->getLab->area}} {{@$labtest->getLab->address}} {{@$labtest->getLab->city}} {{@$labtest->getLab->state}} @else - @endif</p>
			
			</div>
		</div>
		<div class="row">
			<p>Diagonis : @if(@$labtest->diagonsis) {{@$labtest->diagonsis}} @else - @endif</p>
			<p>Notes :@if(@$labtest->notes) {{@$labtest->notes}} @else - @endif </p>
		</div>
		
		<div class="row">
			<h1>Receipt Details</h1>

			<div class="table-responsive">
			   <table class="table table-stripped">
			   	<tr>
				   	<th>Test Name</th>
				   	<th>Sample Data</th>
				   	<th>Test Result</th>
				   	<th>Measurement</th>
				   	<th>Price</th>
				   	<th>Reference Start Range</th>
				   	<th>Reference End Range</th>
				</tr>
				
			@foreach ($labtest->getReciept()->get() as $recipt)
			<tr style="text-align: center;">
				<td>{{ @$recipt->testData->name }}</td>
				<td> {{@$recipt->subcatName->name}} </td>
				<td> {{ @$recipt->result}} </td>
				<td> {{ @$recipt->units }} </td>
				<td> {{ @$recipt->amount}} </td>
				<td> {{ @$recipt->reference_range}} </td>
				<td> {{ @$recipt->reference_endrange}} </td>
			</tr>
			@endforeach
		
		</table>
	</div>	
			
		</div>
		<div class="row">
		</div>

			{{-- <div class="row">
			<div class="table-responsive">
			   <table class="table table-stripped">
			   	<tr>
			   		<td>Test Name</td>
			   		<td>{{$labtest->getLabTest->name }}</td>
			   	</tr>
			   	<tr>
			   		<td>Raised By ({{ $labtest->whoRaised() }})</td>
			   		<td>{{ $labtest->whoName() }}</td>
			   	</tr>
			   	<tr>
			   		<td>Test Handled In</td>
			   		<td>{{ $labtest->getLab->name }}</td>
			   	</tr>
			   	<tr>
			   		<td>Measurement Units</td>
			   		<td>{{ $labtest->getReport->measurement_units }}</td>
			   	</tr>
			   	<tr>
			   		<td>Result</td>
			   		<td>{{ $labtest->getReport->test_result }}</td>
			   	</tr>
			   	<tr>
			   		<td>Result1</td>
			   		<td>{{ $labtest->getReport->test_default1 }}</td>
			   	</tr>
			   	<tr>
			   		<td>Result2</td>
			   		<td>{{ $labtest->getReport->test_default1 }}</td>
			   	</tr>
			   	<tr>
			   		<td>Specimen</td>
			   		<td>{{ $labtest->getReport->specimen }}</td>
			   	</tr>


			   </table>

			</div>
		</div> --}}
		</div>
	
	</div>

	
</div>
</div>

    <script src="http://belongcare.com/vendor/laravel-admin/AdminLTE/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script src="http://belongcare.com/vendor/laravel-admin/AdminLTE/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>