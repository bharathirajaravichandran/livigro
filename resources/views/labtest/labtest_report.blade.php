<!DOCTYPE html>
<html lang="en-US" class="no-js no-svg">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">

</head>
<body style="height: 100%;margin: 0 auto;font-family: 'Roboto', sans-serif;background:#fff;display: table;width: 100%;">
	<div class="col-md-12">
    <div class="box">
        <div class="box-body">
<div style="float:left;width:95%;padding: 10px 40px 0 40px;">	
<div style="width:100%;display: block;float: left;clear: both;">
	<div style="width: 50%;float: left;">
		<form method="post" action="/admin/reportdownload">
                {!! csrf_field() !!}
                <input type="hidden" name="raise_test_id" value="{{@$data['raise_test']->id }}" >
                <button class="btn btn-success" type="submit"> Download Report </button>
                </form>
		<div><h3 style="display: inline-block;font-weight: 600;color: #1a9fc5">POWERED <span style="color: #dd3e7d">BY</span></h3></div>
		<img src="{{url('/').'/images/logo.png'}}" style="max-width: 80px;margin: 0px 0 0 0;">
	</div>
	@if(isset($data['lab_data']))
	<div style="width: 50%;float: left;text-align: right;">
		<div style="float: right;">
		<ul style="list-style-type:  none;margin: 5px 0px 0 0px;padding: 0;float: right;">
			<li style="padding: 3px 0px;display:block;"><span style="position: relative;top:-5px;">@if($data['lab_data']->name){{ $data['lab_data']->name }}@else Nil @endif</span></li>
			<?php if($data['lab_data']->email != '' || $data['lab_data']->email != null){ ?>
			<li style="padding: 3px 0px;display:block;"><img src="{{url('/').'/images/ph_icon.png'}}" > <span style="position: relative;top:-5px;">{{ $data['lab_data']->email }}</span></li>
		<?php } ?>
		<?php if($data['lab_data']->mobile != '' || $data['lab_data']->mobile != null){ ?>
			<li style="padding: 3px 0px;display:block;"><img src="{{url('/').'/images/ph_icon1.png'}}" > <span style="position: relative;top:-5px;"> {{ $data['lab_data']->mobile }} </span></li>
		<?php } ?>
			<?php if($data['lab_data']->phone != '' || $data['lab_data']->phone != null){ ?>
			<li style="padding: 3px 0px;display:block;"><img src="{{url('/').'/images/ph_icon2.png'}}" > <span style="position: relative;top:-5px;">{{ $data['lab_data']->phone }} </span></li>
		<?php } ?>
		</ul>
	</div>
	</div>
	@endif
</div>
 <?php
        $gender = $address = '';
        if($data['patient']->gender == '1')
            $gender = 'Male';
        else if($data['patient']->gender == '0')
            $gender = 'Female';
        else
            $gender = "Others";
        if($data['patient']->address != '')
            $address .= $data['patient']->address.',';
        else if($data['patient']->city != '')
            $address .= $data['patient']->city.',';
        else if($data['patient']->state != '')
            $address .= $data['patient']->state.',';
        else if($data['patient']->zip != '')
            $address .= $data['patient']->zip.'.';
    ?>
<div style="clear: both"></div>
<div style="width:100%;display: block;float: left;margin-top: 20px;">
		<div style="width: 50%;float: left;">
			<div style="width: 100%;float: left;margin-bottom:10px;">
				<div style="padding: 0px 0;font-size: 14px;float:left;font-weight:bold;margin-bottom:0px;width:140px;">Patient's Name</div>
				<div style="padding: 0px 0;font-size: 14px;float:left;border-bottom:0px;font-weight:normal;margin-bottom:0px;width:48%;">: {{ $data['patient']->name}}</div>			
			</div>
			<div style="width: 100%;float: left;margin-bottom:10px;">
				<div style="padding: 0px 0;font-size: 14px;float:left;font-weight:bold;margin-bottom:0px;width:140px;">Patient's ID </div>
				<div style="padding: 0px 0;font-size: 14px;float:left;border-bottom:0px;font-weight:normal;margin-bottom:0px;width:48%;">: {{ $data['patient']->id}}</div>			
			</div>			
			<div style="width: 100%;float: left;margin-bottom:10px;">
				<div style="padding: 0px 0;font-size: 14px;float:left;font-weight:bold;margin-bottom:0px;width:140px;">Age & Sex	</div>
				<div style="padding: 0px 0;font-size: 14px;float:left;border-bottom:0px;font-weight:normal;margin-bottom:0px;width:48%;">: {{ $data['patient']->age() }} Years, {{ $gender }}</div>	
			</div>
			<div style="width: 100%;float: left;margin-bottom:10px;">
				<div style="padding: 0px 0;font-size: 14px;float:left;font-weight:bold;margin-bottom:0px;width:140px;">Ref by Dr	</div>
				<div style="padding: 0px 0;font-size: 14px;float:left;border-bottom:0px;font-weight:normal;margin-bottom:0px;width:48%;">: {{ isset($data['raise_test']->doctor_referer_name)?$data['raise_test']->doctor_referer_name: 'Self' }}</div>		
			</div>
		</div>
		@if(isset($data['lab_data']))
		<div style="width: 50%;float: right;text-align: left;">
			<div style="width: 100%;float: left;margin-bottom:10px;">
				<div style="padding: 0px 0;font-size: 14px;float:left;font-weight:bold;margin-bottom:0px;width:140px;">Lab ID </div>
				<div style="padding: 0px 0;font-size: 14px;float:left;border-bottom:0px;font-weight:normal;margin-bottom:0px;width:48%;">: {{ $data['lab_data']->id }}</div>			
			</div>
			
		</div>
		@endif
		<div style="width: 50%;float: right;text-align: left;">
			<div style="width: 100%;float: left;margin-bottom:10px;">
				<div style="padding: 0px 0;font-size: 14px;float:left;font-weight:bold;margin-bottom:0px;width:140px;">Collection Date </div>
				<div style="padding: 0px 0;font-size: 14px;float:left;border-bottom:0px;font-weight:normal;margin-bottom:0px;width:48%;">: {{ $data['raise_test']->collection_date }}</div>			
			</div>
			
		</div>
		<div style="width: 50%;float: right;text-align: left;">
			<div style="width: 100%;float: left;margin-bottom:10px;">
				<div style="padding: 0px 0;font-size: 14px;float:left;font-weight:bold;margin-bottom:0px;width:140px;">Report Date </div>
				<div style="padding: 0px 0;font-size: 14px;float:left;border-bottom:0px;font-weight:normal;margin-bottom:0px;width:48%;">: {{ $data['raise_test']->report_date }}</div>			
			</div>
			
		</div>
</div>
<div style="clear: both"></div>
<div style="border:2px solid #000;margin: 15px 0 15px 0;float:left;width:100%;"></div>
	<table style="width:100%; " cellspacing="0"  cellpadding='0'  repeat_header='1'>
		
		<thead >
			<tr style="padding: 5px 10px;background: #1a9fc5;color: #fff;">
			<td style="width:30%; padding: 5px 10px; color: #fff; font-size:15px; font-weight:bold;">Test Names</td>
			<td style="width:25%; padding: 5px 10px; color: #fff; font-size:15px; font-weight:bold;">Analysed Result</td>
			<td style="width:20%: padding: 5px 10px; color: #fff; font-size:15px; font-weight:bold;">Units</td>
			<td style="width:25%: padding: 5px 10px; color: #fff; font-size:15px; font-weight:bold;">Normal Range</td>
		</tr>

		</thead>
		<tbody>
			@foreach($data['receipt_data'] as $key => $value)
			@if($value['test_type'] == 'single')
				<tr style="background-color: #fff;">
					<td style="width:35%; padding: 5px 10px; color: #000; font-size:14px; font-weight:bold; ; border: 1px solid #ddd;">{{ $value['name']}}</td>
					<td style="width:25%; padding: 5px 10px; color: #000; font-size:14px; font-weight:bold; border: 1px solid #ddd; border-left:none; ">{{ $value['result']}}</td>
					<td style="width:15%; padding: 5px 10px; color: #000; font-size:14px; font-weight:bold; border: 1px solid #ddd;border-left:none; ">{{ $value['units']}}</td>
					<td style="width:25%; padding: 5px 10px; color: #000; font-size:14px; font-weight:bold; border: 1px solid #ddd;">{{ $value['reference_range'].' - '.$value['reference_endrange']  }}</td>
				</tr>
				<tr style="background-color: #fff; padding:5px;" ><td style="padding:5px;" colspan="4"></td></tr>
			@else
				<tr>
					<td colspan="4" style="padding: 10px 15px 10px 5px;color: #000;font-weight:bold; border:1px solid #ddd; font-size:14px;">{{ $value['packageName']}}</td>
				</tr>
				@foreach($value['package'] as $val)
				@if($val['showName'] == 1)
				<tr>
					<td colspan="4" style="padding: 10px 15px 10px 5px;color: #000;font-weight:bold; font-size:14px; border:1px solid #ddd; border-top:none; border-bottom:none;"> 

					{{ $val['groupName']}}</td>
				</tr>
				<?php /*@else
				<tr>
					<td colspan="4" style="padding: 10px 15px 10px 5px;color: #000;font-weight:bold; font-size:14px;"></td>
				</tr>*/ ?>
				@endif
				@foreach($val['testList'] as $v)
				@if($v['testData']['name'] != '')
					<tr style="background-color: #fff;">
					<td style="width:35%; padding: 5px 10px; color: #000; font-size:14px; font-weight:normal;border: 1px solid #ddd;">{{ $v['testData']['name']}}</td>
					<td style="width:25%; padding: 5px 10px; color: #000; font-size:14px; font-weight:normal;border: 1px solid #ddd;border-left:none;">{{ ($v['result']!='')?$v['result']:'-'}}</td>
					<td style="width:15%; padding: 5px 10px; color: #000; font-size:14px; font-weight:normal;border: 1px solid #ddd;border-left:none;">
					  {{ ($v['units']!='-nill-' && $v['units']!='') ? $v['units']: ' ' }}

					  <?php  $unit_value = ' ';
					       if($v['units'] == '')
					           {
					             $unit_value = ' ';
					           }elseif($v['units'] == '-nill-'){
                                $unit_value = ' ';
					       }elseif($v['units'] !=0 && $v['units'] != ''){
                                $unit_value = $v['units'];
					       }
					  ?> 
                         {{ $unit_value }}

					  </td>
					<td style="width:25%; padding: 5px 10px; color: #000; font-size:14px; font-weight:normal;border: 1px solid #ddd; ">
					 <?php  $range_value = ' ';
					      if($v['reference_range'] ==0 && $v['reference_endrange'] ==0)
					           {
					             $range_value = ' ';
					           }elseif($v['reference_range'] !=0 && $v['reference_endrange'] != ''){
                                $range_value = $v['reference_range'].' - '.$v['reference_endrange'] ;
					       }elseif($v['reference_endrange'] ==0){
					       	    if($v['reference_range'] ==0){
					       	    	$range_value = ' ';
					       	     }else{ 
					       	     	$range_value = $v['reference_range'] ; 
					       	     }                                
					       }
					  ?>   {{ $range_value }}
					</td>
				</tr>
				@endif
				@endforeach
				@endforeach
				<tr style="background-color: #fff; padding:5px;" ><td style="padding:5px;" colspan="4"></td></tr>
			@endif
			@endforeach
		</tbody>
	</table>



		
	
	</div>

	<div style="width:100%;float:left;margin-bottom:10px; margin-top:30px;">
<h5 style="font-weight: 600;font-size: 16px;text-align: center;">KINDLY CORRELATE CLINICALLY,IF NECESSARY DISCUSS/REPEAT.</h5>
<p style="text-align: center;font-size: 14px;line-height: 1.8;padding: 0px 30px;">References range and unit measurements can vary per lab. And what is “normal” can also vary per person and race, if your lab results are outside the normal range. We suggest that you discuss with your doctor.Often it is not the result, but the change from a previous test that is most instructive. For best comparision of lab result, the tests should be done in the same lab.</p>
	</div>
	


</div>

</div> <!-- box-body -->
</div> <!-- box -->
</div>
</body>
</html>