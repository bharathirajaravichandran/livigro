<div>
	<div>
		<strong>Report</strong>
	</div>
	<div>
		<h4>Patient Details</h4>
		<div>
			<div>
				 <div>
					<img src="public/favicon.ico">
				</div>
					<p>{{ $labtest->getPatient->name}}</p>
					<p>{{ $labtest->getPatient->mobile}}</p>
					<p> {{ $labtest->getPatient->blood_group}} </p>
					<p> {{ $labtest->getPatient->dob}} </p>
					<p>{{ $labtest->getPatient->city}} </p> 
				</div>
			</div>
			<div >
				<p>Order Id : {{$labtest->id }}</p>
				<p>Patient Id: {{$labtest->getPatient->id}}

			</div>
		</div>
		<div>
			<p>Diagonis : {{$labtest->diagonsis}} </p>
			<p>Notes : {{$labtest->notes}} </p>
		</div>
		<div>
			<h1> Lab Details </h1>
			<p>Name: {{$labtest->getLab->name }}</p>
			<p>Mobile: {{$labtest->getLab->mobile }}</p>
			<p>City: {{$labtest->getLab->city }}</p>
			<p>Address: {{$labtest->getLab->address}}</p>
		</div>
		<div>
			<h1>Sample Details</h1>

			<div>
			   <table>
			   	<tr>
				   	<th>Test Name</th>
				   	<th>Sample Data</th>
				   	<th>Test Result</th>
				   	<th>Measurement</th>
				   	<th>Price</th>
				   	<th>Reference Start Range</th>
				   	<th>Reference End Range</th>
				</tr>
			@foreach ($labtest->getReciept()->get() as $recipt)
			<tr>
				<td>{{ $recipt->testData->name }}</td>
				<td> {{$recipt->subcatName->name}} </td>
				<td> {{ $recipt->result}} </td>
				<td> {{ $recipt->units }} </td>
				<td> {{ $recipt->amount}} </td>
				<td> {{ $recipt->reference_range}} </td>
				<td> {{ $recipt->reference_endrange}} </td>
			</tr>
			@endforeach
		</table>
	</div>
</div>				
