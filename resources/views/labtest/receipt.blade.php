<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
</head>
<body style="height: 100%;margin: 0 auto;font-family: 'Roboto', sans-serif;background:#f8f9fa;display: table;width: 100%;">
<div style="padding: 40px 40px 0 40px;">
<div style="width:100%;display: block;float: none;clear: both;">
	<div style="width: 50%;float: left;"><p style="margin: 0">21 July 2108</p></div>
	<div style="width: 50%;float: left;"><p style="margin: 0;text-align: right;">Stephen R  |  Payments Received  |  Livigro </p></div>
</div>	
<div style="clear: both"></div>
<div style="width:100%;display: block;float: none;clear: both;margin: 30px 0 0 0">
	<div style="width: 50%;float: left;"><img src="logo.png" style="width: 200px;margin:20px 0 0 0;"></div>
	<div style="width: 50%;float: left;text-align: right;">
		<div style="float: right;">
		<strong style="font-size: 20px;">Order ID : 5752</strong>
		<ul style="list-style-type:  none;margin: 5px 0px 0 0px;padding: 0">
			<li style="padding: 3px 0px;"><img src="ph_icon.png" > <span style="position: relative;top:-5px;">care@brookaven.com</span></li>
			<li style="padding: 3px 0px;"><img src="ph_icon1.png" > <span style="position: relative;top:-5px;"> +91 8825978288 </span></li>
			<li style="padding: 3px 0px;"><img src="ph_icon2.png" > <span style="position: relative;top:-5px;">0452-24124125 </span></li>
		</ul>
	</div>
</div>
</div>
<div style="clear: both"></div>

<div style="border:2px solid #000;margin: 15px 0 15px 0;"></div>

<div style="text-align: center;">
	<h2 style="font-size: 20px;text-transform: uppercase;display: inline-block;border-bottom: 1px solid #676565">Payment receipt</h2>
</div>
<div style="clear: both"></div>
<div style="width: 100%;margin-top: 40px;">
	<div style="width: 50%;float: left;">
		<table style="width: 60%;">
			<tr>
				<td style="padding: 8px 0;font-size: 18px;">Payment Date</td>
				<th align="left" style="padding: 8px 0 0 0;border-bottom: 1px solid #ccc;font-size: 18px;">18 JULY 2018</th>
			</tr>
			<tr>
				<td style="padding: 8px 0;font-size: 18px;">Order ID</td>
				<th align="left" style="padding: 8px 0 0 0;border-bottom: 1px solid #ccc;font-size: 18px;">5752</th>
			</tr>
			<tr>
				<td style="padding: 8px 0;font-size: 18px;">Payment Mode</td>
				<th align="left" style="padding: 8px 0 0 0;border-bottom: 1px solid #ccc;font-size: 18px;">Cash</th>
			</tr>
		</table>
	</div>
	<div style="width: 50%;float: left;text-align: center;padding-top: 40px;">
		<h3 style="margin:0;">Amount Received</h3>
		<h2 style="margin:0;color: #1a9fc5;font-weight: 600;">$ 642.00</h2>
	</div>
</div>
<div style="clear: both"></div>
<div style="margin-top: 40px;">
	<h4 style="margin: 0;font-size: 20px;font-weight: 600;">Bill To</h4>
	<h1 style="margin: 10px 0 0 0;font-size: 30px;font-weight: 600;color: #0e475a;">Stephen R</h1>
	<h3 style="margin: 0;font-size: 20px;font-weight: 200;color: #1a9fc5">51 Years, Male,</h3>
	<p style="margin: 0;font-size: 20px;font-weight: 200;">3/12, Ayyanan Street, Keelaiyur Post,Melur Taluk. Madurai District-625106</p>
	<h5  style="margin: 0;font-size: 20px;font-weight: 200;color: #de3f7e">Mobile No: +91 88259 78288</h5>

</div>
<div style="clear: both"></div>
<div style="margin-top: 40px;">

	<table style="width: 100%;border-collapse: collapse;margin-bottom: 30px;">
		<thead>
		   <tr>
			<th align="left" width="70%" style="padding: 10px 15px 10px 20px;background-color: #1a9fc5;color: #fff;border-right: 1px solid #fff;">Test Names</th>
			<th align="center" style="padding: 10px 15px 10px 20px;background-color: #1a9fc5;color: #fff;border-right: 1px solid #fff">Rate</th>
			
		   </tr>
		</thead>
		<tbody>
			<tr style="border: 1px solid #ccc;">
				<td style="padding: 10px 15px 10px 20px;border-right: 1px solid #ccc;font-weight: 600">Haemoglobin</td>
				<td style="padding: 10px 15px 10px 20px;border-right: 1px solid #ccc;font-weight: 600" align="center">$ 150.00</td>
				
			</tr>
			
			<tr style="border: 1px solid #ccc;">
				<td style="padding: 10px 15px 10px 20px;border-right: 1px solid #ccc;font-weight: 600">Total WBCs Count</td>
				<td style="padding: 10px 15px 10px 20px;border-right: 1px solid #ccc;font-weight: 600" align="center">$ 300.00</td>
				
			</tr>
			
			<tr style="    border: 1px solid #ccc;">
				<td style="padding: 10px 15px 10px 20px;border-right: 1px solid #ccc;font-weight: 600">Neutrophils</td>
				<td style="padding: 10px 15px 10px 20px;border-right: 1px solid #ccc;font-weight: 600" align="center">$ 150.00</td>
				
			</tr>

		
			
		</tbody>
	</table>
</div>

<div>
	<img src="footer_logo.jpg" style="width: 80px;margin-top: 80px;">
</div>
<div style="clear: both"></div>
</div>
<div>
	<div style="background-color: #dbdad9;min-height: 30px;padding: 10px 40px 0 40px;margin-top: 10px;">
		<div style="width: 50%;float: left;"><p style="margin: 0"><strong>www.livigro.com</strong></p></div>
		<div style="width: 50%;float: left;"><p style="margin: 0;text-align: right;">+91 8098094888</p></div>
</div>
</div>
</body>
</html>