<?php
//echo "<pre>";
//print_r($patientsList);?>    
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                </div>
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>ID </th>
                                <th>Patient Name</th>
                                <th>Gender</th>
                                <th>Blood Group</th>
                                <th>Phone number</th>
                                <th>Email</th>
                                <th>City</th>
                                <th>State</th>
                                <th>Status</th>
                                <th>Test id</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($patientsList as $patient)
                                <?php
                                $gender =  '';
                                if($patient->gender == '1')
                                    $gender = 'Male';
                                else if($patient->gender == '0')
                                    $gender = 'Female';
                                else
                                    $gender = "Others";
                                ?>
                                    <tr>
                                        <td>{{ $patient->id }}</td>
                                        <td>{{ $patient->name }}</td>
                                        <td>{{ $gender }}</td>
                                        <td>{{ DB::table('blood_group')->where('id', $patient->blood_group)->pluck('blood_group')->first() }}</td>
                                        <td>{{ $patient->phone }}</td>
                                        <td>{{ $patient->email }}</td>
                                        <td>{{ $patient->city }}</td>
                                        <td>{{ $patient->state }}</td>
                                        <td>{{ $patient->status }}</td>
                                        <td> <?php
                                            $patientTestArr = str_replace(array('{','}'), '', $patient->test_name);
                                            $testList = explode(',',$patientTestArr);
                                            
                                            $data = array();
                                            foreach($testList as $v){
                                                if($v != 'NULL')
                                                    $data[] = '<a href="raisetest/'.$v.'/show" title="View">'.$v.'</a> ';
                                                else
                                                    $data[] = 'Nil';
                                            }
                                            echo implode(', ', $data);
                                            ?>
                                                
                                            </td>
                                        <td><a href="patients/{{ $patient->id }}/edit" title="View"><i class="fa fa-edit"></i></a></td>
                                        
                                    </tr>
                                
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

   
 				 
	