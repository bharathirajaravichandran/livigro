<html>
<head>
	  <link rel="stylesheet" href="{{ admin_asset("/vendor/laravel-admin/AdminLTE/bootstrap/css/bootstrap.min.css") }}">
	  <script src="{{ admin_asset ("/vendor/laravel-admin/AdminLTE/plugins/jQuery/jQuery-2.1.4.min.js") }}"></script>
    <script src="{{ admin_asset ("/vendor/laravel-admin/AdminLTE/bootstrap/js/bootstrap.min.js") }}"></script>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="card" style="width: auto;">
				  <div class="card-body">
				    <h5 class="card-title">DataBase Error</h5>
				    <p class="card-text">Please Contact the Hosting Provider</p>
				  </div>
			</div>
		</div>
	</div>
</body>
</html>