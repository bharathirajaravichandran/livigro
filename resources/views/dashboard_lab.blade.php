<!-- <script type="text/javascript" src="{{asset('assets/vendors/slimscroll/js/jquery.slimscroll.min.js')}}"></script> -->

	 <link type="text/css" rel="stylesheet" href="{{asset('assets/css/components.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/custom.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/layouts.css')}}" />
    <!-- <link type="text/css" rel="stylesheet" href="#" id="skin_change"/> -->
	 <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/c3/css/c3.min.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/toastr/css/toastr.min.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/switchery/css/switchery.min.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/new_dashboard.css')}}"/>

      <div class="outer">
        <div class="inner bg-container">
            <div class="row">
            	  <div class="col-md-3 col-12">
                    <a style="color:white;" href="{{ url('/admin/').'/mypatient' }}">
                            <div class="bg-primary top_cards">
                                <div class="row icon_margin_left">
                                    <div class="col-lg-5 col-5 icon_padd_left">
                                        <div class="float-left">
                											<span class="fa-stack fa-sm">
                											<i class="fa fa-circle fa-stack-2x"></i>
                											<i class="fa fa-user fa-stack-1x fa-inverse text-primary sales_hover"></i>
                											</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-offset-1 icon_padd_right">
                                        <div class="cards_content">
                                            <span class="number_val" >{{@$data['patient_count']}}</span><!-- <i
                                                    class="fa fa-long-arrow-up fa-2x"></i> -->
                                            <br/>
                                            <span class="card_description">Patients</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                          </a>
                        </div>
                        <div class="col-md-3 col-12">
                    <a style="color:white;" href="{{ url('/admin/').'/mydoctor' }}">
                            <div class="bg-primary top_cards">
                                <div class="row icon_margin_left">
                                    <div class="col-lg-5 col-5 icon_padd_left">
                                        <div class="float-left">
                											<span class="fa-stack fa-sm">
                											<i class="fa fa-circle fa-stack-2x"></i>
                											<i class="fa fa-user-md fa-stack-1x fa-inverse text-primary sales_hover"></i>
                											</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-offset-1 icon_padd_right">
                                        <div class="cards_content">
                                            <span class="number_val" >{{@$data['doctors_count']}}</span><!-- <i
                                                    class="fa fa-long-arrow-up fa-2x"></i> -->
                                            <br/>
                                            <span class="card_description">Doctors</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                          </a>
                        </div>
                        <div class="col-md-3 col-12">
                    <a style="color:white;" href="{{ url('/admin/').'/raisetest' }}">
                            <div class="bg-success top_cards">
                                <div class="row icon_margin_left">
                                    <div class="col-lg-5 col-5 icon_padd_left">
                                        <div class="float-left">
											<span class="fa-stack fa-sm">
											<i class="fa fa-circle fa-stack-2x"></i>
											<i class="fa fa-flask fa-stack-1x fa-inverse text-primary sales_hover"></i>
											</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-offset-1 icon_padd_right">
                                        <div class="cards_content">
                                            <span class="number_val" >{{@$data['raised_test_count']}}</span><!-- <i
                                                    class="fa fa-long-arrow-up fa-2x"></i> -->
                                            <br/>
                                            <span class="card_description">Self Raised</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                          </a>
                        </div>
                       
                 <!--<div class="col-md-4 col-12">
                    <a style="color:white;" href="#">
                            <div class="bg-success top_cards">
                                <div class="row icon_margin_left">
                                    <div class="col-lg-5 col-5 icon_padd_left">
                                        <div class="float-left">
											<span class="fa-stack fa-sm">
											<i class="fa fa-circle fa-stack-2x"></i>
											<i class="fa fa-flask fa-stack-1x fa-inverse text-primary sales_hover"></i>
											</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-offset-1 icon_padd_right">
                                        <div class="cards_content">
                                            <span class="number_val" >{{@$data['labs_count']}}</span>
                                            <br/>
                                            <span class="card_description">Labs</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                          </a>
                        </div>-->
                <div class="col-md-3 col-12">
                    <a style="color:white;" href="#">
                            <div class="bg-warning top_cards">
                                <div class="row icon_margin_left">
                                    <div class="col-lg-4 col-4 icon_padd_left">
                                        <div class="float-left">
											<span class="fa-stack fa-sm">
											<i class="fa fa-circle fa-stack-2x"></i>
											<i class="fa fa-money  fa-stack-1x fa-inverse text-primary sales_hover"></i>
											</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-8 col-8 icon_padd_right">
                                        <div class="cards_content">
                                            <span class="number_val" >$ {{@$data['revenue']}}</span><!-- <i
                                                    class="fa fa-long-arrow-up fa-2x"></i> -->
                                            <br/>
                                            <span class="card_description">Revenue</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                      </a>
                </div>
                
            
            <div class="row">
                <div class="col-lg-4 col-12 m-t-35">
                    <div class="card">
                        <div class="card-header bg-white">
                           Completed Raised Test ( {{ @$data['completed_test_count'] }} )
                        </div>
                        <div class="card-body">
                           <table  class="table display nowrap" >
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                         <th>Doctor Name</th>
                                        <th>Patient Name</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    $lab_completed_test =array_slice($data['lab_completed_test'], 0, 5, true);
                                    ?>
                                    @if(count($lab_completed_test) !=0) 

                                    @foreach(@$lab_completed_test as $completed)
                                                    
                                    <tr>
                                       <td><a href="/admin/raisetest/{{ $completed->id }}/show" >{{@$completed->id}}</a> </td>
                                        <td> {{isset($completed->doctor_referer_name)?$completed->doctor_referer_name:'Self'}}</td>
                                        <td>{{$completed->getPatient->name}}</td>  
                                    </tr>
                                    
                                    @endforeach
                                    @else <tr>
                                       <td> No data</td>
                                       </tr>

                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
                <div class="col-lg-4 col-12 m-t-35">
                    <div class="card">
                        <div class="card-header bg-white">
                           Pending Raised Test ( {{ @$data['pending_test_count'] }} )
                        </div>
                        <div class="card-body">
                            <table  class="table display nowrap" >
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Doctor Name</th>
                                        <th>Patient Name</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    $lab_pending_test =array_slice($data['lab_pending_test'], 0, 5, true);
                                    ?>
                                     @if(count($lab_pending_test) !=0) 
                                    @foreach(@$lab_pending_test as $completed)
                                    <tr>
                                    <td>{{@$completed->id}}</td>
                                    <td>{{isset($completed->doctor_referer_name)?$completed->doctor_referer_name:'Self'}}</td>
                                    <td>{{$completed->getPatient->name}}</td>
                                    
                                    </tr>
                                    @endforeach
                                     @else <tr>
                                       <td> No data</td>
                                       </tr>

                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                
                
                
                
                
                </div>
            

        </div>
    </div>

    <script type="text/javascript" src="{{asset('assets/vendors/raphael/js/raphael.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/d3/js/d3.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/c3/js/c3.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/toastr/js/toastr.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/switchery/js/switchery.min.js')}}"></script>
    
    <script type="text/javascript" src="{{asset('assets/vendors/jquery_newsTicker/js/newsTicker.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/countUp.js/js/countUp.min.js')}}"></script>
    <!--end of plugin scripts-->
    <script type="text/javascript" src="{{asset('assets/js/pages/new_dashboard.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/custom.js')}}"></script>
    <!-- <script type="text/javascript" src="{{asset('assets/js/components.js')}}"></script> -->
<!--  -->
<!-- <script type="text/javascript" src="{{asset('assets/js/pages/fixed_menu.js')}}"></script> -->


