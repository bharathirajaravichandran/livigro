
	 <link type="text/css" rel="stylesheet" href="{{asset('assets/css/components.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/custom.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/layouts.css')}}" />
    <!-- <link type="text/css" rel="stylesheet" href="#" id="skin_change"/> -->
	 <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/c3/css/c3.min.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/toastr/css/toastr.min.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/switchery/css/switchery.min.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/new_dashboard.css')}}"/>

   
    <div class="outer">
        <div class="inner bg-container">
            <div class="row">
                <div class="col-md-12">
                    <button id="enable_log" class="btn btn-primary pull-right">Enable Log</button>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-12 col-lg-7 col-12">
                    <div class="row">
                        <div class="col-sm-3 col-12">
                                <a style="color:white;" href="/admin/doctors">

                            <div class="bg-primary top_cards">
                                <div class="row icon_margin_left">

                                    <div class="col-lg-4 col-4 icon_padd_left">
                                        <div class="float-left">
                                <span class="fa-stack fa-sm">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-user-md fa-stack-1x fa-inverse text-primary sales_hover"></i>
                                </span>
                                        </div>
                                    </div>
                                    <div class="col-lg-8 col-8 icon_padd_right">
                                        <div class="cards_content">
                                            <span class="number_val" >{{$data['doctor_count']}}</span><!-- <i
                                                    class="fa fa-long-arrow-up fa-2x"></i> -->
                                            <br/>
                                            <span class="card_description">Doctors</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            </div>
                        </div>
                        <div class="col-sm-3 col-12">
                                <a style="color:white;" href="/admin/patients">

                            <div class="bg-success top_cards">
                                <div class="row icon_margin_left">
                                    <div class="col-lg-4  col-4 icon_padd_left">
                                        <div class="float-left">
                                        <span class="fa-stack fa-sm">
                                        <i class="fa fa-circle fa-stack-2x"></i>
                                        <i class="fa fa-user  fa-stack-1x fa-inverse text-success visit_icon"></i>
                                        </span>
                                        </div>
                                    </div>
                                    <div class="col-lg-8 col-8 icon_padd_right">
                                        <div class="cards_content">
                                            <span class="number_val" id="">{{$data['patient_count']}}</span>
                                            <br/>
                                            <span class="card_description">Patients</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            </div>
                        </div>
                        <div class="col-sm-3 col-12">
                            <div class="bg-warning top_cards">
                                <a style="color:white;" href="/admin/labs">
                                <div class="row icon_margin_left">
                                    <div class="col-lg-4 col-4 icon_padd_left">
                                        <div class="float-left">
                                    <span class="fa-stack fa-sm">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa-flask fa-stack-1x fa-inverse text-warning revenue_icon"></i>
                                    </span>
                                        </div>
                                    </div>
                                    <div class="col-lg-8 col-8 icon_padd_right">
                                        <div class="cards_content">
                                            <span class="number_val" id="">{{$data['labs_count']}}</span>
                                            <br/>
                                            <span class="card_description">Labs</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            </div>
                        </div>
                        <div class="col-sm-3 col-12">
                            <div class="bg-mint top_cards">
                                <a style="color:white;" href="/admin/hospitals">
                                <div class="row icon_margin_left">
                                    <div class="col-lg-4 col-4 icon_padd_left">
                                        <div class="float-left">
                                            <span class="fa-stack fa-sm">
                                            <i class="fa fa-circle fa-stack-2x"></i>
                                            <i class="fa fa-hospital-o  fa-stack-1x fa-inverse text-mint sub"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-lg-8 col-8 icon_padd_right">
                                    
                                        <div class="cards_content">
                                            <span class="number_val" id="">{{$data['hopital_count']}}</span>
                                            <br/>
                                            <span class="card_description">Hospitals</span>
                                        </div>
                                    
                                    </div>
                                </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
{{--
                <div class="col-xl-6 col-lg-5 col-12 stat_align">
                    <div class="card weather_section md_align_section">
                        <div class="card-body">
                            <div class="row margin_align">
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="icon sun-shower">
                                                <div class="cloud"></div>
                                                <div class="sun">
                                                    <div class="rays"></div>
                                                </div>
                                                <div class="rain"></div>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="weather-value">
<span class=" text-white"><span class="degree">25&deg;</span>
</span>
                                            </div>
                                            <div class="weather_location">
                                                <span class="text-white"><i class="fa fa-map-marker"></i> London</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row weekly_report">
                                        <div class="col-3">
                                            <span>Mon</span>
                                            <br/>
                                            <img src="{{asset('assets/img/w1.png')}}" alt="weather">
                                            <p>27&deg;</p>
                                        </div>
                                        <div class="col-3">
                                            <span>Tue</span>
                                            <br/>
                                            <img src="{{asset('assets/img/w2.png')}}" alt="weather">
                                            <p>23&deg;</p>
                                        </div>
                                        <div class="col-3">
                                            <span>Wed</span>
                                            <br/>
                                            <img src="{{asset('assets/img/w3.png')}}" alt="weather">
                                            <p>19&deg;</p>
                                        </div>
                                        <div class="col-3">
                                            <span>Thu</span>
                                            <br/>
                                            <img src="{{asset('assets/img/w4.png')}}" alt="weather">
                                            <p>38&deg;</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
--}}
            </div>

            <div class="row">
                <div class="col-lg-4 col-12 m-t-35">
                    <div class="card">
                        <div class="card-header bg-white">
                           Doctors Vs No of Tests raised
                        </div>
                        <div class="card-body">
                           <table  class="table display nowrap" >
                                <thead>
                                    <tr>
                                        <th>Doctor Name</th>
                                        <th>Count</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($data['doctor_raised_test'] as $doc_lab)
                                    @if($doc_lab->name!="")
                                    <tr>
                                       <td> <a href="/admin/doctors/{{ $doc_lab->doctor_id }}/show" >{{$doc_lab->salutation}} {{$doc_lab->name}} </a> </td>
                                       <td>{{$doc_lab->countno}}</td> 
                                    </tr>
                                    @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
                <div class="col-lg-4 col-12 m-t-35">
                    <div class="card">
                        <div class="card-header bg-white">
                           Doctors Vs No of Paitents
                        </div>
                        <div class="card-body">
                            <table  class="table display nowrap" >
                                <thead>
                                    <tr>
                                        <th>Doctor Name</th>
                                        <th>Count</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($data['doc_patient'] as $doc_lab)
                                    @if($doc_lab->name!="")
                                    <tr>
                                       <td> <a href="/admin/doctors/{{ $doc_lab->doc_id }}/show" >{{$doc_lab->salutation}} {{$doc_lab->name}}</a> </td>
                                       <td>{{$doc_lab->countno}}</td> 
                                    </tr>
                                    @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-12 m-t-35">
                    <div class="card">
                        <div class="card-header bg-white">
                            Doctors Vs No of Labs
                        </div>
                        <div class="card-body">
                            <table  class="table display nowrap" >
                                <thead>
                                    <tr>
                                        <th>Doctor Name</th>
                                        <th>Count</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($data['doc_lab'] as $doc_lab)
                                    @if($doc_lab->name!="")
                                    <tr>
                                       <td> <a href="/admin/doctors/{{ $doc_lab->doctor_id }}/show" >{{$doc_lab->salutation}} {{$doc_lab->name}} </a> </td>
                                       <td>{{$doc_lab->countno}}</td> 
                                    </tr>
                                    @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-4 col-12 m-t-35">
                    <div class="card">
                        <div class="card-header bg-white">
                            Lab Vs No of Tests taken
                        </div>
                        <div class="card-body">
                           <table  class="table display nowrap" >
                                <thead>
                                    <tr>
                                        <th>Lab Name</th>
                                        <th>Count</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($data['lab_taken_test'] as $doc_lab)
                                     @if($doc_lab->lab_name!="")
                                    <tr>
                                       <td> <a href="/admin/labs/{{ $doc_lab->lab_id }}/show" >{{$doc_lab->lab_name}} </a> </td>
                                       <td>{{$doc_lab->countno}}</td> 
                                    </tr>
                                    @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
                <div class="col-lg-4 col-12 m-t-35">
                    <div class="card">
                        <div class="card-header bg-white">
                           Maximum no of tests taken
                        </div>
                        <div class="card-body">
                            <table  class="table display nowrap" >
                                <thead>
                                    <tr>
                                        <th>Test Name</th>
                                        <th>Count</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($data['most_taken_test'] as $doc_lab)
                                    @if($doc_lab->test_name!="")
                                    <tr>
                                       <td> {{$doc_lab->test_name}}  </td>
                                       <td>{{$doc_lab->countno}}</td> 
                                    </tr>
                                    @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            @if(isset($data['latest_data']) && count($data['latest_data']) !=0)
                <div class="col-lg-4 col-12 m-t-35">
                    <div class="card">
                        <div class="card-header bg-white">
                          Recent Activities
                        </div>
                        <div class="card-body">
                            <table  class="table display nowrap" >
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Type</th>
                                        <th>Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($data['latest_data'] as $latest)                                 
                                    <tr>
                                    <?php 
                                    if(array_key_exists("aadhar_id",$latest)){
                                         $name = $latest['name'] ;
                                         $link = 'admin/labs/'.$latest['id'].'/show';
                                         $type= 'Lab';
                                    }else if(array_key_exists("aadhar_no",$latest)){
                                         $name = $latest['name'] ;
                                         $link = 'admin/doctors/'.$latest['id'].'/show';
                                         $type= 'Doctor';
                                    }else{
                                         $name = $latest['name'] ;
                                         $link = 'admin/patient/'.$latest['id'].'/show';
                                         $type= 'Patient';
                                    }

                                    ?>                                        
                                       <td><a href="{{$link}}">{{ $name }}</a></td>
                                       <td><span class="btn btn-success btn-xs" style="height:32px;width:60px">{{$type}}</span></td>         
                                       <td>{{ $latest['created_at'] }}</td> 
                                    </tr>
                                
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
             @endif
                
            </div>
        </div>
    </div>
            
	 <!-- <script type="text/javascript" src="{{asset('assets/vendors/slimscroll/js/jquery.slimscroll.min.js')}}"></script> -->
    <script type="text/javascript" src="{{asset('assets/vendors/raphael/js/raphael.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/d3/js/d3.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/c3/js/c3.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/toastr/js/toastr.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/switchery/js/switchery.min.js')}}"></script>
    
    <script type="text/javascript" src="{{asset('assets/vendors/jquery_newsTicker/js/newsTicker.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/countUp.js/js/countUp.min.js')}}"></script>
    <!--end of plugin scripts-->
    <script type="text/javascript" src="{{asset('assets/js/pages/new_dashboard.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/custom.js')}}"></script>
<script>
    jQuery('#enable_log').click(function() {
                                        $.ajax({
                                             headers: {
                                              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                              },
                                            type: 'POST',
                                            url: '/admin/enable_log',
                                            success: function( msg ) {
                                               jQuery('#enable_log').text(msg);                                      
                                            }
                                        });

    })
</script>
    <!-- <script type="text/javascript" src="{{asset('assets/js/components.js')}}"></script> -->
<!--  -->
<!-- <script type="text/javascript" src="{{asset('assets/js/pages/fixed_menu.js')}}"></script> -->

