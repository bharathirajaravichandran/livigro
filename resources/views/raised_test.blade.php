<div class="content">
<div class="panel-group ">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" aria-expanded="true" href="#collapse1">Raised Test</a>
        </h4>
      </div>
      <div id="collapse1" class="panel-collapse collapse in " aria-expanded="true">
        <div class="panel-body">
            @include('raised_test_table')
        </div>
        <div class="panel-footer"></div>
      </div>
    </div>
</div>
</div>
