<div class="container">
{{-- Labs Table --}}
    <div class="row">
	   <div class="panel">
	        <div class="panel-heading">
	        	<h4>Labs List</h4>
	        </div>
	        <div class="panel-body">
	        	<div class="card-body">
	        		<table id="example1" class="table">
	        			<thead>
	        				<tr>
	        					<th>Lab Name</th>
	        					<th>Phone</th>
	        					<th>Email</th>
	        					<th>City</th>
	        					<th>State</th>
	        					<th>Gst Registered</th>
	        					<th>Home Collection</th>
	        				</tr>
	        			</thead>
	        			<tbody>
	        				@foreach($labs as $lab_data)
		        				<tr>

		        					<td><a href="/admin/labs/{{$lab_data->id}}/show">{{$lab_data->name}}</a></td>
		        					<td>{{$lab_data->phone}}</td>
		        					<td>{{$lab_data->email}}</td>
		        					<td>{{$lab_data->city}}</td>
		        					<td>{{$lab_data->state}}</td>
		        					<td>{{($lab_data->gst_registered == 0) ? "No" : "Yes"}}</td>
		        					<td>{{($lab_data->home_collection == 0) ? "No" : "Yes"}}</td>
		        				</tr>
	        				@endforeach
	        			</tbody>
	        		</table>
	        	</div>
	        </div>
	    </div>
    </div>
    {{-- End of Lab Table --}}
    <div class="row">
   		 {{-- start of Patient count table --}}
    	<div class="col-md-4">
	    	<div class="panel">
	    		<div class="panel-heading">
	    			<h3>Patient Count Table</h3>
	    		</div>
	    		<div class="panel-body">
	    			<div class="card-body">
	    				<table id="example2" class="table">
	    					<thead>
	    						<tr>
	    							<th>Lab Name</th>
	    							<th>City</th>
									<th>Patient Count</th>
	    						</tr>
	    					</thead>
	    						<tbody>
	    							@foreach($lab_patient as $data)
	    								<tr>
	    									<td><a href="/admin/labs/{{$data->lab_id}}/show">{{$data->lab_name}}</a></td>
	    									<td>{{$data->city}}</td>
	    									<td>{{$data->patient_count}}</td>
	    								</tr>
	    							@endforeach
	    						</tbody>
	    				</table>
	    			</div>
	    		</div>
	    	</div>
	    </div>
   		 {{-- End of patient count table --}}
   		  {{-- start of Test Order count table --}}
    	<div class="col-md-4">
	    	<div class="panel">
	    		<div class="panel-heading">
	    			<h3>Order Count Table</h3>
	    		</div>
	    		<div class="panel-body">
	    			<div class="card-body">
	    				<table id ='example3' class="table">
	    					<thead>
	    						<tr>
	    							<th>Lab Name</th>
	    							<th>City</th>
	    							<th>Order Count</th>
	    						</tr>
	    					</thead>
	    					<tbody>
	    						@foreach($labs_order_data as $data)
	    							<tr>
	    								<td><a href="/admin/labs/{{$data->lab_id}}/show">{{$data->lab_name}}</a></td>
    									<td>{{$data->city}}</td>
    									<td>{{$data->test_count}}</td>
	    							</tr>
	    						@endforeach
	    					</tbody>
	    				</table>
	    			</div>
	    		</div>
	    	</div>
	    </div>
   		 {{-- End of Test Order count table --}}
   		  {{-- start of Doctor count table --}}
    	<div class="col-md-4">
	    	<div class="panel">
	    		<div class="panel-heading">
	    			<h3>Doctor Count Table</h3>
	    		</div>
	    		<div class="panel-body">
	    			<div class="card-body">
	    				<table id ='example4' class="table">
	    					<thead>
	    						<tr>
	    							<th>Lab Name</th>
	    							<th>City</th>
	    							<th>Doctor Count</th>
	    						</tr>
	    					</thead>
	    					<tbody>
	    						@foreach($lab_doctor_list as $data)
	    							<tr>
	    								<td><a href="/admin/labs/{{$data->lab_id}}/show">{{$data->lab_name}}</a></td>
    									<td>{{$data->city}}</td>
    									<td>{{$data->doctor_count}}</td>
	    							</tr>
	    						@endforeach
	    					</tbody>
					</table>
	    			</div>
	    		</div>
	    	</div>
	    </div>
   		 {{-- End of Doctor count table --}}

    </div>
</div>

