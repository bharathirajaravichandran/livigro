{{-- Doctors Table  --}}
<div class="container">
    <div class="row">
	   <div class="panel">
            <div class="panel-heading">
            	<h4>Doctors List</h4>
            </div>
            <div class="panel-body">
				<div class="card-body">
					<table id="example1" class="table">
						<thead>
							<tr>
								<th>Doctor Name</th>
								<th>Mobile</th>
								<th>Blood Group</th>
								<th>City</th>
								<th>State</th>
								<th>Gender</th>
							</tr>
						</thead>
						<tbody>
							@foreach($doctors_data as $data_doc)
							<tr>
								@php 
										if(is_numeric($data_doc->gender)) {
											if($data_doc->gender == 0)
											$gender="Female";
											else if($data_doc->gender == 1)
											$gender="Male";
											else if($data_doc->gender == 2)
											$gender="Non Binary";
											else 
											$gender=" ";

										}else {
											$gender = $data_doc->gender;
										}
									@endphp
								<td><a href="/admin/doctors/{{$data_doc->id}}/show">{{$data_doc->salutation}} {{$data_doc->name}}</a></td>
								<td>{{$data_doc->mobile}}</td>
								<td>{{$data_doc->blood_group}}</td>
								<td>{{$data_doc->city}}</td>
								<td>{{$data_doc->state}}</td>
								<td>{{$gender}}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		{{-- Start Doctors Raised Test --}}
		<div class="col-md-4">
		  	<div class="panel">
				<div class="panel-heading">
					Doctors Raised Test
				</div>
				<div class="panel-body">
					<table id="example2" class="table">
						<thead>
							<tr>
								<th>Doctor Name</th>
								<th>City</th>
								<th>Raised Test Count</th>
							</tr>
						</thead>
						<tbody>
							@foreach($doctor_raised_test as $data)
								@php 
									if(is_numeric($data->city)) {
										$city = DB::table('statelist')->where('city_id',$data->city)->first();
										if($city!=""){
											$city_name =$city->city_name;
										}else{
											$city_name = " ";
										}
									}else {
										$city_name = $data->city;
									}
								@endphp
								<tr>
									<td><a href="/admin/doctors/{{$data->doctor_id}}/show">{{$data->doctor_name}}</a></td>
									<td>{{$city_name}}</td>
									<td>{{$data->test_count}}</td>
								</tr>
							@endforeach
						</tbody>
					</table> 
				</div>
			</div>
		</div>
				{{-- End of Doctors Raised Test --}}

				{{-- Start Doctors Lab Table --}}
		<div class="col-md-4">
		  	<div class="panel">
				<div class="panel-heading">
					Doctors Lab Details
				</div>
				<div class="panel-body">
					<table id="example3" class="table">
						<thead>
							<tr>
								<th>Doctor Name</th>
								<th>City</th>
								<th>Lab Count</th>
							</tr>
						</thead>
						<tbody>
							@foreach($doctor_lab_list as $data)
								@php 
									if(is_numeric($data->city)) {
										$city = DB::table('statelist')->where('city_id',$data->city)->first();
										if($city!=""){
											$city_name =$city->city_name;
										}else{
											$city_name = " ";
										}
									}else {
										$city_name = $data->city;
									}
								@endphp
								<tr>
									<td><a href="/admin/doctors/{{$data->doctor_id}}/show">{{$data->doctor_name}}</a></td>
									<td>{{$city_name}}</td>
									<td>{{$data->lab_count}}</td>
								</tr>
							@endforeach
						</tbody>
					</table> 
				</div>
			</div>
		</div>
				{{-- End of Doctors Patient Table --}}

		{{-- Start Doctors Lab Table --}}
		<div class="col-md-4">
		  	<div class="panel">
				<div class="panel-heading">
					Doctors Patient Details
				</div>
				<div class="panel-body">
					<table id="example4" class="table">
						<thead>
							<tr>
								<th>Doctor Name</th>
								<th>City</th>
								<th>Patient Count</th>
							</tr>
						</thead>
						<tbody>
							@foreach($doctor_patient_list as $data)
								@php 
									if(is_numeric($data->city)) {
										$city = DB::table('statelist')->where('city_id',$data->city)->first();
										if($city!=""){
											$city_name =$city->city_name;
										}else{
											$city_name = " ";
										}
									}else {
										$city_name = $data->city;
									}
								@endphp
								<tr>
									<td><a href="/admin/doctors/{{$data->doctor_id}}/show">{{$data->doctor_name}}</a></td>
									<td>{{$city_name}}</td>
									<td>{{$data->patient_count}}</td>
								</tr>
							@endforeach
						</tbody>
					</table> 
				</div>
			</div>
		</div>
				{{-- End of Doctors Patient Table --}}
	</div>
</div>