<div class="container">
{{-- Patient Table --}}
    <div class="row">
	   <div class="panel">
	        <div class="panel-heading">
	        	<h4>Labs List</h4>
	        </div>
	        <div class="panel-body">
	        	<div class="card-body">
	        		<table class="table" id="example1">
	        			<thead>
	        				<tr>
	        					<th>Patient Name</th>
	        					<th>Phone</th>
	        					<th>Email</th>
	        					<th>Gender</th>
	        					<th>Blood Group</th>
	        					<th>Height</th>
	        					<th>Weight</th>
	        				</tr>
	        			</thead>
	        			<tbody>
	        				@foreach($patient as $data)
	        					<tr>
	        						@php 
										if(is_numeric($data->gender)) {
											if($data->gender == 0)
											$gender="Female";
											else if($data->gender == 1)
											$gender="Male";
											else if($data->gender == 2)
											$gender="Non Binary";
											else 
											$gender=" ";

										}else {
											$gender = $data->gender;
										}
									@endphp
	        						<td>{{$data->name}}</td>
	        						<td>{{$data->phone}}</td>
	        						<td>{{$data->email}}</td>
	        						<td>{{$gender}}</td>
	        						<td>{{@$data->blood_group}}</td>
	        						<td>{{$data->height}}</td>
	        						<td>{{$data->weight}}</td>
	        					</tr>
	        				@endforeach
	        			</tbody>
	        		</table>
	        	</div>
	        </div>
	    </div>
	</div>
{{-- End of Patient Table --}}
	<div class="row">
		<div class="col-md-4">
			  	<div class="panel">
					<div class="panel-heading">
						<h3>Patient Raised Test</h3>
					</div>
					<div class="panel-body">
						<table id="example2" class="table">
							<thead>
								<tr>
									<th>Patient Name</th>
									<th>City</th>
									<th>Raised Test Count</th>
								</tr>
							</thead>
							<tbody>
								@foreach($patient_raised_test as $data)
									@php 
										if(is_numeric($data->city)) {
										$city = DB::table('statelist')->where('city_id',$data->city)->first();
											if($city!=""){
												$city_name =$city->city_name;
											}else{
												$city_name = " ";
											}
										}else {
											$city_name = $data->city;
										}
									@endphp
									<tr>
										<td><a href="/admin/patient/{{$data->patients_id}}/show">{{$data->patient_name}}</a></td>
										<td>{{$city_name}}</td>
										<td>{{$data->test_count}}</td>
									</tr>
								@endforeach
							</tbody>
						</table> 
					</div>
				</div>	 
		</div>
		 {{-- start of Test Order count table --}}
    	<div class="col-md-4">
	    	<div class="panel">
	    		<div class="panel-heading">
	    			<h3>Patient Mostly Consult Docter</h3>
	    		</div>
	    		<div class="panel-body">
	    			<div class="card-body">
	    				<table id ='example3' class="table">
	    					<thead>
	    						<tr>
	    							<th>Doctor Name</th>
	    							<th>City</th>
	    							<th>Consult Count</th>
	    						</tr>
	    					</thead>
	    					<tbody>
	    						@foreach($patient_consultant_mostly_test as $data)
	    							<tr>
	    								@php 
											if(is_numeric($data->city)) {
												$city = DB::table('statelist')->where('city_id',$data->city)->first();
												if($city!=""){
													$city_name =$city->city_name;
												}else{
													$city_name = " ";
												}
											}else {
												$city_name = $data->city;
											}
										@endphp
	    								<!-- <td><a href="/admin/patient/{{$data->patients_id}}/show">{{$data->patient_name}}</a></td> -->
	    								<td>{{$data->doctor_name}}</td>
    									<td>{{$city_name}}</td>
    									<td>{{$data->test_count}}</td>
	    							</tr>
	    						@endforeach
	    					</tbody>
	    				</table>
	    			</div>
	    		</div>
	    	</div>
	    </div>
   		 {{-- End of Test Order count table --}}
   		  {{-- start of Test Order count table --}}
    	<div class="col-md-4">
	    	<div class="panel">
	    		<div class="panel-heading">
	    			<h3>Lab Mostly Consulted by Patients</h3>
	    		</div>
	    		<div class="panel-body">
	    			<div class="card-body">
	    				<table id ='example3' class="table">
	    					<thead>
	    						<tr>
	    							<th>Lab Name</th>
	    							<th>City</th>
	    							<th>Consult Count</th>
	    						</tr>
	    					</thead>
	    					<tbody>
	    						@foreach($patient_consultant_mostly_test_lab as $data)
	    							<tr>
	    								@php 
											if(is_numeric($data->city)) {
												$city = DB::table('statelist')->where('city_id',$data->city)->first();
												if($city!=""){
													$city_name =$city->city_name;
												}else{
													$city_name = " ";
												}
											}else {
												$city_name = $data->city;
											}
										@endphp
	    								<td><a href="/admin/labs/{{$data->lab}}/show">{{$data->lab_name}}</a></td>
    									<td>{{$city_name}}</td>
    									<td>{{$data->test_count}}</td>
	    							</tr>
	    						@endforeach
	    					</tbody>
	    				</table>
	    			</div>
	    		</div>
	    	</div>
	    </div>
   		 {{-- End of Test Order count table --}}	
    </div>
</div>
