<div class="panel panel-default">
	<div class="panel-heading">
		<strong>Report</strong>
	</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-sm-8">
				<div class="col-sm-5">
					
					<h1>@if($patient->name){{$patient->name}} @else Nil @endif</h1>
					<p>@if($patient->age() || $patient->getGender() || $patient->city ){{$patient->age()}} {{$patient->getGender()}} {{$patient->city}} @else Nil @endif</p>
					<p>Patient ID:@if($patient->id){{$patient->id}} @else Nil @endif</p>
				</div>
				<div class="col-sm-3">
					<h4>Order Raised: @if($raise_test->created_at->toDayDateTimeString()){{$raise_test->created_at->toDayDateTimeString()}}  @else Nil @endif</h2>
					Doctor:@if($doctor)
					<h4>@if($doctor->name){{ $doctor->name}} @else Nil @endif</h4>
					@else Nil @endif
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-8 mar-setb">
				<div class="col-sm-5">
					<h4>Sample Collected</h4>
					@if($raise_test->updated_at->toDayDateTimeString()){{ $raise_test->updated_at->toDayDateTimeString()}} @else Nil @endif
				</div>
				<div class="col-sm-3">
					<h4>Reported On</h4>
					@if($raise_test->updated_at->toDayDateTimeString()){{ $raise_test->updated_at->toDayDateTimeString()}} @else Nil @endif
				</div>
			</div>
		</div>
		<div class="row">
		<form method="post" action="{{route('reportsave')}}">
			{!! csrf_field() !!}
		<input type="hidden" name="raise_test_id" value={{$id}} />
			<table class="table table-stripped">
				<tr>
					<th>Test Name</th>
					<th>Result</th>
					<th>Units</th>
				</tr>
				<tbody>
					
					@foreach($lab_tests as $test)
						@php 

							@$part_test = DB::table('receipt_table')->where([
								['raise_test_id',$raise_test->id],
								['tests_id',$test->id]
							])->first();
							if($part_test) {
								@$specimen_id = @$part_test->labtest_sub_name;
								@$refre_from = @$part_test->reference_range;
								@$refre_end = @$part_test->reference_endrange;
								@$result = @$part_test->result;
								@$unit = @$part_test->units;
								if($specimen_id!=""){
									@$specimen_name = DB::table('lab_test_subcate')->where('id',$specimen_id)->pluck('name')[0];
									}								
								else{
									@$specimen_name ="";
								}
									
								
							}else{
								@$specimen_id = @$refre_from = @$refre_end = @$result = @$unit = @$specimen_name = null;
							}
						@endphp
						<tr>
							<td><h4>@if(@$test->name){{@$test->name}} @else Nil @endif</h4>
								<p>Specimen : @if(@$specimen_name) {{ @$specimen_name}} @else Nil @endif</p>
								<p>Reference Range : </p>
									<input type="text" name="start_{{@$test->id}}" class="col-sm-3" value="{{$refre_from}}"> 
								<input type="text" name="end_{{@$test->id}}" class="col-sm-3" value="{{@$refre_end}}"> 
							</td>
							<td><input type="hidden" id="test_id" name="test_id" value="{{@$test->id}}" />
								<input class="form-control col-ms-2 result" name="result_{{@$test->id}}" type="text" id="result_{{@$test->id}}" value="{{$result}}" />
								
							</td>
							<td>
								<select class="form-control col-ms-2" name="unit_{{ @$test->id }}">
								<!-- <option value="@if($unit){{$unit}}@else{{@units}}@endif" ></option> -->
								<option value="" >Select</option>
								@foreach($units as $un)
								<option value="{{$un->name}}"  @if($unit == $un->name) selected="selected" @endif>{{$un->name}}</option>
								@endforeach 
							</td>
						<tr>
					@endforeach
				</tbody>

			</table>
	@if($raise_test->raised_by ==1)
			@if($signature !='')
			<button type="submit" class="btn btn-primary btn-mar" id="save" style="float:left">Save</button>
			@if($report ==0)
				<span id="resultError" class="btn btn-mar" style="color: red;"></span>
			@endif
			@else
			<span style="color:red">Please Update Lab Signature</span><span style="padding-left:5px; "> <a href="/admin/labs/{{$raise_test->lab_id}}/edit">Click Here to Update</a></span>
			@endif
	@else
			<button type="submit" class="btn btn-primary btn-mar" id="save" style="float:left">Save</button>
	@endif
			
		
			
		</form>
		@if($report !=0)
			<form method="post" action="{{route('reportgenerate')}}">
				{!! csrf_field() !!}
			<input type="hidden" name="raise_test_id" value={{$id}} />
			<button type="submit" class="btn btn-primary btn-mar" id="generate">Report Generate</button>
			</form>
			@endif
		</div>
	</div>
</div>
<style type="text/css">
	.mar-setb{
		margin-bottom: 15px;
	}
	.table-fix{
		margin: 10px 10px 10px 30px;
	}
	.btn-mar{
		margin: 30px 15px 15px 30px;
	}
</style>
<script type="text/javascript">
$( document ).ready(function() {
	var value = 0;
	$("#save").click(function(){
		$('.result').each(function() {
			if ($(this).val() == "") {
				value =1;
			}else{
				value=0;
			}
			// alert(value);
			
		});
		// alert(value);
		if(value ==1){
				$("#resultError").text("Please Insert Result");
				return false;
			}else{
				$("#resultError").text("");
				return true;
			}
	});
});
</script>