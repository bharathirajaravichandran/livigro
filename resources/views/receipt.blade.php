<div class="panel panel-default">
	<div class="panel-heading">
		<strong>Receipt</strong>
	</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-sm-8">
				<div class="col-sm-5">
					<h1>{{$patient->name}}</h1>
					<p>{{$patient->age()}},{{$patient->getGender()}},{{$patient->city}},{{$patient->state}}</p>
					<p>Mobile Number:{{$patient->phone}}</p>
				</div>
				<div class="col-sm-3">
					<h2>Order id: {{$id}}</h2>
				</div>
			</div>
		</div>
		<form  action="{{route('receiptsave')}}"" method="post" class="form-horizontal">
			{!! csrf_field() !!}
			<?php //echo "<pre>";print_r($lab_tests); print_r($raise_test); die;?>
			@foreach($lab_tests as $key=>$lab_test)
			<?php //echo "<pre>";print_r($lab_test); die;?>
			  <div class="form-group form-inline">
			  	<input type="hidden" value="{{$id}}" id="raise_test_id" name="raise_test_id" />
			    <label for="email" class="col-sm-2">{{ $lab_test['name'] }}</label>
			  	<input type="text" id="{{$lab_test['id']}}" disabled="disabled"   class="col-sm-2"  value="{{$lab_test['amount']}}" />
			  </div>
			@endforeach
			<div class="form-group clearfix">
				<label for="amount_received" class="col-sm-2">Amount Received</label>
				<input type="text" class="col-sm-2" id="amount_receive" disabled="disabled"  value="{{$raise_test->received_amount}}" />
			</div>
			<div class="form-group clearfix">
				<label for="amount_received" class="col-sm-2">Pay Amount</label>
				<input type="text" class="col-sm-2" id="amount_received" name="amount_received"  value="" />
			</div>
				<p class="col-sm-offset-2 col-sm-10 paymentError"></p>
		

			<div class="form-group clearfix">
				<label for="discount_price" class="col-sm-2">Discount Amount</label>
				<input type="text" class="col-sm-2" id="discount_price" name="discount_price"  value="" />
			</div>

			<div class="form-group clearfix">
				<label for="payment_mode" class="col-sm-2"> Payment Mode</label>
				<select name="payment_mode" class="col-sm-2">

					<option value="cash" @if($raise_test->mode_of_payment == 'cash') selected @endif >Cash</option>
					<option value="card" @if($raise_test->mode_of_payment == 'card') selected @endif>Card</option>
				</select>
			</div>
			<div class="form-group clearfix">
				<label for="payment_mode" class="col-sm-2"> Total Price: </label>
				 <span>
				 		
				           {{ $raise_test->original_amount }} 
				        

				 </span>
			</div>
			<div class="form-group clearfix">
				<label for="payment_mode" class="col-sm-2"> Payment Due:</label> <span id="remaining_amount"> {{ $raise_test->remaining_amount }}</span> 
			</div>
			<div class="form-group" id="new_test">
			</div>
		
			  <button type="submit" class="btn btn-primary">Confirm</button>
		</form>
	</div>
</div>
<script>
	$("input").blur(function(){		
		var id = $(this).attr('id');
		var value = $('#'+id).val();		
		var remaining_amount = $('#remaining_amount').html();
		var amount_received = $('#amount_received').val();
		var discount_price = $('#discount_price').val();						
		if(value == ''){
			$('.paymentError').html('')
             return false;	
		}
		$('.paymentError').html('')
		var raise_test_id = $('#raise_test_id').val();
    	 $.ajax({
             headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
            type: 'POST',
            url: '/admin/updatereceipt',
            data: {
            	id: id,
            	value: value,
            	amount_received: amount_received,
            	discount_price:  discount_price,
            	remaining_amount: remaining_amount,
            	raise_test_id : raise_test_id,
            },
            success: function( msg ) {

            	if(msg=='wrong'){
            		$(".btn-primary").prop("disabled", true);
            		//$('#amount_received').focus();
            		$('.paymentError').html('Please check your input amount');
            		jQuery('#remaining_amount').text('');
            		//alert('Please check your input amount');

            		return false;
            	}
            	$('.paymentError').html('')
               jQuery('#remaining_amount').text(msg);
               $(".btn-primary").prop("disabled", false);                                 
            }
        });
		});
</script>
<style type="text/css">
.paymentError{
	padding-left: 0px;
    color: #ff0000;
    font-weight: bold;
}
</style>