<style type="text/css">
    /*Panel tabs*/
.panel-tabs {
    position: relative;
    bottom: 30px;
    clear:both;
    border-bottom: 1px solid transparent;
}

.panel-tabs > li {
    float: left;
    margin-bottom: -1px;
}

.panel-tabs > li > a {
    margin-right: 2px;
    margin-top: 4px;
    line-height: .85;
    border: 1px solid transparent;
    border-radius: 4px 4px 0 0;
    color: black;
}

.panel-tabs > li > a:hover {
    border-color: transparent;
    color: black;
    background-color: transparent;
}

.panel-tabs > li.active > a,
.panel-tabs > li.active > a:hover,
.panel-tabs > li.active > a:focus {
    color: blue;
    cursor: default;
    -webkit-border-radius: 2px;
    -moz-border-radius: 2px;
    border-radius: 2px;
    background-color: rgba(255,255,255, .23);
    border-bottom-color: transparent;
}/*Panel tabs*/
.panel-tabs {
    position: relative;
    bottom: 30px;
    clear:both;
    border-bottom: 1px solid transparent;
}

.panel-tabs > li {
    float: left;
    margin-bottom: -1px;
}

.panel-tabs > li > a {
    margin-right: 2px;
    margin-top: 4px;
    line-height: .85;
    border: 1px solid transparent;
    border-radius: 4px 4px 0 0;
    color: black;
}

.panel-tabs > li > a:hover {
    border-color: transparent;
    color:black;
    background-color: transparent;
}

.panel-tabs > li.active > a,
.panel-tabs > li.active > a:hover,
.panel-tabs > li.active > a:focus {
    color: red;
    cursor: default;
    -webkit-border-radius: 2px;
    -moz-border-radius: 2px;
    border-radius: 2px;
    background-color: rgba(255,255,255, .23);
    border-bottom-color: transparent;
}
</style>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">@if($hospital->name){{$hospital->name }} @else Nil @endif </h3>
                    <span class="pull-right">
                        <!-- Tabs -->
                        <ul class="nav panel-tabs">
                            <li class="active">
                                <a href="#tab1" data-toggle="tab">Hospital Details</a>
                            </li>
                            <li>
                                <a href="#tab4" data-toggle="tab">Doctor List</a>
                            </li>
                        </ul>
                    </span>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab1">
                            <div class="row">
              
                                 <div class="col-md-3" style="visibility: hidden;">test</div>
                            <div class=" col-md-6"> 
                             <div class="container-fluid well span6">
	<div class="row-fluid">
       
                                 <div class="span8">
            <h3>@if($hospital->name){{$hospital->name }} @else Nil @endif </h3>
            <h6>Address: @if($hospital->address){{$hospital->address}} @else Nil @endif</h6>
            <h6>City: @if($hospital->city){{$hospital->city}} @else Nil @endif</h6>
            <h6>State: @if($hospital->state){{$hospital->state}} @else Nil @endif</h6>
            
            
        </div>
                            </div>
            </div>
             </div>     
                    
              </div>
                        </div>
                        
                        <div class="tab-pane" id="tab4">
                            @include('doctors_list')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
