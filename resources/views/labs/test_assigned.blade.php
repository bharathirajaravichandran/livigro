  
   
 				 <div class="card-body">
                    <div class="m-t-35">
                        <table id="example3" class="table display nowrap" >
                            <thead>
                            <tr>
                                <th>TestID</th>
                                <th>Patient Name</th>
                                <th>Doctor Name</th>
                            </tr>
                            </thead>
                            <tbody>
                            	@foreach($lab->test_assigned() as $raise_test)
                            		<tr>
                            			<td>{{@$raise_test->id }}</td>
                            			<td>{{@$raise_test->getPatient->name }} </td>
                            			<td>
                                        {{@$raise_test->getDoctor->salutation }} {{@$raise_test->getDoctor->name}} 
                                    </td>
                            		</tr>
                            	
                            	@endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
	
	