<style type="text/css">
    /*Panel tabs*/
.panel-tabs {
    position: relative;
    bottom: 30px;
    clear:both;
    border-bottom: 1px solid transparent;
}

.panel-tabs > li {
    float: left;
    margin-bottom: -1px;
}

.panel-tabs > li > a {
    margin-right: 2px;
    margin-top: 4px;
    line-height: .85;
    border: 1px solid transparent;
    border-radius: 4px 4px 0 0;
    color: black;
}

.panel-tabs > li > a:hover {
    border-color: transparent;
    color: black;
    background-color: transparent;
}

.panel-tabs > li.active > a,
.panel-tabs > li.active > a:hover,
.panel-tabs > li.active > a:focus {
    color: black;
    cursor: default;
    -webkit-border-radius: 2px;
    -moz-border-radius: 2px;
    border-radius: 2px;
    background-color: rgba(255,255,255, .23);
    border-bottom-color: transparent;
}/*Panel tabs*/
.panel-tabs {
    position: relative;
    bottom: 30px;
    clear:both;
    border-bottom: 1px solid transparent;
}

.panel-tabs > li {
    float: left;
    margin-bottom: -1px;
}

.panel-tabs > li > a {
    margin-right: 2px;
    margin-top: 4px;
    line-height: .85;
    border: 1px solid transparent;
    border-radius: 4px 4px 0 0;
    color: black;
}

.panel-tabs > li > a:hover {
    border-color: transparent;
    color: black;
    background-color: transparent;
}

.panel-tabs > li.active > a,
.panel-tabs > li.active > a:hover,
.panel-tabs > li.active > a:focus {
    color: red;
    cursor: default;
    -webkit-border-radius: 2px;
    -moz-border-radius: 2px;
    border-radius: 2px;
    background-color: rgba(255,255,255, .23);
    border-bottom-color: transparent;
}
</style>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">@if($lab->name!='') {{$lab->name}}  @else Nil @endif</h3>
                    <span class="pull-right">
                        <!-- Tabs -->
                        <ul class="nav panel-tabs">
                            <li class="active">
                                <a href="#tab1" data-toggle="tab">Lab Details</a>
                            </li>
                            <li>
                                <a href="#tab2" data-toggle="tab">Doctors List</a>
                            </li>
                            <li>
                                <a href="#tab3" data-toggle="tab">Tests </a>
                            </li>
                            
                        </ul>
                    </span>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab1">
                            <div class="row">
								<div class="col-md-1" style="visibility: hidden;">test</div>
								<div class=" col-md-6" style="margin-top: 4%;"> 
								
								<div class="container-fluid well span6">
	<div class="row-fluid">
        <div class="span2" >
			<div class=" col-md-3">
	<!--		     <img src="/storage/{{ $lab->profile_image }}" alt="" class="img-rounded img-responsive" /> -->
    <!-- Neelamegam_LabsDetailsView_13/08/2018 -->
                @if($lab->profile_image!="") 
        	       	<img src="{!! \Illuminate\Support\Facades\Storage::disk(config('admin.upload.disk'))->url($lab->profile_image) !!}" alt="" class="img-rounded img-responsive" />
                @else
                   <img src="" />    
                @endif

        </div></div>
        
        <div class="span8">
            <h3> @if($lab->name!='') {{$lab->name}}  @else Nil @endif</h3>
            <h6>Email: @if($lab->email!='') {{$lab->email}}  @else Nil @endif</h6>
            <h6>Mobile : @if($lab->mobile!='')  {{$lab->mobile}}  @else Nil @endif</h6>
            <h6 title="India">Address: 
                @if($lab->area!='' || $lab->address!='' || $lab->city!='' || $lab->state!='')
                {{$lab->area}} {{$lab->address}} {{ $lab->city}} {{ $lab->state}} 
                @else
                 Nil
                @endif
            </h6>
          
        </div>
	</div>        
	</div>        
<!--
        <div class="span2">
            <div class="btn-group">
                <a class="btn dropdown-toggle btn-info" data-toggle="dropdown" href="#">
                    Action 
                    <span class="icon-cog icon-white"></span><span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="#"><span class="icon-wrench"></span> Modify</a></li>
                    <li><a href="#"><span class="icon-trash"></span> Delete</a></li>
                </ul>
            </div>
        </div>
-->

</div>
<div class="col-md-4 col-12 m-t-25">
                    <div class="card" >
                     <!--    <div class="card-header bg-white">
                           Doctors Vs No of Tests raised
                        </div> -->
                        <div class="card-body">
                           <table  class="table display nowrap" >
                                <thead>
                                    <tr>
                                        <th>List</th>
                                        <th>Value</th>
                                    </tr>
                                </thead>
                                <tbody>
                                 
                                    <tr>
                                       <td> No of Test taken </td>
                                       <td> {{$raised_test_count}}</td> 
                                    </tr>                                 
                                    <tr>
                                       <td> No of Completed Test</td>
                                       <td>{{$completed_test}}</td> 
                                    </tr>
                                    <tr>
                                       <td> No of Payment Completed Test</td>
                                       <td>{{$paid_status}}</td> 
                                    </tr>                                 
                                    <tr>
                                       <td> No of Pending Test</td>
                                       <td> {{$pending_test}}</td> 
                                    </tr>     
                                     <tr>
                                       <td> No of Cancel Test</td>
                                       <td> {{$cancel_test}}</td> 
                                    </tr>                            
                                    <tr>
                                       <td>Total Amount</td>
                                       <td> {{$amounts}}</td> 
                                    </tr>                                 
                                   
                                   
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
</div>
 <div >
        @if($working_times->count()>0)
             @include('labs.lab_working_table')
        @endif
</div>
</div>
              
<!--
                            <div class=" col-md-9"> 
                              <div class="well well-sm">
                            <div class="row">
                         
                                <div class="col-sm-6 col-md-8">
                                    <h4>
                                        {{$lab->name}}
                                       </h4>
                                        
                                    <small><cite title="San Francisco, USA">{{ $lab->state}},{{ $lab->city}} <i class="glyphicon glyphicon-map-marker">
                                    </i></cite></small>
                                    <p>
                                        <i class="glyphicon glyphicon-phone"></i> {{$lab->phone}}
                                        <br />
                                    
                                    
                                </div>
                            </div>
            </div>
             </div>     
                    
              </div>
                        </div>
-->
                        <div class="tab-pane" id="tab2">
                            @include('doctors_list')
                        </div>
                        <div class="tab-pane" id="tab3">
                         @include('labs.test_assigned')

                        </div>
                     
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        @if(count($raised_test) > 0)
             @include('raised_test')
        @endif
    </div>
