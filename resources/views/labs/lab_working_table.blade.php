<style type="text/css">
table#working tbody,table#working tbody tr,table#working tbody tr td{
    padding: 5px;
    border: 1px solid black;
    text-align: center;
}
table#working thead,table#working thead tr,table#working thead tr th{
    padding: 7px;
    border: 1px solid black;
    text-align: center;
}
table#working thead tr th#head{
    background: #337ab7;
    color:white;
}
</style>

                 <div class="col-md-12" style="margin-top: 15px;margin-bottom: 15px;">
                     
                        <table id="working" style="width: 100%;border: 1px solid black;padding: 5px;">
                            <thead>
                            <tr>
                                 <th id="head" colspan="8" style="text-align: center;padding: 5px;"> Lab Working Time</th>
                            </tr>
                            <tr>
                                <th>Days</th>
                                <th>Sunday</th>
                                <th>Monday</th>
                                <th>Tuesday</th>
                                <th>Wednesday</th>
                                <th>Thursday</th>
                                <th>Friday</th>
                                <th>Saturday</th>
                            </tr>
                            </thead>
                            <tbody>
                                @if($working_times->count()>0)
                                   
                                <tr>  
                                <td  style="font-weight: bold;">Start & End Time</td>
                                <td>{{$working_times->day_1_open_time}} to {{$working_times->day_1_end_time}}</td>
                                <td>{{$working_times->day_2_open_time}} to {{$working_times->day_2_end_time}}</td>
                                <td>{{$working_times->day_3_open_time}} to {{$working_times->day_3_end_time}}</td>
                                <td>{{$working_times->day_4_open_time}} to {{$working_times->day_4_end_time}}</td>
                                <td>{{$working_times->day_5_open_time}} to {{$working_times->day_5_end_time}}</td>
                                <td>{{$working_times->day_6_open_time}} to {{$working_times->day_6_end_time}}</td>
                                <td>{{$working_times->day_7_open_time}} to {{$working_times->day_7_end_time}}</td>
                                </tr>
                                <tr>    
                                <td  style="font-weight: bold;">Vacation</td>
                                <td>{{$working_times->day_1_vacation}}</td>
                                <td>{{$working_times->day_2_vacation}}</td>
                                <td>{{$working_times->day_3_vacation}}</td>
                                <td>{{$working_times->day_4_vacation}}</td>
                                <td>{{$working_times->day_5_vacation}}</td>
                                <td>{{$working_times->day_6_vacation}}</td>
                                <td>{{$working_times->day_7_vacation}}</td>
                                </tr>
                             
                                   
                                @endif

                            </tbody>
                        </table>
                </div>
            
