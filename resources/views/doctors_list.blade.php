  
    <div class="card-body">
                    <div class="m-t-35">
                        <table id="example1" class="table display nowrap" >
                            <thead>
                            <tr>
                                <th>Doctor Name</th>
                                <th>City</th>
                                <th>State</th>
                                <th>Phone</th>
                                <th>Email</th>
                                <th>Gender</th>
                                <th>Blood Group</th>
                                <th>Created At</th>
                                <th>Updated At</th>
                            </tr>
                            </thead>
                            <tbody>
                            	@foreach($doctors as $doctor)
                            		<tr>
                            			<td><a href="{{$doctor->path()}}" >{{$doctor->salutation}} {{ $doctor->name }} </a></a></td>
                            			<td>{{ $doctor->city}} </td>
                            			<td>{{$doctor->state}}</td>
                            			<td> {{ $doctor->phone }}</td>
                            			<td>{{ $doctor->email}} </td>
                                        <td>{{ $doctor->getGender() }} </td>
                                        <td>{{ $doctor->blood_group }} </td>
                                        <td>{{ $doctor->created_at }} </td>
                                        <td>{{ $doctor->updated_at }} </td>
                            		</tr>
                            	
                            	@endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
	
	
	