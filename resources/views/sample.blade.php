<div class="panel panel-default">
	<div class="panel-heading">
		<strong>Sample</strong>
	</div>
	<div class="panel-body">
		<form  action="{{route('samplesave')}}" method="post" class="form-horizontal">
			{!! csrf_field() !!}
		@foreach($lab_tests as $key=>$lab_test)
		  <div class="form-group form-inline">
		  	<input type="hidden" value="{{$id}}" name="raise_test_id" />
		    <label for="email" class="col-sm-2">@if( $lab_test->name){{ $lab_test->name }}@else Nil @endif</label>
		    <select name="{{$lab_test->id}}" class="form-control col-sm-8">
			    
			    @foreach($lab_sub as $sub_cat)
			    	@if($sub_cat->id == $lab_test->position)
			    		<option value="{{$sub_cat->id}}" selected="selected">{{$sub_cat->name}}</option>
			    	@else
			    		<option value="{{$sub_cat->id}}" >{{$sub_cat->name}}</option>
			    	@endif
			    @endforeach
			</select>
		  </div>
		@endforeach
		<div  id="append">
		</div>
		<div id="new_test" style="display:none;">
			<div class="form-group form-horizontal">
				<div class='col-sm-3'>
					<select id="test" class="form-control col-sm-3">
						<option value="">Select Test</option>
						@foreach($lab_test_all as $test)
							<option value="{{$test->id}}">{{$test->name}}</option>
						@endforeach
					</select>
				</div> 
				<div class='col-sm-3'>
					<select id="specimen"   class="form-control col-sm-3">
						<option value="">Select Specimen</option>
						 @foreach($lab_sub as $sub_cat)
					    	<option value="{{$sub_cat->id}}">{{$sub_cat->name}}</option>
					    @endforeach
					</select>
				</div>
			<a href="#"  class="btn btn-danger remove">Remove</a>
			</div>
		</div>
		<a href="#" id="add_new" class="btn btn-default">+Add New Test</a>
		<a href="#" id="add_package" class="btn btn-default">+Add New Package</a> 

		 <div class="form-group packages" style="display: none;">
		  	<input type="hidden" value="{{$id}}" name="raise_test_id" />
		    <label for="Package" class="col-sm-1">Package</label>
		    <select name="package[]" class="form-control package" multiple="multiple">
		    	@foreach($newPackages as $newPackage)
	    		<option value="{{$newPackage->id}}" >{{$newPackage->package_name}}</option>
	    		@endforeach
			</select>
			<input type="hidden" name="package[]" />
		  </div>
	
		<div class="clearfix mar-btnsub">
		  <button type="submit" class="btn btn-primary">Submit</button>
		</div>
		</form>
	</div>
</div>
<script>
	$(document).ready(function() {
		$(".package").select2({"allowClear":true,"placeholder":"Select User Permission"});
		$('span.select2').css('width','500px');
		$('#add_new').click(function() {
			var cloned = $('#new_test').html();
			$('#append').append(cloned);
		});
		$('#add_package').click(function() {
			$('div.packages').css('display','block');
		
			$('#append').append(cloned);
		});
		$('body').on('click','.remove',function() {
			$(this).parents('div.form-group').remove();
		});

		$('body').on('change','#test',function() {
			var value = $(this).val();
			console.log(value);
			var parents = $(this).parents('div.form-group');
			parents.find('#specimen').attr('name',value);
		});
	});
</script>
<style type="text/css">
	.mar-btnsub,.packages{
		margin:20px 0 0 0;
	}
</style>