  
   
 				 <div class="card-body">
                    <div class="m-t-35">
                        <table id="example3" class="table display nowrap" >
                            <thead>
                            <tr>
                                <th>Raise Test ID</th>
                                <th>Patient Name</th>
                                <th>Doctor Name</th>
                                <th>Paid Status</th>
                                <th>Raised By</th>
                                <th>Specimen Collected</th>
                                <th>Diagonsis</th>
                                <th>Created At</th>
                                <th>Updated At</th>
                            </tr>
                            </thead>
                            <tbody>
								@if(($patient->active_raised->count())>0)
                            	@foreach($patient->active_raised as $raise_test)
                            		<tr>
                            			<td><a href="{{ $raise_test->path() }}"> {{$raise_test->id}}</a></td>
                            			<td>{{$raise_test->getPatient->name }} </td>
                            			<td>
                                        @if($raise_test->getDoctor)
                                        {{$raise_test->getDoctor->salutation }} {{$raise_test->getDoctor->name}}  
                                            </td>
                                        @else 
                                            
                                        @endif 
                                        <td>{{$raise_test->getStatus() }} </td>
                                        <td>{{$raise_test->whoRaised() }} </td>
                                        <td>{{$raise_test->specimen_collected }} </td>
                                        <td>{{$raise_test->diagonsis }} </td>
                                        <td>{{$raise_test->created_at }} </td>
                                        <td>{{$raise_test->updated_at }} </td>
                            		</tr>
                            	@endforeach
                            	@endif
                            </tbody>
                        </table>
                    </div>
                </div>
	
	
