<style type="text/css">
    /*Panel tabs*/
.panel-tabs {
    position: relative;
    bottom: 30px;
    clear:both;
    border-bottom: 1px solid transparent;
}

.panel-tabs > li {
    float: left;
    margin-bottom: -1px;
}

.panel-tabs > li > a {
    margin-right: 2px;
    margin-top: 4px;
    line-height: .85;
    border: 1px solid transparent;
    border-radius: 4px 4px 0 0;
    color: black;
}

.panel-tabs > li > a:hover {
    border-color: transparent;
    color: black;
    background-color: transparent;
}

.panel-tabs > li.active > a,
.panel-tabs > li.active > a:hover,
.panel-tabs > li.active > a:focus {
    color: black;
    cursor: default;
    -webkit-border-radius: 2px;
    -moz-border-radius: 2px;
    border-radius: 2px;
    background-color: rgba(255,255,255, .23);
    border-bottom-color: transparent;
}/*Panel tabs*/
.panel-tabs {
    position: relative;
    bottom: 30px;
    clear:both;
    border-bottom: 1px solid transparent;
}

.panel-tabs > li {
    float: left;
    margin-bottom: -1px;
}

.panel-tabs > li > a {
    margin-right: 2px;
    margin-top: 4px;
    line-height: .85;
    border: 1px solid transparent;
    border-radius: 4px 4px 0 0;
    color: black;
}

.panel-tabs > li > a:hover {
    border-color: transparent;
    color: black;
    background-color: transparent;
}

.panel-tabs > li.active > a,
.panel-tabs > li.active > a:hover,
.panel-tabs > li.active > a:focus {
    color: red;
    cursor: default;
    -webkit-border-radius: 2px;
    -moz-border-radius: 2px;
    border-radius: 2px;
    background-color: rgba(255,255,255, .23);
    border-bottom-color: transparent;
}
</style>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">@if($patient->name!="") {{$patient->name}} @else Nil @endif</h3>
                    <span class="pull-right">
                        <!-- Tabs -->
                        <ul class="nav panel-tabs">
                            <li class="active">
                                <a href="#tab1" data-toggle="tab">Patient Details</a>
                            </li>
                            <li>
                                <a href="#tab2" data-toggle="tab">Doctors List</a>
                            </li>
                            <li>
                                <a href="#tab3" data-toggle="tab">Tests </a>
                            </li>
                            
                        </ul>
                    </span>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab1">
                            <div class="row">
              
              
               <div class="col-md-3" style="visibility: hidden;">test</div>
                            <div class=" col-md-6"> 
                             <div class="container-fluid well span6">
	<div class="row-fluid">
        <div class="span2" >
			<div class=" col-md-4">
                                   <!--  <img src="/storage/{{ @$patient->profile_image }}" alt="" class="img-rounded img-responsive" /> -->
    <!-- Neelamegam_PatientsDetailsView_13/08/2018 -->
    @if($patient->profile_image!="")     
	<img src="{!! \Illuminate\Support\Facades\Storage::disk(config('admin.upload.disk'))->url(@$patient->profile_image) !!}" alt="" class="img-rounded img-responsive" />
    @else
        <img src="" />    
    @endif
                                </div></div>
                                 <div class="span8">
            <h3>@if($patient->name!="") {{$patient->name}} @else Nil @endif</h3>
            <h6>Mobile: @if($patient->phone!="") {{$patient->phone}} @else Nil @endif</h6>
            <h6>Email: @if($patient->email!="") {{$patient->email}} @else Nil @endif</h6>
            <!-- <h6>Mobile:  @if($patient->mobile!="") {{$patient->mobile}} @else Nil @endif</h6> -->
            <h6>Date Of Birth: @if($patient->dob!="") {{$patient->dob}} @else Nil @endif</h6>
            <h6>Age: @if( $age!=0) {{ $age}} @else 0 @endif</h6>
            <h6>Blood Group:  @if($bg!=""){{$bg}} @else Nil @endif</h6>
            <h6 title="India">Address: @if($patient->area!='' || $patient->address!='' || $patient->city!='' || $patient->state!=''){{$patient->area}} {{$patient->address}} {{ $patient->city}} {{ $patient->state}} @else Nil @endif</h6>
            
           
            
        </div>
                            </div>
            </div>
             </div>      
                    
              </div>
<!--
                            <div class=" col-md-9"> 
                              <div class="well well-sm">
                            <div class="row">
                                <div class="col-sm-6 col-md-4">
                                    <img src="{{ $patient->profile_image }}" alt="" class="img-rounded img-responsive" />
                                </div>
                                <div class="col-sm-6 col-md-8">
                                    <h4>
                                        {{$patient->name}}
                                       </h4>
                                        
                                    <small><cite title="San Francisco, USA">{{ $patient->state}},{{ $patient->city}} <i class="glyphicon glyphicon-map-marker">
                                    </i></cite></small>
                                    <p>
                                        <i class="glyphicon glyphicon-envelope"></i> {{$patient->phone}}
                                        <br />
                                        <i class="glyphicon glyphicon-gift"></i> {{$patient->dob}}
                                        <br>
                                        <i class="glyphicon glyphicon-heart"></i> {{$patient->blood_group}}
                                        <br>
                                        <i class="glyphicon glyphicon-book"></i> {{$patient->height}},{{$patient->weight}}
                                        </p>
                                   
                                    
                                </div>
                            </div>
            </div>
             </div>     
                    
              </div>
-->
                        </div>
                        <div class="tab-pane" id="tab2">
                            @include('doctors_list')
                        </div>
                        <div class="tab-pane" id="tab3">
                           @include('patient.test_assigned')

                        </div>
                     
                    </div>
                </div>
            </div>
        </div>
    </div>
