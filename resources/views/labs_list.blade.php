  
   
 				 <div class="card-body">
                    <div class="m-t-35">
                        <table id="example1" class="table display nowrap" >
                            <thead>
                            <tr>
                                <th>Lab Name</th>
                                <th>City</th>
                                <th>State</th>
                                <th>Mobile</th>
                                <th>Email</th>
                                <th>Home Collection</th>
                                <th>Gst Registered</th>
                                <th>Created At</th>
                                <th>Updated At</th>
                            </tr>
                            </thead>
                            <tbody>
                            	@foreach($labs as $lab)
                            		<tr>
                            			<td>{{ $lab->name }}</td>
                            			<td>{{ $lab->city}} </td>
                            			<td>{{$lab->state}}</td>
                            			<td> <a href="{{ $lab->path() }}">{{ $lab->mobile }}</a></td>
                            			<td>{{ $lab->email}} </td>
                                         <!-- Neelamegam_LabsListView_13/08/2018 -->
                                        <td>{{ $lab->getHomeCollection()}} </td>
                                        <td>{{ $lab->getGstRegister()}} </td>
                                        <td>{{ $lab->created_at}} </td>
                                        <td>{{ $lab->updated_at}} </td>
                            		</tr>
                            	
                            	@endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
	
	