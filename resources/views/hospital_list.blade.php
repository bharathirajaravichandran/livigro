  
     
   
 				 <div class="card-body">
                    <div class="m-t-35">
                        <table id="example3" class="table display nowrap" >
                            <thead>
                            <tr>
                                <th>Hospital Name</th>
                                <th>City</th>
                                <th>State</th>
                                <th>Address</th>
                                <th>Clinic Reg No</th>
                                <th>24/7 Emergency Ward</th>
                                <th>Patient Beds</th>
                                <th>Wheel Chair</th>
                                <th>Pharmacy</th>
                                <th>In House Lab</th>
                                <th>Car Parking</th>
                                <th>Ambulance Service</th>
                                <th>Created At</th>
                                <th>Updated At</th>
                            </tr>
                            </thead>
                            <tbody>
                                @php
                                    $ward = '24_7_emerg_ward';
                                @endphp
                            	@foreach($hospitals as $hospital)
                            		<tr>
                            			<td><a href="{{ $hospital->path() }}">{{ $hospital->name }}</a></td>
                            			<td>{{ $hospital->city}} </td>
                            			<td>{{$hospital->state}}</td>
                                        <td>{{$hospital->address}}</td>
                                        <td>{{$hospital->clinic_reg_no}}</td>
                                         <!-- Neelamegam_HospitalsDetailsView_13/08/2018 -->
                                        <td>
                                            @if($hospital->$ward == 0){{$hospital->$ward="No"  }}
                                            @elseif($hospital->$ward == 1){{$hospital->$ward="Yes"  }}
                                            @else{{$hospital->$ward=""  }} 
                                            @endif
                                        </td>

                                        <td>{{$hospital->patient_beds}}</td>
                                        <td>{{$hospital->getWheelChair()}}</td>
                                        <td>{{$hospital->getPharmacy()}}</td>
                                        <td>{{$hospital->getHouseLab()}}</td>
                                        <td>{{$hospital->getCarParking()}}</td>
                                        <td>{{$hospital->getAmbulanceService()}} </td>
                                        <td>{{$hospital->created_at}}</td>
                                        <td>{{$hospital->updated_at}}</td>
                            		</tr>
                            	
                            	@endforeach

                            </tbody>
                        </table>
                    </div>
                </div>

	
	
	