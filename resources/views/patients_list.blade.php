
 				 <div class="card-body">
                    <div class="m-t-35">
                        <table id="example2" class="table display nowrap" >
                            <thead>
                            <tr>
                                <th>Patient Name</th>
                                <th>City</th>
                                <th>Phone</th>
                                <th>Gender</th>
                                <th>Email</th>
                                <th>Date Of Birth</th>
                                <th>Blood Group</th>
                                <th>Height</th>
                                <th>Weight</th>
                                <th>Created At</th>
                                <th>Updated At</th>
                            </tr>
                            </thead>
                            <tbody>
								@if($patients->count()>0)
									@foreach($patients as $patient)
										<tr>
											<td><a href="{{ $patient->path() }}">{{ $patient->name }}</a></td>
											<td>{{ $patient->city}}</td>
											<td> {{ $patient->phone }}</td>
											<td>{{ $patient->getGender()}} </td>
											<td>{{ $patient->email }}</td>
											<td>{{ $patient->dob }}</td>
											<td>{{ $patient->blood_group }}</td>
											<td>{{ $patient->height }}</td>
											<td>{{ $patient->weight }}</td>
											<td>{{ $patient->created_at }}</td>
											<td>{{ $patient->updated_at }}</td>
										</tr>
									
									@endforeach
                            	@endif

                            </tbody>
                        </table>
                    </div>
                </div>
	
