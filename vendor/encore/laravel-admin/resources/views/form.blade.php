<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">{{ $form->title() }}</h3>

        <div class="box-tools">
            {!! $form->renderHeaderTools() !!}
        </div>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    @if($form->hasRows())
        {!! $form->open() !!}
    @else
        {!! $form->open(['class' => "form-horizontal"]) !!}
    @endif

        <div class="box-body">

            @if(!$tabObj->isEmpty())
                @include('admin::form.tab', compact('tabObj'))
            @else
                <div class="fields-group">

                    @if($form->hasRows())
                        @foreach($form->getRows() as $row)
                            {!! $row->render() !!}
                        @endforeach
                    @else
                        @foreach($form->fields() as $field)
                            {!! $field->render() !!}
                        @endforeach
                    @endif


                </div>
            @endif

        </div>
        <!-- /.box-body -->
        
        <div class="box-footer" id="dci" >

            @if( ! $form->isMode(\Encore\Admin\Form\Builder::MODE_VIEW)  || ! $form->option('enableSubmit'))
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
            @endif
            <div class="col-md-{{$width['label']}}">

            </div>
            <div class="col-md-{{$width['field']}}">

                {!! $form->submitButton() !!}

                {!! $form->resetButton() !!}

            </div>

        </div>
<!-- <script type="text/javascript">
          var url      = window.location.href;  
          var pathname = window.location.pathname; // Returns path only
         
        if(pathname == '/admin/doctors/create') {
                jQuery('#dci').css('display','none');
                 jQuery('.nav-tabs li a').on('click',function() {

                        var href = $(this).attr('href');
                        if(href == '#tab-form-5')
                             jQuery('#dci').css('display','block');
                         else
                            jQuery('#dci').css('display','none');
                 });
                
        }
          if(pathname == '/admin/patients/create') {
                jQuery('#dci').css('display','none');
                 jQuery('.nav-tabs li a').on('click',function() {

                        var href = $(this).attr('href');
                        if(href == '#tab-form-4')
                             jQuery('#dci').css('display','block');
                         else
                            jQuery('#dci').css('display','none');
                 });
                
        }
          if(pathname == '/admin/labs/create') {
                jQuery('#dci').css('display','none');
                 jQuery('.nav-tabs li a').on('click',function() {

                        var href = $(this).attr('href');
                        if(href == '#tab-form-7')
                             jQuery('#dci').css('display','block');
                         else
                            jQuery('#dci').css('display','none');
                 });
                
        }

          
        </script> -->
        @foreach($form->getHiddenFields() as $hiddenField)
            {!! $hiddenField->render() !!}
        @endforeach

        <!-- /.box-footer -->
    {!! $form->close() !!}
</div>

