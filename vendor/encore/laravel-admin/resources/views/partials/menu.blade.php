<?php 
if(Admin::user()->user_type_id == '1')
    $roletype = "lab";
else if(Admin::user()->user_type_id == '2')
    $roletype = "doctor";
else
    $roletype = "administrator";

$selectedRoleArray = array();
foreach($item['roles'] as $val){
    if(isset($val['slug'])){
        $selectedRoleArray[] = $val['slug'];
    }else{
        $selectedRoleArray = array('administrator','lab','doctor');
    }
}
        
?>

@if(Admin::user()->visible($item['roles']))
    @if(!isset($item['children']))
    <?php 
    if(count($item['roles']) == 0 )
        $selectedRoleArray = array('administrator','lab','doctor');

    if(in_array($roletype, $selectedRoleArray)){
    ?>
        <li>
            @if(url()->isValidUrl($item['uri']))
                <a href="{{ $item['uri'] }}" target="_blank">
            @else
                 <a href="{{ admin_base_path($item['uri']) }}">
            @endif
                <i class="fa {{$item['icon']}}"></i>
                <span>{{$item['title'] }}</span>
            </a>
        </li>
    <?php } ?>
    @else
        <li class="treeview">
            <a href="#">
                <i class="fa {{$item['icon']}}"></i>
                <span>{{$item['title']}}</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                @foreach($item['children'] as $item)
                    @include('admin::partials.menu', $item)
                @endforeach
            </ul>
        </li>
    @endif
@endif