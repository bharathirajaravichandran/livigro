<?php

namespace Encore\Admin\Controllers;

use Encore\Admin\Auth\Database\Administrator;
use Encore\Admin\Auth\Database\Permission;
use Encore\Admin\Auth\Database\Role;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Illuminate\Routing\Controller;
use App\Models\Labs;
use App\Models\Doctors;
use App\Models\Patients;
use App\User;
use Request;
use DB;

class UserController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {
            $content->header(trans('admin.administrator'));
            $content->description(trans('admin.list'));
            $content->body($this->grid()->render());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     *
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            $content->header(trans('admin.administrator'));
            $content->description(trans('admin.edit'));
            $content->body($this->form($id)->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {
            $content->header(trans('admin.administrator'));
            $content->description(trans('admin.create'));
            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Administrator::grid(function (Grid $grid) {

            $grid->id('ID')->sortable();
             $grid->model()->orderBy('id', 'desc');
            $grid->name(trans('admin.name'));
            // $grid->username(trans('admin.username')); Neelamegam_20/08/2018
            $grid->username('Mobile',trans('admin.username'));                 
            $grid->roles(trans('admin.roles'))->pluck('name')->label();
            $grid->status(trans('admin.status'))->display(function ($title) {
                    return $this->status == '1' ? '<span class="btn btn-success btn-xs">Active</span>' : ($this->status == '0' ? '<span class="btn btn-primary btn-xs">InActive</span>': '<span class="btn btn-danger btn-xs">Deleted</span>');
                });
            $grid->created_at(trans('admin.created_at'));
            $grid->updated_at(trans('admin.updated_at'));

            $grid->actions(function (Grid\Displayers\Actions $actions) {
                if ($actions->getKey() == 1) {
                    $actions->disableDelete();
                }
                $actions->disableDelete();
                $key = $actions->getKey();
                   $UserStatus = User::find($key);
                  $value = $UserStatus->status;
                  if($value !=2 && $actions->getKey() != 1){
                          $actions->append("<a href=''><i class='fa fa-trash delete' id='$key'></i></a>");
                           }
                    
            });

            $grid->tools(function (Grid\Tools $tools) {
                $tools->batch(function (Grid\Tools\BatchActions $actions) {
                    $actions->disableDelete();
                });
            });
             $grid->disableRowSelector();
              // Neelamegam_29/08/2018_CommonDelete
            Admin::script('
                $( document ).ready(function(){
                    $(".delete").click(function(){
                        var id = $(this).prop("id");
                        var url = "CommonDelete/" + id;
                        var fullurl = window.location.pathname.split("/"); 
                        var uri_segment = fullurl[3];

                        swal({
                          title: "DeleteConfirm",
                          type: "warning",
                          showCancelButton: true,
                          confirmButtonColor: "#DD6B55",
                          confirmButtonText: "Confirm",
                          closeOnConfirm: false,
                          cancelButtonText: "Cancel"
                        },
                        function(){
                            $.ajax({

                                method: "post",
                                url: url,
                                data: {
                                    id:id,
                                    _token:LA.token,
                                    uri_segment:uri_segment,
                                },
                                success: function (data) {
                                    $.pjax.reload("#pjax-container");

                                    if (typeof data === "object") {
                                        if (data.status) {
                                            swal(data.message, "", "success");
                                        } else {
                                            swal(data.message, "", "error");
                                        }
                                    }
                                }
                            });
                        });            
                    });
                });
            ');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    public function form($id='')
    {
        return Administrator::form(function (Form $form) use ($id) {
          // dd($id);
            $form->display('id', 'ID');
            $form->text('name', trans('admin.name'))->rules('required');
             $form->select('roles', trans('admin.roles'))->options(Role::all()->pluck('name', 'id'));
              if($id) {
             $get_role = DB::table('admin_role_users')->where('user_id',$id)->pluck('role_id')->first();
             // dd($get_role);
             $form->hidden('roles_id')->attribute(['id'=>'roles_id'])->default($get_role);
            }
            $form->multipleSelect('permissions', trans('admin.permissions'))->options(Permission::all()->pluck('name', 'id'));
            //user_type_id save to DB
            // Neelamegam_20/08/2018
            $form->hidden('user_type_id', trans('admin.user_type_id'));
            $form->hidden('id', trans('admin.id'));
            $form->text('username', trans('admin.username'))->rules('required');            
            //$form->image('avatar', trans('admin.avatar'));
            
            // $form->image('avatar','admin.avatar')->move(function($form) {
            //         return 'Labs/'.encrypt($form->username);
            //     });
            
            $form->image('avatar','admin.avatar')->move(function($form) {
                if($form->roles == 4)
                return 'Patient/'.encrypt($form->username);
                if($form->roles == 3)
                return 'Labs/'.encrypt($form->username); 
                 if($form->roles == 2)
                return 'Doctor/'.encrypt($form->username);    
            });

            $form->password('password', trans('admin.password'))->rules('required|confirmed');
            $form->password('password_confirmation', trans('admin.password_confirmation'))->rules('required')
                ->default(function ($form) {
                    return $form->model()->password;
                });

            $form->ignore(['password_confirmation']);

            /*$get_role = DB::table('admin_role_users')->where('user_id',$form->id)->first();
            if($get_role){
                        if($get_role->role_id ==''){
                          $role_id = 2 ;
                        }
                       $role_id = $get_role->role_id ;
            }else{     $role_id = 2 ;  }*/

            //  if($id) {
            //      $items = DB::table('admin_role_users')->where('user_id',$id)->pluck('DistrictName','id');
            //     $items = DB::table('admin_roles')                  
            //       ->rightjoin('admin_role_users','admin_role_users.role_id','=','admin_roles.id')
            //       ->where('admin_role_users.user_id',$id)
            //       ->pluck('id');                   
            //      $itemss = Role::all()->pluck('name', 'id'); 
            //       $form->select('roles', trans('admin.roles'))->options($itemss);
            // }
            // else
            // {
            //     $itemss = Role::all()->pluck('name', 'id');                  
            //     $form->select('roles', trans('admin.roles'))->options($itemss);
            // }
            
            
            
             $states = [
                        'off'  => ['value' => 0, 'text' => 'Inactive', 'color' => 'danger'],
                        'on' => ['value' => 1, 'text' => 'Active', 'color' => 'success'],
                        ];
            $form->switch('status','Status')->states($states)->default('1');
            /*$form->hidden('segment')->value(Request::segment(5));*/
            $form->display('created_at', trans('admin.created_at'));
            $form->display('updated_at', trans('admin.updated_at'));

            $form->saving(function (Form $form) {
                //user_type_id save to DB
                 // Neelamegam_20/08/2018
                if($form->roles == 1)
                   $form->user_type_id = "";  
                if ($form->roles == 2)
                    $form->user_type_id = '2';
                if ($form->roles == 3)
                    $form->user_type_id = '1';
                if ($form->roles == 4)
                    $form->user_type_id = '3';
                if ($form->password && $form->model()->password != $form->password) {
                    $form->password = bcrypt($form->password);

                }              
                
            });

      $form->saved(function (Form $form) { 
              // dd($form->model()->id);
               $users = User::find($form->model()->id);
               $users->username = "+91".$form->username;
              $users->save();

            $user = User::where('username',$form->username)->first();
            if($user){
                if($user->user_type_id==3){
                    $labs = Patients::where('phone',$form->username)->first();
                     if($labs){
                        $labs->profile_image  = $user->avatar;
                        $labs->save();
                    }

                }
                if($user->user_type_id==1){
                    $labs = Labs::where('mobile',$form->username)->first();
                    if($labs){
                        $labs->profile_image  = $user->avatar;
                        $labs->save();
                    }
                }
                if($user->user_type_id==2){
                    $labs = Doctors::where('mobile',$form->username)->first();
                    if($labs){
                        $labs->profile_image  = $user->avatar;
                        $labs->save();
                    }
                }
            }
                if($form->status == "on"){
                    $status =1;
                }
                else{
                    $status=0;
                }
                if($form->roles == 2)
                    {

                       $doctors = Doctors::where('mobile',$form->username)->first();
                      if($doctors){
                         $detail_data = Doctors::find($doctors->id);
                          $detail_data->mobile    = "+91".$form->username; 
                          $detail_data->name     = $form->name;
                          $detail_data->password = $form->password;                          
                          $detail_data->status = $status;
                          $detail_data->save();
                       }else{
                          $detail_data = new Doctors();
                          $detail_data->mobile   = "+91".$form->username; 
                          $detail_data->name     = $form->name;
                          $detail_data->password = $form->password;
                          $detail_data->status = $status;
                          // $detail_data->status   = 1;
                          $detail_data->save();
                          $user = User::where('username',$form->username)->first();
                          $user->linked_id  = $detail_data->id;
                          $user->status  = $status;
                          $user->save();
                       }
                   
                       
                     } 

                if($form->roles == 3)
                    {
                       $labs = Labs::where('mobile',$form->username)->first();
                       // dd($labs);
                      if($labs){
                          $detail_data = Labs::find($labs->id);
                          $detail_data->mobile    = "+91".$form->username; 
                          $detail_data->name     = $form->name;
                          $detail_data->password = $form->password;                          
                          $detail_data->status = $status;
                          $detail_data->save();
                       }else{
                          $detail_data = new Labs();
                          $detail_data->mobile    = "+91".$form->username; 
                          $detail_data->name     = $form->name;
                          $detail_data->password = $form->password;                          
                          $detail_data->status = $status;
                          $detail_data->save();
                          $user = User::where('username',$form->username)->first();
                          $user->linked_id  = $detail_data->id;
                          $user->status  = $status;
                          $user->save();
                        }

                    }

                 if ($form->roles == 4)
                    {
                       $patients = Patients::where('mobile',$form->username)->first();
                      if($patients){
                          $detail_data = Patients::find($patients->id);
                          $detail_data->mobile    = "+91".$form->username; 
                          $detail_data->name     = $form->name;
                          $detail_data->password = $form->password;                          
                          $detail_data->status = $status;
                          $detail_data->save();
                       }else{
                          $detail_data = new Patients();
                          $detail_data->mobile    = "+91".$form->username; 
                          $detail_data->name     = $form->name;
                          $detail_data->password = $form->password;                          
                          $detail_data->status = $status;
                          $detail_data->reference_doctor_id = 0;
                          $detail_data->save();
                          $user = User::where('username',$form->username)->first();
                          $user->linked_id  = $detail_data->id;
                          $user->status  = $status;
                          $user->save();
                        }
                       
                    }
               
                      
                       
                
            });



            // Neelamegam_20/08/2018
            // Ajax call pass To Route.php
            // For Mobile Number Unique Valiadtion


            Admin::script('
                $( document ).ready(function(){

                  var fullurl = window.location.pathname.split("/"); 
                  var uri_segment = fullurl[5];

                        var phone_no = $(".username").val();
                        if(uri_segment == "edit"){
                           var str_phone = phone_no.substring(3);
                           // alert(str_phone);
                           $(".username").val(str_phone);
                        }

                   var mobile_icon = $("#username").closest(".form-group").children(".col-sm-8").children(".input-group").find(".input-group-addon");
                    var mobile_icons = $("#username").closest(".form-group").children(".col-sm-8").children(".input-group").children(".input-group-addon").find("i");
                   // alert(mobile_icons);
                   var new_icon = "<span><i>+91</i></span>";
                    mobile_icons.removeClass("fa fa-pencil");
                   mobile_icons.append(new_icon);


                  var r_id = $("#roles_id").val();
                  if(uri_segment == "edit"){
                    if(r_id == 1)
                     $("select[name=roles] option[value =1]").attr("selected","selected");
                    if(r_id == 2)
                     $("select[name=roles] option[value =2]").attr("selected","selected");
                    if(r_id == 3){
                     $("select[name=roles] option[value =3]").attr("selected","selected");
                    }
                    if(r_id == 4)
                     $("select[name=roles] option[value =4]").attr("selected","selected");
                  }else{
                     $("select[name=roles] option[value=1]").attr("selected","selected");
                  }

                  
                    var roles = $(".roles").val();
                    
                 
                  $("#username").keypress(function(){

                      var firstVal = $("#username").val().charAt(0);
                      var numbericCheck = $("#username").val();

                      if(!$.isNumeric(numbericCheck)){
                        var error="<small id=small class=help-block>Mobile number must contain only numeric</small>";
                            $("#small").remove();                          
                            $("#username").closest(".form-group").addClass("has-error");
                            $("#username").closest(".col-sm-8").append(error);
                            $("#username").closest(".nav-tabs-custom").children("ul.nav-tabs").children("li.active").addClass("bv-tab-error").removeClass("bv-tab-sucess");
                      }else {
                         $("#small").remove();         
                            $("#username").closest(".form-group").removeClass("has-error").addClass("has-success");
                               $("#username").closest(".nav-tabs-custom").children("ul.nav-tabs").children("li.active").removeClass("bv-tab-error").addClass("bv-tab-sucess");
                          }

                      });

                    $("#username").closest(".form-group").find(".control-label").text("Mobile");
                     $(".username").blur(function(){
                      // alert("hi");
                     var roles = $(".roles").val();
                     $("#username").closest(".form-group").removeClass("has-error").addClass("has-success");
                     $("#small").remove();
                     var phone_number = $("#username").val();
                      var firstVal = $("#username").val().charAt(0);
                      var numbericCheck = $("#username").val();

                      if(!$.isNumeric(numbericCheck)){
                        var error="<small id=small class=help-block>Mobile number must contain only numeric</small>";
                            $("#small").remove();                          
                            $("#username").closest(".form-group").addClass("has-error");
                            $("#username").closest(".col-sm-8").append(error);
                            $("#username").closest(".nav-tabs-custom").children("ul.nav-tabs").children("li.active").addClass("bv-tab-error").removeClass("bv-tab-sucess");
                      }else{
                         $("#small").remove();         
                            $("#username").closest(".form-group").removeClass("has-error").addClass("has-success");
                               $("#username").closest(".nav-tabs-custom").children("ul.nav-tabs").children("li.active").removeClass("bv-tab-error").addClass("bv-tab-sucess");
                          }
                     if(phone_number==""){
                    $("#small").remove();      
                    var error="<small id=small style=color:red;>Please Enter Mobile Number</small>";  
                      $("#username").closest(".form-group").addClass("has-error");
                      $("#username").closest(".col-sm-8").append(error);
                       }else{
                         var url = "phonevalidation";
                         var token = $("input[name=_token]").val();
                          var roles = $(".roles").val();
                             $.ajax({
                                type:"post",
                                 dataType: "json",
                                 url: url,                       
                                 data: {
                                     _token: token,
                                     phone_number: phone_number,
                                     roles: roles,
                                 },                        
                                 success: function( responses ){
                                    
                                     if(responses.phone_id == 0){                                          
                                         var error="<small id=small class=help-block>  The Mobile Number is Already Exists</small>";
                                         $("#small").remove();                                       
                                         $("#username").closest(".form-group").addClass("has-error");
                                         $("#username").closest(".col-sm-8").append(error);
                                     }
                                 },
                             });
                       
                         }
                   
                     });
                });
            ');
        });
    }
    public function getphone(Request $request)
    {
       dd($request->phone_number);
        $mobile=DB::table('doctors_details')->where('phone',$request->phone_number)->get();
         // dd($mobile);
        $mobile_count=count($mobile);
          // dd($mobile_count);
        if($mobile_count!=0){
             $responses = array(
                'phone_id' => '0',
                'message' =>'Mobile Number Already Exist',                
                );
            return response()->json($responses);
        }

    }
}
